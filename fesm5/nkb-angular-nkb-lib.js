import { __awaiter, __generator } from 'tslib';
import axios from 'axios';
import { forEach, extend, get, find, each, sortBy, includes, keyBy, upperFirst, lowerCase, map, head, capitalize, forOwn, filter, isEmpty, assignIn, isObject, isArray, isNumber, isNaN, size, isNull, isFunction, toNumber, toString, debounce, isEqual } from 'lodash';
import { Injectable, Component, Pipe, defineInjectable, NgModule, EventEmitter, Input, Output, inject } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';

/**
 * @fileoverview added by tsickle
 * Generated from: lib/domain/TYPES.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {string} */
var NODE_TYPE = {
    'INDIVIDUAL_IDENTITY': "INDIVIDUAL_IDENTITY",
    'INDIVIDUAL': "INDIVIDUAL",
    'COMPANY': "COMPANY",
    'EGRUL': "EGRUL",
    'ADDRESS': "ADDRESS",
    'PHONE': "PHONE",
};
/** @enum {string} */
var LINK_TYPES = {
    'FOUNDER_INDIVIDUAL': "FOUNDER_INDIVIDUAL",
    'FOUNDER_COMPANY': "FOUNDER_COMPANY",
    'EXECUTIVE_COMPANY': "EXECUTIVE_COMPANY",
    'EXECUTIVE_INDIVIDUAL': "EXECUTIVE_INDIVIDUAL",
    'AFFILIATED_COMPANY': "AFFILIATED_COMPANY",
    'AFFILIATED_INDIVIDUAL': "AFFILIATED_INDIVIDUAL",
    'EMPLOYEE': "EMPLOYEE",
    'PARTICIPANT_COMPANY': "PARTICIPANT_COMPANY",
    'PARTICIPANT_INDIVIDUAL': "PARTICIPANT_INDIVIDUAL",
    'PREDECESSOR_COMPANY': "PREDECESSOR_COMPANY",
    'HEAD_COMPANY': "HEAD_COMPANY",
    'COMMISSION_MEMBER': "COMMISSION_MEMBER",
    'CUSTOMER_COMPANY': "CUSTOMER_COMPANY",
    'ADDRESS': "ADDRESS",
    'PHONE': "PHONE",
    'REGISTER_HOLDER': "REGISTER_HOLDER",
};
/** @type {?} */
var LINK_TYPE_GROUPS = {
    FOUNDER: [LINK_TYPES.FOUNDER_INDIVIDUAL, LINK_TYPES.FOUNDER_COMPANY],
    EXECUTIVE: [LINK_TYPES.EXECUTIVE_COMPANY, LINK_TYPES.EXECUTIVE_INDIVIDUAL],
    PURCHASE: [LINK_TYPES.EMPLOYEE, LINK_TYPES.PARTICIPANT_COMPANY, LINK_TYPES.COMMISSION_MEMBER, LINK_TYPES.CUSTOMER_COMPANY]
};

/**
 * @fileoverview added by tsickle
 * Generated from: lib/meta/rsearchMeta.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
var rsearchMeta = {
    // additional properties for nodes
    nodeTypes: {
        'COMPANY': {
            search: true,
            searchResultPriority: 60,
            accentedResultPriority: 30
        },
        'INDIVIDUAL_IDENTITY': {
            search: true,
            searchResultPriority: 50,
            accentedResultPriority: 10
        },
        'INDIVIDUAL': {
            search: true,
            searchResultPriority: 40,
            accentedResultPriority: 20
        },
        'ADDRESS': {
            search: true,
            searchResultPriority: 30,
            accentedResultPriority: 0
        },
        'PHONE': {
            search: true,
            searchResultPriority: 20,
            accentedResultPriority: 0
        },
        'PURCHASE': {
            search: false,
            searchResultPriority: 10,
            accentedResultPriority: 0
        }
    },
    // Дополнительные свойства для связей
    //
    // TODO автоматическое формирование свойств для объединенных типов на основе базовых типов
    relationTypes: {
        'FOUNDER': {
            name: 'FOUNDER',
            'parents': {
                order: 100,
                top: true,
                group: 'BASE',
                pageSize: 'all',
                history: {
                    opposite: false
                }
            }
        },
        'FOUNDER_INDIVIDUAL': {
            'children': {
                order: 250,
                top: true,
                group: 'BASE',
                history: {
                    opposite: true
                }
            },
            'parents': {
                order: 200,
                top: true,
                group: 'BASE',
                pageSize: 'all',
                history: {
                    opposite: false
                }
            }
        },
        'FOUNDER_COMPANY': {
            'children': {
                order: 350,
                top: true,
                group: 'BASE',
                history: {
                    opposite: true
                }
            },
            'parents': {
                order: 300,
                top: true,
                group: 'BASE',
                pageSize: 'all',
                history: {
                    opposite: false
                }
            }
        },
        'HEAD_COMPANY': {
            'children': {
                order: 400,
                top: true,
                group: 'BASE'
            },
            'parents': {
                order: 450,
                group: 'BASE'
            }
        },
        'PREDECESSOR_COMPANY': {
            'children': {
                order: 550,
                top: true,
                group: 'BASE'
            },
            'parents': {
                order: 500,
                top: true,
                group: 'BASE'
            }
        },
        'REGISTER_HOLDER': {
            'children': {
                order: 650,
                group: 'BASE'
            },
            'parents': {
                order: 600,
                group: 'BASE'
            }
        },
        'EXECUTIVE_COMPANY': {
            'children': {
                order: 750,
                group: 'BASE'
            },
            'parents': {
                order: 700,
                group: 'BASE'
            }
        },
        'EXECUTIVE_INDIVIDUAL': {
            'children': {
                order: 850,
                top: true,
                group: 'BASE',
                history: {
                    opposite: true
                }
            },
            'parents': {
                order: 800,
                group: 'BASE',
                pageSize: 'all',
                history: {
                    opposite: false
                }
            }
        },
        'AFFILIATED_INDIVIDUAL': {
            'children': {
                order: 950,
                top: true,
                group: 'AFFILIATED'
            },
            'parents': {
                order: 900,
                top: true,
                group: 'AFFILIATED'
            }
        },
        'AFFILIATED_COMPANY': {
            'children': {
                order: 1050,
                top: true,
                group: 'AFFILIATED'
            },
            'parents': {
                order: 1000,
                top: true,
                group: 'AFFILIATED'
            }
        },
        'ADDRESS': {
            'children': {
                order: 1150,
                top: true,
                group: 'BASE'
            },
            'parents': {
                order: 1100,
                group: 'CONTACTS'
            }
        },
        'PHONE': {
            'children': {
                order: 1250,
                top: true,
                group: 'BASE'
            },
            'parents': {
                order: 1200,
                group: 'CONTACTS'
            }
        },
        'CUSTOMER_COMPANY': {
            'children': {
                order: 1300,
                group: 'PURCHASE'
            },
            'parents': {
                order: 1350,
                top: true,
                group: 'BASE'
            }
        },
        'PARTICIPANT_COMPANY': {
            'children': {
                order: 1400,
                group: 'PURCHASE'
            },
            'parents': {
                order: 1670,
                top: true,
                group: 'BASE'
            }
        },
        'EMPLOYEE': {
            'children': {
                order: 1720,
                group: 'PURCHASE'
            },
            'parents': {
                order: 1500,
                group: 'PURCHASE'
            }
        },
        'PARTICIPANT_INDIVIDUAL': {
            'children': {
                order: 1600,
                group: 'PURCHASE'
            },
            'parents': {
                order: 1650,
                top: true,
                group: 'BASE'
            }
        },
        'COMMISSION_MEMBER': {
            'children': {
                order: 1700,
                group: 'PURCHASE'
            },
            'parents': {
                order: 1750,
                group: 'BASE'
            }
        },
        'kinsmen': {
            name: 'kinsmen',
            sourceNodeType: 'INDIVIDUAL',
            destinationNodeType: 'INDIVIDUAL'
        }
    },
    mergedRelationTypes: {
        'FOUNDER': {
            'parents': ['FOUNDER_INDIVIDUAL', 'FOUNDER_COMPANY']
        }
    },
    relationGroups: {
        'BASE': {
            order: 10
        },
        'AFFILIATED': {
            order: 20
        },
        'CONTACTS': {
            order: 30
        },
        'PURCHASE': {
            order: 40
        }
    }
};

/**
 * @fileoverview added by tsickle
 * Generated from: lib/meta/metaService.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
//todo покрыть тестами
var  
//todo покрыть тестами
Meta = /** @class */ (function () {
    function Meta() {
        this.SEARCHABLE_TYPES = [NODE_TYPE.COMPANY, NODE_TYPE.INDIVIDUAL, NODE_TYPE.ADDRESS, NODE_TYPE.PHONE];
        this.state = {
            inited: false,
            pending: false,
            error: false,
        };
        // @Deprecated
        this.nodeTypesMeta = {};
        this.relationTypesMeta = {};
    }
    Object.defineProperty(Meta.prototype, "inited", {
        get: /**
         * @return {?}
         */
        function () {
            return this.state.inited;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @return {?}
     */
    Meta.prototype.init = /**
     * @return {?}
     */
    function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a, _b, err_1;
            var _this = this;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        if (!!this.state.inited) return [3 /*break*/, 8];
                        if (!!this.state.pending) return [3 /*break*/, 7];
                        this.state.pending = true;
                        _c.label = 1;
                    case 1:
                        _c.trys.push([1, 4, 5, 6]);
                        _a = this;
                        return [4 /*yield*/, axios.get('/nkbrelation/api/meta/relation/types').then((/**
                             * @param {?} res
                             * @return {?}
                             */
                            function (res) { return res.data; }))];
                    case 2:
                        _a.relationTypes = _c.sent();
                        _b = this;
                        return [4 /*yield*/, axios.get('/nkbrelation/api/meta/node/types').then((/**
                             * @param {?} res
                             * @return {?}
                             */
                            function (res) { return res.data; }))];
                    case 3:
                        _b.nodeTypes = _c.sent();
                        // TODO legacy - refactor this - a lot of additional not needed work
                        forEach(this.nodeTypes, (/**
                         * @param {?} nodeType
                         * @return {?}
                         */
                        function (nodeType) {
                            _this.nodeTypesMeta[nodeType.name] = nodeType;
                        }));
                        forEach(rsearchMeta.nodeTypes, (/**
                         * @param {?} nodeType
                         * @param {?} nodeTypeName
                         * @return {?}
                         */
                        function (nodeType, nodeTypeName) {
                            _this.nodeTypesMeta[nodeTypeName] = extend({}, _this.nodeTypesMeta[nodeTypeName], nodeType);
                        }));
                        // relationTypesMeta
                        forEach(this.relationTypes, (/**
                         * @param {?} relationType
                         * @return {?}
                         */
                        function (relationType) {
                            _this.relationTypesMeta[relationType.name] = relationType;
                        }));
                        forEach(rsearchMeta.relationTypes, (/**
                         * @param {?} relationType
                         * @param {?} relationTypeName
                         * @return {?}
                         */
                        function (relationType, relationTypeName) {
                            _this.relationTypesMeta[relationTypeName] = extend({}, _this.relationTypesMeta[relationTypeName], relationType);
                        }));
                        // end TODO legacy - refactor this - a lot of additional not needed work
                        this.state.inited = true;
                        return [3 /*break*/, 6];
                    case 4:
                        err_1 = _c.sent();
                        console.error(err_1);
                        this.state.error = err_1;
                        return [3 /*break*/, 6];
                    case 5:
                        console.log("meta service inited");
                        this.state.pending = false;
                        return [7 /*endfinally*/];
                    case 6: return [3 /*break*/, 8];
                    case 7: return [2 /*return*/, new Promise((/**
                         * @param {?} resolve
                         * @return {?}
                         */
                        function (resolve) {
                            /** @type {?} */
                            var interval = setInterval((/**
                             * @return {?}
                             */
                            function () {
                                if (_this.state.inited) {
                                    resolve();
                                    clearInterval(interval);
                                }
                            }), 1000);
                        }))];
                    case 8: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * @param {?} node
     * @return {?}
     */
    Meta.prototype.getNodeName = /**
     * @param {?} node
     * @return {?}
     */
    function (node) {
        /** @type {?} */
        var nameField = get(find(this.nodeTypes, { name: get(node, '_type') }), 'nameField');
        return get(node, [nameField], get(node, '_name', ''));
    };
    /**
     * @param {?} node
     * @return {?}
     */
    Meta.prototype.getNodeId = /**
     * @param {?} node
     * @return {?}
     */
    function (node) {
        if (this.inited) {
            /** @type {?} */
            var idField = get(find(this.nodeTypes, { name: get(node, '_type') }), 'idField');
            return idField === 'id' ? get(node, '_id') : get(node, idField, get(node, '_id'));
        }
    };
    /**
     * @param {?} type
     * @return {?}
     */
    Meta.prototype.getIdFieldForType = /**
     * @param {?} type
     * @return {?}
     */
    function (type) {
        return get(find(this.nodeTypes, { name: type }), 'idField', '_id');
    };
    /**
     * @param {?} node
     * @return {?}
     */
    Meta.prototype.buildRelationDataByNodeInfo = /**
     * @param {?} node
     * @return {?}
     */
    function (node) {
        var _this = this;
        /** @type {?} */
        var relationCountMap = {};
        /** @type {?} */
        var relationCounts = [];
        /** @type {?} */
        var groups = {};
        each(['in', 'out'], (/**
         * @param {?} infoDirection
         * @return {?}
         */
        function (infoDirection) {
            each(get(node, ['_info', infoDirection]), (/**
             * @param {?} info
             * @param {?} relationType
             * @return {?}
             */
            function (info, relationType) {
                /** @type {?} */
                var historyRelationCounts = _this.getHistoryRelationCountsByInfo(info);
                /** @type {?} */
                var relationCount = historyRelationCounts.all;
                /** @type {?} */
                var direction = _this.getDirection(infoDirection);
                /** @type {?} */
                var relationTypeMeta = _this.getRelationTypeMeta(relationType, direction);
                /** @type {?} */
                var groupKey = relationTypeMeta.group;
                /** @type {?} */
                var relationGroupMeta = _this.getRelationGroupMeta(groupKey);
                /** @type {?} */
                var mergedTypeInfo = _this.getMergedTypeInfoByRelationType(relationType, direction);
                /** @type {?} */
                var mergedTypeKey = mergedTypeInfo.mergedType && _this.buildNodeRelationKey(direction, mergedTypeInfo.mergedType);
                /** @type {?} */
                var mergedTypeCountData = relationCountMap[mergedTypeKey];
                /** @type {?} */
                var relationKey = _this.buildNodeRelationKey(direction, relationType);
                /** @type {?} */
                var relationPluralKey = _this.buildNodeRelationPluralKey(direction, relationType);
                /** @type {?} */
                var mergedTypeRelationCount = 0;
                /** @type {?} */
                var group = groups[groupKey] = groups[groupKey] || {
                    key: groupKey,
                    order: relationGroupMeta.order,
                    relationCounts: {},
                };
                // TODO Объединить код с mergedTypeCountData
                /** @type {?} */
                var countData = {
                    key: relationKey,
                    order: relationTypeMeta.order,
                    top: relationTypeMeta.top,
                    history: relationTypeMeta.history,
                    relationType: relationType,
                    direction: direction,
                    relationCount: relationCount,
                    historyRelationCounts: historyRelationCounts,
                    pluralKey: relationPluralKey,
                };
                /** @type {?} */
                var mergedTypeHistoryRelationCounts = {
                    actual: 0,
                    outdated: 0,
                    all: 0,
                };
                forEach(!mergedTypeCountData && mergedTypeInfo.relationTypes, (/**
                 * @param {?} t
                 * @return {?}
                 */
                function (t) {
                    /** @type {?} */
                    var info = get(node, ['_info', infoDirection, t]);
                    /** @type {?} */
                    var counts = info ? _this.getHistoryRelationCountsByInfo(info) : null;
                    if (!counts) {
                        return;
                    }
                    mergedTypeHistoryRelationCounts.actual += counts.actual;
                    mergedTypeHistoryRelationCounts.outdated += counts.outdated;
                    mergedTypeHistoryRelationCounts.all += counts.all;
                }));
                mergedTypeRelationCount = mergedTypeHistoryRelationCounts.all;
                if (!mergedTypeCountData && mergedTypeRelationCount > relationCount) {
                    /** @type {?} */
                    var mergedType = mergedTypeInfo.mergedType;
                    /** @type {?} */
                    var mergedTypeMeta = _this.getRelationTypeMeta(mergedType, direction);
                    // TODO Объединить код с countData
                    mergedTypeCountData = {
                        key: mergedTypeKey,
                        order: mergedTypeMeta.order,
                        top: mergedTypeMeta.top,
                        history: mergedTypeMeta.history,
                        relationType: mergedType,
                        direction: direction,
                        relationCount: mergedTypeRelationCount,
                        historyRelationCounts: mergedTypeHistoryRelationCounts,
                        pluralKey: _this.buildNodeRelationPluralKey(direction, mergedType),
                    };
                    group.relationCounts[mergedTypeKey] = mergedTypeCountData;
                    relationCountMap[mergedTypeKey] = mergedTypeCountData;
                }
                if (!relationCountMap[mergedTypeKey]) {
                    group.relationCounts[relationKey] = countData;
                }
                relationCountMap[relationKey] = countData;
            }));
        }));
        groups = sortBy(groups, 'order');
        forEach(groups, (/**
         * @param {?} group
         * @return {?}
         */
        function (group) {
            group.relationCounts = sortBy(group.relationCounts, 'order');
            relationCounts = relationCounts.concat(group.relationCounts);
        }));
        return {
            relationCountMap: relationCountMap,
            relationCounts: relationCounts,
            groups: groups,
        };
    };
    /**
     * @param {?} info
     * @return {?}
     */
    Meta.prototype.getHistoryRelationCountsByInfo = /**
     * @param {?} info
     * @return {?}
     */
    function (info) {
        info = info || {};
        return {
            actual: info.actual || 0,
            all: (info.actual || 0) + (info.outdated || 0),
            outdated: info.outdated || 0,
        };
    };
    /**
     * @param {?} infoDirection
     * @return {?}
     */
    Meta.prototype.getDirection = /**
     * @param {?} infoDirection
     * @return {?}
     */
    function (infoDirection) {
        return infoDirection === 'in' ? 'parents' : 'children';
    };
    /**
     * @param {?} relationType
     * @param {?} direction
     * @return {?}
     */
    Meta.prototype.getRelationTypeMeta = /**
     * @param {?} relationType
     * @param {?} direction
     * @return {?}
     */
    function (relationType, direction) {
        return get(rsearchMeta.relationTypes, [relationType, direction]);
    };
    /**
     * @param {?} relationGroup
     * @return {?}
     */
    Meta.prototype.getRelationGroupMeta = /**
     * @param {?} relationGroup
     * @return {?}
     */
    function (relationGroup) {
        return rsearchMeta.relationGroups[relationGroup];
    };
    /**
     * @param {?} relationType
     * @param {?} direction
     * @return {?}
     */
    Meta.prototype.getMergedTypeInfoByRelationType = /**
     * @param {?} relationType
     * @param {?} direction
     * @return {?}
     */
    function (relationType, direction) {
        /** @type {?} */
        var info = {
            mergedType: null,
            relationTypes: null,
        };
        forEach(rsearchMeta.mergedRelationTypes, (/**
         * @param {?} byDirections
         * @param {?} mergedType
         * @return {?}
         */
        function (byDirections, mergedType) {
            /** @type {?} */
            var relationTypes = byDirections[direction];
            if (includes(relationTypes, relationType)) {
                info.mergedType = mergedType;
                info.relationTypes = relationTypes;
                return false;
            }
        }));
        return info;
    };
    /**
     * @param {?} direction
     * @param {?} relationType
     * @return {?}
     */
    Meta.prototype.buildNodeRelationKey = /**
     * @param {?} direction
     * @param {?} relationType
     * @return {?}
     */
    function (direction, relationType) {
        return direction + '::' + relationType;
    };
    /**
     * @param {?} direction
     * @param {?} relationType
     * @return {?}
     */
    Meta.prototype.buildNodeRelationPluralKey = /**
     * @param {?} direction
     * @param {?} relationType
     * @return {?}
     */
    function (direction, relationType) {
        return 'NG_PLURALIZE::RELATION_' + relationType + '-' + direction;
    };
    /**
     * @param {?} node
     * @return {?}
     */
    Meta.prototype.buildNodeExtraMeta = /**
     * @param {?} node
     * @return {?}
     */
    function (node) {
        if (!node) {
            return;
        }
        // uid
        this.buildNodeUID(node);
        //
        node.__relationData = metaService.buildRelationDataByNodeInfo(node);
        //
        node.__idField = get(this.nodeTypesMeta[node._type], 'idField');
        if (node._type === 'COMPANY' || node._type === 'EGRUL') { // компания
            this.buildCompanyState(node);
            node.__isFavoritesSupported = true;
        }
        else if (node._type === 'INDIVIDUAL') { // физик
            node.__isFavoritesSupported = true;
        }
        else if (node._type === 'PURCHASE') { // закупка
            node.currency = node.currency || 'RUB';
            node.__lotMap = keyBy(node.lots, 'lot');
            this.resolvePurchaseHref(node);
        }
    };
    /**
     * @param {?} node
     * @return {?}
     */
    Meta.prototype.buildNodeUID = /**
     * @param {?} node
     * @return {?}
     */
    function (node) {
        node.__uid = node.$$hashKey = node.__uid || this.buildNodeUIDByType(node._id, node._type);
        return node.__uid;
    };
    /**
     * @param {?} id
     * @param {?} type
     * @return {?}
     */
    Meta.prototype.buildNodeUIDByType = /**
     * @param {?} id
     * @param {?} type
     * @return {?}
     */
    function (id, type) {
        return ('node-' + type + '-' + id);
    };
    /**
     * @param {?} node
     * @return {?}
     */
    Meta.prototype.buildCompanyState = /**
     * @param {?} node
     * @return {?}
     */
    function (node) {
        // юридическое состояние
        /** @type {?} */
        var egrulState = node.egrul_state;
        /** @type {?} */
        var aliveCode = '5';
        // действующее
        /** @type {?} */
        var intermediateCodes = ['6', '111', '121', '122', '123', '124', '131', '132'];
        // коды промежуточных состояний
        /** @type {?} */
        var _liquidate;
        if (node.egrul_liq) {
            _liquidate = {
                state: {
                    _actual: null,
                    _since: node.dead_dt,
                    type: upperFirst(lowerCase(node.egrul_liq.type)),
                },
            };
        }
        else if (node.dead_dt) {
            _liquidate = {
                state: {
                    _actual: null,
                    _since: node.dead_dt,
                    type: 'Ликвидировано',
                },
            };
        }
        else if (egrulState) {
            if (egrulState.code !== aliveCode) {
                _liquidate = {
                    state: {
                        _actual: egrulState._actual,
                        _since: egrulState._since,
                        type: egrulState.type,
                        intermediate: includes(intermediateCodes, egrulState.code),
                    },
                };
            }
        }
        node._liquidate = _liquidate;
        // способ организации компании
        /** @type {?} */
        var egrulReg = node.egrul_reg;
        /** @type {?} */
        var baseCodes = ['11', '01', '03'];
        if (egrulReg && !includes(baseCodes, egrulReg.code)) {
            node._reg = {
                state: {
                    _actual: egrulReg._actual,
                    _since: egrulReg._since,
                    type: egrulReg.type,
                },
            };
        }
    };
    /**
     * @param {?} node
     * @return {?}
     */
    Meta.prototype.resolvePurchaseHref = /**
     * @param {?} node
     * @return {?}
     */
    function (node) {
        if (node.law === 'FZ_44') {
            node.__href = node.href;
        }
        else {
            node.__href = 'http://zakupki.gov.ru/epz/order/quicksearch/search_eis.html?strictEqual=on&pageNumber=1&sortDirection=false&recordsPerPage=_10&showLotsInfoHidden=false&fz44=on&fz223=on&fz94=on&regions=&priceFrom=&priceTo=&currencyId=-1&publishDateFrom=&publishDateTo=&updateDateFrom=&updateDateTo=&sortBy=UPDATE_DATE&searchString=' + node._id;
        }
    };
    return Meta;
}());
/** @type {?} */
var metaService = new Meta();

/**
 * @fileoverview added by tsickle
 * Generated from: lib/domain/Node.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var Node = /** @class */ (function () {
    function Node(newNode) {
        /** @type {?} */
        var node = Object.assign({}, newNode);
        // TODO add wrapper for this - need somehow add this info to node, when needed
        // if (nkbSearchMetaService) {
        metaService.buildNodeExtraMeta(node);
        // }
        node.id = metaService.getNodeId(node);
        node._name = metaService.getNodeName(node);
        delete node.name;
        Object.assign(this, node, { _metaService: metaService });
    }
    /**
     * @param {?} node
     * @return {?}
     */
    Node.nodeWrapper = /**
     * @param {?} node
     * @return {?}
     */
    function (node) {
        if (node) {
            return node instanceof Node ? node : new Node(node);
        }
        else {
            return new Node(node);
        }
    };
    Object.defineProperty(Node.prototype, "idField", {
        get: /**
         * @return {?}
         */
        function () {
            /** @type {?} */
            var node = this;
            return metaService.getIdFieldForType(node.type);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Node.prototype, "aggregates", {
        get: /**
         * @return {?}
         */
        function () {
            return get(this, '_aggregates', {});
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Node.prototype, "name", {
        get: /**
         * @return {?}
         */
        function () {
            /** @type {?} */
            var node = this;
            return node._name;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Node.prototype, "fullName", {
        get: /**
         * @return {?}
         */
        function () {
            return get(this, 'namesort', '');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Node.prototype, "chiefName", {
        get: /**
         * @return {?}
         */
        function () {
            return get(this, 'chief_name', '');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Node.prototype, "type", {
        get: /**
         * @return {?}
         */
        function () {
            return get(this, '_type', '');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Node.prototype, "debts", {
        get: /**
         * @return {?}
         */
        function () {
            return get(this, '_debts', null);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Node.prototype, "licencies", {
        get: /**
         * @return {?}
         */
        function () {
            return map(get(this._egrulProfile, 'infoLicense'), (/**
             * @param {?} licence
             * @return {?}
             */
            function (licence) {
                return {
                    terminationDate: get(head(get(licence, 'profile')), 'terminationDate', false),
                    number: get(licence, 'data.number'),
                    type: map(get(licence, 'data.type'), (/**
                     * @param {?} type
                     * @return {?}
                     */
                    function (type) { return upperFirst(lowerCase(type)); })),
                    issued: upperFirst(lowerCase(get(licence, 'data.issued'))),
                    dateBegin: get(licence, 'data.dateBegin'),
                    date: get(licence, 'data.date'),
                };
            }));
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Node.prototype, "relations", {
        get: /**
         * @return {?}
         */
        function () {
            return get(this, '_relations', []);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Node.prototype, "disqualification", {
        get: /**
         * @return {?}
         */
        function () {
            /** @type {?} */
            var disqualification = head(get(head(get(this._egrulProfile, 'infoPersonOfficial')), 'data.disqualification'));
            return disqualification;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @return {?}
     */
    Node.prototype.getFullName = /**
     * @return {?}
     */
    function () {
        return get(this, 'namesort', '');
    };
    /**
     * @return {?}
     */
    Node.prototype.getChiefName = /**
     * @return {?}
     */
    function () {
        return get(this, 'chief_name', '');
    };
    /**
     * @return {?}
     */
    Node.prototype.getType = /**
     * @return {?}
     */
    function () {
        return get(this, '_type', '');
    };
    /**
     * @return {?}
     */
    Node.prototype.getCity = /**
     * @return {?}
     */
    function () {
        if (this.type === NODE_TYPE.INDIVIDUAL_IDENTITY) {
            return capitalize(get(head(get(this, 'selfemployedInfo.infoAddress')), 'data.адресРФ.город.наимГород', ''));
        }
        if (this.type === NODE_TYPE.COMPANY) {
            return capitalize(get(head(get(this, '_egrulProfile.infoAddress')), 'data.address.regionName', ''));
        }
    };
    /**
     * @return {?}
     */
    Node.prototype.getOgrnName = /**
     * @return {?}
     */
    function () {
        return this.ogrn ? 'ОГРН' : this.selfemployedInfo ? 'ОГРНИП' : '';
    };
    /**
     * @return {?}
     */
    Node.prototype.getEgrulName = /**
     * @return {?}
     */
    function () {
        return this.ogrn ? 'ЕГРЮЛ' : this.selfemployedInfo ? 'ЕГРИП' : '';
    };
    /**
     * @return {?}
     */
    Node.prototype.getOgrn = /**
     * @return {?}
     */
    function () {
        return this.ogrn ? this.ogrn : this.selfemployedInfo ? this.selfemployedInfo.ogrnip : '';
    };
    /**
     * @param {?} selfemployedInfo
     * @return {?}
     */
    Node.prototype.setSelfemployedInfo = /**
     * @param {?} selfemployedInfo
     * @return {?}
     */
    function (selfemployedInfo) {
        if (selfemployedInfo !== undefined) {
            this.selfemployedInfo = selfemployedInfo;
        }
    };
    /**
     * @param {?} egrulProfile
     * @return {?}
     */
    Node.prototype.setEgrulProfile = /**
     * @param {?} egrulProfile
     * @return {?}
     */
    function (egrulProfile) {
        this._egrulProfile = egrulProfile;
    };
    /**
     * @param {?} balance
     * @return {?}
     */
    Node.prototype.setBalance = /**
     * @param {?} balance
     * @return {?}
     */
    function (balance) {
        this._balance = balance;
    };
    return Node;
}());

/**
 * @fileoverview added by tsickle
 * Generated from: lib/domain/SearchFilter.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
//Класс Фильтров для каждого из типов поиска
/**
 * @return {?}
 */
function Filter() {
    return {
        regions: [],
        inns: [],
        makeActive: makeActive
    };
    /**
     * @param {?} elementToMake
     * @return {?}
     */
    function makeActive(elementToMake) {
        forEach(this.regions, (/**
         * @param {?} region
         * @return {?}
         */
        function (region) {
            region.active = (elementToMake && region.regionCode === elementToMake.regionCode);
        }));
    }
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/domain/SearchType.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
//Класс типов поиска
/**
 * @param {?} nodeType
 * @param {?} data
 * @return {?}
 */
function SearchType(nodeType, data) {
    /** @type {?} */
    var searchType = {
        nodeType: nodeType,
        resultPriority: data.searchResultPriority,
        accentedResultPriority: data.accentedResultPriority,
        pageConfig: null,
        filters: null,
        filter: Filter(),
        request: null,
        result: null,
        nodeList: null,
        active: false,
        setResultRegions: setResultRegions,
        setResultInns: setResultInns,
        resetActive: resetActive
    };
    return searchType;
    /**
     * @return {?}
     */
    function resetActive() {
        searchType.active = false;
    }
    /**
     * @return {?}
     */
    function setResultRegions() {
        /** @type {?} */
        var regions = [];
        if (hasRegionCodes.call(this)) {
            /** @type {?} */
            var data_1 = this.result['info']['nodeFacet']['region_code'];
            forOwn(data_1, (/**
             * @param {?} number
             * @param {?} regionCode
             * @return {?}
             */
            function (number, regionCode) {
                /** @type {?} */
                var res = {
                    number: number,
                    regionCode: regionCode,
                    active: false
                };
                regions.push(res);
            }));
        }
        this.filter.regions = regions;
        /**
         * @return {?}
         */
        function hasRegionCodes() {
            return get(this, 'result.info.nodeFacet.region_code', false);
        }
    }
    /**
     * @param {?} searchType
     * @return {?}
     */
    function setResultInns(searchType) {
        /** @type {?} */
        var inns = [];
        hasResult(searchType) ? forEach(searchType.result.list, (/**
         * @param {?} element
         * @return {?}
         */
        function (element) {
            element.inn ? inns.push(element.inn) : '';
        })) : '';
        searchType.filter.inns = inns;
        /**
         * @param {?} searchType
         * @return {?}
         */
        function hasResult(searchType) {
            return get(searchType, 'result.list', undefined);
        }
    }
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/angular-nkb-lib.service.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var AngularNkbLibService = /** @class */ (function () {
    function AngularNkbLibService() {
    }
    AngularNkbLibService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    AngularNkbLibService.ctorParameters = function () { return []; };
    /** @nocollapse */ AngularNkbLibService.ngInjectableDef = defineInjectable({ factory: function AngularNkbLibService_Factory() { return new AngularNkbLibService(); }, token: AngularNkbLibService, providedIn: "root" });
    return AngularNkbLibService;
}());

/**
 * @fileoverview added by tsickle
 * Generated from: lib/angular-nkb-lib.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var AngularNkbLibComponent = /** @class */ (function () {
    function AngularNkbLibComponent() {
    }
    /**
     * @return {?}
     */
    AngularNkbLibComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        console.log(this);
    };
    AngularNkbLibComponent.decorators = [
        { type: Component, args: [{
                    selector: 'lib-angular-nkb-lib',
                    template: "\n    <p>\n      angular-nkb-lib works!\n    </p>\n  "
                }] }
    ];
    /** @nocollapse */
    AngularNkbLibComponent.ctorParameters = function () { return []; };
    return AngularNkbLibComponent;
}());

/**
 * @fileoverview added by tsickle
 * Generated from: lib/cache/CacheElement.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var CacheElement = /** @class */ (function () {
    function CacheElement(el) {
        Object.assign(this, { promise: el });
    }
    /**
     * @return {?}
     */
    CacheElement.prototype.head = /**
     * @return {?}
     */
    function () {
        return __awaiter(this, void 0, void 0, function () {
            var res;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.promise];
                    case 1:
                        res = _a.sent();
                        return [2 /*return*/, head(get(res, 'data.list', [res]))];
                }
            });
        });
    };
    /**
     * @return {?}
     */
    CacheElement.prototype.list = /**
     * @return {?}
     */
    function () {
        return __awaiter(this, void 0, void 0, function () {
            var res;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.promise];
                    case 1:
                        res = _a.sent();
                        return [2 /*return*/, get(res, 'data.list')];
                }
            });
        });
    };
    /**
     * @return {?}
     */
    CacheElement.prototype.data = /**
     * @return {?}
     */
    function () {
        return __awaiter(this, void 0, void 0, function () {
            var res;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.promise];
                    case 1:
                        res = _a.sent();
                        return [2 /*return*/, get(res, 'data')];
                }
            });
        });
    };
    /**
     * @return {?}
     */
    CacheElement.prototype.get = /**
     * @return {?}
     */
    function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.promise];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    return CacheElement;
}());

/**
 * @fileoverview added by tsickle
 * Generated from: lib/cache/Cache.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
var METHODS = {
    'GET': 'GET',
    'POST': 'POST',
    'PUT': 'PUT',
    'DELETE': 'DELETE',
};
var Cache = /** @class */ (function () {
    function Cache() {
        this.obj = {};
    }
    /**
     * @param {?} key
     * @return {?}
     */
    Cache.prototype.get = /**
     * @param {?} key
     * @return {?}
     */
    function (key) {
        return this.obj[key];
    };
    /**
     * @param {?} key
     * @param {?} value
     * @return {?}
     */
    Cache.prototype.set = /**
     * @param {?} key
     * @param {?} value
     * @return {?}
     */
    function (key, value) {
        this.obj[key] = value;
    };
    /**
     * @param {?} url
     * @param {?=} options
     * @return {?}
     */
    Cache.prototype.try = /**
     * @param {?} url
     * @param {?=} options
     * @return {?}
     */
    function (url, options) {
        if (options === void 0) { options = { params: {}, method: METHODS.GET }; }
        return __awaiter(this, void 0, void 0, function () {
            var result;
            return __generator(this, function (_a) {
                result = this.get(url);
                if (result === undefined) {
                    // @ts-ignore
                    result = new CacheElement(axios({ url: url, method: options.method }));
                    this.set(url, result);
                }
                return [2 /*return*/, result];
            });
        });
    };
    return Cache;
}());

/**
 * @fileoverview added by tsickle
 * Generated from: lib/autokad/autokadResource.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
var seldon = axios.create({
    baseURL: '/api/v1/companies/',
});
var AutokadResource = /** @class */ (function () {
    function AutokadResource() {
        this.cache = new Cache();
    }
    /**
     * @param {?} arg0
     * @return {?}
     */
    AutokadResource.prototype.kadSearch = /**
     * @param {?} arg0
     * @return {?}
     */
    function (arg0) {
        throw new Error("Method not implemented.");
    };
    /**
     * @return {?}
     */
    AutokadResource.prototype.init = /**
     * @return {?}
     */
    function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/];
            });
        });
    };
    /**
     * @param {?} options
     * @return {?}
     */
    AutokadResource.prototype.search = /**
     * @param {?} options
     * @return {?}
     */
    function (options) {
        return __awaiter(this, void 0, void 0, function () {
            /**
             * @param {?} options
             * @return {?}
             */
            function buildRequestParams(options) {
                /** @type {?} */
                var req = {
                    type: options.type,
                    pageSize: options.pageSize || 100,
                    pageNo: options.pageIndex || 1,
                };
                options.ogrn !== undefined ? req.ogrn = options.ogrn :
                    options.inn !== undefined ? req.inn = options.inn :
                        console.warn('there is no inn or ogrn in request!', options);
                return req;
            }
            var result, req;
            return __generator(this, function (_a) {
                req = buildRequestParams(options);
                result = this.cache.get(req.ogrn + "_" + req.type + "_" + req.pageSize + "_" + req.pageNo);
                if (result === undefined) {
                    result = seldon.get("ogrn-" + req.ogrn + "/cases", { params: req });
                    this.cache.set(req.ogrn + "_" + req.type + "_" + req.pageSize + "_" + req.pageNo, result);
                }
                return [2 /*return*/, result];
            });
        });
    };
    /**
     * @param {?} options
     * @return {?}
     */
    AutokadResource.prototype.aggregates = /**
     * @param {?} options
     * @return {?}
     */
    function (options) {
        return __awaiter(this, void 0, void 0, function () {
            var res;
            return __generator(this, function (_a) {
                if (options.ogrn) {
                    res = seldon.get("ogrn-" + options.ogrn + "/aggregates");
                }
                return [2 /*return*/, res];
            });
        });
    };
    /**
     * @param {?} ogrn
     * @return {?}
     */
    AutokadResource.prototype.getAggregates = /**
     * @param {?} ogrn
     * @return {?}
     */
    function (ogrn) {
        return __awaiter(this, void 0, void 0, function () {
            var res, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.aggregates({ ogrn: ogrn })];
                    case 1:
                        res = _a.sent();
                        return [2 /*return*/, head(get(res, 'data.entries'))];
                    case 2:
                        e_1 = _a.sent();
                        return [2 /*return*/, Promise.reject()];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    AutokadResource.decorators = [
        { type: Injectable, args: [{ providedIn: 'root' },] }
    ];
    /** @nocollapse */ AutokadResource.ngInjectableDef = defineInjectable({ factory: function AutokadResource_Factory() { return new AutokadResource(); }, token: AutokadResource, providedIn: "root" });
    return AutokadResource;
}());

/**
 * @fileoverview added by tsickle
 * Generated from: lib/autokad/autokadService.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
var Search = (/**
 * @return {?}
 */
function () {
    return {
        params: null,
        requestData: null,
        request: null,
        result: {
            // В формате kad.arbitr.ru
            TotalCount: null,
            PagesCount: null,
            Items: [],
            entries: []
        },
        noResult: true,
        error: null,
        watch: true,
        sources: undefined,
    };
});
/** @type {?} */
var DEFAULT_SEARCH_PARAMS = {
    side: 4
};
/** @type {?} */
var DEFAULT_SEARCH_REQUEST_DATA = {
    q: null,
    dateFrom: null,
    dateTo: null,
    page: 1,
    pageIndex: 1
};
var AutokadService = /** @class */ (function () {
    function AutokadService(autokadResourceService) {
        this.autokadResourceService = autokadResourceService;
        /** @type {?} */
        var node = null;
        /** @type {?} */
        var autokadService = {
            search: Search(),
            loading: false,
            pager: Pager(),
            initSearch: initSearch,
            doSearch: doSearch,
            buildSearchRequestData: buildSearchRequestData,
            getResultTotal: getResultTotal,
            getCaseCount: getCaseCount,
            hasError: hasError,
            isLoading: isLoading,
            isNoResult: isNoResult,
        };
        Object.assign(this, autokadService);
        /**
         * @param {?} newNode
         * @param {?} params
         * @return {?}
         */
        function initSearch(newNode, params) {
            node = newNode;
            autokadService.search.params = assignIn({}, DEFAULT_SEARCH_PARAMS, params, {
                source: 0
            });
        }
        /**
         * @return {?}
         */
        function resetSearchRequest() {
            if (autokadService.search.request && autokadService.search.request.abort) {
                autokadService.search.request.abort();
            }
            autokadService.search.requestData = getDefaultSearchRequestData();
        }
        /**
         * @return {?}
         */
        function doSearch() {
            var _this = this;
            autokadService.search.result = {
                TotalCount: null,
                PagesCount: null,
                Items: [],
                entries: []
            };
            resetSearchRequest();
            autokadService.search.requestData = autokadService.buildSearchRequestData(autokadService.search.params);
            return new Promise((/**
             * @param {?} resolve
             * @return {?}
             */
            function (resolve) {
                if (isBlank(autokadService.search.requestData.inn)) {
                    console.warn('search.requestData.inn is empty ');
                    resolve();
                }
                else {
                    searchRequestPromise.call(_this).then((/**
                     * @param {?} data
                     * @return {?}
                     */
                    function (data) {
                        autokadService.search.result = data;
                        autokadService.pager.reset(data);
                        hideLoading();
                        resolve();
                    }), (/**
                     * @param {?} data
                     * @return {?}
                     */
                    function (data) {
                        autokadService.search.result = data;
                        autokadService.pager.reset(data);
                        hideLoading();
                        resolve();
                    }));
                }
            }));
        }
        /**
         * @return {?}
         */
        function searchRequestPromise() {
            return new Promise((/**
             * @param {?} resolve
             * @param {?} reject
             * @return {?}
             */
            function (resolve, reject) {
                showLoading();
                autokadService.search.noResult = true;
                autokadService.search.request = autokadResourceService.search(autokadService.search.requestData).then(success, error);
                /**
                 * @param {?} data
                 * @return {?}
                 */
                function success(data) {
                    data = data.data;
                    autokadService.search.noResult = (data.total === 0);
                    autokadService.search.error = null;
                    if (!hasResultItems(data)) {
                        autokadService.search.error = true;
                        hideLoading();
                        reject(data);
                    }
                    hideLoading();
                    resolve(data);
                }
                /**
                 * @param {?} err
                 * @return {?}
                 */
                function error(err) {
                    autokadService.search.noResult = false;
                    autokadService.search.error = true;
                    hideLoading();
                    reject(err);
                }
            }));
        }
        /**
         * @param {?} result
         * @return {?}
         */
        function hasResultItems(result) {
            return isObject(result) && isArray(result['entries']);
        }
        /**
         * @param {?} value
         * @return {?}
         */
        function isBlank(value) {
            return isEmpty(value) && !isNumber(value) || isNaN(value);
        }
        /**
         * @return {?}
         */
        function hasError() {
            return autokadService.search.error;
        }
        /**
         * @param {?} searchParams
         * @return {?}
         */
        function buildSearchRequestData(searchParams) {
            /** @type {?} */
            var requestData = assignIn({}, DEFAULT_SEARCH_REQUEST_DATA);
            requestData['type'] = (searchParams['side'] === -1 ? 4 : searchParams['side']);
            requestData['inn'] = node['inn'];
            requestData['ogrn'] = node['ogrn'];
            return requestData;
        }
        /*
                    * case
                    *
                    */
        /**
         * @param {?} search
         * @param {?} success
         * @param {?} error
         * @param {?} complete
         * @return {?}
         */
        function getCaseCount(search, success, error, complete) {
            if (!isArray(autokadService.search.sources) || !size(autokadService.search.sources)) {
                console.warn('getCaseCount... error: search.sources is blank');
                errorCallback();
                return null;
            }
            /** @type {?} */
            var sourceIndex = 0;
            /** @type {?} */
            var sourceCount = size(autokadService.search.sources);
            /** @type {?} */
            var caseCountRequest = CaseCountRequest();
            /** @type {?} */
            var result = null;
            /** @type {?} */
            var source;
            /** @type {?} */
            var request;
            nextRequest();
            return caseCountRequest;
            /**
             * @return {?}
             */
            function nextRequest() {
                /** @type {?} */
                var error = false;
                /** @type {?} */
                var next = false;
                /** @type {?} */
                var q;
                source = autokadService.search.sources[sourceIndex++];
                q = source.value;
                if (isBlank(q)) {
                    request = null;
                    result = 0;
                    next = true;
                    caseCountRequest._setRequest(request);
                    check();
                }
                else {
                    request = autokadResourceService.kadSearch({
                        r: {
                            q: q
                        },
                        success: (/**
                         * @param {?} data
                         * @return {?}
                         */
                        function (data) {
                            result = getResultTotal(data['Result']);
                            if (isNull(result)) {
                                error = true;
                            }
                            else if (result === 0) {
                                next = true;
                            }
                        }),
                        error: (/**
                         * @return {?}
                         */
                        function () {
                            error = true;
                        })
                    });
                    caseCountRequest._setRequest(request);
                    request.completePromise.then((/**
                     * @return {?}
                     */
                    function () {
                        check();
                    }));
                }
                /**
                 * @return {?}
                 */
                function check() {
                    if (error) {
                        errorCallback();
                    }
                    else if (!next || sourceIndex === sourceCount) {
                        successCallback();
                    }
                    else if (next) {
                        nextRequest();
                    }
                }
            }
            /**
             * @return {?}
             */
            function successCallback() {
                if (isFunction(success)) {
                    success(result, sourceIndex - 1);
                }
                completeCallback();
            }
            /**
             * @return {?}
             */
            function errorCallback() {
                if (isFunction(error)) {
                    error();
                }
                completeCallback();
            }
            /**
             * @return {?}
             */
            function completeCallback() {
                if (isFunction(complete)) {
                    complete();
                }
            }
            /**
             * @param {?} value
             * @return {?}
             */
            function isBlank(value) {
                return isEmpty(value) && !isNumber(value) || isNaN(value);
            }
            /**
             * @return {?}
             */
            function CaseCountRequest() {
                /** @type {?} */
                var request;
                return {
                    _setRequest: (/**
                     * @param {?} r
                     * @return {?}
                     */
                    function (r) {
                        request = r;
                    }),
                    abort: (/**
                     * @return {?}
                     */
                    function () {
                        if (request) {
                            request.abort();
                        }
                    })
                };
            }
        }
        /**
         * @param {?} result
         * @return {?}
         */
        function getResultTotal(result) {
            /** @type {?} */
            var r = result && result['TotalCount'];
            if (!isNumber(r)) {
                r = null;
            }
            return r;
        }
        /**
         * @return {?}
         */
        function showLoading() {
            autokadService.loading = true;
        }
        /**
         * @return {?}
         */
        function hideLoading() {
            autokadService.loading = false;
        }
        /**
         * @return {?}
         */
        function getDefaultSearchRequestData() {
            return DEFAULT_SEARCH_REQUEST_DATA;
        }
        /**
         * @return {?}
         */
        function isLoading() {
            return autokadService.loading;
        }
        /**
         * @return {?}
         */
        function isNoResult() {
            return get(autokadService.search, 'result.status.itemsFound', 0) === 0;
        }
        //Pager
        /**
         * @return {?}
         * @this {*}
         */
        function Pager() {
            /** @type {?} */
            var internalDisabled = false;
            /** @type {?} */
            var noNextPage = false;
            return {
                reset: reset,
                nextPage: nextPage,
                isDisabled: isDisabled
            };
            /**
             * @return {?}
             */
            function isDisabled() {
                return internalDisabled || noNextPage;
            }
            /**
             * @param {?} result
             * @return {?}
             */
            function reset(result) {
                internalDisabled = false;
                noNextPage = noMore(false, result);
            }
            /**
             * @param {?} hasError
             * @param {?} result
             * @return {?}
             */
            function noMore(hasError, result) {
                return hasError || (autokadService.search.requestData.pageIndex * result.size >= result.total);
            }
            /**
             * @return {?}
             * @this {*}
             */
            function nextPage() {
                return __awaiter(this, void 0, void 0, function () {
                    return __generator(this, function (_a) {
                        return [2 /*return*/, new Promise((/**
                             * @param {?} resolve
                             * @return {?}
                             */
                            function (resolve) {
                                showLoading();
                                if (!isDisabled() && autokadService.search.requestData) {
                                    internalDisabled = true;
                                    autokadService.search.requestData.pageIndex++;
                                    searchRequestPromise().then((/**
                                     * @param {?} result
                                     * @return {?}
                                     */
                                    function (result) {
                                        /** @type {?} */
                                        var hasError = !hasResultItems(result);
                                        if (!hasError) {
                                            forEach(result.entries, (/**
                                             * @param {?} item
                                             * @return {?}
                                             */
                                            function (item) {
                                                autokadService.search.result.entries.push(item);
                                            }));
                                            internalDisabled = false;
                                            noNextPage = noMore(false, result);
                                        }
                                        else {
                                            internalDisabled = false;
                                            noNextPage = true;
                                        }
                                        hideLoading();
                                        resolve();
                                    }));
                                }
                                resolve();
                            }))];
                    });
                });
            }
        }
    }
    AutokadService.decorators = [
        { type: Injectable, args: [{ providedIn: 'root' },] }
    ];
    /** @nocollapse */
    AutokadService.ctorParameters = function () { return [
        { type: AutokadResource }
    ]; };
    /** @nocollapse */ AutokadService.ngInjectableDef = defineInjectable({ factory: function AutokadService_Factory() { return new AutokadService(inject(AutokadResource)); }, token: AutokadService, providedIn: "root" });
    return AutokadService;
}());

/**
 * @fileoverview added by tsickle
 * Generated from: lib/autokad/autokadComponent.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var AutokadComponent = /** @class */ (function () {
    function AutokadComponent(autokadService) {
        this.autokadService = autokadService;
        this.onAddArbitrationBreadcrumb = new EventEmitter();
        this.loading = true;
        Object.assign(this, {
            pager: autokadService.pager,
        });
    }
    /**
     * @return {?}
     */
    AutokadComponent.prototype.ngOnChanges = /**
     * @return {?}
     */
    function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.autokadService.initSearch(this.node);
                        if (this.side) {
                            this.autokadService.search.params.side = this.side;
                        }
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, , 3, 4]);
                        return [4 /*yield*/, this.autokadService.doSearch()];
                    case 2:
                        _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        this.loading = false;
                        if (this.scrollTop) {
                            window.scrollTo(0, this.scrollTop);
                        }
                        return [7 /*endfinally*/];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    Object.defineProperty(AutokadComponent.prototype, "list", {
        get: /**
         * @return {?}
         */
        function () {
            return this.autokadService.search.result.entries;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @param {?} type
     * @return {?}
     */
    AutokadComponent.prototype.changeSide = /**
     * @param {?} type
     * @return {?}
     */
    function (type) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.loading = true;
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, , 3, 4]);
                        this.autokadService.search.params.side = type;
                        return [4 /*yield*/, this.autokadService.doSearch()];
                    case 2:
                        _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        this.loading = false;
                        return [7 /*endfinally*/];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * @param {?} type
     * @return {?}
     */
    AutokadComponent.prototype.isSelectedSide = /**
     * @param {?} type
     * @return {?}
     */
    function (type) {
        return this.autokadService.search.params
            && (parseInt(type) === parseInt(this.autokadService.search.params.side));
    };
    /**
     * @param {?} caseSides
     * @return {?}
     */
    AutokadComponent.prototype.getPlaintiff = /**
     * @param {?} caseSides
     * @return {?}
     */
    function (caseSides) {
        return filter(caseSides, (/**
         * @param {?} item
         * @return {?}
         */
        function (item) { return item.type === 0; }));
    };
    /**
     * @param {?} caseSides
     * @return {?}
     */
    AutokadComponent.prototype.getDefendant = /**
     * @param {?} caseSides
     * @return {?}
     */
    function (caseSides) {
        return filter(caseSides, (/**
         * @param {?} item
         * @return {?}
         */
        function (item) { return item.type === 1; }));
    };
    /**
     * @param {?} caseSides
     * @return {?}
     */
    AutokadComponent.prototype.getOthers = /**
     * @param {?} caseSides
     * @return {?}
     */
    function (caseSides) {
        return filter(caseSides, (/**
         * @param {?} item
         * @return {?}
         */
        function (item) { return item.type !== 1 && item.type !== 0; }));
    };
    /**
     * @return {?}
     */
    AutokadComponent.prototype.getTotal = /**
     * @return {?}
     */
    function () {
        var _this = this;
        /** @type {?} */
        var hasSearchResult = (/**
         * @return {?}
         */
        function () {
            return !isEmpty(_this.autokadService.search.result);
        });
        return hasSearchResult() ? get(this.autokadService, 'search.result.total', 0) : 0;
    };
    /**
     * @return {?}
     */
    AutokadComponent.prototype.isEmptyResult = /**
     * @return {?}
     */
    function () {
        return !this.getTotal();
    };
    /**
     * @return {?}
     */
    AutokadComponent.prototype.isNoResult = /**
     * @return {?}
     */
    function () {
        return this.autokadService.search.noResult;
    };
    /**
     * @return {?}
     */
    AutokadComponent.prototype.hasError = /**
     * @return {?}
     */
    function () {
        return this.autokadService.search.error;
    };
    Object.defineProperty(AutokadComponent.prototype, "isLoading", {
        get: /**
         * @return {?}
         */
        function () {
            return this.loading;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @param {?} id
     * @return {?}
     */
    AutokadComponent.prototype.addArbitrationBreadcrumb = /**
     * @param {?} id
     * @return {?}
     */
    function (id) {
        this.onAddArbitrationBreadcrumb.emit(id);
    };
    /**
     * @return {?}
     */
    AutokadComponent.prototype.nextPage = /**
     * @return {?}
     */
    function () {
        return __awaiter(this, void 0, void 0, function () {
            var promise;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.loading = true;
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, , 3, 4]);
                        promise = this.pager.nextPage().then((/**
                         * @return {?}
                         */
                        function () {
                            _this.loading = false;
                        }));
                        console.log(promise);
                        return [4 /*yield*/, promise];
                    case 2:
                        _a.sent();
                        return [3 /*break*/, 4];
                    case 3: return [7 /*endfinally*/];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    AutokadComponent.decorators = [
        { type: Component, args: [{
                    selector: 'nkb-autokad',
                    template: "\n        <div class=\"autokad autokad__inner\">\n            <div class=\"autokad__filters\">\n                <div class=\"\">\n                    <span>\u0421\u0442\u043E\u0440\u043E\u043D\u0430 \u0434\u0435\u043B\u0430</span><br>\n                    <button class=\"btn  btn-sm flat\" [ngClass]=\"isSelectedSide(4) ? 'active':''\"\n                            (click)=\"changeSide(4)\">\u043B\u044E\u0431\u043E\u0435\n                    </button>\n                    <button class=\"btn  btn-sm flat\" [ngClass]=\"isSelectedSide(0) ? 'active':''\"\n                            (click)=\"changeSide(0)\">\u0438\u0441\u0442\u0435\u0446\n                    </button>\n                    <button class=\"btn  btn-sm flat\" [ngClass]=\"isSelectedSide(1) ? 'active':''\"\n                            (click)=\"changeSide(1)\">\u043E\u0442\u0432\u0435\u0442\u0447\u0438\u043A\n                    </button>\n                    <button class=\"btn  btn-sm flat\" [ngClass]=\"isSelectedSide(2) ? 'active':''\"\n                            (click)=\"changeSide(2)\">\u0438\u043D\u043E\u0435 \u043B\u0438\u0446\u043E\n                    </button>\n                    <button class=\"btn  btn-sm flat\" [ngClass]=\"isSelectedSide(3) ? 'active':''\"\n                            (click)=\"changeSide(3)\">\u0442\u0440\u0435\u0442\u044C\u0435 \u043B\u0438\u0446\u043E\n                    </button>\n                </div>\n                <div class=\"autokad__total\">\n                    <a class=\"underline\" [attr.href]=\"'http://kad.arbitr.ru/ ' | externalUrl\" target=\"_blank\">\u041A\u0430\u0440\u0442\u043E\u0442\u0435\u043A\u0430\n                        \u0430\u0440\u0431\u0438\u0442\u0440\u0430\u0436\u043D\u044B\u0445 \u0434\u0435\u043B</a>\n                    <div>\n                        <span [hidden]=\"getTotal()===0 || isLoading\">\n                            <span>{{getTotal()}}</span>\n                            <!-- | translate: {number: getTotal()} -->\n                            <!-- {{'AUTOKAD.KAD_SEARCH_TOTAL' | async }} -->\n                            &nbsp;\n                        </span>\n                    </div>\n                </div>\n            </div>\n            <div class=\"autokad__result\">\n                <div class=\"autokad__result-list\"\n                    infiniteScroll\n                    (scrolled)=\"nextPage()\"\n                >\n                    <div\n                        class=\"result-list-element autokad__list-element autokad-case autokad-case__inner\"\n                        *ngFor=\"let item of list\"\n                    >\n                        <div class=\"autokad-case__header\">\n                            <div class=\"autokad-case__caption\">\n                                <div class=\"autokad-case__number\">\n                                    {{item.id}}\n                                </div>\n                                <div class=\"autokad-case__instance-container\">\n                                    <div>{{item.filledMillis * 1000 | date : 'dd.MM.yyyy'}}</div>\n                                    <div class=\"autokad-case__instance\">{{item.currentInstance}}</div>\n                                </div>\n                                <div class=\"autokad-case__type\">{{item.type.name}}</div>\n                            </div>\n                            <div\n                                class=\"autokad-case__process-container autokad-case__process-container--1\"\n                            >\n                                <span\n                                    class=\"autokad-case__process autokad-case__process--1\"\n                                    title=\"\u0411\u0430\u043D\u043A\u0440\u043E\u0442\u043D\u043E\u0435 \u0434\u0435\u043B\u043E\"\n                                    *ngIf=\"item.type.code===1\"\n                                >\u0411</span>\n                            </div>\n\n                            <div class=\"autokad-case__process-container\">\n                                <span class=\"autokad-case__process\" *ngIf=\"item.active===true\">\u043D\u0430 \u0440\u0430\u0441\u0441\u043C\u043E\u0442\u0440\u0435\u043D\u0438\u0438</span>\n                            </div>\n                            <div class=\"autokad-case__sum\">\n                                <span *ngIf=\"item.claimAmount !== 0\">{{item.claimAmount | number}}\u0440.</span>\n                            </div>\n                        </div>\n\n                        <div class=\"autokad-sides autokad-sides__inner\">\n                            <div class=\"autokad-sides__row\">\n                                <div class=\"autokad-sides__caption\">\n                                    \u0418\u0441\u0442\u0435\u0446:\n                                </div>\n                                <div class=\"autokad-sides__companies\">\n                                    <div *ngFor=\"let company of getPlaintiff(item.parties) ; last as isLast\">\n                                        <nkb-company-name-with-link\n                                            (addBreadcrumb)=\"addArbitrationBreadcrumb(id)\"\n                                            [node]=\"company\"\n                                            [ogrn]=\"node.ogrn\"\n                                        ></nkb-company-name-with-link>\n                                        <span [hidden]=\"isLast\">, &nbsp;</span>\n                                    </div>\n                                </div>\n                            </div>\n                            <div class=\"autokad-sides__row\">\n                                <div class=\"autokad-sides__caption\">\n                                    \u041E\u0442\u0432\u0435\u0442\u0447\u0438\u043A:\n                                </div>\n                                <div class=\"autokad-sides__companies\">\n                                    <div *ngFor=\"let company of getDefendant(item.parties); last as isLast\">\n                                        <nkb-company-name-with-link\n                                            [ogrn]=\"node.ogrn\"\n                                            (addBreadcrumb)=\"addArbitrationBreadcrumb(id)\"\n                                            [node]=\"company\"></nkb-company-name-with-link>\n                                        <span [hidden]=\"isLast\">, &nbsp;</span>\n                                    </div>\n                                </div>\n                            </div>\n                            <div class=\"autokad-sides__row\" *ngIf=\"getOthers(item.parties).length>0\">\n                                <div class=\"autokad-sides__caption\">\n                                    \u041F\u0440\u043E\u0447\u0438\u0435 \u043B\u0438\u0446\u0430:\n                                </div>\n                                <div class=\"autokad-sides__companies\">\n                                    <div *ngFor=\"let company of getOthers(item.parties); last as isLast\">\n                                        <nkb-company-name-with-link\n                                            [ogrn]=\"node.ogrn\"\n                                            (addBreadcrumb)=\"addArbitrationBreadcrumb(id)\"\n                                            [node]=\"company\"\n                                        ></nkb-company-name-with-link>\n                                        <span [hidden]=\"isLast\">, &nbsp;</span>\n                                    </div>\n                                </div>\n                            </div>\n\n                            <div class=\"autokad-documents\" *ngIf=\"item.documents\">\n                                <h5>\u0414\u043E\u043A\u0443\u043C\u0435\u043D\u0442\u044B \u043F\u043E \u0434\u0435\u043B\u0443:</h5>\n                                <ul>\n                                    <li *ngFor=\" let document of item.documents\">\n                                        <a target=\"_blank\" [attr.href]=\"document.url\">\n                                            <span>{{document.date * 1000 | date:'dd.MM.yyyy'}}</span>\n                                            .\n                                            {{document.name}}\n                                        </a>\n                                    </li>\n                                </ul>\n\n                            </div>\n\n                        </div>\n                    </div>\n                </div>\n            </div>\n\n            <div class=\"align-center empty-result muted\"\n                *ngIf=\"isNoResult()  && !hasError() && !isLoading\" style=\"margin-top: 4em;\">\n                \u0410\u0440\u0431\u0438\u0442\u0440\u0430\u0436\u043D\u044B\u0445 \u0434\u0435\u043B \u043D\u0435 \u043D\u0430\u0439\u0434\u0435\u043D\u043E\n            </div>\n            <div class=\"align-center empty-result\" *ngIf=\"hasError() && !isLoading\">\n                \u041F\u043E\u0438\u0441\u043A \u0432\n                <a class=\"underline\" [attr.href]=\"'http://kad.arbitr.ru/' | externalUrl\" target=\"_blank\">\n                    \u043A\u0430\u0440\u0442\u043E\u0442\u0435\u043A\u0435 \u0430\u0440\u0431\u0438\u0442\u0440\u0430\u0436\u043D\u044B\u0445 \u0434\u0435\u043B\n                </a>\n                \u0432\u0440\u0435\u043C\u0435\u043D\u043D\u043E \u043D\u0435\u0434\u043E\u0441\u0442\u0443\u043F\u0435\u043D\n            </div>\n            <div class=\"loader\" [hidden]=\"!loading\" style=\"margin-top:1em; \"></div>\n        </div>\n    ",
                    styles: [".autokad__filters{display:-webkit-box;display:flex;-webkit-box-pack:justify;justify-content:space-between}.autokad__total{display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column;-webkit-box-pack:justify;justify-content:space-between}.autokad__result-list{margin-top:1em}.autokad-case__inner{padding:1em;border-color:#444;cursor:default}.autokad-case__inner:hover{border-color:#444;cursor:default}.autokad-case__header{display:-webkit-box;display:flex;-webkit-box-pack:justify;justify-content:space-between;margin-bottom:1em}.autokad-case__caption{flex-basis:60%;display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column}.autokad-case__process{background-color:#2f96b4;color:#fff;display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column;-webkit-box-pack:center;justify-content:center;text-align:center;font-weight:700;border-radius:3px;height:2em;width:10em;align-self:center}.autokad-case__process--1{width:2em;background-color:#bd362f;margin-left:.5em;margin-right:.5em}.autokad-case__process-container{flex-basis:10%}.autokad-case__process-container--1{flex-basis:1em}.autokad-case__sum{font-size:large;font-weight:700;flex-basis:20%;display:-webkit-box;display:flex;-webkit-box-pack:end;justify-content:flex-end}.autokad-case__number{font-size:large;font-weight:700}.autokad-case__instance-container{display:-webkit-box;display:flex;-webkit-box-orient:horizontal;-webkit-box-direction:normal;flex-direction:row}.autokad-case__instance-container div{margin:0 1em}.autokad-case__instance-container div:first-of-type{margin:0}.autokad-case__type{font-style:italic}.autokad-sides__row{display:-webkit-box;display:flex;-webkit-box-orient:horizontal;-webkit-box-direction:normal;flex-direction:row}.autokad-sides__caption{flex-basis:20%;font-weight:700}.autokad-sides__companies{flex-basis:80%;display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column}.autokad-sides__companies div{margin:.2em 0}.autokad-documents h5{text-align:center;font-weight:700}.autokad__company-name--highlight{background-color:#ff0}"]
                }] }
    ];
    /** @nocollapse */
    AutokadComponent.ctorParameters = function () { return [
        { type: AutokadService }
    ]; };
    AutokadComponent.propDecorators = {
        scrollTop: [{ type: Input }],
        node: [{ type: Input }],
        side: [{ type: Input }],
        onAddArbitrationBreadcrumb: [{ type: Output }]
    };
    return AutokadComponent;
}());

/**
 * @fileoverview added by tsickle
 * Generated from: lib/search/SearchResourceClass.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
var search = axios.create({
    baseURL: '/nkbrelation/api/nodes'
});
//old but fast api , maybe after _relations=false new is the same on speed - TODO check speen and if neer is faster - move to it
/** @type {?} */
var company = axios.create({
    baseURL: '/nkbrelation/api/company'
});
var SearchResource = /** @class */ (function () {
    function SearchResource(metaService) {
        Object.assign(this, {
            cache: new Cache(
            // {keyFunction: (type, idField, id) => `/nkbrelation/api/nodes/${type}?${idField}.equals=${id}`}
            ),
            queue: [],
            queueType: null,
            queueResults: [],
            queueParams: {},
            pedning: false,
            debounce: debounce(this.searchByTypeAndIdsFromQueue, 500, { maxWait: 1000 }),
            metaService: metaService
        });
        metaService.init();
    }
    /**
     * @return {?}
     */
    SearchResource.prototype.updateQueueSearchByTypeAndIds = /**
     * @return {?}
     */
    function () {
        return this.queue.length < 10 ? this.debounce() : this.searchByTypeAndIdsFromQueue();
    };
    /**
     * @param {?} options
     * @return {?}
     */
    SearchResource.prototype.searchRequest = /**
     * @param {?} options
     * @return {?}
     */
    function (options) {
        return __awaiter(this, void 0, void 0, function () {
            var params, res;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        params = assignIn({}, options.filter, options.pageConfig, {
                            q: options.q,
                            exclude: '_relations'
                        });
                        return [4 /*yield*/, search.get("/" + options.nodeType, { params: params })];
                    case 1:
                        res = _a.sent();
                        return [2 /*return*/, get(res, 'data', [])];
                }
            });
        });
    };
    /**
     * @param {?} options
     * @return {?}
     */
    SearchResource.prototype.searchByTypeAndId = /**
     * @param {?} options
     * @return {?}
     */
    function (options) {
        return __awaiter(this, void 0, void 0, function () {
            var idField;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!!this.metaService.inited) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.metaService.init()];
                    case 1:
                        _a.sent();
                        _a.label = 2;
                    case 2:
                        idField = this.metaService.getIdFieldForType(options.type);
                        return [2 /*return*/, search.get(options.type + "?" + idField + ".equals=" + options.id + "&exclude=_relations", { params: options }).then((/**
                             * @param {?} res
                             * @return {?}
                             */
                            function (res) { return head(res.data.list); }))];
                }
            });
        });
    };
    /**
     * @param {?=} options
     * @return {?}
     */
    SearchResource.prototype.searchByTypeAndIdQueued = /**
     * @param {?=} options
     * @return {?}
     */
    function (options) {
        if (options === void 0) { options = { type: NODE_TYPE.COMPANY, id: null, params: {} }; }
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, new Promise((/**
                     * @param {?} resolve
                     * @return {?}
                     */
                    function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                        var doInterval, idField, inCache, isInqueue, res;
                        var _this = this;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    doInterval = (/**
                                     * @return {?}
                                     */
                                    function () {
                                        /** @type {?} */
                                        var intervalID = setInterval((/**
                                         * @return {?}
                                         */
                                        function () {
                                            if (!_this.pending) {
                                                /** @type {?} */
                                                var res = find(_this.queueResults, (/**
                                                 * @param {?} company
                                                 * @return {?}
                                                 */
                                                function (company) { return isEqual("" + company[idField], "" + options.id); }));
                                                if (res) {
                                                    clearInterval(intervalID);
                                                    resolve(res);
                                                }
                                            }
                                        }), 100);
                                    });
                                    idField = this.metaService.getIdFieldForType(options.type);
                                    inCache = this.cache.get("/nkbrelation/api/nodes/" + options.type + "?" + idField + ".equals=" + options.id);
                                    if (inCache) {
                                        resolve(inCache.head());
                                    }
                                    isInqueue = this.queue.includes(options.id);
                                    if (!!isInqueue) return [3 /*break*/, 4];
                                    if (!(this.queueType === options.type || this.queueType === null)) return [3 /*break*/, 1];
                                    this.queueType = options.type;
                                    this.queueParams = options.params;
                                    this.queue.push(options.id);
                                    this.updateQueueSearchByTypeAndIds();
                                    doInterval();
                                    return [3 /*break*/, 3];
                                case 1:
                                    if (!(this.queueType !== options.type)) return [3 /*break*/, 3];
                                    return [4 /*yield*/, this.cache.try("/nkbrelation/api/nodes/" + options.type + "?" + idField + ".equals=" + options.id).then((/**
                                         * @param {?} r
                                         * @return {?}
                                         */
                                        function (r) { return head(r); }))];
                                case 2:
                                    res = _a.sent();
                                    resolve(res);
                                    _a.label = 3;
                                case 3: return [3 /*break*/, 5];
                                case 4:
                                    doInterval();
                                    _a.label = 5;
                                case 5: return [2 /*return*/];
                            }
                        });
                    }); }))];
            });
        });
    };
    /**
     * @return {?}
     */
    SearchResource.prototype.searchByTypeAndIdsFromQueue = /**
     * @return {?}
     */
    function () {
        return __awaiter(this, void 0, void 0, function () {
            var type, idField;
            var _this = this;
            return __generator(this, function (_a) {
                this.pending = true;
                type = this.queueType;
                idField = this.metaService.getIdFieldForType(type);
                this.searchByTypeAndIds({ type: type, ids: this.queue, params: this.queueParams }).then((/**
                 * @param {?} res
                 * @return {?}
                 */
                function (res) {
                    _this.queueResults = _this.queueResults.concat(res);
                    res.forEach((/**
                     * @param {?} el
                     * @return {?}
                     */
                    function (el) {
                        _this.cache.set("/nkbrelation/api/nodes/" + type + "?" + idField + ".equals=" + el[idField], new CacheElement(Promise.resolve(el)));
                    }));
                    _this.pending = false;
                }));
                this.queueType = null;
                this.queue = [];
                return [2 /*return*/];
            });
        });
    };
    /**
     * @param {?=} options
     * @return {?}
     */
    SearchResource.prototype.searchByTypeAndIds = /**
     * @param {?=} options
     * @return {?}
     */
    function (options) {
        if (options === void 0) { options = { type: NODE_TYPE.COMPANY, ids: [], params: {} }; }
        return __awaiter(this, void 0, void 0, function () {
            var result, urlparams, idField, url;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.metaService.init()];
                    case 1:
                        _a.sent();
                        if (options.ids.length < 1) {
                            return [2 /*return*/, Promise.resolve({})];
                        }
                        urlparams = new URLSearchParams();
                        idField = this.metaService.getIdFieldForType(options.type);
                        options.ids.forEach((/**
                         * @param {?} id
                         * @return {?}
                         */
                        function (id) { return urlparams.append(idField + ".equals", id); }));
                        if (options.params)
                            Object.keys(options.params).forEach((/**
                             * @param {?} key
                             * @return {?}
                             */
                            function (key) { return urlparams.append(key, options.params[key]); }));
                        url = "/nkbrelation/api/nodes/" + options.type + "?" + urlparams.toString();
                        return [4 /*yield*/, this.cache.try(url).then((/**
                             * @param {?} res
                             * @return {?}
                             */
                            function (res) { return res.list(); }))];
                    case 2:
                        result = _a.sent();
                        return [2 /*return*/, result];
                }
            });
        });
    };
    /**
     * @param {?} params
     * @return {?}
     */
    SearchResource.prototype.searchCompany = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var res;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, company.get('/', { params: params })];
                    case 1:
                        res = _a.sent();
                        return [2 /*return*/, get(res, 'data', [])];
                }
            });
        });
    };
    return SearchResource;
}());

/**
 * @fileoverview added by tsickle
 * Generated from: lib/search/searchResource.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
var searchResource = new SearchResource(metaService);

/**
 * @fileoverview added by tsickle
 * Generated from: lib/search/searchService.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var SearchService = /** @class */ (function () {
    function SearchService(metaService$$1) {
        this.metaService = metaService$$1;
    }
    /**
     * @return {?}
     */
    SearchService.prototype.init = /**
     * @return {?}
     */
    function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.metaService.init()];
                    case 1:
                        _b.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        _a = _b.sent();
                        console.error("cannot init SearchService");
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * @return {?}
     */
    SearchService.prototype.getSearchableNodeTypes = /**
     * @return {?}
     */
    function () {
        /** @type {?} */
        var searchableNodeTypes = {};
        // TODO rewrite to make rsearchMeta, metaService one service
        /** @type {?} */
        var nodeTypes = rsearchMeta.nodeTypes;
        forEach(nodeTypes, (/**
         * @param {?} data
         * @param {?} nodeType
         * @return {?}
         */
        function (data, nodeType) {
            // TODO if nodeType is serachABLE
            if (data.search) {
                searchableNodeTypes[nodeType] = SearchType(nodeType, data);
            }
        }));
        return searchableNodeTypes;
    };
    /**
     * @param {?} searchType
     * @param {?} query
     * @return {?}
     */
    SearchService.prototype.searchByTypeAndText = /**
     * @param {?} searchType
     * @param {?} query
     * @return {?}
     */
    function (searchType, query) {
        return __awaiter(this, void 0, void 0, function () {
            var filter$$1, activeRegion, filterRegion;
            var _this = this;
            return __generator(this, function (_a) {
                filter$$1 = {};
                if (searchType && searchType.filter) {
                    activeRegion = find(searchType.filter.regions, { active: true });
                    if (activeRegion && activeRegion.regionCode) {
                        filterRegion = { 'region_code.equals': activeRegion.regionCode };
                        assignIn(filter$$1, filterRegion);
                    }
                }
                return [2 /*return*/, new Promise((/**
                     * @param {?} resolve
                     * @return {?}
                     */
                    function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                        var _a, err_1;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _b.trys.push([0, 2, , 3]);
                                    _a = searchType;
                                    return [4 /*yield*/, searchResource.searchRequest({
                                            q: query,
                                            nodeType: searchType.nodeType,
                                            pageConfig: searchType.pageConfig,
                                            filter: filter$$1,
                                        })];
                                case 1:
                                    _a.result = _b.sent();
                                    resolve(searchType);
                                    return [3 /*break*/, 3];
                                case 2:
                                    err_1 = _b.sent();
                                    console.log(err_1);
                                    resolve({ result: null });
                                    return [3 /*break*/, 3];
                                case 3: return [2 /*return*/];
                            }
                        });
                    }); }))];
            });
        });
    };
    /**
     * @param {?} options
     * @return {?}
     */
    SearchService.prototype.searchByTypeAndId = /**
     * @param {?} options
     * @return {?}
     */
    function (options) {
        return __awaiter(this, void 0, void 0, function () {
            var params, err_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        params = {
                            id: options.id,
                            type: options.searchtype || options.type,
                        };
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, searchResource.searchByTypeAndId(params)];
                    case 2: return [2 /*return*/, _a.sent()];
                    case 3:
                        err_2 = _a.sent();
                        console.error(err_2);
                        return [2 /*return*/, {}];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    return SearchService;
}());
/** @type {?} */
var searchService = new SearchService(metaService);
searchService.init();

/**
 * @fileoverview added by tsickle
 * Generated from: lib/ui/CompanyNameWithLink.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var CompanyNameWithLinkComponent = /** @class */ (function () {
    function CompanyNameWithLinkComponent() {
        this.addBreadcrumb = new EventEmitter();
        this.onCompanyClick = new EventEmitter();
        this.loading = false;
    }
    Object.defineProperty(CompanyNameWithLinkComponent.prototype, "title", {
        get: /**
         * @return {?}
         */
        function () {
            return this.node.state ? this.node.state._since + " : " + this.node.state.type : '';
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @return {?}
     */
    CompanyNameWithLinkComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        if (this.showParams === undefined) {
            this.showParams = {
                ogrn: true,
                inn: false,
                created: false,
            };
        }
    };
    /**
     * @param {?} company
     * @return {?}
     */
    CompanyNameWithLinkComponent.prototype.goToDetails = /**
     * @param {?} company
     * @return {?}
     */
    function (company) {
        return __awaiter(this, void 0, void 0, function () {
            var nodeType, node, getNodeTypeByOgrn, getNodeTypeByInn, getNode, params;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        getNodeTypeByOgrn = (/**
                         * @param {?} ogrn
                         * @return {?}
                         */
                        function (ogrn) {
                            return _this.isCompanyOgrn(ogrn) ? NODE_TYPE.COMPANY : _this.isIndividualOgrn(ogrn) ? NODE_TYPE.INDIVIDUAL_IDENTITY : false;
                        });
                        getNodeTypeByInn = (/**
                         * @param {?} inn
                         * @return {?}
                         */
                        function (inn) {
                            return _this.isCompanyInn(inn) ? NODE_TYPE.COMPANY : _this.isIndividualInn(inn) ? NODE_TYPE.INDIVIDUAL_IDENTITY : false;
                        });
                        getNode = (/**
                         * @param {?} nodeType
                         * @param {?} id
                         * @return {?}
                         */
                        function (nodeType, id) { return __awaiter(_this, void 0, void 0, function () {
                            var res;
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0: return [4 /*yield*/, searchService.searchByTypeAndText({ nodeType: nodeType }, id)];
                                    case 1:
                                        res = _a.sent();
                                        return [2 /*return*/, head(get(res, 'result.list', get(res, 'list')))];
                                }
                            });
                        }); });
                        company = this.node;
                        this.loading = true;
                        if (!(company.id && company.type)) return [3 /*break*/, 1];
                        this.addBreadcrumb.emit({ id: company.id });
                        params = {
                            searchtype: company.type,
                            text: company.name,
                            id: company.id,
                        };
                        this.onCompanyClick.emit({ node: company });
                        return [3 /*break*/, 6];
                    case 1:
                        if (!company.ogrn) return [3 /*break*/, 3];
                        nodeType = getNodeTypeByOgrn(company.ogrn);
                        return [4 /*yield*/, getNode(nodeType, company.ogrn)];
                    case 2:
                        node = _a.sent();
                        return [3 /*break*/, 5];
                    case 3:
                        if (!company.inn) return [3 /*break*/, 5];
                        nodeType = getNodeTypeByInn(company.inn);
                        return [4 /*yield*/, getNode(nodeType, company.inn)];
                    case 4:
                        node = _a.sent();
                        _a.label = 5;
                    case 5:
                        node = Node.nodeWrapper(node);
                        this.addBreadcrumb.emit({ id: node.id });
                        this.onCompanyClick.emit({ node: node });
                        _a.label = 6;
                    case 6: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * @param {?} node
     * @return {?}
     */
    CompanyNameWithLinkComponent.prototype.isNotActive = /**
     * @param {?} node
     * @return {?}
     */
    function (node) {
        return toNumber(get(node, 'state.code')) === 0;
    };
    /**
     * @param {?} textOgrn
     * @param {?} queryOgrn
     * @return {?}
     */
    CompanyNameWithLinkComponent.prototype.isHighlightSearch = /**
     * @param {?} textOgrn
     * @param {?} queryOgrn
     * @return {?}
     */
    function (textOgrn, queryOgrn) {
        return toString(textOgrn) === queryOgrn;
    };
    /**
     * @param {?} inn
     * @return {?}
     */
    CompanyNameWithLinkComponent.prototype.isIndividualInn = /**
     * @param {?} inn
     * @return {?}
     */
    function (inn) {
        return String(inn).length === 12;
    };
    /**
     * @param {?} ogrn
     * @return {?}
     */
    CompanyNameWithLinkComponent.prototype.isCompanyOgrn = /**
     * @param {?} ogrn
     * @return {?}
     */
    function (ogrn) {
        return String(ogrn).length === 13;
    };
    /**
     * @param {?} ogrn
     * @return {?}
     */
    CompanyNameWithLinkComponent.prototype.isIndividualOgrn = /**
     * @param {?} ogrn
     * @return {?}
     */
    function (ogrn) {
        return String(ogrn).length === 15;
    };
    /**
     * @param {?} inn
     * @return {?}
     */
    CompanyNameWithLinkComponent.prototype.isCompanyInn = /**
     * @param {?} inn
     * @return {?}
     */
    function (inn) {
        return String(inn).length === 10;
    };
    CompanyNameWithLinkComponent.decorators = [
        { type: Component, args: [{
                    selector: 'nkb-company-name-with-link',
                    template: "\n        <div *ngIf=\"node.shortname\">{{node.shortname}}</div>\n        <a\n                *ngIf=\"node.ogrn || node.inn\"\n                (click)=\"goToDetails(node)\"\n                [ngClass]=\"{danger:isNotActive(node),'autokad__company-name--highlight':isHighlightSearch(node.ogrn,ogrn)}\"\n                class=\"autokad__company-name\"\n                [title]=\"title\"\n        >\n            {{node.name}}\n        </a>\n        <span\n                *ngIf=\"!node.ogrn && !node.inn\"\n                [ngClass]=\"{danger:isNotActive(node),'autokad__company-name--highlight':isHighlightSearch(node.ogrn,ogrn)}\"\n                [title]=\"title\"\n        >\n        {{node.name}}\n    </span>\n\n        <span *ngIf=\"node.ogrn!==undefined && showParams.ogrn\">\n        <span *ngIf=\"isCompanyOgrn(node.ogrn)\">\u041E\u0413\u0420\u041D:</span>\n        <span *ngIf=\"isIndividualOgrn(node.ogrn)\">\u041E\u0413\u0420\u041D\u0418\u041F:</span>\n            {{node.ogrn}}\n    </span>\n        <span *ngIf=\"node.ogrnip!==undefined\">\n        <span>\u041E\u0413\u0420\u041D\u0418\u041F:</span>\n            {{node.ogrnip}}\n    </span>\n        <span *ngIf=\"node.inn && showParams.inn\">\n        <span>\u0418\u041D\u041D:</span>\n            {{node.inn}}\n    </span>\n\n        <span *ngIf=\"node && node.founded_dt && showParams.created\">\n          c      {{ node.founded_dt | date: 'dd.MM.yyyy'}}\n          <!-- c      {{ node.founded_dt | date}} -->\n            </span>\n        <div class=\"loader\" *ngIf=\"loading\"></div>\n\n    "
                }] }
    ];
    /** @nocollapse */
    CompanyNameWithLinkComponent.ctorParameters = function () { return []; };
    CompanyNameWithLinkComponent.propDecorators = {
        node: [{ type: Input }],
        type: [{ type: Input }],
        showParams: [{ type: Input }],
        ogrn: [{ type: Input }],
        addBreadcrumb: [{ type: Output }],
        onCompanyClick: [{ type: Output }]
    };
    return CompanyNameWithLinkComponent;
}());

/**
 * @fileoverview added by tsickle
 * Generated from: lib/pipes/externalUrl.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var ExternalUrlPipe = /** @class */ (function () {
    function ExternalUrlPipe() {
    }
    /**
     * @param {?} url
     * @return {?}
     */
    ExternalUrlPipe.prototype.transform = /**
     * @param {?} url
     * @return {?}
     */
    function (url) {
        return '/siteapp/url/external' + '?url=' + encodeURIComponent(url);
    };
    ExternalUrlPipe.decorators = [
        { type: Pipe, args: [{ name: 'externalUrl' },] }
    ];
    return ExternalUrlPipe;
}());

/**
 * @fileoverview added by tsickle
 * Generated from: lib/angular-nkb-lib.module.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var AngularNkbLibModule = /** @class */ (function () {
    function AngularNkbLibModule() {
    }
    AngularNkbLibModule.decorators = [
        { type: NgModule, args: [{
                    imports: [
                        CommonModule
                    ],
                    declarations: [
                        AngularNkbLibComponent,
                        AutokadComponent,
                        CompanyNameWithLinkComponent,
                        ExternalUrlPipe,
                    ],
                    providers: [
                        AutokadResource,
                        AutokadService,
                        DatePipe,
                    ],
                    exports: [
                        AngularNkbLibComponent,
                        AutokadComponent,
                        CompanyNameWithLinkComponent,
                        ExternalUrlPipe,
                    ]
                },] }
    ];
    return AngularNkbLibModule;
}());

/**
 * @fileoverview added by tsickle
 * Generated from: public-api.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * Generated from: nkb-angular-nkb-lib.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

export { Meta, metaService, rsearchMeta, Node, NODE_TYPE, LINK_TYPES, LINK_TYPE_GROUPS, AngularNkbLibService, AngularNkbLibComponent, AngularNkbLibModule, AutokadComponent, AutokadResource, AutokadService, ExternalUrlPipe, CompanyNameWithLinkComponent, CacheElement, Cache, searchService, searchResource, SearchResource };

//# sourceMappingURL=nkb-angular-nkb-lib.js.map