/**
 * @fileoverview added by tsickle
 * Generated from: lib/ui/CompanyNameWithLink.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/**
 * Created by radigost on 19.04.18.
 */
import * as _ from 'lodash';
import { searchService } from '../search/searchService';
import { NODE_TYPE } from '../domain/TYPES';
import { Node } from '../domain/Node';
import { Component, Input, Output, EventEmitter } from '@angular/core';
export class CompanyNameWithLinkComponent {
    constructor() {
        this.addBreadcrumb = new EventEmitter();
        this.onCompanyClick = new EventEmitter();
        this.loading = false;
    }
    /**
     * @return {?}
     */
    get title() {
        return this.node.state ? `${this.node.state._since} : ${this.node.state.type}` : '';
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        if (this.showParams === undefined) {
            this.showParams = {
                ogrn: true,
                inn: false,
                created: false,
            };
        }
    }
    /**
     * @param {?} company
     * @return {?}
     */
    goToDetails(company) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            let nodeType;
            /** @type {?} */
            let node;
            /** @type {?} */
            const getNodeTypeByOgrn = (/**
             * @param {?} ogrn
             * @return {?}
             */
            (ogrn) => {
                return this.isCompanyOgrn(ogrn) ? NODE_TYPE.COMPANY : this.isIndividualOgrn(ogrn) ? NODE_TYPE.INDIVIDUAL_IDENTITY : false;
            });
            /** @type {?} */
            const getNodeTypeByInn = (/**
             * @param {?} inn
             * @return {?}
             */
            (inn) => {
                return this.isCompanyInn(inn) ? NODE_TYPE.COMPANY : this.isIndividualInn(inn) ? NODE_TYPE.INDIVIDUAL_IDENTITY : false;
            });
            /** @type {?} */
            const getNode = (/**
             * @param {?} nodeType
             * @param {?} id
             * @return {?}
             */
            (nodeType, id) => tslib_1.__awaiter(this, void 0, void 0, function* () {
                /** @type {?} */
                const res = yield searchService.searchByTypeAndText({ nodeType }, id);
                return _.head(_.get(res, 'result.list', _.get(res, 'list')));
            }));
            company = this.node;
            this.loading = true;
            if (company.id && company.type) {
                this.addBreadcrumb.emit({ id: company.id });
                /** @type {?} */
                const params = {
                    searchtype: company.type,
                    text: company.name,
                    id: company.id,
                };
                this.onCompanyClick.emit({ node: company });
                // this.$state.go('main.search.details', params);
            }
            else {
                if (company.ogrn) {
                    nodeType = getNodeTypeByOgrn(company.ogrn);
                    node = yield getNode(nodeType, company.ogrn);
                }
                else if (company.inn) {
                    nodeType = getNodeTypeByInn(company.inn);
                    node = yield getNode(nodeType, company.inn);
                }
                node = Node.nodeWrapper(node);
                this.addBreadcrumb.emit({ id: node.id });
                this.onCompanyClick.emit({ node });
                // const params = {
                //     searchtype: node.type,
                //     text: node.name,
                //     id: node.id,
                // };
                // this.$state.go('main.search.details', params);
            }
        });
    }
    /**
     * @param {?} node
     * @return {?}
     */
    isNotActive(node) {
        return _.toNumber(_.get(node, 'state.code')) === 0;
    }
    /**
     * @param {?} textOgrn
     * @param {?} queryOgrn
     * @return {?}
     */
    isHighlightSearch(textOgrn, queryOgrn) {
        return _.toString(textOgrn) === queryOgrn;
    }
    /**
     * @param {?} inn
     * @return {?}
     */
    isIndividualInn(inn) {
        return String(inn).length === 12;
    }
    /**
     * @param {?} ogrn
     * @return {?}
     */
    isCompanyOgrn(ogrn) {
        return String(ogrn).length === 13;
    }
    /**
     * @param {?} ogrn
     * @return {?}
     */
    isIndividualOgrn(ogrn) {
        return String(ogrn).length === 15;
    }
    /**
     * @param {?} inn
     * @return {?}
     */
    isCompanyInn(inn) {
        return String(inn).length === 10;
    }
}
CompanyNameWithLinkComponent.decorators = [
    { type: Component, args: [{
                selector: 'nkb-company-name-with-link',
                template: `
        <div *ngIf="node.shortname">{{node.shortname}}</div>
        <a
                *ngIf="node.ogrn || node.inn"
                (click)="goToDetails(node)"
                [ngClass]="{danger:isNotActive(node),'autokad__company-name--highlight':isHighlightSearch(node.ogrn,ogrn)}"
                class="autokad__company-name"
                [title]="title"
        >
            {{node.name}}
        </a>
        <span
                *ngIf="!node.ogrn && !node.inn"
                [ngClass]="{danger:isNotActive(node),'autokad__company-name--highlight':isHighlightSearch(node.ogrn,ogrn)}"
                [title]="title"
        >
        {{node.name}}
    </span>

        <span *ngIf="node.ogrn!==undefined && showParams.ogrn">
        <span *ngIf="isCompanyOgrn(node.ogrn)">ОГРН:</span>
        <span *ngIf="isIndividualOgrn(node.ogrn)">ОГРНИП:</span>
            {{node.ogrn}}
    </span>
        <span *ngIf="node.ogrnip!==undefined">
        <span>ОГРНИП:</span>
            {{node.ogrnip}}
    </span>
        <span *ngIf="node.inn && showParams.inn">
        <span>ИНН:</span>
            {{node.inn}}
    </span>

        <span *ngIf="node && node.founded_dt && showParams.created">
          c      {{ node.founded_dt | date: 'dd.MM.yyyy'}}
          <!-- c      {{ node.founded_dt | date}} -->
            </span>
        <div class="loader" *ngIf="loading"></div>

    `
            }] }
];
/** @nocollapse */
CompanyNameWithLinkComponent.ctorParameters = () => [];
CompanyNameWithLinkComponent.propDecorators = {
    node: [{ type: Input }],
    type: [{ type: Input }],
    showParams: [{ type: Input }],
    ogrn: [{ type: Input }],
    addBreadcrumb: [{ type: Output }],
    onCompanyClick: [{ type: Output }]
};
if (false) {
    /** @type {?} */
    CompanyNameWithLinkComponent.prototype.node;
    /** @type {?} */
    CompanyNameWithLinkComponent.prototype.type;
    /** @type {?} */
    CompanyNameWithLinkComponent.prototype.showParams;
    /** @type {?} */
    CompanyNameWithLinkComponent.prototype.ogrn;
    /** @type {?} */
    CompanyNameWithLinkComponent.prototype.addBreadcrumb;
    /** @type {?} */
    CompanyNameWithLinkComponent.prototype.onCompanyClick;
    /** @type {?} */
    CompanyNameWithLinkComponent.prototype.loading;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQ29tcGFueU5hbWVXaXRoTGluay5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bua2IvYW5ndWxhci1ua2ItbGliLyIsInNvdXJjZXMiOlsibGliL3VpL0NvbXBhbnlOYW1lV2l0aExpbmsudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7O0FBR0EsT0FBTyxLQUFLLENBQUMsTUFBTSxRQUFRLENBQUM7QUFDNUIsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLHlCQUF5QixDQUFDO0FBQ3hELE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUM1QyxPQUFPLEVBQUUsSUFBSSxFQUFFLE1BQU0sZ0JBQWdCLENBQUE7QUFDckMsT0FBTyxFQUFFLFNBQVMsRUFBVSxLQUFLLEVBQUUsTUFBTSxFQUFFLFlBQVksRUFBRSxNQUFNLGVBQWUsQ0FBQztBQWdEL0UsTUFBTSxPQUFPLDRCQUE0QjtJQWFyQztRQUpVLGtCQUFhLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUNuQyxtQkFBYyxHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFDOUMsWUFBTyxHQUFHLEtBQUssQ0FBQztJQUloQixDQUFDOzs7O0lBZEQsSUFBSSxLQUFLO1FBQ0wsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLE1BQU0sSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztJQUN4RixDQUFDOzs7O0lBY0QsUUFBUTtRQUNKLElBQUksSUFBSSxDQUFDLFVBQVUsS0FBSyxTQUFTLEVBQUU7WUFDL0IsSUFBSSxDQUFDLFVBQVUsR0FBRztnQkFDZCxJQUFJLEVBQUUsSUFBSTtnQkFDVixHQUFHLEVBQUUsS0FBSztnQkFDVixPQUFPLEVBQUUsS0FBSzthQUNqQixDQUFDO1NBQ0w7SUFDTCxDQUFDOzs7OztJQUVLLFdBQVcsQ0FBQyxPQUFPOzs7Z0JBQ2pCLFFBQVE7O2dCQUFFLElBQUk7O2tCQUNaLGlCQUFpQjs7OztZQUFHLENBQUMsSUFBSSxFQUFFLEVBQUU7Z0JBQy9CLE9BQU8sSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztZQUM5SCxDQUFDLENBQUE7O2tCQUNLLGdCQUFnQjs7OztZQUFHLENBQUMsR0FBRyxFQUFFLEVBQUU7Z0JBQzdCLE9BQU8sSUFBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLG1CQUFtQixDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7WUFDMUgsQ0FBQyxDQUFBOztrQkFDSyxPQUFPOzs7OztZQUFHLENBQU8sUUFBUSxFQUFFLEVBQUUsRUFBRSxFQUFFOztzQkFDN0IsR0FBRyxHQUFHLE1BQU0sYUFBYSxDQUFDLG1CQUFtQixDQUFDLEVBQUUsUUFBUSxFQUFFLEVBQUUsRUFBRSxDQUFDO2dCQUNyRSxPQUFPLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxHQUFHLEVBQUUsYUFBYSxFQUFFLENBQUMsQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNqRSxDQUFDLENBQUEsQ0FBQTtZQUVELE9BQU8sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO1lBQ3BCLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO1lBRXBCLElBQUksT0FBTyxDQUFDLEVBQUUsSUFBSSxPQUFPLENBQUMsSUFBSSxFQUFFO2dCQUM1QixJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxFQUFFLEVBQUUsRUFBRSxPQUFPLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQzs7c0JBQ3RDLE1BQU0sR0FBRztvQkFDWCxVQUFVLEVBQUUsT0FBTyxDQUFDLElBQUk7b0JBQ3hCLElBQUksRUFBRSxPQUFPLENBQUMsSUFBSTtvQkFDbEIsRUFBRSxFQUFFLE9BQU8sQ0FBQyxFQUFFO2lCQUNqQjtnQkFDRCxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxFQUFFLElBQUksRUFBRSxPQUFPLEVBQUUsQ0FBQyxDQUFDO2dCQUM1QyxpREFBaUQ7YUFDcEQ7aUJBQU07Z0JBQ0gsSUFBSSxPQUFPLENBQUMsSUFBSSxFQUFFO29CQUNkLFFBQVEsR0FBRyxpQkFBaUIsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBQzNDLElBQUksR0FBRyxNQUFNLE9BQU8sQ0FBQyxRQUFRLEVBQUUsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO2lCQUNoRDtxQkFBTSxJQUFJLE9BQU8sQ0FBQyxHQUFHLEVBQUU7b0JBQ3BCLFFBQVEsR0FBRyxnQkFBZ0IsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUM7b0JBQ3pDLElBQUksR0FBRyxNQUFNLE9BQU8sQ0FBQyxRQUFRLEVBQUUsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDO2lCQUMvQztnQkFDRCxJQUFJLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFFOUIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsRUFBRSxFQUFFLEVBQUUsSUFBSSxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUM7Z0JBQ3pDLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQztnQkFDbkMsbUJBQW1CO2dCQUNuQiw2QkFBNkI7Z0JBQzdCLHVCQUF1QjtnQkFDdkIsbUJBQW1CO2dCQUNuQixLQUFLO2dCQUNMLGlEQUFpRDthQUNwRDtRQUVMLENBQUM7S0FBQTs7Ozs7SUFFRCxXQUFXLENBQUMsSUFBSTtRQUNaLE9BQU8sQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxZQUFZLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUN2RCxDQUFDOzs7Ozs7SUFFRCxpQkFBaUIsQ0FBQyxRQUFRLEVBQUUsU0FBUztRQUNqQyxPQUFPLENBQUMsQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLEtBQUssU0FBUyxDQUFDO0lBQzlDLENBQUM7Ozs7O0lBR0QsZUFBZSxDQUFDLEdBQUc7UUFDZixPQUFPLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxNQUFNLEtBQUssRUFBRSxDQUFDO0lBQ3JDLENBQUM7Ozs7O0lBRUQsYUFBYSxDQUFDLElBQUk7UUFDZCxPQUFPLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxNQUFNLEtBQUssRUFBRSxDQUFDO0lBQ3RDLENBQUM7Ozs7O0lBRUQsZ0JBQWdCLENBQUMsSUFBSTtRQUNqQixPQUFPLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxNQUFNLEtBQUssRUFBRSxDQUFDO0lBQ3RDLENBQUM7Ozs7O0lBRUQsWUFBWSxDQUFDLEdBQUc7UUFDWixPQUFPLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxNQUFNLEtBQUssRUFBRSxDQUFDO0lBQ3JDLENBQUM7OztZQTdJSixTQUFTLFNBQUM7Z0JBQ1AsUUFBUSxFQUFFLDRCQUE0QjtnQkFDdEMsUUFBUSxFQUFFOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7S0F1Q1Q7YUFDSjs7Ozs7bUJBT0ksS0FBSzttQkFDTCxLQUFLO3lCQUNMLEtBQUs7bUJBQ0wsS0FBSzs0QkFDTCxNQUFNOzZCQUNOLE1BQU07Ozs7SUFMUCw0Q0FBb0I7O0lBQ3BCLDRDQUFjOztJQUNkLGtEQUEwRTs7SUFDMUUsNENBQXNCOztJQUN0QixxREFBNkM7O0lBQzdDLHNEQUE4Qzs7SUFDOUMsK0NBQWdCIiwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBDcmVhdGVkIGJ5IHJhZGlnb3N0IG9uIDE5LjA0LjE4LlxuICovXG5pbXBvcnQgKiBhcyBfIGZyb20gJ2xvZGFzaCc7XG5pbXBvcnQgeyBzZWFyY2hTZXJ2aWNlIH0gZnJvbSAnLi4vc2VhcmNoL3NlYXJjaFNlcnZpY2UnO1xuaW1wb3J0IHsgTk9ERV9UWVBFIH0gZnJvbSAnLi4vZG9tYWluL1RZUEVTJztcbmltcG9ydCB7IE5vZGUgfSBmcm9tICcuLi9kb21haW4vTm9kZSdcbmltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbnB1dCwgT3V0cHV0LCBFdmVudEVtaXR0ZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuXG5cbkBDb21wb25lbnQoe1xuICAgIHNlbGVjdG9yOiAnbmtiLWNvbXBhbnktbmFtZS13aXRoLWxpbmsnLFxuICAgIHRlbXBsYXRlOiBgXG4gICAgICAgIDxkaXYgKm5nSWY9XCJub2RlLnNob3J0bmFtZVwiPnt7bm9kZS5zaG9ydG5hbWV9fTwvZGl2PlxuICAgICAgICA8YVxuICAgICAgICAgICAgICAgICpuZ0lmPVwibm9kZS5vZ3JuIHx8IG5vZGUuaW5uXCJcbiAgICAgICAgICAgICAgICAoY2xpY2spPVwiZ29Ub0RldGFpbHMobm9kZSlcIlxuICAgICAgICAgICAgICAgIFtuZ0NsYXNzXT1cIntkYW5nZXI6aXNOb3RBY3RpdmUobm9kZSksJ2F1dG9rYWRfX2NvbXBhbnktbmFtZS0taGlnaGxpZ2h0Jzppc0hpZ2hsaWdodFNlYXJjaChub2RlLm9ncm4sb2dybil9XCJcbiAgICAgICAgICAgICAgICBjbGFzcz1cImF1dG9rYWRfX2NvbXBhbnktbmFtZVwiXG4gICAgICAgICAgICAgICAgW3RpdGxlXT1cInRpdGxlXCJcbiAgICAgICAgPlxuICAgICAgICAgICAge3tub2RlLm5hbWV9fVxuICAgICAgICA8L2E+XG4gICAgICAgIDxzcGFuXG4gICAgICAgICAgICAgICAgKm5nSWY9XCIhbm9kZS5vZ3JuICYmICFub2RlLmlublwiXG4gICAgICAgICAgICAgICAgW25nQ2xhc3NdPVwie2Rhbmdlcjppc05vdEFjdGl2ZShub2RlKSwnYXV0b2thZF9fY29tcGFueS1uYW1lLS1oaWdobGlnaHQnOmlzSGlnaGxpZ2h0U2VhcmNoKG5vZGUub2dybixvZ3JuKX1cIlxuICAgICAgICAgICAgICAgIFt0aXRsZV09XCJ0aXRsZVwiXG4gICAgICAgID5cbiAgICAgICAge3tub2RlLm5hbWV9fVxuICAgIDwvc3Bhbj5cblxuICAgICAgICA8c3BhbiAqbmdJZj1cIm5vZGUub2dybiE9PXVuZGVmaW5lZCAmJiBzaG93UGFyYW1zLm9ncm5cIj5cbiAgICAgICAgPHNwYW4gKm5nSWY9XCJpc0NvbXBhbnlPZ3JuKG5vZGUub2dybilcIj7QntCT0KDQnTo8L3NwYW4+XG4gICAgICAgIDxzcGFuICpuZ0lmPVwiaXNJbmRpdmlkdWFsT2dybihub2RlLm9ncm4pXCI+0J7Qk9Cg0J3QmNCfOjwvc3Bhbj5cbiAgICAgICAgICAgIHt7bm9kZS5vZ3JufX1cbiAgICA8L3NwYW4+XG4gICAgICAgIDxzcGFuICpuZ0lmPVwibm9kZS5vZ3JuaXAhPT11bmRlZmluZWRcIj5cbiAgICAgICAgPHNwYW4+0J7Qk9Cg0J3QmNCfOjwvc3Bhbj5cbiAgICAgICAgICAgIHt7bm9kZS5vZ3JuaXB9fVxuICAgIDwvc3Bhbj5cbiAgICAgICAgPHNwYW4gKm5nSWY9XCJub2RlLmlubiAmJiBzaG93UGFyYW1zLmlublwiPlxuICAgICAgICA8c3Bhbj7QmNCd0J06PC9zcGFuPlxuICAgICAgICAgICAge3tub2RlLmlubn19XG4gICAgPC9zcGFuPlxuXG4gICAgICAgIDxzcGFuICpuZ0lmPVwibm9kZSAmJiBub2RlLmZvdW5kZWRfZHQgJiYgc2hvd1BhcmFtcy5jcmVhdGVkXCI+XG4gICAgICAgICAgYyAgICAgIHt7IG5vZGUuZm91bmRlZF9kdCB8IGRhdGU6ICdkZC5NTS55eXl5J319XG4gICAgICAgICAgPCEtLSBjICAgICAge3sgbm9kZS5mb3VuZGVkX2R0IHwgZGF0ZX19IC0tPlxuICAgICAgICAgICAgPC9zcGFuPlxuICAgICAgICA8ZGl2IGNsYXNzPVwibG9hZGVyXCIgKm5nSWY9XCJsb2FkaW5nXCI+PC9kaXY+XG5cbiAgICBgLFxufSlcblxuZXhwb3J0IGNsYXNzIENvbXBhbnlOYW1lV2l0aExpbmtDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICAgIGdldCB0aXRsZSgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMubm9kZS5zdGF0ZSA/IGAke3RoaXMubm9kZS5zdGF0ZS5fc2luY2V9IDogJHt0aGlzLm5vZGUuc3RhdGUudHlwZX1gIDogJyc7XG4gICAgfVxuXG4gICAgQElucHV0KCkgbm9kZTogTm9kZTtcbiAgICBASW5wdXQoKSB0eXBlO1xuICAgIEBJbnB1dCgpIHNob3dQYXJhbXM6IHsgaW5uPzogYm9vbGVhbiwgb2dybj86IGJvb2xlYW4sIGNyZWF0ZWQ/OiBib29sZWFuIH07XG4gICAgQElucHV0KCkgb2dybjogc3RyaW5nO1xuICAgIEBPdXRwdXQoKSBhZGRCcmVhZGNydW1iID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICAgIEBPdXRwdXQoKSBvbkNvbXBhbnlDbGljayA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiAgICBsb2FkaW5nID0gZmFsc2U7XG5cbiAgICBjb25zdHJ1Y3RvcigpIHtcblxuICAgIH1cblxuICAgIG5nT25Jbml0KCkge1xuICAgICAgICBpZiAodGhpcy5zaG93UGFyYW1zID09PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIHRoaXMuc2hvd1BhcmFtcyA9IHtcbiAgICAgICAgICAgICAgICBvZ3JuOiB0cnVlLFxuICAgICAgICAgICAgICAgIGlubjogZmFsc2UsXG4gICAgICAgICAgICAgICAgY3JlYXRlZDogZmFsc2UsXG4gICAgICAgICAgICB9O1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgYXN5bmMgZ29Ub0RldGFpbHMoY29tcGFueSkge1xuICAgICAgICBsZXQgbm9kZVR5cGUsIG5vZGU7XG4gICAgICAgIGNvbnN0IGdldE5vZGVUeXBlQnlPZ3JuID0gKG9ncm4pID0+IHtcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmlzQ29tcGFueU9ncm4ob2dybikgPyBOT0RFX1RZUEUuQ09NUEFOWSA6IHRoaXMuaXNJbmRpdmlkdWFsT2dybihvZ3JuKSA/IE5PREVfVFlQRS5JTkRJVklEVUFMX0lERU5USVRZIDogZmFsc2U7XG4gICAgICAgIH07XG4gICAgICAgIGNvbnN0IGdldE5vZGVUeXBlQnlJbm4gPSAoaW5uKSA9PiB7XG4gICAgICAgICAgICByZXR1cm4gdGhpcy5pc0NvbXBhbnlJbm4oaW5uKSA/IE5PREVfVFlQRS5DT01QQU5ZIDogdGhpcy5pc0luZGl2aWR1YWxJbm4oaW5uKSA/IE5PREVfVFlQRS5JTkRJVklEVUFMX0lERU5USVRZIDogZmFsc2U7XG4gICAgICAgIH07XG4gICAgICAgIGNvbnN0IGdldE5vZGUgPSBhc3luYyAobm9kZVR5cGUsIGlkKSA9PiB7XG4gICAgICAgICAgICBjb25zdCByZXMgPSBhd2FpdCBzZWFyY2hTZXJ2aWNlLnNlYXJjaEJ5VHlwZUFuZFRleHQoeyBub2RlVHlwZSB9LCBpZCk7XG4gICAgICAgICAgICByZXR1cm4gXy5oZWFkKF8uZ2V0KHJlcywgJ3Jlc3VsdC5saXN0JywgXy5nZXQocmVzLCAnbGlzdCcpKSk7XG4gICAgICAgIH07XG5cbiAgICAgICAgY29tcGFueSA9IHRoaXMubm9kZTtcbiAgICAgICAgdGhpcy5sb2FkaW5nID0gdHJ1ZTtcblxuICAgICAgICBpZiAoY29tcGFueS5pZCAmJiBjb21wYW55LnR5cGUpIHtcbiAgICAgICAgICAgIHRoaXMuYWRkQnJlYWRjcnVtYi5lbWl0KHsgaWQ6IGNvbXBhbnkuaWQgfSk7XG4gICAgICAgICAgICBjb25zdCBwYXJhbXMgPSB7XG4gICAgICAgICAgICAgICAgc2VhcmNodHlwZTogY29tcGFueS50eXBlLFxuICAgICAgICAgICAgICAgIHRleHQ6IGNvbXBhbnkubmFtZSxcbiAgICAgICAgICAgICAgICBpZDogY29tcGFueS5pZCxcbiAgICAgICAgICAgIH07XG4gICAgICAgICAgICB0aGlzLm9uQ29tcGFueUNsaWNrLmVtaXQoeyBub2RlOiBjb21wYW55IH0pO1xuICAgICAgICAgICAgLy8gdGhpcy4kc3RhdGUuZ28oJ21haW4uc2VhcmNoLmRldGFpbHMnLCBwYXJhbXMpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgaWYgKGNvbXBhbnkub2dybikge1xuICAgICAgICAgICAgICAgIG5vZGVUeXBlID0gZ2V0Tm9kZVR5cGVCeU9ncm4oY29tcGFueS5vZ3JuKTtcbiAgICAgICAgICAgICAgICBub2RlID0gYXdhaXQgZ2V0Tm9kZShub2RlVHlwZSwgY29tcGFueS5vZ3JuKTtcbiAgICAgICAgICAgIH0gZWxzZSBpZiAoY29tcGFueS5pbm4pIHtcbiAgICAgICAgICAgICAgICBub2RlVHlwZSA9IGdldE5vZGVUeXBlQnlJbm4oY29tcGFueS5pbm4pO1xuICAgICAgICAgICAgICAgIG5vZGUgPSBhd2FpdCBnZXROb2RlKG5vZGVUeXBlLCBjb21wYW55Lmlubik7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBub2RlID0gTm9kZS5ub2RlV3JhcHBlcihub2RlKTtcblxuICAgICAgICAgICAgdGhpcy5hZGRCcmVhZGNydW1iLmVtaXQoeyBpZDogbm9kZS5pZCB9KTtcbiAgICAgICAgICAgIHRoaXMub25Db21wYW55Q2xpY2suZW1pdCh7IG5vZGUgfSk7XG4gICAgICAgICAgICAvLyBjb25zdCBwYXJhbXMgPSB7XG4gICAgICAgICAgICAvLyAgICAgc2VhcmNodHlwZTogbm9kZS50eXBlLFxuICAgICAgICAgICAgLy8gICAgIHRleHQ6IG5vZGUubmFtZSxcbiAgICAgICAgICAgIC8vICAgICBpZDogbm9kZS5pZCxcbiAgICAgICAgICAgIC8vIH07XG4gICAgICAgICAgICAvLyB0aGlzLiRzdGF0ZS5nbygnbWFpbi5zZWFyY2guZGV0YWlscycsIHBhcmFtcyk7XG4gICAgICAgIH1cblxuICAgIH1cblxuICAgIGlzTm90QWN0aXZlKG5vZGUpIHtcbiAgICAgICAgcmV0dXJuIF8udG9OdW1iZXIoXy5nZXQobm9kZSwgJ3N0YXRlLmNvZGUnKSkgPT09IDA7XG4gICAgfVxuXG4gICAgaXNIaWdobGlnaHRTZWFyY2godGV4dE9ncm4sIHF1ZXJ5T2dybikge1xuICAgICAgICByZXR1cm4gXy50b1N0cmluZyh0ZXh0T2dybikgPT09IHF1ZXJ5T2dybjtcbiAgICB9XG5cblxuICAgIGlzSW5kaXZpZHVhbElubihpbm4pIHtcbiAgICAgICAgcmV0dXJuIFN0cmluZyhpbm4pLmxlbmd0aCA9PT0gMTI7XG4gICAgfVxuXG4gICAgaXNDb21wYW55T2dybihvZ3JuKSB7XG4gICAgICAgIHJldHVybiBTdHJpbmcob2dybikubGVuZ3RoID09PSAxMztcbiAgICB9XG5cbiAgICBpc0luZGl2aWR1YWxPZ3JuKG9ncm4pIHtcbiAgICAgICAgcmV0dXJuIFN0cmluZyhvZ3JuKS5sZW5ndGggPT09IDE1O1xuICAgIH1cblxuICAgIGlzQ29tcGFueUlubihpbm4pIHtcbiAgICAgICAgcmV0dXJuIFN0cmluZyhpbm4pLmxlbmd0aCA9PT0gMTA7XG4gICAgfVxufVxuIl19