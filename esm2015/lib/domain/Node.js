/**
 * @fileoverview added by tsickle
 * Generated from: lib/domain/Node.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * Created by radigost on 27.09.17.
 */
import * as _ from 'lodash';
import { metaService } from '../meta/metaService';
// import { nkbSearchMetaService } from '../meta/npRsearchMeta';
import { NODE_TYPE } from './TYPES';
/**
 * @record
 */
export function IEgrulProfile() { }
/**
 * @record
 */
function IGrn() { }
if (false) {
    /** @type {?} */
    IGrn.prototype.date;
    /** @type {?} */
    IGrn.prototype.grn;
}
/**
 * @record
 */
export function IDisqualifiedChef() { }
if (false) {
    /** @type {?} */
    IDisqualifiedChef.prototype.dateBegin;
    /** @type {?} */
    IDisqualifiedChef.prototype.dateEnd;
    /** @type {?} */
    IDisqualifiedChef.prototype.grn;
}
export class Node {
    /**
     * @param {?} node
     * @return {?}
     */
    static nodeWrapper(node) {
        if (node) {
            return node instanceof Node ? node : new Node(node);
        }
        else {
            return new Node(node);
        }
    }
    /**
     * @return {?}
     */
    get idField() {
        /** @type {?} */
        const node = this;
        return metaService.getIdFieldForType(node.type);
    }
    /**
     * @return {?}
     */
    get aggregates() {
        return _.get(this, '_aggregates', {});
    }
    /**
     * @return {?}
     */
    get name() {
        /** @type {?} */
        const node = this;
        return node._name;
    }
    /**
     * @return {?}
     */
    get fullName() {
        return _.get(this, 'namesort', '');
    }
    /**
     * @return {?}
     */
    get chiefName() {
        return _.get(this, 'chief_name', '');
    }
    /**
     * @return {?}
     */
    get type() {
        return _.get(this, '_type', '');
    }
    /**
     * @return {?}
     */
    get debts() {
        return _.get(this, '_debts', null);
    }
    /**
     * @return {?}
     */
    get licencies() {
        return _.map(_.get(this._egrulProfile, 'infoLicense'), (/**
         * @param {?} licence
         * @return {?}
         */
        (licence) => {
            return {
                terminationDate: _.get(_.head(_.get(licence, 'profile')), 'terminationDate', false),
                number: _.get(licence, 'data.number'),
                type: _.map(_.get(licence, 'data.type'), (/**
                 * @param {?} type
                 * @return {?}
                 */
                (type) => _.upperFirst(_.lowerCase(type)))),
                issued: _.upperFirst(_.lowerCase(_.get(licence, 'data.issued'))),
                dateBegin: _.get(licence, 'data.dateBegin'),
                date: _.get(licence, 'data.date'),
            };
        }));
    }
    /**
     * @return {?}
     */
    get relations() {
        return _.get(this, '_relations', []);
    }
    /**
     * @return {?}
     */
    get disqualification() {
        /** @type {?} */
        const disqualification = _.head(_.get(_.head(_.get(this._egrulProfile, 'infoPersonOfficial')), 'data.disqualification'));
        return disqualification;
    }
    /**
     * @param {?} newNode
     */
    constructor(newNode) {
        /** @type {?} */
        const node = Object.assign({}, newNode);
        // TODO add wrapper for this - need somehow add this info to node, when needed
        // if (nkbSearchMetaService) {
        metaService.buildNodeExtraMeta(node);
        // }
        node.id = metaService.getNodeId(node);
        node._name = metaService.getNodeName(node);
        delete node.name;
        Object.assign(this, node, { _metaService: metaService });
    }
    /**
     * @return {?}
     */
    getFullName() {
        return _.get(this, 'namesort', '');
    }
    /**
     * @return {?}
     */
    getChiefName() {
        return _.get(this, 'chief_name', '');
    }
    /**
     * @return {?}
     */
    getType() {
        return _.get(this, '_type', '');
    }
    /**
     * @return {?}
     */
    getCity() {
        if (this.type === NODE_TYPE.INDIVIDUAL_IDENTITY) {
            return _.capitalize(_.get(_.head(_.get(this, 'selfemployedInfo.infoAddress')), 'data.адресРФ.город.наимГород', ''));
        }
        if (this.type === NODE_TYPE.COMPANY) {
            return _.capitalize(_.get(_.head(_.get(this, '_egrulProfile.infoAddress')), 'data.address.regionName', ''));
        }
    }
    /**
     * @return {?}
     */
    getOgrnName() {
        return this.ogrn ? 'ОГРН' : this.selfemployedInfo ? 'ОГРНИП' : '';
    }
    /**
     * @return {?}
     */
    getEgrulName() {
        return this.ogrn ? 'ЕГРЮЛ' : this.selfemployedInfo ? 'ЕГРИП' : '';
    }
    /**
     * @return {?}
     */
    getOgrn() {
        return this.ogrn ? this.ogrn : this.selfemployedInfo ? this.selfemployedInfo.ogrnip : '';
    }
    /**
     * @param {?} selfemployedInfo
     * @return {?}
     */
    setSelfemployedInfo(selfemployedInfo) {
        if (selfemployedInfo !== undefined) {
            this.selfemployedInfo = selfemployedInfo;
        }
    }
    /**
     * @param {?} egrulProfile
     * @return {?}
     */
    setEgrulProfile(egrulProfile) {
        this._egrulProfile = egrulProfile;
    }
    /**
     * @param {?} balance
     * @return {?}
     */
    setBalance(balance) {
        this._balance = balance;
    }
}
if (false) {
    /** @type {?} */
    Node.prototype._id;
    /** @type {?} */
    Node.prototype._type;
    /** @type {?} */
    Node.prototype._aggregates;
    /** @type {?} */
    Node.prototype._name;
    /** @type {?} */
    Node.prototype.ogrn;
    /** @type {?} */
    Node.prototype._balance;
    /** @type {?} */
    Node.prototype._egrulProfile;
    /** @type {?} */
    Node.prototype.ogrnip;
    /** @type {?} */
    Node.prototype.selfemployedInfo;
    /** @type {?} */
    Node.prototype._debts;
    /** @type {?} */
    Node.prototype.inn;
    /** @type {?} */
    Node.prototype.id;
    /** @type {?} */
    Node.prototype.state;
    /** @type {?} */
    Node.prototype.balance;
    /** @type {?} */
    Node.prototype.chief_name;
    /** @type {?} */
    Node.prototype.chief_gender;
    /** @type {?} */
    Node.prototype.shortname;
    /** @type {?} */
    Node.prototype.founded_dt;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiTm9kZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bua2IvYW5ndWxhci1ua2ItbGliLyIsInNvdXJjZXMiOlsibGliL2RvbWFpbi9Ob2RlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBSUEsT0FBTyxLQUFLLENBQUMsTUFBTSxRQUFRLENBQUM7QUFDNUIsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLHFCQUFxQixDQUFDOztBQUVsRCxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sU0FBUyxDQUFDOzs7O0FBQ3BDLG1DQUVDOzs7O0FBR0QsbUJBR0M7OztJQUZHLG9CQUFhOztJQUNiLG1CQUFZOzs7OztBQUdoQix1Q0FJQzs7O0lBSEcsc0NBQWtCOztJQUNsQixvQ0FBZ0I7O0lBQ2hCLGdDQUFVOztBQUlkLE1BQU0sT0FBTyxJQUFJOzs7OztJQUNiLE1BQU0sQ0FBQyxXQUFXLENBQUMsSUFBSTtRQUNuQixJQUFJLElBQUksRUFBRTtZQUNOLE9BQU8sSUFBSSxZQUFZLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUN2RDthQUFNO1lBQ0gsT0FBTyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUN6QjtJQUVMLENBQUM7Ozs7SUFvQkQsSUFBSSxPQUFPOztjQUNELElBQUksR0FBRyxJQUFJO1FBQ2pCLE9BQU8sV0FBVyxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUNwRCxDQUFDOzs7O0lBRUQsSUFBSSxVQUFVO1FBQ1YsT0FBTyxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxhQUFhLEVBQUUsRUFBRSxDQUFDLENBQUM7SUFDMUMsQ0FBQzs7OztJQUVELElBQUksSUFBSTs7Y0FDRSxJQUFJLEdBQUcsSUFBSTtRQUNqQixPQUFPLElBQUksQ0FBQyxLQUFLLENBQUM7SUFDdEIsQ0FBQzs7OztJQUVELElBQUksUUFBUTtRQUNSLE9BQU8sQ0FBQyxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsVUFBVSxFQUFFLEVBQUUsQ0FBQyxDQUFDO0lBQ3ZDLENBQUM7Ozs7SUFFRCxJQUFJLFNBQVM7UUFDVCxPQUFPLENBQUMsQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLFlBQVksRUFBRSxFQUFFLENBQUMsQ0FBQztJQUN6QyxDQUFDOzs7O0lBRUQsSUFBSSxJQUFJO1FBQ0osT0FBTyxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxPQUFPLEVBQUUsRUFBRSxDQUFDLENBQUM7SUFDcEMsQ0FBQzs7OztJQUVELElBQUksS0FBSztRQUNMLE9BQU8sQ0FBQyxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsUUFBUSxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQ3ZDLENBQUM7Ozs7SUFFRCxJQUFJLFNBQVM7UUFDVCxPQUFPLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsYUFBYSxFQUFFLGFBQWEsQ0FBQzs7OztRQUFFLENBQUMsT0FBTyxFQUFFLEVBQUU7WUFDL0QsT0FBTztnQkFDSCxlQUFlLEVBQUUsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsT0FBTyxFQUFFLFNBQVMsQ0FBQyxDQUFDLEVBQUUsaUJBQWlCLEVBQUUsS0FBSyxDQUFDO2dCQUNuRixNQUFNLEVBQUUsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxPQUFPLEVBQUUsYUFBYSxDQUFDO2dCQUNyQyxJQUFJLEVBQUUsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLE9BQU8sRUFBRSxXQUFXLENBQUM7Ozs7Z0JBQUUsQ0FBQyxJQUFJLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFDO2dCQUNuRixNQUFNLEVBQUUsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsT0FBTyxFQUFFLGFBQWEsQ0FBQyxDQUFDLENBQUM7Z0JBQ2hFLFNBQVMsRUFBRSxDQUFDLENBQUMsR0FBRyxDQUFDLE9BQU8sRUFBRSxnQkFBZ0IsQ0FBQztnQkFDM0MsSUFBSSxFQUFFLENBQUMsQ0FBQyxHQUFHLENBQUMsT0FBTyxFQUFFLFdBQVcsQ0FBQzthQUNwQyxDQUFDO1FBQ04sQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDOzs7O0lBRUQsSUFBSSxTQUFTO1FBQ1QsT0FBTyxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxZQUFZLEVBQUUsRUFBRSxDQUFDLENBQUM7SUFDekMsQ0FBQzs7OztJQUVELElBQUksZ0JBQWdCOztjQUNWLGdCQUFnQixHQUFzQixDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUUsb0JBQW9CLENBQUMsQ0FBQyxFQUFFLHVCQUF1QixDQUFDLENBQUM7UUFDM0ksT0FBTyxnQkFBZ0IsQ0FBQztJQUM1QixDQUFDOzs7O0lBRUQsWUFBWSxPQUFPOztjQUNULElBQUksR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxPQUFPLENBQUM7UUFHdkMsOEVBQThFO1FBQzlFLDhCQUE4QjtRQUM5QixXQUFXLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDckMsSUFBSTtRQUVKLElBQUksQ0FBQyxFQUFFLEdBQUcsV0FBVyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN0QyxJQUFJLENBQUMsS0FBSyxHQUFHLFdBQVcsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDM0MsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDO1FBQ2pCLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLElBQUksRUFBRSxFQUFFLFlBQVksRUFBRSxXQUFXLEVBQUUsQ0FBQyxDQUFDO0lBQzdELENBQUM7Ozs7SUFFRCxXQUFXO1FBQ1AsT0FBTyxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxVQUFVLEVBQUUsRUFBRSxDQUFDLENBQUM7SUFDdkMsQ0FBQzs7OztJQUVELFlBQVk7UUFDUixPQUFPLENBQUMsQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLFlBQVksRUFBRSxFQUFFLENBQUMsQ0FBQztJQUN6QyxDQUFDOzs7O0lBRUQsT0FBTztRQUNILE9BQU8sQ0FBQyxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsT0FBTyxFQUFFLEVBQUUsQ0FBQyxDQUFDO0lBQ3BDLENBQUM7Ozs7SUFFRCxPQUFPO1FBQ0gsSUFBSSxJQUFJLENBQUMsSUFBSSxLQUFLLFNBQVMsQ0FBQyxtQkFBbUIsRUFBRTtZQUM3QyxPQUFPLENBQUMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLDhCQUE4QixDQUFDLENBQUMsRUFBRSw4QkFBOEIsRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDO1NBQ3ZIO1FBQ0QsSUFBSSxJQUFJLENBQUMsSUFBSSxLQUFLLFNBQVMsQ0FBQyxPQUFPLEVBQUU7WUFDakMsT0FBTyxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSwyQkFBMkIsQ0FBQyxDQUFDLEVBQUUseUJBQXlCLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQztTQUMvRztJQUNMLENBQUM7Ozs7SUFFRCxXQUFXO1FBQ1AsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7SUFDdEUsQ0FBQzs7OztJQUVELFlBQVk7UUFDUixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztJQUN0RSxDQUFDOzs7O0lBRUQsT0FBTztRQUNILE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7SUFDN0YsQ0FBQzs7Ozs7SUFFRCxtQkFBbUIsQ0FBQyxnQkFBZ0I7UUFDaEMsSUFBSSxnQkFBZ0IsS0FBSyxTQUFTLEVBQUU7WUFDaEMsSUFBSSxDQUFDLGdCQUFnQixHQUFHLGdCQUFnQixDQUFDO1NBQzVDO0lBQ0wsQ0FBQzs7Ozs7SUFFRCxlQUFlLENBQUMsWUFBWTtRQUN4QixJQUFJLENBQUMsYUFBYSxHQUFHLFlBQVksQ0FBQztJQUN0QyxDQUFDOzs7OztJQUVELFVBQVUsQ0FBQyxPQUFPO1FBQ2QsSUFBSSxDQUFDLFFBQVEsR0FBRyxPQUFPLENBQUM7SUFDNUIsQ0FBQztDQUNKOzs7SUFuSUcsbUJBQVk7O0lBQ1oscUJBQU87O0lBQ1AsMkJBQWE7O0lBQ2IscUJBQU87O0lBQ1Asb0JBQU07O0lBQ04sd0JBQVU7O0lBQ1YsNkJBQWU7O0lBQ2Ysc0JBQVE7O0lBQ1IsZ0NBQWtCOztJQUNsQixzQkFBUTs7SUFDUixtQkFBSzs7SUFDTCxrQkFBRzs7SUFDSCxxQkFBd0I7O0lBQ3hCLHVCQUFtQjs7SUFDbkIsMEJBQW9COztJQUNwQiw0QkFBcUI7O0lBQ3JCLHlCQUFrQjs7SUFDbEIsMEJBQVciLCJzb3VyY2VzQ29udGVudCI6WyIvKipcclxuICogQ3JlYXRlZCBieSByYWRpZ29zdCBvbiAyNy4wOS4xNy5cclxuICovXHJcblxyXG5pbXBvcnQgKiBhcyBfIGZyb20gJ2xvZGFzaCc7XHJcbmltcG9ydCB7IG1ldGFTZXJ2aWNlIH0gZnJvbSAnLi4vbWV0YS9tZXRhU2VydmljZSc7XHJcbi8vIGltcG9ydCB7IG5rYlNlYXJjaE1ldGFTZXJ2aWNlIH0gZnJvbSAnLi4vbWV0YS9ucFJzZWFyY2hNZXRhJztcclxuaW1wb3J0IHsgTk9ERV9UWVBFIH0gZnJvbSAnLi9UWVBFUyc7XHJcbmV4cG9ydCBpbnRlcmZhY2UgSUVncnVsUHJvZmlsZSB7XHJcblxyXG59XHJcblxyXG5cclxuaW50ZXJmYWNlIElHcm4ge1xyXG4gICAgZGF0ZTogc3RyaW5nO1xyXG4gICAgZ3JuOiBudW1iZXI7XHJcbn1cclxuXHJcbmV4cG9ydCBpbnRlcmZhY2UgSURpc3F1YWxpZmllZENoZWYge1xyXG4gICAgZGF0ZUJlZ2luOiBzdHJpbmc7XHJcbiAgICBkYXRlRW5kOiBzdHJpbmc7XHJcbiAgICBncm46IElHcm47XHJcbn1cclxuXHJcblxyXG5leHBvcnQgY2xhc3MgTm9kZSB7XHJcbiAgICBzdGF0aWMgbm9kZVdyYXBwZXIobm9kZSkge1xyXG4gICAgICAgIGlmIChub2RlKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBub2RlIGluc3RhbmNlb2YgTm9kZSA/IG5vZGUgOiBuZXcgTm9kZShub2RlKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICByZXR1cm4gbmV3IE5vZGUobm9kZSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgIH1cclxuXHJcbiAgICBfaWQ6IHN0cmluZztcclxuICAgIF90eXBlPztcclxuICAgIF9hZ2dyZWdhdGVzPztcclxuICAgIF9uYW1lPztcclxuICAgIG9ncm4/O1xyXG4gICAgX2JhbGFuY2U/O1xyXG4gICAgX2VncnVsUHJvZmlsZT87XHJcbiAgICBvZ3JuaXA/O1xyXG4gICAgc2VsZmVtcGxveWVkSW5mbz87XHJcbiAgICBfZGVidHM/O1xyXG4gICAgaW5uPztcclxuICAgIGlkO1xyXG4gICAgc3RhdGU6IHsgX3NpbmNlLCB0eXBlIH07XHJcbiAgICBiYWxhbmNlPzogbnVtYmVyW107XHJcbiAgICBjaGllZl9uYW1lPzogc3RyaW5nOyAvLyBvbmx5IGZvciBjb21wYW5pZXNcclxuICAgIGNoaWVmX2dlbmRlcjogc3RyaW5nOyAvLyBvbmx5IGZvciBjb21wYW5pZXNcclxuICAgIHNob3J0bmFtZTogc3RyaW5nO1xyXG4gICAgZm91bmRlZF9kdDtcclxuICAgIGdldCBpZEZpZWxkKCkge1xyXG4gICAgICAgIGNvbnN0IG5vZGUgPSB0aGlzO1xyXG4gICAgICAgIHJldHVybiBtZXRhU2VydmljZS5nZXRJZEZpZWxkRm9yVHlwZShub2RlLnR5cGUpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldCBhZ2dyZWdhdGVzKCkge1xyXG4gICAgICAgIHJldHVybiBfLmdldCh0aGlzLCAnX2FnZ3JlZ2F0ZXMnLCB7fSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IG5hbWUoKSB7XHJcbiAgICAgICAgY29uc3Qgbm9kZSA9IHRoaXM7XHJcbiAgICAgICAgcmV0dXJuIG5vZGUuX25hbWU7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IGZ1bGxOYW1lKCkge1xyXG4gICAgICAgIHJldHVybiBfLmdldCh0aGlzLCAnbmFtZXNvcnQnLCAnJyk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IGNoaWVmTmFtZSgpIHtcclxuICAgICAgICByZXR1cm4gXy5nZXQodGhpcywgJ2NoaWVmX25hbWUnLCAnJyk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IHR5cGUoKSB7XHJcbiAgICAgICAgcmV0dXJuIF8uZ2V0KHRoaXMsICdfdHlwZScsICcnKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXQgZGVidHMoKSB7XHJcbiAgICAgICAgcmV0dXJuIF8uZ2V0KHRoaXMsICdfZGVidHMnLCBudWxsKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXQgbGljZW5jaWVzKCkge1xyXG4gICAgICAgIHJldHVybiBfLm1hcChfLmdldCh0aGlzLl9lZ3J1bFByb2ZpbGUsICdpbmZvTGljZW5zZScpLCAobGljZW5jZSkgPT4ge1xyXG4gICAgICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICAgICAgdGVybWluYXRpb25EYXRlOiBfLmdldChfLmhlYWQoXy5nZXQobGljZW5jZSwgJ3Byb2ZpbGUnKSksICd0ZXJtaW5hdGlvbkRhdGUnLCBmYWxzZSksXHJcbiAgICAgICAgICAgICAgICBudW1iZXI6IF8uZ2V0KGxpY2VuY2UsICdkYXRhLm51bWJlcicpLFxyXG4gICAgICAgICAgICAgICAgdHlwZTogXy5tYXAoXy5nZXQobGljZW5jZSwgJ2RhdGEudHlwZScpLCAodHlwZSkgPT4gXy51cHBlckZpcnN0KF8ubG93ZXJDYXNlKHR5cGUpKSksXHJcbiAgICAgICAgICAgICAgICBpc3N1ZWQ6IF8udXBwZXJGaXJzdChfLmxvd2VyQ2FzZShfLmdldChsaWNlbmNlLCAnZGF0YS5pc3N1ZWQnKSkpLFxyXG4gICAgICAgICAgICAgICAgZGF0ZUJlZ2luOiBfLmdldChsaWNlbmNlLCAnZGF0YS5kYXRlQmVnaW4nKSxcclxuICAgICAgICAgICAgICAgIGRhdGU6IF8uZ2V0KGxpY2VuY2UsICdkYXRhLmRhdGUnKSxcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBnZXQgcmVsYXRpb25zKCkge1xyXG4gICAgICAgIHJldHVybiBfLmdldCh0aGlzLCAnX3JlbGF0aW9ucycsIFtdKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXQgZGlzcXVhbGlmaWNhdGlvbigpOiBJRGlzcXVhbGlmaWVkQ2hlZiB8IHVuZGVmaW5lZCB7XHJcbiAgICAgICAgY29uc3QgZGlzcXVhbGlmaWNhdGlvbjogSURpc3F1YWxpZmllZENoZWYgPSBfLmhlYWQoXy5nZXQoXy5oZWFkKF8uZ2V0KHRoaXMuX2VncnVsUHJvZmlsZSwgJ2luZm9QZXJzb25PZmZpY2lhbCcpKSwgJ2RhdGEuZGlzcXVhbGlmaWNhdGlvbicpKTtcclxuICAgICAgICByZXR1cm4gZGlzcXVhbGlmaWNhdGlvbjtcclxuICAgIH1cclxuXHJcbiAgICBjb25zdHJ1Y3RvcihuZXdOb2RlKSB7XHJcbiAgICAgICAgY29uc3Qgbm9kZSA9IE9iamVjdC5hc3NpZ24oe30sIG5ld05vZGUpO1xyXG5cclxuXHJcbiAgICAgICAgLy8gVE9ETyBhZGQgd3JhcHBlciBmb3IgdGhpcyAtIG5lZWQgc29tZWhvdyBhZGQgdGhpcyBpbmZvIHRvIG5vZGUsIHdoZW4gbmVlZGVkXHJcbiAgICAgICAgLy8gaWYgKG5rYlNlYXJjaE1ldGFTZXJ2aWNlKSB7XHJcbiAgICAgICAgbWV0YVNlcnZpY2UuYnVpbGROb2RlRXh0cmFNZXRhKG5vZGUpO1xyXG4gICAgICAgIC8vIH1cclxuXHJcbiAgICAgICAgbm9kZS5pZCA9IG1ldGFTZXJ2aWNlLmdldE5vZGVJZChub2RlKTtcclxuICAgICAgICBub2RlLl9uYW1lID0gbWV0YVNlcnZpY2UuZ2V0Tm9kZU5hbWUobm9kZSk7XHJcbiAgICAgICAgZGVsZXRlIG5vZGUubmFtZTtcclxuICAgICAgICBPYmplY3QuYXNzaWduKHRoaXMsIG5vZGUsIHsgX21ldGFTZXJ2aWNlOiBtZXRhU2VydmljZSB9KTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRGdWxsTmFtZSgpIHtcclxuICAgICAgICByZXR1cm4gXy5nZXQodGhpcywgJ25hbWVzb3J0JywgJycpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldENoaWVmTmFtZSgpIHtcclxuICAgICAgICByZXR1cm4gXy5nZXQodGhpcywgJ2NoaWVmX25hbWUnLCAnJyk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0VHlwZSgpIHtcclxuICAgICAgICByZXR1cm4gXy5nZXQodGhpcywgJ190eXBlJywgJycpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldENpdHkoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMudHlwZSA9PT0gTk9ERV9UWVBFLklORElWSURVQUxfSURFTlRJVFkpIHtcclxuICAgICAgICAgICAgcmV0dXJuIF8uY2FwaXRhbGl6ZShfLmdldChfLmhlYWQoXy5nZXQodGhpcywgJ3NlbGZlbXBsb3llZEluZm8uaW5mb0FkZHJlc3MnKSksICdkYXRhLtCw0LTRgNC10YHQoNCkLtCz0L7RgNC+0LQu0L3QsNC40LzQk9C+0YDQvtC0JywgJycpKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHRoaXMudHlwZSA9PT0gTk9ERV9UWVBFLkNPTVBBTlkpIHtcclxuICAgICAgICAgICAgcmV0dXJuIF8uY2FwaXRhbGl6ZShfLmdldChfLmhlYWQoXy5nZXQodGhpcywgJ19lZ3J1bFByb2ZpbGUuaW5mb0FkZHJlc3MnKSksICdkYXRhLmFkZHJlc3MucmVnaW9uTmFtZScsICcnKSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGdldE9ncm5OYW1lKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLm9ncm4gPyAn0J7Qk9Cg0J0nIDogdGhpcy5zZWxmZW1wbG95ZWRJbmZvID8gJ9Ce0JPQoNCd0JjQnycgOiAnJztcclxuICAgIH1cclxuXHJcbiAgICBnZXRFZ3J1bE5hbWUoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMub2dybiA/ICfQldCT0KDQrtCbJyA6IHRoaXMuc2VsZmVtcGxveWVkSW5mbyA/ICfQldCT0KDQmNCfJyA6ICcnO1xyXG4gICAgfVxyXG5cclxuICAgIGdldE9ncm4oKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMub2dybiA/IHRoaXMub2dybiA6IHRoaXMuc2VsZmVtcGxveWVkSW5mbyA/IHRoaXMuc2VsZmVtcGxveWVkSW5mby5vZ3JuaXAgOiAnJztcclxuICAgIH1cclxuXHJcbiAgICBzZXRTZWxmZW1wbG95ZWRJbmZvKHNlbGZlbXBsb3llZEluZm8pIHtcclxuICAgICAgICBpZiAoc2VsZmVtcGxveWVkSW5mbyAhPT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgICAgICAgIHRoaXMuc2VsZmVtcGxveWVkSW5mbyA9IHNlbGZlbXBsb3llZEluZm87XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHNldEVncnVsUHJvZmlsZShlZ3J1bFByb2ZpbGUpIHtcclxuICAgICAgICB0aGlzLl9lZ3J1bFByb2ZpbGUgPSBlZ3J1bFByb2ZpbGU7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0QmFsYW5jZShiYWxhbmNlKSB7XHJcbiAgICAgICAgdGhpcy5fYmFsYW5jZSA9IGJhbGFuY2U7XHJcbiAgICB9XHJcbn1cclxuIl19