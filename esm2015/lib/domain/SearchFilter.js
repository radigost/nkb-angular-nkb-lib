/**
 * @fileoverview added by tsickle
 * Generated from: lib/domain/SearchFilter.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as _ from 'lodash';
//Класс Фильтров для каждого из типов поиска
/**
 * @return {?}
 */
export default function Filter() {
    return {
        regions: [],
        inns: [],
        makeActive
    };
    /**
     * @param {?} elementToMake
     * @return {?}
     */
    function makeActive(elementToMake) {
        _.forEach(this.regions, (/**
         * @param {?} region
         * @return {?}
         */
        function (region) {
            region.active = (elementToMake && region.regionCode === elementToMake.regionCode);
        }));
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiU2VhcmNoRmlsdGVyLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQG5rYi9hbmd1bGFyLW5rYi1saWIvIiwic291cmNlcyI6WyJsaWIvZG9tYWluL1NlYXJjaEZpbHRlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sS0FBSyxDQUFDLE1BQU0sUUFBUSxDQUFDOzs7OztBQUU1QixNQUFNLENBQUMsT0FBTyxVQUFVLE1BQU07SUFDMUIsT0FBTztRQUNILE9BQU8sRUFBRSxFQUFFO1FBQ1gsSUFBSSxFQUFFLEVBQUU7UUFDUixVQUFVO0tBQ2IsQ0FBQzs7Ozs7SUFFRixTQUFTLFVBQVUsQ0FBQyxhQUFhO1FBQzdCLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLE9BQU87Ozs7UUFBRSxVQUFVLE1BQU07WUFDcEMsTUFBTSxDQUFDLE1BQU0sR0FBRyxDQUFDLGFBQWEsSUFBSSxNQUFNLENBQUMsVUFBVSxLQUFLLGFBQWEsQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUN0RixDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7QUFDTCxDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0ICogYXMgXyBmcm9tICdsb2Rhc2gnO1xyXG4vL9Ca0LvQsNGB0YEg0KTQuNC70YzRgtGA0L7QsiDQtNC70Y8g0LrQsNC20LTQvtCz0L4g0LjQtyDRgtC40L/QvtCyINC/0L7QuNGB0LrQsFxyXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBGaWx0ZXIoKSB7XHJcbiAgICByZXR1cm4ge1xyXG4gICAgICAgIHJlZ2lvbnM6IFtdLFxyXG4gICAgICAgIGlubnM6IFtdLFxyXG4gICAgICAgIG1ha2VBY3RpdmVcclxuICAgIH07XHJcblxyXG4gICAgZnVuY3Rpb24gbWFrZUFjdGl2ZShlbGVtZW50VG9NYWtlKSB7XHJcbiAgICAgICAgXy5mb3JFYWNoKHRoaXMucmVnaW9ucywgZnVuY3Rpb24gKHJlZ2lvbikge1xyXG4gICAgICAgICAgICByZWdpb24uYWN0aXZlID0gKGVsZW1lbnRUb01ha2UgJiYgcmVnaW9uLnJlZ2lvbkNvZGUgPT09IGVsZW1lbnRUb01ha2UucmVnaW9uQ29kZSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcbn1cclxuIl19