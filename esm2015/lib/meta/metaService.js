/**
 * @fileoverview added by tsickle
 * Generated from: lib/meta/metaService.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/**
 * Created by radigost on 07.09.17.
 */
import axios from 'axios';
//todo импортировать только то что нужно
import * as _ from 'lodash';
import { NODE_TYPE } from '../domain/TYPES';
import { rsearchMeta } from './rsearchMeta';
/**
 * @record
 */
export function NodeType() { }
if (false) {
    /** @type {?} */
    NodeType.prototype.name;
    /** @type {?} */
    NodeType.prototype.sortField;
    /** @type {?} */
    NodeType.prototype.sortAscending;
    /** @type {?} */
    NodeType.prototype.idField;
    /** @type {?} */
    NodeType.prototype.nameField;
    /** @type {?} */
    NodeType.prototype.properties;
}
//todo покрыть тестами
export class Meta {
    constructor() {
        this.SEARCHABLE_TYPES = [NODE_TYPE.COMPANY, NODE_TYPE.INDIVIDUAL, NODE_TYPE.ADDRESS, NODE_TYPE.PHONE];
        this.state = {
            inited: false,
            pending: false,
            error: false,
        };
        // @Deprecated
        this.nodeTypesMeta = {};
        this.relationTypesMeta = {};
    }
    /**
     * @return {?}
     */
    get inited() {
        return this.state.inited;
    }
    /**
     * @return {?}
     */
    init() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            if (!this.state.inited) {
                if (!this.state.pending) {
                    this.state.pending = true;
                    try {
                        this.relationTypes = yield axios.get('/nkbrelation/api/meta/relation/types').then((/**
                         * @param {?} res
                         * @return {?}
                         */
                        res => res.data));
                        this.nodeTypes = yield axios.get('/nkbrelation/api/meta/node/types').then((/**
                         * @param {?} res
                         * @return {?}
                         */
                        res => res.data));
                        // TODO legacy - refactor this - a lot of additional not needed work
                        _.forEach(this.nodeTypes, (/**
                         * @param {?} nodeType
                         * @return {?}
                         */
                        (nodeType) => {
                            this.nodeTypesMeta[nodeType.name] = nodeType;
                        }));
                        _.forEach(rsearchMeta.nodeTypes, (/**
                         * @param {?} nodeType
                         * @param {?} nodeTypeName
                         * @return {?}
                         */
                        (nodeType, nodeTypeName) => {
                            this.nodeTypesMeta[nodeTypeName] = _.extend({}, this.nodeTypesMeta[nodeTypeName], nodeType);
                        }));
                        // relationTypesMeta
                        _.forEach(this.relationTypes, (/**
                         * @param {?} relationType
                         * @return {?}
                         */
                        (relationType) => {
                            this.relationTypesMeta[relationType.name] = relationType;
                        }));
                        _.forEach(rsearchMeta.relationTypes, (/**
                         * @param {?} relationType
                         * @param {?} relationTypeName
                         * @return {?}
                         */
                        (relationType, relationTypeName) => {
                            this.relationTypesMeta[relationTypeName] = _.extend({}, this.relationTypesMeta[relationTypeName], relationType);
                        }));
                        // end TODO legacy - refactor this - a lot of additional not needed work
                        this.state.inited = true;
                    }
                    catch (err) {
                        console.error(err);
                        this.state.error = err;
                    }
                    finally {
                        console.log("meta service inited");
                        this.state.pending = false;
                    }
                }
                else {
                    return new Promise((/**
                     * @param {?} resolve
                     * @return {?}
                     */
                    (resolve) => {
                        /** @type {?} */
                        const interval = setInterval((/**
                         * @return {?}
                         */
                        () => {
                            if (this.state.inited) {
                                resolve();
                                clearInterval(interval);
                            }
                        }), 1000);
                    }));
                }
            }
        });
    }
    /**
     * @param {?} node
     * @return {?}
     */
    getNodeName(node) {
        /** @type {?} */
        const nameField = _.get(_.find(this.nodeTypes, { name: _.get(node, '_type') }), 'nameField');
        return _.get(node, [nameField], _.get(node, '_name', ''));
    }
    /**
     * @param {?} node
     * @return {?}
     */
    getNodeId(node) {
        if (this.inited) {
            /** @type {?} */
            const idField = _.get(_.find(this.nodeTypes, { name: _.get(node, '_type') }), 'idField');
            return idField === 'id' ? _.get(node, '_id') : _.get(node, idField, _.get(node, '_id'));
        }
    }
    /**
     * @param {?} type
     * @return {?}
     */
    getIdFieldForType(type) {
        return _.get(_.find(this.nodeTypes, { name: type }), 'idField', '_id');
    }
    /**
     * @param {?} node
     * @return {?}
     */
    buildRelationDataByNodeInfo(node) {
        /** @type {?} */
        const relationCountMap = {};
        /** @type {?} */
        let relationCounts = [];
        /** @type {?} */
        let groups = {};
        _.each(['in', 'out'], (/**
         * @param {?} infoDirection
         * @return {?}
         */
        (infoDirection) => {
            _.each(_.get(node, ['_info', infoDirection]), (/**
             * @param {?} info
             * @param {?} relationType
             * @return {?}
             */
            (info, relationType) => {
                /** @type {?} */
                const historyRelationCounts = this.getHistoryRelationCountsByInfo(info);
                /** @type {?} */
                const relationCount = historyRelationCounts.all;
                /** @type {?} */
                const direction = this.getDirection(infoDirection);
                /** @type {?} */
                const relationTypeMeta = this.getRelationTypeMeta(relationType, direction);
                /** @type {?} */
                const groupKey = relationTypeMeta.group;
                /** @type {?} */
                const relationGroupMeta = this.getRelationGroupMeta(groupKey);
                /** @type {?} */
                const mergedTypeInfo = this.getMergedTypeInfoByRelationType(relationType, direction);
                /** @type {?} */
                const mergedTypeKey = mergedTypeInfo.mergedType && this.buildNodeRelationKey(direction, mergedTypeInfo.mergedType);
                /** @type {?} */
                let mergedTypeCountData = relationCountMap[mergedTypeKey];
                /** @type {?} */
                const relationKey = this.buildNodeRelationKey(direction, relationType);
                /** @type {?} */
                const relationPluralKey = this.buildNodeRelationPluralKey(direction, relationType);
                /** @type {?} */
                let mergedTypeRelationCount = 0;
                /** @type {?} */
                const group = groups[groupKey] = groups[groupKey] || {
                    key: groupKey,
                    order: relationGroupMeta.order,
                    relationCounts: {},
                };
                // TODO Объединить код с mergedTypeCountData
                /** @type {?} */
                const countData = {
                    key: relationKey,
                    order: relationTypeMeta.order,
                    top: relationTypeMeta.top,
                    history: relationTypeMeta.history,
                    relationType,
                    direction,
                    relationCount,
                    historyRelationCounts,
                    pluralKey: relationPluralKey,
                };
                /** @type {?} */
                const mergedTypeHistoryRelationCounts = {
                    actual: 0,
                    outdated: 0,
                    all: 0,
                };
                _.forEach(!mergedTypeCountData && mergedTypeInfo.relationTypes, (/**
                 * @param {?} t
                 * @return {?}
                 */
                (t) => {
                    /** @type {?} */
                    const info = _.get(node, ['_info', infoDirection, t]);
                    /** @type {?} */
                    const counts = info ? this.getHistoryRelationCountsByInfo(info) : null;
                    if (!counts) {
                        return;
                    }
                    mergedTypeHistoryRelationCounts.actual += counts.actual;
                    mergedTypeHistoryRelationCounts.outdated += counts.outdated;
                    mergedTypeHistoryRelationCounts.all += counts.all;
                }));
                mergedTypeRelationCount = mergedTypeHistoryRelationCounts.all;
                if (!mergedTypeCountData && mergedTypeRelationCount > relationCount) {
                    /** @type {?} */
                    const mergedType = mergedTypeInfo.mergedType;
                    /** @type {?} */
                    const mergedTypeMeta = this.getRelationTypeMeta(mergedType, direction);
                    // TODO Объединить код с countData
                    mergedTypeCountData = {
                        key: mergedTypeKey,
                        order: mergedTypeMeta.order,
                        top: mergedTypeMeta.top,
                        history: mergedTypeMeta.history,
                        relationType: mergedType,
                        direction,
                        relationCount: mergedTypeRelationCount,
                        historyRelationCounts: mergedTypeHistoryRelationCounts,
                        pluralKey: this.buildNodeRelationPluralKey(direction, mergedType),
                    };
                    group.relationCounts[mergedTypeKey] = mergedTypeCountData;
                    relationCountMap[mergedTypeKey] = mergedTypeCountData;
                }
                if (!relationCountMap[mergedTypeKey]) {
                    group.relationCounts[relationKey] = countData;
                }
                relationCountMap[relationKey] = countData;
            }));
        }));
        groups = _.sortBy(groups, 'order');
        _.forEach(groups, (/**
         * @param {?} group
         * @return {?}
         */
        (group) => {
            group.relationCounts = _.sortBy(group.relationCounts, 'order');
            relationCounts = relationCounts.concat(group.relationCounts);
        }));
        return {
            relationCountMap,
            relationCounts,
            groups,
        };
    }
    /**
     * @param {?} info
     * @return {?}
     */
    getHistoryRelationCountsByInfo(info) {
        info = info || {};
        return {
            actual: info.actual || 0,
            all: (info.actual || 0) + (info.outdated || 0),
            outdated: info.outdated || 0,
        };
    }
    /**
     * @param {?} infoDirection
     * @return {?}
     */
    getDirection(infoDirection) {
        return infoDirection === 'in' ? 'parents' : 'children';
    }
    /**
     * @param {?} relationType
     * @param {?} direction
     * @return {?}
     */
    getRelationTypeMeta(relationType, direction) {
        return _.get(rsearchMeta.relationTypes, [relationType, direction]);
    }
    /**
     * @param {?} relationGroup
     * @return {?}
     */
    getRelationGroupMeta(relationGroup) {
        return rsearchMeta.relationGroups[relationGroup];
    }
    /**
     * @param {?} relationType
     * @param {?} direction
     * @return {?}
     */
    getMergedTypeInfoByRelationType(relationType, direction) {
        /** @type {?} */
        const info = {
            mergedType: null,
            relationTypes: null,
        };
        _.forEach(rsearchMeta.mergedRelationTypes, (/**
         * @param {?} byDirections
         * @param {?} mergedType
         * @return {?}
         */
        (byDirections, mergedType) => {
            /** @type {?} */
            const relationTypes = byDirections[direction];
            if (_.includes(relationTypes, relationType)) {
                info.mergedType = mergedType;
                info.relationTypes = relationTypes;
                return false;
            }
        }));
        return info;
    }
    /**
     * @param {?} direction
     * @param {?} relationType
     * @return {?}
     */
    buildNodeRelationKey(direction, relationType) {
        return direction + '::' + relationType;
    }
    /**
     * @param {?} direction
     * @param {?} relationType
     * @return {?}
     */
    buildNodeRelationPluralKey(direction, relationType) {
        return 'NG_PLURALIZE::RELATION_' + relationType + '-' + direction;
    }
    /**
     * @param {?} node
     * @return {?}
     */
    buildNodeExtraMeta(node) {
        if (!node) {
            return;
        }
        // uid
        this.buildNodeUID(node);
        //
        node.__relationData = metaService.buildRelationDataByNodeInfo(node);
        //
        node.__idField = _.get(this.nodeTypesMeta[node._type], 'idField');
        if (node._type === 'COMPANY' || node._type === 'EGRUL') { // компания
            this.buildCompanyState(node);
            node.__isFavoritesSupported = true;
        }
        else if (node._type === 'INDIVIDUAL') { // физик
            node.__isFavoritesSupported = true;
        }
        else if (node._type === 'PURCHASE') { // закупка
            node.currency = node.currency || 'RUB';
            node.__lotMap = _.keyBy(node.lots, 'lot');
            this.resolvePurchaseHref(node);
        }
    }
    /**
     * @param {?} node
     * @return {?}
     */
    buildNodeUID(node) {
        node.__uid = node.$$hashKey = node.__uid || this.buildNodeUIDByType(node._id, node._type);
        return node.__uid;
    }
    /**
     * @param {?} id
     * @param {?} type
     * @return {?}
     */
    buildNodeUIDByType(id, type) {
        return ('node-' + type + '-' + id);
    }
    /**
     * @param {?} node
     * @return {?}
     */
    buildCompanyState(node) {
        // юридическое состояние
        /** @type {?} */
        let egrulState = node.egrul_state;
        /** @type {?} */
        let aliveCode = '5';
        // действующее
        /** @type {?} */
        let intermediateCodes = ['6', '111', '121', '122', '123', '124', '131', '132'];
        // коды промежуточных состояний
        /** @type {?} */
        let _liquidate;
        if (node.egrul_liq) {
            _liquidate = {
                state: {
                    _actual: null,
                    _since: node.dead_dt,
                    type: _.upperFirst(_.lowerCase(node.egrul_liq.type)),
                },
            };
        }
        else if (node.dead_dt) {
            _liquidate = {
                state: {
                    _actual: null,
                    _since: node.dead_dt,
                    type: 'Ликвидировано',
                },
            };
        }
        else if (egrulState) {
            if (egrulState.code !== aliveCode) {
                _liquidate = {
                    state: {
                        _actual: egrulState._actual,
                        _since: egrulState._since,
                        type: egrulState.type,
                        intermediate: _.includes(intermediateCodes, egrulState.code),
                    },
                };
            }
        }
        node._liquidate = _liquidate;
        // способ организации компании
        /** @type {?} */
        const egrulReg = node.egrul_reg;
        /** @type {?} */
        const baseCodes = ['11', '01', '03'];
        if (egrulReg && !_.includes(baseCodes, egrulReg.code)) {
            node._reg = {
                state: {
                    _actual: egrulReg._actual,
                    _since: egrulReg._since,
                    type: egrulReg.type,
                },
            };
        }
    }
    /**
     * @param {?} node
     * @return {?}
     */
    resolvePurchaseHref(node) {
        if (node.law === 'FZ_44') {
            node.__href = node.href;
        }
        else {
            node.__href = 'http://zakupki.gov.ru/epz/order/quicksearch/search_eis.html?strictEqual=on&pageNumber=1&sortDirection=false&recordsPerPage=_10&showLotsInfoHidden=false&fz44=on&fz223=on&fz94=on&regions=&priceFrom=&priceTo=&currencyId=-1&publishDateFrom=&publishDateTo=&updateDateFrom=&updateDateTo=&sortBy=UPDATE_DATE&searchString=' + node._id;
        }
    }
}
if (false) {
    /** @type {?} */
    Meta.prototype.relationTypes;
    /** @type {?} */
    Meta.prototype.nodeTypes;
    /** @type {?} */
    Meta.prototype.SEARCHABLE_TYPES;
    /** @type {?} */
    Meta.prototype.state;
    /** @type {?} */
    Meta.prototype.nodeTypesMeta;
    /** @type {?} */
    Meta.prototype.relationTypesMeta;
}
/** @type {?} */
const metaService = new Meta();
export { metaService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWV0YVNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AbmtiL2FuZ3VsYXItbmtiLWxpYi8iLCJzb3VyY2VzIjpbImxpYi9tZXRhL21ldGFTZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7OztBQUlBLE9BQU8sS0FBSyxNQUFNLE9BQU8sQ0FBQzs7QUFHMUIsT0FBTyxLQUFLLENBQUMsTUFBTSxRQUFRLENBQUM7QUFFNUIsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQzVDLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxlQUFlLENBQUM7Ozs7QUFFNUMsOEJBT0M7OztJQU5HLHdCQUFnQjs7SUFDaEIsNkJBQVU7O0lBQ1YsaUNBQWM7O0lBQ2QsMkJBQVE7O0lBQ1IsNkJBQVU7O0lBQ1YsOEJBQTRDOzs7QUFHaEQsTUFBTSxPQUFPLElBQUk7SUFBakI7UUFHSSxxQkFBZ0IsR0FBRyxDQUFDLFNBQVMsQ0FBQyxPQUFPLEVBQUUsU0FBUyxDQUFDLFVBQVUsRUFBRSxTQUFTLENBQUMsT0FBTyxFQUFFLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQTtRQUNoRyxVQUFLLEdBQUc7WUFDSixNQUFNLEVBQUUsS0FBSztZQUNiLE9BQU8sRUFBRSxLQUFLO1lBQ2QsS0FBSyxFQUFFLEtBQUs7U0FDZixDQUFBOztRQUdELGtCQUFhLEdBQUcsRUFBRSxDQUFBO1FBQ2xCLHNCQUFpQixHQUFHLEVBQUUsQ0FBQTtJQTRVMUIsQ0FBQzs7OztJQXpVRyxJQUFJLE1BQU07UUFDTixPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDO0lBQzdCLENBQUM7Ozs7SUFFSyxJQUFJOztZQUNOLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sRUFBRTtnQkFDcEIsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxFQUFFO29CQUNyQixJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7b0JBQzFCLElBQUk7d0JBQ0EsSUFBSSxDQUFDLGFBQWEsR0FBRyxNQUFNLEtBQUssQ0FBQyxHQUFHLENBQUMsc0NBQXNDLENBQUMsQ0FBQyxJQUFJOzs7O3dCQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxDQUFDLElBQUksRUFBQyxDQUFDO3dCQUNuRyxJQUFJLENBQUMsU0FBUyxHQUFHLE1BQU0sS0FBSyxDQUFDLEdBQUcsQ0FBYSxrQ0FBa0MsQ0FBQyxDQUFDLElBQUk7Ozs7d0JBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFDLENBQUM7d0JBRXZHLG9FQUFvRTt3QkFDcEUsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsU0FBUzs7Ozt3QkFBRSxDQUFDLFFBQVEsRUFBRSxFQUFFOzRCQUNuQyxJQUFJLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsR0FBRyxRQUFRLENBQUM7d0JBQ2pELENBQUMsRUFBQyxDQUFDO3dCQUNILENBQUMsQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLFNBQVM7Ozs7O3dCQUFFLENBQUMsUUFBUSxFQUFFLFlBQVksRUFBRSxFQUFFOzRCQUN4RCxJQUFJLENBQUMsYUFBYSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQ3ZDLEVBQUUsRUFDRixJQUFJLENBQUMsYUFBYSxDQUFDLFlBQVksQ0FBQyxFQUNoQyxRQUFRLENBQ1gsQ0FBQzt3QkFDTixDQUFDLEVBQUMsQ0FBQzt3QkFFSCxvQkFBb0I7d0JBQ3BCLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLGFBQWE7Ozs7d0JBQUUsQ0FBQyxZQUFZLEVBQUUsRUFBRTs0QkFDM0MsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsR0FBRyxZQUFZLENBQUM7d0JBQzdELENBQUMsRUFBQyxDQUFDO3dCQUNILENBQUMsQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLGFBQWE7Ozs7O3dCQUFFLENBQUMsWUFBWSxFQUFFLGdCQUFnQixFQUFFLEVBQUU7NEJBQ3BFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQy9DLEVBQUUsRUFDRixJQUFJLENBQUMsaUJBQWlCLENBQUMsZ0JBQWdCLENBQUMsRUFDeEMsWUFBWSxDQUNmLENBQUM7d0JBQ04sQ0FBQyxFQUFDLENBQUM7d0JBQ0gsd0VBQXdFO3dCQUd4RSxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7cUJBQzVCO29CQUNELE9BQU8sR0FBRyxFQUFFO3dCQUNSLE9BQU8sQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7d0JBQ25CLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLEdBQUcsQ0FBQztxQkFDMUI7NEJBQ087d0JBQ0osT0FBTyxDQUFDLEdBQUcsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO3dCQUNuQyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7cUJBQzlCO2lCQUNKO3FCQUNJO29CQUNELE9BQU8sSUFBSSxPQUFPOzs7O29CQUFDLENBQUMsT0FBTyxFQUFFLEVBQUU7OzhCQUNyQixRQUFRLEdBQUcsV0FBVzs7O3dCQUFDLEdBQUcsRUFBRTs0QkFDOUIsSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sRUFBRTtnQ0FDbkIsT0FBTyxFQUFFLENBQUM7Z0NBQ1YsYUFBYSxDQUFDLFFBQVEsQ0FBQyxDQUFDOzZCQUMzQjt3QkFDTCxDQUFDLEdBQUUsSUFBSSxDQUFDO29CQUNaLENBQUMsRUFBQyxDQUFDO2lCQUNOO2FBRUo7UUFDTCxDQUFDO0tBQUE7Ozs7O0lBRUQsV0FBVyxDQUFDLElBQUk7O2NBQ04sU0FBUyxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLE9BQU8sQ0FBQyxFQUFFLENBQUMsRUFBRSxXQUFXLENBQUM7UUFDNUYsT0FBTyxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLE9BQU8sRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDO0lBQzlELENBQUM7Ozs7O0lBRUQsU0FBUyxDQUFDLElBQUk7UUFDVixJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUU7O2tCQUNQLE9BQU8sR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxFQUFFLElBQUksRUFBRSxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxPQUFPLENBQUMsRUFBRSxDQUFDLEVBQUUsU0FBUyxDQUFDO1lBQ3hGLE9BQU8sT0FBTyxLQUFLLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLE9BQU8sRUFBRSxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxLQUFLLENBQUMsQ0FBQyxDQUFDO1NBQzNGO0lBQ0wsQ0FBQzs7Ozs7SUFFRCxpQkFBaUIsQ0FBQyxJQUFJO1FBQ2xCLE9BQU8sQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLENBQUMsRUFBRSxTQUFTLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDM0UsQ0FBQzs7Ozs7SUFFRCwyQkFBMkIsQ0FBQyxJQUFJOztjQUN0QixnQkFBZ0IsR0FBRyxFQUFFOztZQUN2QixjQUFjLEdBQUcsRUFBRTs7WUFDbkIsTUFBTSxHQUFRLEVBQUU7UUFFcEIsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksRUFBRSxLQUFLLENBQUM7Ozs7UUFBRSxDQUFDLGFBQWEsRUFBRSxFQUFFO1lBQ3BDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxPQUFPLEVBQUUsYUFBYSxDQUFDLENBQUM7Ozs7O1lBQUUsQ0FBQyxJQUFJLEVBQUUsWUFBWSxFQUFFLEVBQUU7O3NCQUUzRCxxQkFBcUIsR0FBRyxJQUFJLENBQUMsOEJBQThCLENBQUMsSUFBSSxDQUFDOztzQkFDakUsYUFBYSxHQUFHLHFCQUFxQixDQUFDLEdBQUc7O3NCQUV6QyxTQUFTLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxhQUFhLENBQUM7O3NCQUM1QyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUMsWUFBWSxFQUFFLFNBQVMsQ0FBQzs7c0JBQ3BFLFFBQVEsR0FBRyxnQkFBZ0IsQ0FBQyxLQUFLOztzQkFDakMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFFBQVEsQ0FBQzs7c0JBQ3ZELGNBQWMsR0FBRyxJQUFJLENBQUMsK0JBQStCLENBQUMsWUFBWSxFQUFFLFNBQVMsQ0FBQzs7c0JBQzlFLGFBQWEsR0FBRyxjQUFjLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxTQUFTLEVBQUUsY0FBYyxDQUFDLFVBQVUsQ0FBQzs7b0JBQzlHLG1CQUFtQixHQUFHLGdCQUFnQixDQUFDLGFBQWEsQ0FBQzs7c0JBQ25ELFdBQVcsR0FBRyxJQUFJLENBQUMsb0JBQW9CLENBQUMsU0FBUyxFQUFFLFlBQVksQ0FBQzs7c0JBQ2hFLGlCQUFpQixHQUFHLElBQUksQ0FBQywwQkFBMEIsQ0FBQyxTQUFTLEVBQUUsWUFBWSxDQUFDOztvQkFDOUUsdUJBQXVCLEdBQUcsQ0FBQzs7c0JBRXpCLEtBQUssR0FBRyxNQUFNLENBQUMsUUFBUSxDQUFDLEdBQUcsTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJO29CQUNqRCxHQUFHLEVBQUUsUUFBUTtvQkFDYixLQUFLLEVBQUUsaUJBQWlCLENBQUMsS0FBSztvQkFDOUIsY0FBYyxFQUFFLEVBQUU7aUJBQ3JCOzs7c0JBR0ssU0FBUyxHQUFHO29CQUNkLEdBQUcsRUFBRSxXQUFXO29CQUNoQixLQUFLLEVBQUUsZ0JBQWdCLENBQUMsS0FBSztvQkFDN0IsR0FBRyxFQUFFLGdCQUFnQixDQUFDLEdBQUc7b0JBQ3pCLE9BQU8sRUFBRSxnQkFBZ0IsQ0FBQyxPQUFPO29CQUNqQyxZQUFZO29CQUNaLFNBQVM7b0JBQ1QsYUFBYTtvQkFDYixxQkFBcUI7b0JBQ3JCLFNBQVMsRUFBRSxpQkFBaUI7aUJBQy9COztzQkFFSywrQkFBK0IsR0FBRztvQkFDcEMsTUFBTSxFQUFFLENBQUM7b0JBQ1QsUUFBUSxFQUFFLENBQUM7b0JBQ1gsR0FBRyxFQUFFLENBQUM7aUJBQ1Q7Z0JBRUQsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLG1CQUFtQixJQUFJLGNBQWMsQ0FBQyxhQUFhOzs7O2dCQUFFLENBQUMsQ0FBQyxFQUFFLEVBQUU7OzBCQUM1RCxJQUFJLEdBQUcsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxPQUFPLEVBQUUsYUFBYSxFQUFFLENBQUMsQ0FBQyxDQUFDOzswQkFDL0MsTUFBTSxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLDhCQUE4QixDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJO29CQUV0RSxJQUFJLENBQUMsTUFBTSxFQUFFO3dCQUNULE9BQU87cUJBQ1Y7b0JBRUQsK0JBQStCLENBQUMsTUFBTSxJQUFJLE1BQU0sQ0FBQyxNQUFNLENBQUM7b0JBQ3hELCtCQUErQixDQUFDLFFBQVEsSUFBSSxNQUFNLENBQUMsUUFBUSxDQUFDO29CQUM1RCwrQkFBK0IsQ0FBQyxHQUFHLElBQUksTUFBTSxDQUFDLEdBQUcsQ0FBQztnQkFFdEQsQ0FBQyxFQUFDLENBQUM7Z0JBRUgsdUJBQXVCLEdBQUcsK0JBQStCLENBQUMsR0FBRyxDQUFDO2dCQUU5RCxJQUFJLENBQUMsbUJBQW1CLElBQUksdUJBQXVCLEdBQUcsYUFBYSxFQUFFOzswQkFDM0QsVUFBVSxHQUFHLGNBQWMsQ0FBQyxVQUFVOzswQkFDdEMsY0FBYyxHQUFHLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxVQUFVLEVBQUUsU0FBUyxDQUFDO29CQUV0RSxrQ0FBa0M7b0JBQ2xDLG1CQUFtQixHQUFHO3dCQUNsQixHQUFHLEVBQUUsYUFBYTt3QkFDbEIsS0FBSyxFQUFFLGNBQWMsQ0FBQyxLQUFLO3dCQUMzQixHQUFHLEVBQUUsY0FBYyxDQUFDLEdBQUc7d0JBQ3ZCLE9BQU8sRUFBRSxjQUFjLENBQUMsT0FBTzt3QkFDL0IsWUFBWSxFQUFFLFVBQVU7d0JBQ3hCLFNBQVM7d0JBQ1QsYUFBYSxFQUFFLHVCQUF1Qjt3QkFDdEMscUJBQXFCLEVBQUUsK0JBQStCO3dCQUN0RCxTQUFTLEVBQUUsSUFBSSxDQUFDLDBCQUEwQixDQUFDLFNBQVMsRUFBRSxVQUFVLENBQUM7cUJBQ3BFLENBQUM7b0JBRUYsS0FBSyxDQUFDLGNBQWMsQ0FBQyxhQUFhLENBQUMsR0FBRyxtQkFBbUIsQ0FBQztvQkFDMUQsZ0JBQWdCLENBQUMsYUFBYSxDQUFDLEdBQUcsbUJBQW1CLENBQUM7aUJBQ3pEO2dCQUVELElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxhQUFhLENBQUMsRUFBRTtvQkFDbEMsS0FBSyxDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUMsR0FBRyxTQUFTLENBQUM7aUJBQ2pEO2dCQUVELGdCQUFnQixDQUFDLFdBQVcsQ0FBQyxHQUFHLFNBQVMsQ0FBQztZQUM5QyxDQUFDLEVBQUMsQ0FBQztRQUNQLENBQUMsRUFBQyxDQUFDO1FBRUgsTUFBTSxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFLE9BQU8sQ0FBQyxDQUFDO1FBRW5DLENBQUMsQ0FBQyxPQUFPLENBQUMsTUFBTTs7OztRQUFFLENBQUMsS0FBSyxFQUFFLEVBQUU7WUFDeEIsS0FBSyxDQUFDLGNBQWMsR0FBRyxDQUFDLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxjQUFjLEVBQUUsT0FBTyxDQUFDLENBQUM7WUFDL0QsY0FBYyxHQUFHLGNBQWMsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyxDQUFDO1FBQ2pFLENBQUMsRUFBQyxDQUFDO1FBRUgsT0FBTztZQUNILGdCQUFnQjtZQUNoQixjQUFjO1lBQ2QsTUFBTTtTQUNULENBQUM7SUFDTixDQUFDOzs7OztJQUVELDhCQUE4QixDQUFDLElBQUk7UUFDL0IsSUFBSSxHQUFHLElBQUksSUFBSSxFQUFFLENBQUM7UUFDbEIsT0FBTztZQUNILE1BQU0sRUFBRSxJQUFJLENBQUMsTUFBTSxJQUFJLENBQUM7WUFDeEIsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLE1BQU0sSUFBSSxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxRQUFRLElBQUksQ0FBQyxDQUFDO1lBQzlDLFFBQVEsRUFBRSxJQUFJLENBQUMsUUFBUSxJQUFJLENBQUM7U0FFL0IsQ0FBQztJQUNOLENBQUM7Ozs7O0lBRUQsWUFBWSxDQUFDLGFBQWE7UUFDdEIsT0FBTyxhQUFhLEtBQUssSUFBSSxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQztJQUMzRCxDQUFDOzs7Ozs7SUFFRCxtQkFBbUIsQ0FBQyxZQUFZLEVBQUUsU0FBUztRQUN2QyxPQUFPLENBQUMsQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLGFBQWEsRUFBRSxDQUFDLFlBQVksRUFBRSxTQUFTLENBQUMsQ0FBQyxDQUFDO0lBQ3ZFLENBQUM7Ozs7O0lBRUQsb0JBQW9CLENBQUMsYUFBYTtRQUM5QixPQUFPLFdBQVcsQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUFDLENBQUM7SUFDckQsQ0FBQzs7Ozs7O0lBRUQsK0JBQStCLENBQUMsWUFBWSxFQUFFLFNBQVM7O2NBQzdDLElBQUksR0FBRztZQUNULFVBQVUsRUFBRSxJQUFJO1lBQ2hCLGFBQWEsRUFBRSxJQUFJO1NBQ3RCO1FBRUQsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsbUJBQW1COzs7OztRQUFFLENBQUMsWUFBWSxFQUFFLFVBQVUsRUFBRSxFQUFFOztrQkFDOUQsYUFBYSxHQUFHLFlBQVksQ0FBQyxTQUFTLENBQUM7WUFDN0MsSUFBSSxDQUFDLENBQUMsUUFBUSxDQUFDLGFBQWEsRUFBRSxZQUFZLENBQUMsRUFBRTtnQkFDekMsSUFBSSxDQUFDLFVBQVUsR0FBRyxVQUFVLENBQUM7Z0JBQzdCLElBQUksQ0FBQyxhQUFhLEdBQUcsYUFBYSxDQUFDO2dCQUNuQyxPQUFPLEtBQUssQ0FBQzthQUNoQjtRQUNMLENBQUMsRUFBQyxDQUFDO1FBRUgsT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQzs7Ozs7O0lBRUQsb0JBQW9CLENBQUMsU0FBUyxFQUFFLFlBQVk7UUFDeEMsT0FBTyxTQUFTLEdBQUcsSUFBSSxHQUFHLFlBQVksQ0FBQztJQUMzQyxDQUFDOzs7Ozs7SUFFRCwwQkFBMEIsQ0FBQyxTQUFTLEVBQUUsWUFBWTtRQUM5QyxPQUFPLHlCQUF5QixHQUFHLFlBQVksR0FBRyxHQUFHLEdBQUcsU0FBUyxDQUFDO0lBQ3RFLENBQUM7Ozs7O0lBRUQsa0JBQWtCLENBQUMsSUFBSTtRQUNuQixJQUFJLENBQUMsSUFBSSxFQUFFO1lBQ1AsT0FBTztTQUNWO1FBRUQsTUFBTTtRQUNOLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7UUFFeEIsRUFBRTtRQUNGLElBQUksQ0FBQyxjQUFjLEdBQUcsV0FBVyxDQUFDLDJCQUEyQixDQUFDLElBQUksQ0FBQyxDQUFDO1FBRXBFLEVBQUU7UUFDRixJQUFJLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsU0FBUyxDQUFDLENBQUM7UUFFbEUsSUFBSSxJQUFJLENBQUMsS0FBSyxLQUFLLFNBQVMsSUFBSSxJQUFJLENBQUMsS0FBSyxLQUFLLE9BQU8sRUFBRSxFQUFFLFdBQVc7WUFDakUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzdCLElBQUksQ0FBQyxzQkFBc0IsR0FBRyxJQUFJLENBQUM7U0FDdEM7YUFBTSxJQUFJLElBQUksQ0FBQyxLQUFLLEtBQUssWUFBWSxFQUFFLEVBQWMsUUFBUTtZQUMxRCxJQUFJLENBQUMsc0JBQXNCLEdBQUcsSUFBSSxDQUFDO1NBQ3RDO2FBQU0sSUFBSSxJQUFJLENBQUMsS0FBSyxLQUFLLFVBQVUsRUFBRSxFQUFFLFVBQVU7WUFDOUMsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsUUFBUSxJQUFJLEtBQUssQ0FBQztZQUN2QyxJQUFJLENBQUMsUUFBUSxHQUFHLENBQUMsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxLQUFLLENBQUMsQ0FBQztZQUMxQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDbEM7SUFFTCxDQUFDOzs7OztJQUNELFlBQVksQ0FBQyxJQUFJO1FBQ2IsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxLQUFLLElBQUksSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzFGLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQztJQUN0QixDQUFDOzs7Ozs7SUFFRCxrQkFBa0IsQ0FBQyxFQUFFLEVBQUUsSUFBSTtRQUN2QixPQUFPLENBQUMsT0FBTyxHQUFHLElBQUksR0FBRyxHQUFHLEdBQUcsRUFBRSxDQUFDLENBQUM7SUFDdkMsQ0FBQzs7Ozs7SUFFRCxpQkFBaUIsQ0FBQyxJQUFJOzs7WUFFZCxVQUFVLEdBQUcsSUFBSSxDQUFDLFdBQVc7O1lBQzdCLFNBQVMsR0FBRyxHQUFHOzs7WUFDZixpQkFBaUIsR0FBRyxDQUFDLEdBQUcsRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLENBQUM7OztZQUMxRSxVQUFVO1FBQ2QsSUFBSSxJQUFJLENBQUMsU0FBUyxFQUFFO1lBQ2hCLFVBQVUsR0FBRztnQkFDVCxLQUFLLEVBQUU7b0JBQ0gsT0FBTyxFQUFFLElBQUk7b0JBQ2IsTUFBTSxFQUFFLElBQUksQ0FBQyxPQUFPO29CQUNwQixJQUFJLEVBQUUsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUM7aUJBQ3ZEO2FBQ0osQ0FBQztTQUNMO2FBQU0sSUFBSSxJQUFJLENBQUMsT0FBTyxFQUFFO1lBQ3JCLFVBQVUsR0FBRztnQkFDVCxLQUFLLEVBQUU7b0JBQ0gsT0FBTyxFQUFFLElBQUk7b0JBQ2IsTUFBTSxFQUFFLElBQUksQ0FBQyxPQUFPO29CQUNwQixJQUFJLEVBQUUsZUFBZTtpQkFDeEI7YUFDSixDQUFDO1NBQ0w7YUFBTSxJQUFJLFVBQVUsRUFBRTtZQUNuQixJQUFJLFVBQVUsQ0FBQyxJQUFJLEtBQUssU0FBUyxFQUFFO2dCQUMvQixVQUFVLEdBQUc7b0JBQ1QsS0FBSyxFQUFFO3dCQUNILE9BQU8sRUFBRSxVQUFVLENBQUMsT0FBTzt3QkFDM0IsTUFBTSxFQUFFLFVBQVUsQ0FBQyxNQUFNO3dCQUN6QixJQUFJLEVBQUUsVUFBVSxDQUFDLElBQUk7d0JBQ3JCLFlBQVksRUFBRSxDQUFDLENBQUMsUUFBUSxDQUFDLGlCQUFpQixFQUFFLFVBQVUsQ0FBQyxJQUFJLENBQUM7cUJBQy9EO2lCQUNKLENBQUM7YUFDTDtTQUNKO1FBRUQsSUFBSSxDQUFDLFVBQVUsR0FBRyxVQUFVLENBQUM7OztjQUd2QixRQUFRLEdBQUcsSUFBSSxDQUFDLFNBQVM7O2NBQ3pCLFNBQVMsR0FBRyxDQUFDLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDO1FBRXBDLElBQUksUUFBUSxJQUFJLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxTQUFTLEVBQUUsUUFBUSxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQ25ELElBQUksQ0FBQyxJQUFJLEdBQUc7Z0JBQ1IsS0FBSyxFQUFFO29CQUNILE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTztvQkFDekIsTUFBTSxFQUFFLFFBQVEsQ0FBQyxNQUFNO29CQUN2QixJQUFJLEVBQUUsUUFBUSxDQUFDLElBQUk7aUJBQ3RCO2FBQ0osQ0FBQztTQUNMO0lBQ0wsQ0FBQzs7Ozs7SUFFRCxtQkFBbUIsQ0FBQyxJQUFJO1FBQ3BCLElBQUksSUFBSSxDQUFDLEdBQUcsS0FBSyxPQUFPLEVBQUU7WUFDdEIsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO1NBQzNCO2FBQU07WUFDSCxJQUFJLENBQUMsTUFBTSxHQUFHLDRUQUE0VCxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUM7U0FDelY7SUFDTCxDQUFDO0NBR0o7OztJQXZWRyw2QkFBYzs7SUFDZCx5QkFBc0I7O0lBQ3RCLGdDQUFnRzs7SUFDaEcscUJBSUM7O0lBR0QsNkJBQWtCOztJQUNsQixpQ0FBc0I7OztNQStVcEIsV0FBVyxHQUFHLElBQUksSUFBSSxFQUFFO0FBQzlCLE9BQU8sRUFBRSxXQUFXLEVBQUUsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbIi8qKlxyXG4gKiBDcmVhdGVkIGJ5IHJhZGlnb3N0IG9uIDA3LjA5LjE3LlxyXG4gKi9cclxuXHJcbmltcG9ydCBheGlvcyBmcm9tICdheGlvcyc7XHJcblxyXG4vL3RvZG8g0LjQvNC/0L7RgNGC0LjRgNC+0LLQsNGC0Ywg0YLQvtC70YzQutC+INGC0L4g0YfRgtC+INC90YPQttC90L5cclxuaW1wb3J0ICogYXMgXyBmcm9tICdsb2Rhc2gnO1xyXG5cclxuaW1wb3J0IHsgTk9ERV9UWVBFIH0gZnJvbSAnLi4vZG9tYWluL1RZUEVTJztcclxuaW1wb3J0IHsgcnNlYXJjaE1ldGEgfSBmcm9tICcuL3JzZWFyY2hNZXRhJztcclxuXHJcbmV4cG9ydCBpbnRlcmZhY2UgTm9kZVR5cGUge1xyXG4gICAgbmFtZTogTk9ERV9UWVBFO1xyXG4gICAgc29ydEZpZWxkO1xyXG4gICAgc29ydEFzY2VuZGluZztcclxuICAgIGlkRmllbGQ7XHJcbiAgICBuYW1lRmllbGQ7XHJcbiAgICBwcm9wZXJ0aWVzOiBbeyBbcHJvcGVydHk6IHN0cmluZ106IHN0cmluZyB9XVxyXG59XHJcbi8vdG9kbyDQv9C+0LrRgNGL0YLRjCDRgtC10YHRgtCw0LzQuFxyXG5leHBvcnQgY2xhc3MgTWV0YSB7XHJcbiAgICByZWxhdGlvblR5cGVzO1xyXG4gICAgbm9kZVR5cGVzOiBOb2RlVHlwZVtdO1xyXG4gICAgU0VBUkNIQUJMRV9UWVBFUyA9IFtOT0RFX1RZUEUuQ09NUEFOWSwgTk9ERV9UWVBFLklORElWSURVQUwsIE5PREVfVFlQRS5BRERSRVNTLCBOT0RFX1RZUEUuUEhPTkVdXHJcbiAgICBzdGF0ZSA9IHtcclxuICAgICAgICBpbml0ZWQ6IGZhbHNlLFxyXG4gICAgICAgIHBlbmRpbmc6IGZhbHNlLFxyXG4gICAgICAgIGVycm9yOiBmYWxzZSxcclxuICAgIH1cclxuXHJcbiAgICAvLyBARGVwcmVjYXRlZFxyXG4gICAgbm9kZVR5cGVzTWV0YSA9IHt9XHJcbiAgICByZWxhdGlvblR5cGVzTWV0YSA9IHt9XHJcblxyXG5cclxuICAgIGdldCBpbml0ZWQoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuc3RhdGUuaW5pdGVkO1xyXG4gICAgfVxyXG5cclxuICAgIGFzeW5jIGluaXQoKSB7XHJcbiAgICAgICAgaWYgKCF0aGlzLnN0YXRlLmluaXRlZCkge1xyXG4gICAgICAgICAgICBpZiAoIXRoaXMuc3RhdGUucGVuZGluZykge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zdGF0ZS5wZW5kaW5nID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5yZWxhdGlvblR5cGVzID0gYXdhaXQgYXhpb3MuZ2V0KCcvbmticmVsYXRpb24vYXBpL21ldGEvcmVsYXRpb24vdHlwZXMnKS50aGVuKHJlcyA9PiByZXMuZGF0YSk7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5ub2RlVHlwZXMgPSBhd2FpdCBheGlvcy5nZXQ8Tm9kZVR5cGVbXT4oJy9ua2JyZWxhdGlvbi9hcGkvbWV0YS9ub2RlL3R5cGVzJykudGhlbihyZXMgPT4gcmVzLmRhdGEpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAvLyBUT0RPIGxlZ2FjeSAtIHJlZmFjdG9yIHRoaXMgLSBhIGxvdCBvZiBhZGRpdGlvbmFsIG5vdCBuZWVkZWQgd29ya1xyXG4gICAgICAgICAgICAgICAgICAgIF8uZm9yRWFjaCh0aGlzLm5vZGVUeXBlcywgKG5vZGVUeXBlKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubm9kZVR5cGVzTWV0YVtub2RlVHlwZS5uYW1lXSA9IG5vZGVUeXBlO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIF8uZm9yRWFjaChyc2VhcmNoTWV0YS5ub2RlVHlwZXMsIChub2RlVHlwZSwgbm9kZVR5cGVOYW1lKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubm9kZVR5cGVzTWV0YVtub2RlVHlwZU5hbWVdID0gXy5leHRlbmQoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB7fSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubm9kZVR5cGVzTWV0YVtub2RlVHlwZU5hbWVdLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbm9kZVR5cGUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIC8vIHJlbGF0aW9uVHlwZXNNZXRhXHJcbiAgICAgICAgICAgICAgICAgICAgXy5mb3JFYWNoKHRoaXMucmVsYXRpb25UeXBlcywgKHJlbGF0aW9uVHlwZSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnJlbGF0aW9uVHlwZXNNZXRhW3JlbGF0aW9uVHlwZS5uYW1lXSA9IHJlbGF0aW9uVHlwZTtcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICBfLmZvckVhY2gocnNlYXJjaE1ldGEucmVsYXRpb25UeXBlcywgKHJlbGF0aW9uVHlwZSwgcmVsYXRpb25UeXBlTmFtZSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnJlbGF0aW9uVHlwZXNNZXRhW3JlbGF0aW9uVHlwZU5hbWVdID0gXy5leHRlbmQoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB7fSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucmVsYXRpb25UeXBlc01ldGFbcmVsYXRpb25UeXBlTmFtZV0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZWxhdGlvblR5cGUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gZW5kIFRPRE8gbGVnYWN5IC0gcmVmYWN0b3IgdGhpcyAtIGEgbG90IG9mIGFkZGl0aW9uYWwgbm90IG5lZWRlZCB3b3JrXHJcblxyXG5cclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnN0YXRlLmluaXRlZCA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBjYXRjaCAoZXJyKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5lcnJvcihlcnIpO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc3RhdGUuZXJyb3IgPSBlcnI7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBmaW5hbGx5IHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcIm1ldGEgc2VydmljZSBpbml0ZWRcIik7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zdGF0ZS5wZW5kaW5nID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgZWxzZSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBpbnRlcnZhbCA9IHNldEludGVydmFsKCgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMuc3RhdGUuaW5pdGVkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXNvbHZlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbGVhckludGVydmFsKGludGVydmFsKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH0sIDEwMDApO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGdldE5vZGVOYW1lKG5vZGUpIHtcclxuICAgICAgICBjb25zdCBuYW1lRmllbGQgPSBfLmdldChfLmZpbmQodGhpcy5ub2RlVHlwZXMsIHsgbmFtZTogXy5nZXQobm9kZSwgJ190eXBlJykgfSksICduYW1lRmllbGQnKTtcclxuICAgICAgICByZXR1cm4gXy5nZXQobm9kZSwgW25hbWVGaWVsZF0sIF8uZ2V0KG5vZGUsICdfbmFtZScsICcnKSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0Tm9kZUlkKG5vZGUpIHtcclxuICAgICAgICBpZiAodGhpcy5pbml0ZWQpIHtcclxuICAgICAgICAgICAgY29uc3QgaWRGaWVsZCA9IF8uZ2V0KF8uZmluZCh0aGlzLm5vZGVUeXBlcywgeyBuYW1lOiBfLmdldChub2RlLCAnX3R5cGUnKSB9KSwgJ2lkRmllbGQnKTtcclxuICAgICAgICAgICAgcmV0dXJuIGlkRmllbGQgPT09ICdpZCcgPyBfLmdldChub2RlLCAnX2lkJykgOiBfLmdldChub2RlLCBpZEZpZWxkLCBfLmdldChub2RlLCAnX2lkJykpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBnZXRJZEZpZWxkRm9yVHlwZSh0eXBlKSB7XHJcbiAgICAgICAgcmV0dXJuIF8uZ2V0KF8uZmluZCh0aGlzLm5vZGVUeXBlcywgeyBuYW1lOiB0eXBlIH0pLCAnaWRGaWVsZCcsICdfaWQnKTtcclxuICAgIH1cclxuXHJcbiAgICBidWlsZFJlbGF0aW9uRGF0YUJ5Tm9kZUluZm8obm9kZSkge1xyXG4gICAgICAgIGNvbnN0IHJlbGF0aW9uQ291bnRNYXAgPSB7fTtcclxuICAgICAgICBsZXQgcmVsYXRpb25Db3VudHMgPSBbXTtcclxuICAgICAgICBsZXQgZ3JvdXBzOiBhbnkgPSB7fTtcclxuXHJcbiAgICAgICAgXy5lYWNoKFsnaW4nLCAnb3V0J10sIChpbmZvRGlyZWN0aW9uKSA9PiB7XHJcbiAgICAgICAgICAgIF8uZWFjaChfLmdldChub2RlLCBbJ19pbmZvJywgaW5mb0RpcmVjdGlvbl0pLCAoaW5mbywgcmVsYXRpb25UeXBlKSA9PiB7XHJcblxyXG4gICAgICAgICAgICAgICAgY29uc3QgaGlzdG9yeVJlbGF0aW9uQ291bnRzID0gdGhpcy5nZXRIaXN0b3J5UmVsYXRpb25Db3VudHNCeUluZm8oaW5mbyk7XHJcbiAgICAgICAgICAgICAgICBjb25zdCByZWxhdGlvbkNvdW50ID0gaGlzdG9yeVJlbGF0aW9uQ291bnRzLmFsbDtcclxuXHJcbiAgICAgICAgICAgICAgICBjb25zdCBkaXJlY3Rpb24gPSB0aGlzLmdldERpcmVjdGlvbihpbmZvRGlyZWN0aW9uKTtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHJlbGF0aW9uVHlwZU1ldGEgPSB0aGlzLmdldFJlbGF0aW9uVHlwZU1ldGEocmVsYXRpb25UeXBlLCBkaXJlY3Rpb24pO1xyXG4gICAgICAgICAgICAgICAgY29uc3QgZ3JvdXBLZXkgPSByZWxhdGlvblR5cGVNZXRhLmdyb3VwO1xyXG4gICAgICAgICAgICAgICAgY29uc3QgcmVsYXRpb25Hcm91cE1ldGEgPSB0aGlzLmdldFJlbGF0aW9uR3JvdXBNZXRhKGdyb3VwS2V5KTtcclxuICAgICAgICAgICAgICAgIGNvbnN0IG1lcmdlZFR5cGVJbmZvID0gdGhpcy5nZXRNZXJnZWRUeXBlSW5mb0J5UmVsYXRpb25UeXBlKHJlbGF0aW9uVHlwZSwgZGlyZWN0aW9uKTtcclxuICAgICAgICAgICAgICAgIGNvbnN0IG1lcmdlZFR5cGVLZXkgPSBtZXJnZWRUeXBlSW5mby5tZXJnZWRUeXBlICYmIHRoaXMuYnVpbGROb2RlUmVsYXRpb25LZXkoZGlyZWN0aW9uLCBtZXJnZWRUeXBlSW5mby5tZXJnZWRUeXBlKTtcclxuICAgICAgICAgICAgICAgIGxldCBtZXJnZWRUeXBlQ291bnREYXRhID0gcmVsYXRpb25Db3VudE1hcFttZXJnZWRUeXBlS2V5XTtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHJlbGF0aW9uS2V5ID0gdGhpcy5idWlsZE5vZGVSZWxhdGlvbktleShkaXJlY3Rpb24sIHJlbGF0aW9uVHlwZSk7XHJcbiAgICAgICAgICAgICAgICBjb25zdCByZWxhdGlvblBsdXJhbEtleSA9IHRoaXMuYnVpbGROb2RlUmVsYXRpb25QbHVyYWxLZXkoZGlyZWN0aW9uLCByZWxhdGlvblR5cGUpO1xyXG4gICAgICAgICAgICAgICAgbGV0IG1lcmdlZFR5cGVSZWxhdGlvbkNvdW50ID0gMDtcclxuXHJcbiAgICAgICAgICAgICAgICBjb25zdCBncm91cCA9IGdyb3Vwc1tncm91cEtleV0gPSBncm91cHNbZ3JvdXBLZXldIHx8IHtcclxuICAgICAgICAgICAgICAgICAgICBrZXk6IGdyb3VwS2V5LFxyXG4gICAgICAgICAgICAgICAgICAgIG9yZGVyOiByZWxhdGlvbkdyb3VwTWV0YS5vcmRlcixcclxuICAgICAgICAgICAgICAgICAgICByZWxhdGlvbkNvdW50czoge30sXHJcbiAgICAgICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgICAgIC8vIFRPRE8g0J7QsdGK0LXQtNC40L3QuNGC0Ywg0LrQvtC0INGBIG1lcmdlZFR5cGVDb3VudERhdGFcclxuICAgICAgICAgICAgICAgIGNvbnN0IGNvdW50RGF0YSA9IHtcclxuICAgICAgICAgICAgICAgICAgICBrZXk6IHJlbGF0aW9uS2V5LFxyXG4gICAgICAgICAgICAgICAgICAgIG9yZGVyOiByZWxhdGlvblR5cGVNZXRhLm9yZGVyLFxyXG4gICAgICAgICAgICAgICAgICAgIHRvcDogcmVsYXRpb25UeXBlTWV0YS50b3AsXHJcbiAgICAgICAgICAgICAgICAgICAgaGlzdG9yeTogcmVsYXRpb25UeXBlTWV0YS5oaXN0b3J5LFxyXG4gICAgICAgICAgICAgICAgICAgIHJlbGF0aW9uVHlwZSxcclxuICAgICAgICAgICAgICAgICAgICBkaXJlY3Rpb24sXHJcbiAgICAgICAgICAgICAgICAgICAgcmVsYXRpb25Db3VudCxcclxuICAgICAgICAgICAgICAgICAgICBoaXN0b3J5UmVsYXRpb25Db3VudHMsXHJcbiAgICAgICAgICAgICAgICAgICAgcGx1cmFsS2V5OiByZWxhdGlvblBsdXJhbEtleSxcclxuICAgICAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICAgICAgY29uc3QgbWVyZ2VkVHlwZUhpc3RvcnlSZWxhdGlvbkNvdW50cyA9IHtcclxuICAgICAgICAgICAgICAgICAgICBhY3R1YWw6IDAsXHJcbiAgICAgICAgICAgICAgICAgICAgb3V0ZGF0ZWQ6IDAsXHJcbiAgICAgICAgICAgICAgICAgICAgYWxsOiAwLFxyXG4gICAgICAgICAgICAgICAgfTtcclxuXHJcbiAgICAgICAgICAgICAgICBfLmZvckVhY2goIW1lcmdlZFR5cGVDb3VudERhdGEgJiYgbWVyZ2VkVHlwZUluZm8ucmVsYXRpb25UeXBlcywgKHQpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBpbmZvID0gXy5nZXQobm9kZSwgWydfaW5mbycsIGluZm9EaXJlY3Rpb24sIHRdKTtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBjb3VudHMgPSBpbmZvID8gdGhpcy5nZXRIaXN0b3J5UmVsYXRpb25Db3VudHNCeUluZm8oaW5mbykgOiBudWxsO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICBpZiAoIWNvdW50cykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICBtZXJnZWRUeXBlSGlzdG9yeVJlbGF0aW9uQ291bnRzLmFjdHVhbCArPSBjb3VudHMuYWN0dWFsO1xyXG4gICAgICAgICAgICAgICAgICAgIG1lcmdlZFR5cGVIaXN0b3J5UmVsYXRpb25Db3VudHMub3V0ZGF0ZWQgKz0gY291bnRzLm91dGRhdGVkO1xyXG4gICAgICAgICAgICAgICAgICAgIG1lcmdlZFR5cGVIaXN0b3J5UmVsYXRpb25Db3VudHMuYWxsICs9IGNvdW50cy5hbGw7XHJcblxyXG4gICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAgICAgbWVyZ2VkVHlwZVJlbGF0aW9uQ291bnQgPSBtZXJnZWRUeXBlSGlzdG9yeVJlbGF0aW9uQ291bnRzLmFsbDtcclxuXHJcbiAgICAgICAgICAgICAgICBpZiAoIW1lcmdlZFR5cGVDb3VudERhdGEgJiYgbWVyZ2VkVHlwZVJlbGF0aW9uQ291bnQgPiByZWxhdGlvbkNvdW50KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgbWVyZ2VkVHlwZSA9IG1lcmdlZFR5cGVJbmZvLm1lcmdlZFR5cGU7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgbWVyZ2VkVHlwZU1ldGEgPSB0aGlzLmdldFJlbGF0aW9uVHlwZU1ldGEobWVyZ2VkVHlwZSwgZGlyZWN0aW9uKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gVE9ETyDQntCx0YrQtdC00LjQvdC40YLRjCDQutC+0LQg0YEgY291bnREYXRhXHJcbiAgICAgICAgICAgICAgICAgICAgbWVyZ2VkVHlwZUNvdW50RGF0YSA9IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAga2V5OiBtZXJnZWRUeXBlS2V5LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBvcmRlcjogbWVyZ2VkVHlwZU1ldGEub3JkZXIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRvcDogbWVyZ2VkVHlwZU1ldGEudG9wLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBoaXN0b3J5OiBtZXJnZWRUeXBlTWV0YS5oaXN0b3J5LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZWxhdGlvblR5cGU6IG1lcmdlZFR5cGUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRpcmVjdGlvbixcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVsYXRpb25Db3VudDogbWVyZ2VkVHlwZVJlbGF0aW9uQ291bnQsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGhpc3RvcnlSZWxhdGlvbkNvdW50czogbWVyZ2VkVHlwZUhpc3RvcnlSZWxhdGlvbkNvdW50cyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcGx1cmFsS2V5OiB0aGlzLmJ1aWxkTm9kZVJlbGF0aW9uUGx1cmFsS2V5KGRpcmVjdGlvbiwgbWVyZ2VkVHlwZSksXHJcbiAgICAgICAgICAgICAgICAgICAgfTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgZ3JvdXAucmVsYXRpb25Db3VudHNbbWVyZ2VkVHlwZUtleV0gPSBtZXJnZWRUeXBlQ291bnREYXRhO1xyXG4gICAgICAgICAgICAgICAgICAgIHJlbGF0aW9uQ291bnRNYXBbbWVyZ2VkVHlwZUtleV0gPSBtZXJnZWRUeXBlQ291bnREYXRhO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIGlmICghcmVsYXRpb25Db3VudE1hcFttZXJnZWRUeXBlS2V5XSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGdyb3VwLnJlbGF0aW9uQ291bnRzW3JlbGF0aW9uS2V5XSA9IGNvdW50RGF0YTtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICByZWxhdGlvbkNvdW50TWFwW3JlbGF0aW9uS2V5XSA9IGNvdW50RGF0YTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIGdyb3VwcyA9IF8uc29ydEJ5KGdyb3VwcywgJ29yZGVyJyk7XHJcblxyXG4gICAgICAgIF8uZm9yRWFjaChncm91cHMsIChncm91cCkgPT4ge1xyXG4gICAgICAgICAgICBncm91cC5yZWxhdGlvbkNvdW50cyA9IF8uc29ydEJ5KGdyb3VwLnJlbGF0aW9uQ291bnRzLCAnb3JkZXInKTtcclxuICAgICAgICAgICAgcmVsYXRpb25Db3VudHMgPSByZWxhdGlvbkNvdW50cy5jb25jYXQoZ3JvdXAucmVsYXRpb25Db3VudHMpO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICByZWxhdGlvbkNvdW50TWFwLFxyXG4gICAgICAgICAgICByZWxhdGlvbkNvdW50cyxcclxuICAgICAgICAgICAgZ3JvdXBzLFxyXG4gICAgICAgIH07XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0SGlzdG9yeVJlbGF0aW9uQ291bnRzQnlJbmZvKGluZm8pIHtcclxuICAgICAgICBpbmZvID0gaW5mbyB8fCB7fTtcclxuICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICBhY3R1YWw6IGluZm8uYWN0dWFsIHx8IDAsXHJcbiAgICAgICAgICAgIGFsbDogKGluZm8uYWN0dWFsIHx8IDApICsgKGluZm8ub3V0ZGF0ZWQgfHwgMCksXHJcbiAgICAgICAgICAgIG91dGRhdGVkOiBpbmZvLm91dGRhdGVkIHx8IDAsXHJcblxyXG4gICAgICAgIH07XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0RGlyZWN0aW9uKGluZm9EaXJlY3Rpb24pIHtcclxuICAgICAgICByZXR1cm4gaW5mb0RpcmVjdGlvbiA9PT0gJ2luJyA/ICdwYXJlbnRzJyA6ICdjaGlsZHJlbic7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0UmVsYXRpb25UeXBlTWV0YShyZWxhdGlvblR5cGUsIGRpcmVjdGlvbikge1xyXG4gICAgICAgIHJldHVybiBfLmdldChyc2VhcmNoTWV0YS5yZWxhdGlvblR5cGVzLCBbcmVsYXRpb25UeXBlLCBkaXJlY3Rpb25dKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRSZWxhdGlvbkdyb3VwTWV0YShyZWxhdGlvbkdyb3VwKSB7XHJcbiAgICAgICAgcmV0dXJuIHJzZWFyY2hNZXRhLnJlbGF0aW9uR3JvdXBzW3JlbGF0aW9uR3JvdXBdO1xyXG4gICAgfVxyXG5cclxuICAgIGdldE1lcmdlZFR5cGVJbmZvQnlSZWxhdGlvblR5cGUocmVsYXRpb25UeXBlLCBkaXJlY3Rpb24pIHtcclxuICAgICAgICBjb25zdCBpbmZvID0ge1xyXG4gICAgICAgICAgICBtZXJnZWRUeXBlOiBudWxsLFxyXG4gICAgICAgICAgICByZWxhdGlvblR5cGVzOiBudWxsLFxyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIF8uZm9yRWFjaChyc2VhcmNoTWV0YS5tZXJnZWRSZWxhdGlvblR5cGVzLCAoYnlEaXJlY3Rpb25zLCBtZXJnZWRUeXBlKSA9PiB7XHJcbiAgICAgICAgICAgIGNvbnN0IHJlbGF0aW9uVHlwZXMgPSBieURpcmVjdGlvbnNbZGlyZWN0aW9uXTtcclxuICAgICAgICAgICAgaWYgKF8uaW5jbHVkZXMocmVsYXRpb25UeXBlcywgcmVsYXRpb25UeXBlKSkge1xyXG4gICAgICAgICAgICAgICAgaW5mby5tZXJnZWRUeXBlID0gbWVyZ2VkVHlwZTtcclxuICAgICAgICAgICAgICAgIGluZm8ucmVsYXRpb25UeXBlcyA9IHJlbGF0aW9uVHlwZXM7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgcmV0dXJuIGluZm87XHJcbiAgICB9XHJcblxyXG4gICAgYnVpbGROb2RlUmVsYXRpb25LZXkoZGlyZWN0aW9uLCByZWxhdGlvblR5cGUpIHtcclxuICAgICAgICByZXR1cm4gZGlyZWN0aW9uICsgJzo6JyArIHJlbGF0aW9uVHlwZTtcclxuICAgIH1cclxuXHJcbiAgICBidWlsZE5vZGVSZWxhdGlvblBsdXJhbEtleShkaXJlY3Rpb24sIHJlbGF0aW9uVHlwZSkge1xyXG4gICAgICAgIHJldHVybiAnTkdfUExVUkFMSVpFOjpSRUxBVElPTl8nICsgcmVsYXRpb25UeXBlICsgJy0nICsgZGlyZWN0aW9uO1xyXG4gICAgfVxyXG5cclxuICAgIGJ1aWxkTm9kZUV4dHJhTWV0YShub2RlKSB7XHJcbiAgICAgICAgaWYgKCFub2RlKSB7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIHVpZFxyXG4gICAgICAgIHRoaXMuYnVpbGROb2RlVUlEKG5vZGUpO1xyXG5cclxuICAgICAgICAvL1xyXG4gICAgICAgIG5vZGUuX19yZWxhdGlvbkRhdGEgPSBtZXRhU2VydmljZS5idWlsZFJlbGF0aW9uRGF0YUJ5Tm9kZUluZm8obm9kZSk7XHJcblxyXG4gICAgICAgIC8vXHJcbiAgICAgICAgbm9kZS5fX2lkRmllbGQgPSBfLmdldCh0aGlzLm5vZGVUeXBlc01ldGFbbm9kZS5fdHlwZV0sICdpZEZpZWxkJyk7XHJcblxyXG4gICAgICAgIGlmIChub2RlLl90eXBlID09PSAnQ09NUEFOWScgfHwgbm9kZS5fdHlwZSA9PT0gJ0VHUlVMJykgeyAvLyDQutC+0LzQv9Cw0L3QuNGPXHJcbiAgICAgICAgICAgIHRoaXMuYnVpbGRDb21wYW55U3RhdGUobm9kZSk7XHJcbiAgICAgICAgICAgIG5vZGUuX19pc0Zhdm9yaXRlc1N1cHBvcnRlZCA9IHRydWU7XHJcbiAgICAgICAgfSBlbHNlIGlmIChub2RlLl90eXBlID09PSAnSU5ESVZJRFVBTCcpIHsgICAgICAgICAgICAgLy8g0YTQuNC30LjQulxyXG4gICAgICAgICAgICBub2RlLl9faXNGYXZvcml0ZXNTdXBwb3J0ZWQgPSB0cnVlO1xyXG4gICAgICAgIH0gZWxzZSBpZiAobm9kZS5fdHlwZSA9PT0gJ1BVUkNIQVNFJykgeyAvLyDQt9Cw0LrRg9C/0LrQsFxyXG4gICAgICAgICAgICBub2RlLmN1cnJlbmN5ID0gbm9kZS5jdXJyZW5jeSB8fCAnUlVCJztcclxuICAgICAgICAgICAgbm9kZS5fX2xvdE1hcCA9IF8ua2V5Qnkobm9kZS5sb3RzLCAnbG90Jyk7XHJcbiAgICAgICAgICAgIHRoaXMucmVzb2x2ZVB1cmNoYXNlSHJlZihub2RlKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgfVxyXG4gICAgYnVpbGROb2RlVUlEKG5vZGUpIHtcclxuICAgICAgICBub2RlLl9fdWlkID0gbm9kZS4kJGhhc2hLZXkgPSBub2RlLl9fdWlkIHx8IHRoaXMuYnVpbGROb2RlVUlEQnlUeXBlKG5vZGUuX2lkLCBub2RlLl90eXBlKTtcclxuICAgICAgICByZXR1cm4gbm9kZS5fX3VpZDtcclxuICAgIH1cclxuXHJcbiAgICBidWlsZE5vZGVVSURCeVR5cGUoaWQsIHR5cGUpIHtcclxuICAgICAgICByZXR1cm4gKCdub2RlLScgKyB0eXBlICsgJy0nICsgaWQpO1xyXG4gICAgfVxyXG5cclxuICAgIGJ1aWxkQ29tcGFueVN0YXRlKG5vZGUpIHtcclxuICAgICAgICAvLyDRjtGA0LjQtNC40YfQtdGB0LrQvtC1INGB0L7RgdGC0L7Rj9C90LjQtVxyXG4gICAgICAgIGxldCBlZ3J1bFN0YXRlID0gbm9kZS5lZ3J1bF9zdGF0ZTtcclxuICAgICAgICBsZXQgYWxpdmVDb2RlID0gJzUnOyAvLyDQtNC10LnRgdGC0LLRg9GO0YnQtdC1XHJcbiAgICAgICAgbGV0IGludGVybWVkaWF0ZUNvZGVzID0gWyc2JywgJzExMScsICcxMjEnLCAnMTIyJywgJzEyMycsICcxMjQnLCAnMTMxJywgJzEzMiddOyAvLyDQutC+0LTRiyDQv9GA0L7QvNC10LbRg9GC0L7Rh9C90YvRhSDRgdC+0YHRgtC+0Y/QvdC40LlcclxuICAgICAgICBsZXQgX2xpcXVpZGF0ZTtcclxuICAgICAgICBpZiAobm9kZS5lZ3J1bF9saXEpIHtcclxuICAgICAgICAgICAgX2xpcXVpZGF0ZSA9IHtcclxuICAgICAgICAgICAgICAgIHN0YXRlOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgX2FjdHVhbDogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICBfc2luY2U6IG5vZGUuZGVhZF9kdCxcclxuICAgICAgICAgICAgICAgICAgICB0eXBlOiBfLnVwcGVyRmlyc3QoXy5sb3dlckNhc2Uobm9kZS5lZ3J1bF9saXEudHlwZSkpLFxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICB9IGVsc2UgaWYgKG5vZGUuZGVhZF9kdCkge1xyXG4gICAgICAgICAgICBfbGlxdWlkYXRlID0ge1xyXG4gICAgICAgICAgICAgICAgc3RhdGU6IHtcclxuICAgICAgICAgICAgICAgICAgICBfYWN0dWFsOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgIF9zaW5jZTogbm9kZS5kZWFkX2R0LFxyXG4gICAgICAgICAgICAgICAgICAgIHR5cGU6ICfQm9C40LrQstC40LTQuNGA0L7QstCw0L3QvicsXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgIH0gZWxzZSBpZiAoZWdydWxTdGF0ZSkge1xyXG4gICAgICAgICAgICBpZiAoZWdydWxTdGF0ZS5jb2RlICE9PSBhbGl2ZUNvZGUpIHtcclxuICAgICAgICAgICAgICAgIF9saXF1aWRhdGUgPSB7XHJcbiAgICAgICAgICAgICAgICAgICAgc3RhdGU6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgX2FjdHVhbDogZWdydWxTdGF0ZS5fYWN0dWFsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBfc2luY2U6IGVncnVsU3RhdGUuX3NpbmNlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0eXBlOiBlZ3J1bFN0YXRlLnR5cGUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGludGVybWVkaWF0ZTogXy5pbmNsdWRlcyhpbnRlcm1lZGlhdGVDb2RlcywgZWdydWxTdGF0ZS5jb2RlKSxcclxuICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgbm9kZS5fbGlxdWlkYXRlID0gX2xpcXVpZGF0ZTtcclxuXHJcbiAgICAgICAgLy8g0YHQv9C+0YHQvtCxINC+0YDQs9Cw0L3QuNC30LDRhtC40Lgg0LrQvtC80L/QsNC90LjQuFxyXG4gICAgICAgIGNvbnN0IGVncnVsUmVnID0gbm9kZS5lZ3J1bF9yZWc7XHJcbiAgICAgICAgY29uc3QgYmFzZUNvZGVzID0gWycxMScsICcwMScsICcwMyddOyAvLyDQutC+0LTRiyDRgdGC0LDQvdC00LDRgNGC0L3Ri9GFINGA0LXQs9C40YHRgtGA0LDRhtC40LlcclxuXHJcbiAgICAgICAgaWYgKGVncnVsUmVnICYmICFfLmluY2x1ZGVzKGJhc2VDb2RlcywgZWdydWxSZWcuY29kZSkpIHtcclxuICAgICAgICAgICAgbm9kZS5fcmVnID0ge1xyXG4gICAgICAgICAgICAgICAgc3RhdGU6IHtcclxuICAgICAgICAgICAgICAgICAgICBfYWN0dWFsOiBlZ3J1bFJlZy5fYWN0dWFsLFxyXG4gICAgICAgICAgICAgICAgICAgIF9zaW5jZTogZWdydWxSZWcuX3NpbmNlLFxyXG4gICAgICAgICAgICAgICAgICAgIHR5cGU6IGVncnVsUmVnLnR5cGUsXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICByZXNvbHZlUHVyY2hhc2VIcmVmKG5vZGUpIHtcclxuICAgICAgICBpZiAobm9kZS5sYXcgPT09ICdGWl80NCcpIHtcclxuICAgICAgICAgICAgbm9kZS5fX2hyZWYgPSBub2RlLmhyZWY7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgbm9kZS5fX2hyZWYgPSAnaHR0cDovL3pha3Vwa2kuZ292LnJ1L2Vwei9vcmRlci9xdWlja3NlYXJjaC9zZWFyY2hfZWlzLmh0bWw/c3RyaWN0RXF1YWw9b24mcGFnZU51bWJlcj0xJnNvcnREaXJlY3Rpb249ZmFsc2UmcmVjb3Jkc1BlclBhZ2U9XzEwJnNob3dMb3RzSW5mb0hpZGRlbj1mYWxzZSZmejQ0PW9uJmZ6MjIzPW9uJmZ6OTQ9b24mcmVnaW9ucz0mcHJpY2VGcm9tPSZwcmljZVRvPSZjdXJyZW5jeUlkPS0xJnB1Ymxpc2hEYXRlRnJvbT0mcHVibGlzaERhdGVUbz0mdXBkYXRlRGF0ZUZyb209JnVwZGF0ZURhdGVUbz0mc29ydEJ5PVVQREFURV9EQVRFJnNlYXJjaFN0cmluZz0nICsgbm9kZS5faWQ7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuXHJcbn1cclxuXHJcblxyXG5jb25zdCBtZXRhU2VydmljZSA9IG5ldyBNZXRhKCk7XHJcbmV4cG9ydCB7IG1ldGFTZXJ2aWNlIH07XHJcbiJdfQ==