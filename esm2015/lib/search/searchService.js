/**
 * @fileoverview added by tsickle
 * Generated from: lib/search/searchService.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/**
 * Created by radigost on 07.09.17.
 */
import * as _ from 'lodash';
import SearchType from '../domain/SearchType';
import { metaService } from '../meta/metaService';
import { searchResource } from './searchResource';
import { rsearchMeta } from '../meta/rsearchMeta';
class SearchService {
    /**
     * @param {?} metaService
     */
    constructor(metaService) {
        this.metaService = metaService;
    }
    /**
     * @return {?}
     */
    init() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            try {
                yield this.metaService.init();
            }
            catch (_a) {
                console.error("cannot init SearchService");
            }
        });
    }
    /**
     * @return {?}
     */
    getSearchableNodeTypes() {
        /** @type {?} */
        const searchableNodeTypes = {};
        // TODO rewrite to make rsearchMeta, metaService one service
        /** @type {?} */
        const nodeTypes = rsearchMeta.nodeTypes;
        _.forEach(nodeTypes, (/**
         * @param {?} data
         * @param {?} nodeType
         * @return {?}
         */
        (data, nodeType) => {
            // TODO if nodeType is serachABLE
            if (data.search) {
                searchableNodeTypes[nodeType] = SearchType(nodeType, data);
            }
        }));
        return searchableNodeTypes;
    }
    /**
     * @param {?} searchType
     * @param {?} query
     * @return {?}
     */
    searchByTypeAndText(searchType, query) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const filter = {};
            if (searchType && searchType.filter) {
                /** @type {?} */
                const activeRegion = _.find(searchType.filter.regions, { active: true });
                if (activeRegion && activeRegion.regionCode) {
                    /** @type {?} */
                    const filterRegion = { 'region_code.equals': activeRegion.regionCode };
                    _.assignIn(filter, filterRegion);
                }
            }
            return new Promise((/**
             * @param {?} resolve
             * @return {?}
             */
            (resolve) => tslib_1.__awaiter(this, void 0, void 0, function* () {
                try {
                    searchType.result = yield searchResource.searchRequest({
                        q: query,
                        nodeType: searchType.nodeType,
                        pageConfig: searchType.pageConfig,
                        filter,
                    });
                    resolve(searchType);
                }
                catch (err) {
                    console.log(err);
                    resolve({ result: null });
                }
            })));
        });
    }
    /**
     * @param {?} options
     * @return {?}
     */
    searchByTypeAndId(options) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const params = {
                id: options.id,
                type: options.searchtype || options.type,
            };
            try {
                return yield searchResource.searchByTypeAndId(params);
            }
            catch (err) {
                console.error(err);
                return {};
            }
        });
    }
}
if (false) {
    /** @type {?} */
    SearchService.prototype.metaService;
}
/** @type {?} */
const searchService = new SearchService(metaService);
searchService.init();
export { searchService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VhcmNoU2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bua2IvYW5ndWxhci1ua2ItbGliLyIsInNvdXJjZXMiOlsibGliL3NlYXJjaC9zZWFyY2hTZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7OztBQUdBLE9BQU8sS0FBSyxDQUFDLE1BQU0sUUFBUSxDQUFDO0FBQzVCLE9BQU8sVUFBVSxNQUFNLHNCQUFzQixDQUFDO0FBQzlDLE9BQU8sRUFBRSxXQUFXLEVBQVEsTUFBTSxxQkFBcUIsQ0FBQTtBQUN2RCxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sa0JBQWtCLENBQUM7QUFDbEQsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBRWxELE1BQU0sYUFBYTs7OztJQUVmLFlBQVksV0FBVztRQUNuQixJQUFJLENBQUMsV0FBVyxHQUFHLFdBQVcsQ0FBQztJQUNuQyxDQUFDOzs7O0lBQ0ssSUFBSTs7WUFDTixJQUFJO2dCQUNBLE1BQU0sSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLEVBQUUsQ0FBQTthQUNoQztZQUNELFdBQUs7Z0JBQ0QsT0FBTyxDQUFDLEtBQUssQ0FBQywyQkFBMkIsQ0FBQyxDQUFDO2FBQzlDO1FBRUwsQ0FBQztLQUFBOzs7O0lBQ0Qsc0JBQXNCOztjQUNaLG1CQUFtQixHQUFPLEVBQUU7OztjQUU1QixTQUFTLEdBQUcsV0FBVyxDQUFDLFNBQVM7UUFDdkMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxTQUFTOzs7OztRQUFFLENBQUMsSUFBSSxFQUFFLFFBQVEsRUFBRSxFQUFFO1lBQ3BDLGlDQUFpQztZQUNqQyxJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUU7Z0JBQ2IsbUJBQW1CLENBQUMsUUFBUSxDQUFDLEdBQUcsVUFBVSxDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsQ0FBQzthQUM5RDtRQUNMLENBQUMsRUFBQyxDQUFDO1FBQ0gsT0FBTyxtQkFBbUIsQ0FBQztJQUMvQixDQUFDOzs7Ozs7SUFFSyxtQkFBbUIsQ0FBQyxVQUFVLEVBQUUsS0FBSzs7O2tCQUNqQyxNQUFNLEdBQUcsRUFBRTtZQUVqQixJQUFJLFVBQVUsSUFBSSxVQUFVLENBQUMsTUFBTSxFQUFFOztzQkFDM0IsWUFBWSxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEVBQUUsRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFFLENBQUM7Z0JBQ3hFLElBQUksWUFBWSxJQUFJLFlBQVksQ0FBQyxVQUFVLEVBQUU7OzBCQUNuQyxZQUFZLEdBQUcsRUFBRSxvQkFBb0IsRUFBRSxZQUFZLENBQUMsVUFBVSxFQUFFO29CQUN0RSxDQUFDLENBQUMsUUFBUSxDQUFDLE1BQU0sRUFBRSxZQUFZLENBQUMsQ0FBQztpQkFDcEM7YUFDSjtZQUVELE9BQU8sSUFBSSxPQUFPOzs7O1lBQUMsQ0FBTyxPQUFPLEVBQUUsRUFBRTtnQkFDakMsSUFBSTtvQkFDQSxVQUFVLENBQUMsTUFBTSxHQUFHLE1BQU0sY0FBYyxDQUFDLGFBQWEsQ0FBQzt3QkFDbkQsQ0FBQyxFQUFFLEtBQUs7d0JBQ1IsUUFBUSxFQUFFLFVBQVUsQ0FBQyxRQUFRO3dCQUM3QixVQUFVLEVBQUUsVUFBVSxDQUFDLFVBQVU7d0JBQ2pDLE1BQU07cUJBQ1QsQ0FBQyxDQUFDO29CQUNILE9BQU8sQ0FBQyxVQUFVLENBQUMsQ0FBQztpQkFDdkI7Z0JBQUMsT0FBTyxHQUFHLEVBQUU7b0JBQ1YsT0FBTyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQztvQkFDakIsT0FBTyxDQUFDLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7aUJBQzdCO1lBQ0wsQ0FBQyxDQUFBLEVBQUMsQ0FBQztRQUNQLENBQUM7S0FBQTs7Ozs7SUFFSyxpQkFBaUIsQ0FBQyxPQUFPOzs7a0JBQ3JCLE1BQU0sR0FBRztnQkFDWCxFQUFFLEVBQUUsT0FBTyxDQUFDLEVBQUU7Z0JBQ2QsSUFBSSxFQUFFLE9BQU8sQ0FBQyxVQUFVLElBQUksT0FBTyxDQUFDLElBQUk7YUFDM0M7WUFDRCxJQUFJO2dCQUNBLE9BQU8sTUFBTSxjQUFjLENBQUMsaUJBQWlCLENBQUMsTUFBTSxDQUFDLENBQUM7YUFDekQ7WUFBQyxPQUFPLEdBQUcsRUFBRTtnQkFDVixPQUFPLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUNuQixPQUFPLEVBQUUsQ0FBQzthQUNiO1FBRUwsQ0FBQztLQUFBO0NBQ0o7OztJQWxFRyxvQ0FBaUI7OztNQW9FZixhQUFhLEdBQUcsSUFBSSxhQUFhLENBQUMsV0FBVyxDQUFDO0FBQ3BELGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQTtBQUNwQixPQUFPLEVBQUUsYUFBYSxFQUFFLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyIvKipcclxuICogQ3JlYXRlZCBieSByYWRpZ29zdCBvbiAwNy4wOS4xNy5cclxuICovXHJcbmltcG9ydCAqIGFzIF8gZnJvbSAnbG9kYXNoJztcclxuaW1wb3J0IFNlYXJjaFR5cGUgZnJvbSAnLi4vZG9tYWluL1NlYXJjaFR5cGUnO1xyXG5pbXBvcnQgeyBtZXRhU2VydmljZSwgTWV0YSB9IGZyb20gJy4uL21ldGEvbWV0YVNlcnZpY2UnXHJcbmltcG9ydCB7IHNlYXJjaFJlc291cmNlIH0gZnJvbSAnLi9zZWFyY2hSZXNvdXJjZSc7XHJcbmltcG9ydCB7IHJzZWFyY2hNZXRhIH0gZnJvbSAnLi4vbWV0YS9yc2VhcmNoTWV0YSc7XHJcblxyXG5jbGFzcyBTZWFyY2hTZXJ2aWNlIHtcclxuICAgIG1ldGFTZXJ2aWNlOiBNZXRhXHJcbiAgICBjb25zdHJ1Y3RvcihtZXRhU2VydmljZSkge1xyXG4gICAgICAgIHRoaXMubWV0YVNlcnZpY2UgPSBtZXRhU2VydmljZTtcclxuICAgIH1cclxuICAgIGFzeW5jIGluaXQoKSB7XHJcbiAgICAgICAgdHJ5IHtcclxuICAgICAgICAgICAgYXdhaXQgdGhpcy5tZXRhU2VydmljZS5pbml0KClcclxuICAgICAgICB9XHJcbiAgICAgICAgY2F0Y2h7XHJcbiAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoXCJjYW5ub3QgaW5pdCBTZWFyY2hTZXJ2aWNlXCIpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICB9XHJcbiAgICBnZXRTZWFyY2hhYmxlTm9kZVR5cGVzKCkge1xyXG4gICAgICAgIGNvbnN0IHNlYXJjaGFibGVOb2RlVHlwZXM6IHt9ID0ge307XHJcbiAgICAgICAgLy8gVE9ETyByZXdyaXRlIHRvIG1ha2UgcnNlYXJjaE1ldGEsIG1ldGFTZXJ2aWNlIG9uZSBzZXJ2aWNlXHJcbiAgICAgICAgY29uc3Qgbm9kZVR5cGVzID0gcnNlYXJjaE1ldGEubm9kZVR5cGVzO1xyXG4gICAgICAgIF8uZm9yRWFjaChub2RlVHlwZXMsIChkYXRhLCBub2RlVHlwZSkgPT4ge1xyXG4gICAgICAgICAgICAvLyBUT0RPIGlmIG5vZGVUeXBlIGlzIHNlcmFjaEFCTEVcclxuICAgICAgICAgICAgaWYgKGRhdGEuc2VhcmNoKSB7XHJcbiAgICAgICAgICAgICAgICBzZWFyY2hhYmxlTm9kZVR5cGVzW25vZGVUeXBlXSA9IFNlYXJjaFR5cGUobm9kZVR5cGUsIGRhdGEpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgcmV0dXJuIHNlYXJjaGFibGVOb2RlVHlwZXM7XHJcbiAgICB9XHJcblxyXG4gICAgYXN5bmMgc2VhcmNoQnlUeXBlQW5kVGV4dChzZWFyY2hUeXBlLCBxdWVyeSkge1xyXG4gICAgICAgIGNvbnN0IGZpbHRlciA9IHt9O1xyXG5cclxuICAgICAgICBpZiAoc2VhcmNoVHlwZSAmJiBzZWFyY2hUeXBlLmZpbHRlcikge1xyXG4gICAgICAgICAgICBjb25zdCBhY3RpdmVSZWdpb24gPSBfLmZpbmQoc2VhcmNoVHlwZS5maWx0ZXIucmVnaW9ucywgeyBhY3RpdmU6IHRydWUgfSk7XHJcbiAgICAgICAgICAgIGlmIChhY3RpdmVSZWdpb24gJiYgYWN0aXZlUmVnaW9uLnJlZ2lvbkNvZGUpIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGZpbHRlclJlZ2lvbiA9IHsgJ3JlZ2lvbl9jb2RlLmVxdWFscyc6IGFjdGl2ZVJlZ2lvbi5yZWdpb25Db2RlIH07XHJcbiAgICAgICAgICAgICAgICBfLmFzc2lnbkluKGZpbHRlciwgZmlsdGVyUmVnaW9uKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIG5ldyBQcm9taXNlKGFzeW5jIChyZXNvbHZlKSA9PiB7XHJcbiAgICAgICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgICAgICBzZWFyY2hUeXBlLnJlc3VsdCA9IGF3YWl0IHNlYXJjaFJlc291cmNlLnNlYXJjaFJlcXVlc3Qoe1xyXG4gICAgICAgICAgICAgICAgICAgIHE6IHF1ZXJ5LFxyXG4gICAgICAgICAgICAgICAgICAgIG5vZGVUeXBlOiBzZWFyY2hUeXBlLm5vZGVUeXBlLFxyXG4gICAgICAgICAgICAgICAgICAgIHBhZ2VDb25maWc6IHNlYXJjaFR5cGUucGFnZUNvbmZpZyxcclxuICAgICAgICAgICAgICAgICAgICBmaWx0ZXIsXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIHJlc29sdmUoc2VhcmNoVHlwZSk7XHJcbiAgICAgICAgICAgIH0gY2F0Y2ggKGVycikge1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coZXJyKTtcclxuICAgICAgICAgICAgICAgIHJlc29sdmUoeyByZXN1bHQ6IG51bGwgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBhc3luYyBzZWFyY2hCeVR5cGVBbmRJZChvcHRpb25zKSB7XHJcbiAgICAgICAgY29uc3QgcGFyYW1zID0ge1xyXG4gICAgICAgICAgICBpZDogb3B0aW9ucy5pZCxcclxuICAgICAgICAgICAgdHlwZTogb3B0aW9ucy5zZWFyY2h0eXBlIHx8IG9wdGlvbnMudHlwZSxcclxuICAgICAgICB9O1xyXG4gICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgIHJldHVybiBhd2FpdCBzZWFyY2hSZXNvdXJjZS5zZWFyY2hCeVR5cGVBbmRJZChwYXJhbXMpO1xyXG4gICAgICAgIH0gY2F0Y2ggKGVycikge1xyXG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKGVycik7XHJcbiAgICAgICAgICAgIHJldHVybiB7fTtcclxuICAgICAgICB9XHJcblxyXG4gICAgfVxyXG59XHJcblxyXG5jb25zdCBzZWFyY2hTZXJ2aWNlID0gbmV3IFNlYXJjaFNlcnZpY2UobWV0YVNlcnZpY2UpO1xyXG5zZWFyY2hTZXJ2aWNlLmluaXQoKVxyXG5leHBvcnQgeyBzZWFyY2hTZXJ2aWNlIH07XHJcbiJdfQ==