/**
 * @fileoverview added by tsickle
 * Generated from: lib/search/SearchResourceClass.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import axios from 'axios';
import { Cache } from '../cache/Cache';
import { NODE_TYPE as TYPE } from '../domain/TYPES';
import { head, get, debounce, assignIn, find, isEqual } from 'lodash';
import { CacheElement } from '../cache/CacheElement';
/** @type {?} */
const search = axios.create({
    baseURL: '/nkbrelation/api/nodes'
});
//old but fast api , maybe after _relations=false new is the same on speed - TODO check speen and if neer is faster - move to it
/** @type {?} */
const company = axios.create({
    baseURL: '/nkbrelation/api/company'
});
export class SearchResource {
    /**
     * @param {?} metaService
     */
    constructor(metaService) {
        Object.assign(this, {
            cache: new Cache(
            // {keyFunction: (type, idField, id) => `/nkbrelation/api/nodes/${type}?${idField}.equals=${id}`}
            ),
            queue: [],
            queueType: null,
            queueResults: [],
            queueParams: {},
            pedning: false,
            debounce: debounce(this.searchByTypeAndIdsFromQueue, 500, { maxWait: 1000 }),
            metaService: metaService
        });
        metaService.init();
    }
    /**
     * @return {?}
     */
    updateQueueSearchByTypeAndIds() {
        return this.queue.length < 10 ? this.debounce() : this.searchByTypeAndIdsFromQueue();
    }
    /**
     * @param {?} options
     * @return {?}
     */
    searchRequest(options) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const params = assignIn({}, options.filter, options.pageConfig, {
                q: options.q,
                exclude: '_relations'
            });
            /** @type {?} */
            const res = yield search.get(`/${options.nodeType}`, { params });
            return get(res, 'data', []);
        });
    }
    /**
     * @param {?} options
     * @return {?}
     */
    searchByTypeAndId(options) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            if (!this.metaService.inited)
                yield this.metaService.init();
            /** @type {?} */
            const idField = this.metaService.getIdFieldForType(options.type);
            return search.get(`${options.type}?${idField}.equals=${options.id}&exclude=_relations`, { params: options }).then((/**
             * @param {?} res
             * @return {?}
             */
            res => head(res.data.list)));
        });
    }
    /**
     * @param {?=} options
     * @return {?}
     */
    searchByTypeAndIdQueued(options = { type: TYPE.COMPANY, id: null, params: {} }) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            return new Promise((/**
             * @param {?} resolve
             * @return {?}
             */
            (resolve) => tslib_1.__awaiter(this, void 0, void 0, function* () {
                /** @type {?} */
                const doInterval = (/**
                 * @return {?}
                 */
                () => {
                    /** @type {?} */
                    const intervalID = setInterval((/**
                     * @return {?}
                     */
                    () => {
                        if (!this.pending) {
                            /** @type {?} */
                            let res = find(this.queueResults, (/**
                             * @param {?} company
                             * @return {?}
                             */
                            (company) => isEqual(`${company[idField]}`, `${options.id}`)));
                            if (res) {
                                clearInterval(intervalID);
                                resolve(res);
                            }
                        }
                    }), 100);
                });
                /** @type {?} */
                const idField = this.metaService.getIdFieldForType(options.type);
                /** @type {?} */
                const inCache = this.cache.get(`/nkbrelation/api/nodes/${options.type}?${idField}.equals=${options.id}`);
                if (inCache) {
                    resolve(inCache.head());
                }
                /** @type {?} */
                const isInqueue = this.queue.includes(options.id);
                if (!isInqueue) {
                    if (this.queueType === options.type || this.queueType === null) {
                        this.queueType = options.type;
                        this.queueParams = options.params;
                        this.queue.push(options.id);
                        this.updateQueueSearchByTypeAndIds();
                        doInterval();
                    }
                    else if (this.queueType !== options.type) {
                        /** @type {?} */
                        const res = yield this.cache.try(`/nkbrelation/api/nodes/${options.type}?${idField}.equals=${options.id}`).then((/**
                         * @param {?} r
                         * @return {?}
                         */
                        r => head(r)));
                        resolve(res);
                    }
                }
                else {
                    doInterval();
                }
            })));
        });
    }
    /**
     * @return {?}
     */
    searchByTypeAndIdsFromQueue() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            this.pending = true;
            /** @type {?} */
            const type = this.queueType;
            /** @type {?} */
            const idField = this.metaService.getIdFieldForType(type);
            this.searchByTypeAndIds({ type, ids: this.queue, params: this.queueParams }).then((/**
             * @param {?} res
             * @return {?}
             */
            res => {
                this.queueResults = this.queueResults.concat(res);
                res.forEach((/**
                 * @param {?} el
                 * @return {?}
                 */
                el => {
                    this.cache.set(`/nkbrelation/api/nodes/${type}?${idField}.equals=${el[idField]}`, new CacheElement(Promise.resolve(el)));
                }));
                this.pending = false;
            }));
            this.queueType = null;
            this.queue = [];
        });
    }
    /**
     * @param {?=} options
     * @return {?}
     */
    searchByTypeAndIds(options = { type: TYPE.COMPANY, ids: [], params: {} }) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            let result;
            yield this.metaService.init();
            if (options.ids.length < 1) {
                return Promise.resolve({});
            }
            /** @type {?} */
            const urlparams = new URLSearchParams();
            /** @type {?} */
            const idField = this.metaService.getIdFieldForType(options.type);
            options.ids.forEach((/**
             * @param {?} id
             * @return {?}
             */
            id => urlparams.append(`${idField}.equals`, id)));
            if (options.params)
                Object.keys(options.params).forEach((/**
                 * @param {?} key
                 * @return {?}
                 */
                key => urlparams.append(key, options.params[key])));
            /** @type {?} */
            const url = `/nkbrelation/api/nodes/${options.type}?${urlparams.toString()}`;
            result = yield this.cache.try(url).then((/**
             * @param {?} res
             * @return {?}
             */
            (res) => res.list()));
            return result;
        });
    }
    /**
     * @param {?} params
     * @return {?}
     */
    searchCompany(params) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const res = yield company.get('/', { params });
            return get(res, 'data', []);
        });
    }
}
if (false) {
    /** @type {?} */
    SearchResource.prototype.queue;
    /** @type {?} */
    SearchResource.prototype.debounce;
    /** @type {?} */
    SearchResource.prototype.pending;
    /** @type {?} */
    SearchResource.prototype.queueResults;
    /** @type {?} */
    SearchResource.prototype.cache;
    /** @type {?} */
    SearchResource.prototype.queueType;
    /** @type {?} */
    SearchResource.prototype.queueParams;
    /** @type {?} */
    SearchResource.prototype.metaService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiU2VhcmNoUmVzb3VyY2VDbGFzcy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bua2IvYW5ndWxhci1ua2ItbGliLyIsInNvdXJjZXMiOlsibGliL3NlYXJjaC9TZWFyY2hSZXNvdXJjZUNsYXNzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLE9BQU8sS0FBSyxNQUFNLE9BQU8sQ0FBQTtBQUN6QixPQUFPLEVBQUUsS0FBSyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDdkMsT0FBTyxFQUFFLFNBQVMsSUFBSSxJQUFJLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUNwRCxPQUFPLEVBQUUsSUFBSSxFQUFFLEdBQUcsRUFBRSxRQUFRLEVBQUUsUUFBUSxFQUFFLElBQUksRUFBRSxPQUFPLEVBQUUsTUFBTSxRQUFRLENBQUM7QUFDdEUsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLHVCQUF1QixDQUFDOztNQUcvQyxNQUFNLEdBQUcsS0FBSyxDQUFDLE1BQU0sQ0FBQztJQUN4QixPQUFPLEVBQUUsd0JBQXdCO0NBQ3BDLENBQUM7OztNQUlJLE9BQU8sR0FBRyxLQUFLLENBQUMsTUFBTSxDQUFDO0lBQ3pCLE9BQU8sRUFBRSwwQkFBMEI7Q0FDdEMsQ0FBQztBQUVGLE1BQU0sT0FBTyxjQUFjOzs7O0lBU3ZCLFlBQVksV0FBVztRQUNuQixNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRTtZQUNoQixLQUFLLEVBQUUsSUFBSSxLQUFLO1lBQ1osaUdBQWlHO2FBQ3BHO1lBQ0QsS0FBSyxFQUFFLEVBQUU7WUFDVCxTQUFTLEVBQUUsSUFBSTtZQUNmLFlBQVksRUFBRSxFQUFFO1lBQ2hCLFdBQVcsRUFBRSxFQUFFO1lBQ2YsT0FBTyxFQUFFLEtBQUs7WUFDZCxRQUFRLEVBQUUsUUFBUSxDQUFDLElBQUksQ0FBQywyQkFBMkIsRUFBRSxHQUFHLEVBQUUsRUFBRSxPQUFPLEVBQUUsSUFBSSxFQUFFLENBQUM7WUFDNUUsV0FBVyxFQUFFLFdBQVc7U0FDM0IsQ0FBQyxDQUFDO1FBRUgsV0FBVyxDQUFDLElBQUksRUFBRSxDQUFDO0lBQ3ZCLENBQUM7Ozs7SUFFRCw2QkFBNkI7UUFDekIsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLDJCQUEyQixFQUFFLENBQUM7SUFDekYsQ0FBQzs7Ozs7SUFFSyxhQUFhLENBQUMsT0FBTzs7O2tCQUNqQixNQUFNLEdBQUcsUUFBUSxDQUFDLEVBQUUsRUFDdEIsT0FBTyxDQUFDLE1BQU0sRUFDZCxPQUFPLENBQUMsVUFBVSxFQUNsQjtnQkFDSSxDQUFDLEVBQUUsT0FBTyxDQUFDLENBQUM7Z0JBQ1osT0FBTyxFQUFFLFlBQVk7YUFDeEIsQ0FDSjs7a0JBQ0ssR0FBRyxHQUFHLE1BQU0sTUFBTSxDQUFDLEdBQUcsQ0FBQyxJQUFJLE9BQU8sQ0FBQyxRQUFRLEVBQUUsRUFBRSxFQUFFLE1BQU0sRUFBRSxDQUFDO1lBQ2hFLE9BQU8sR0FBRyxDQUFDLEdBQUcsRUFBRSxNQUFNLEVBQUUsRUFBRSxDQUFDLENBQUM7UUFDaEMsQ0FBQztLQUFBOzs7OztJQUVLLGlCQUFpQixDQUFDLE9BQU87O1lBQzNCLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU07Z0JBQUUsTUFBTSxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksRUFBRSxDQUFDOztrQkFDdEQsT0FBTyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsaUJBQWlCLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQztZQUNoRSxPQUFPLE1BQU0sQ0FBQyxHQUFHLENBQUMsR0FBRyxPQUFPLENBQUMsSUFBSSxJQUFJLE9BQU8sV0FBVyxPQUFPLENBQUMsRUFBRSxxQkFBcUIsRUFBRSxFQUFFLE1BQU0sRUFBRSxPQUFPLEVBQUUsQ0FBQyxDQUFDLElBQUk7Ozs7WUFBQyxHQUFHLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFDLENBQUM7UUFDbEosQ0FBQztLQUFBOzs7OztJQUVLLHVCQUF1QixDQUFDLE9BQU8sR0FBRyxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsT0FBTyxFQUFFLEVBQUUsRUFBRSxJQUFJLEVBQUUsTUFBTSxFQUFFLEVBQUUsRUFBRTs7WUFDaEYsT0FBTyxJQUFJLE9BQU87Ozs7WUFBQyxDQUFPLE9BQU8sRUFBRSxFQUFFOztzQkFDM0IsVUFBVTs7O2dCQUFHLEdBQUcsRUFBRTs7MEJBQ2QsVUFBVSxHQUFHLFdBQVc7OztvQkFBQyxHQUFHLEVBQUU7d0JBQ2hDLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFOztnQ0FDWCxHQUFHLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZOzs7OzRCQUFFLENBQUMsT0FBTyxFQUFFLEVBQUUsQ0FBQyxPQUFPLENBQUMsR0FBRyxPQUFPLENBQUMsT0FBTyxDQUFDLEVBQUUsRUFBRSxHQUFHLE9BQU8sQ0FBQyxFQUFFLEVBQUUsQ0FBQyxFQUFDOzRCQUMvRixJQUFJLEdBQUcsRUFBRTtnQ0FDTCxhQUFhLENBQUMsVUFBVSxDQUFDLENBQUM7Z0NBQzFCLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQzs2QkFDaEI7eUJBQ0o7b0JBQ0wsQ0FBQyxHQUFFLEdBQUcsQ0FBQztnQkFDWCxDQUFDLENBQUE7O3NCQUVLLE9BQU8sR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLGlCQUFpQixDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUM7O3NCQUMxRCxPQUFPLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsMEJBQTBCLE9BQU8sQ0FBQyxJQUFJLElBQUksT0FBTyxXQUFXLE9BQU8sQ0FBQyxFQUFFLEVBQUUsQ0FBQztnQkFDeEcsSUFBSSxPQUFPLEVBQUU7b0JBQ1QsT0FBTyxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDO2lCQUMzQjs7c0JBRUssU0FBUyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUM7Z0JBQ2pELElBQUksQ0FBQyxTQUFTLEVBQUU7b0JBQ1osSUFBSSxJQUFJLENBQUMsU0FBUyxLQUFLLE9BQU8sQ0FBQyxJQUFJLElBQUksSUFBSSxDQUFDLFNBQVMsS0FBSyxJQUFJLEVBQUU7d0JBQzVELElBQUksQ0FBQyxTQUFTLEdBQUcsT0FBTyxDQUFDLElBQUksQ0FBQzt3QkFDOUIsSUFBSSxDQUFDLFdBQVcsR0FBRyxPQUFPLENBQUMsTUFBTSxDQUFDO3dCQUNsQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDLENBQUM7d0JBQzVCLElBQUksQ0FBQyw2QkFBNkIsRUFBRSxDQUFDO3dCQUNyQyxVQUFVLEVBQUUsQ0FBQztxQkFDaEI7eUJBQU0sSUFBSSxJQUFJLENBQUMsU0FBUyxLQUFLLE9BQU8sQ0FBQyxJQUFJLEVBQUU7OzhCQUNsQyxHQUFHLEdBQUcsTUFBTSxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQywwQkFBMEIsT0FBTyxDQUFDLElBQUksSUFBSSxPQUFPLFdBQVcsT0FBTyxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUMsSUFBSTs7Ozt3QkFBQyxDQUFDLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBQzt3QkFDN0gsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDO3FCQUNoQjtpQkFDSjtxQkFBTTtvQkFDSCxVQUFVLEVBQUUsQ0FBQztpQkFDaEI7WUFDTCxDQUFDLENBQUEsRUFBQyxDQUFBO1FBQ04sQ0FBQztLQUFBOzs7O0lBRUssMkJBQTJCOztZQUM3QixJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQzs7a0JBQ2QsSUFBSSxHQUFHLElBQUksQ0FBQyxTQUFTOztrQkFDckIsT0FBTyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDO1lBQ3hELElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxFQUFFLElBQUksRUFBRSxHQUFHLEVBQUUsSUFBSSxDQUFDLEtBQUssRUFBRSxNQUFNLEVBQUUsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUMsSUFBSTs7OztZQUFDLEdBQUcsQ0FBQyxFQUFFO2dCQUNwRixJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUVsRCxHQUFHLENBQUMsT0FBTzs7OztnQkFBQyxFQUFFLENBQUMsRUFBRTtvQkFDYixJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQywwQkFBMEIsSUFBSSxJQUFJLE9BQU8sV0FBVyxFQUFFLENBQUMsT0FBTyxDQUFDLEVBQUUsRUFBRSxJQUFJLFlBQVksQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQTtnQkFDNUgsQ0FBQyxFQUFDLENBQUM7Z0JBQ0gsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7WUFDekIsQ0FBQyxFQUFDLENBQUM7WUFDSCxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztZQUN0QixJQUFJLENBQUMsS0FBSyxHQUFHLEVBQUUsQ0FBQztRQUNwQixDQUFDO0tBQUE7Ozs7O0lBRUssa0JBQWtCLENBQUMsT0FBTyxHQUFHLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxPQUFPLEVBQUUsR0FBRyxFQUFFLEVBQUUsRUFBRSxNQUFNLEVBQUUsRUFBRSxFQUFFOzs7Z0JBQ3RFLE1BQU07WUFDVixNQUFNLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDOUIsSUFBSSxPQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7Z0JBQ3hCLE9BQU8sT0FBTyxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUMsQ0FBQTthQUM3Qjs7a0JBQ0ssU0FBUyxHQUFHLElBQUksZUFBZSxFQUFFOztrQkFDakMsT0FBTyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsaUJBQWlCLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQztZQUNoRSxPQUFPLENBQUMsR0FBRyxDQUFDLE9BQU87Ozs7WUFBQyxFQUFFLENBQUMsRUFBRSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsR0FBRyxPQUFPLFNBQVMsRUFBRSxFQUFFLENBQUMsRUFBQyxDQUFDO1lBQ3JFLElBQUksT0FBTyxDQUFDLE1BQU07Z0JBQUUsTUFBTSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsT0FBTzs7OztnQkFBQyxHQUFHLENBQUMsRUFBRSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsR0FBRyxFQUFFLE9BQU8sQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBQyxDQUFDOztrQkFDckcsR0FBRyxHQUFHLDBCQUEwQixPQUFPLENBQUMsSUFBSSxJQUFJLFNBQVMsQ0FBQyxRQUFRLEVBQUUsRUFBRTtZQUM1RSxNQUFNLEdBQUcsTUFBTSxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJOzs7O1lBQUMsQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsRUFBQyxDQUFDO1lBQzdELE9BQU8sTUFBTSxDQUFBO1FBQ2pCLENBQUM7S0FBQTs7Ozs7SUFFSyxhQUFhLENBQUMsTUFBTTs7O2tCQUNoQixHQUFHLEdBQUcsTUFBTSxPQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsRUFBRSxFQUFFLE1BQU0sRUFBRSxDQUFDO1lBQzlDLE9BQU8sR0FBRyxDQUFDLEdBQUcsRUFBRSxNQUFNLEVBQUUsRUFBRSxDQUFDLENBQUM7UUFDaEMsQ0FBQztLQUFBO0NBQ0o7OztJQXpIRywrQkFBTTs7SUFDTixrQ0FBUzs7SUFDVCxpQ0FBUTs7SUFDUixzQ0FBYTs7SUFDYiwrQkFBTTs7SUFDTixtQ0FBVTs7SUFDVixxQ0FBWTs7SUFDWixxQ0FBa0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgYXhpb3MgZnJvbSAnYXhpb3MnXG5pbXBvcnQgeyBDYWNoZSB9IGZyb20gJy4uL2NhY2hlL0NhY2hlJztcbmltcG9ydCB7IE5PREVfVFlQRSBhcyBUWVBFIH0gZnJvbSAnLi4vZG9tYWluL1RZUEVTJztcbmltcG9ydCB7IGhlYWQsIGdldCwgZGVib3VuY2UsIGFzc2lnbkluLCBmaW5kLCBpc0VxdWFsIH0gZnJvbSAnbG9kYXNoJztcbmltcG9ydCB7IENhY2hlRWxlbWVudCB9IGZyb20gJy4uL2NhY2hlL0NhY2hlRWxlbWVudCc7XG5pbXBvcnQgeyBNZXRhIH0gZnJvbSAnLi4vbWV0YS9tZXRhU2VydmljZSc7XG5cbmNvbnN0IHNlYXJjaCA9IGF4aW9zLmNyZWF0ZSh7XG4gICAgYmFzZVVSTDogJy9ua2JyZWxhdGlvbi9hcGkvbm9kZXMnXG59KTtcblxuXG4vL29sZCBidXQgZmFzdCBhcGkgLCBtYXliZSBhZnRlciBfcmVsYXRpb25zPWZhbHNlIG5ldyBpcyB0aGUgc2FtZSBvbiBzcGVlZCAtIFRPRE8gY2hlY2sgc3BlZW4gYW5kIGlmIG5lZXIgaXMgZmFzdGVyIC0gbW92ZSB0byBpdFxuY29uc3QgY29tcGFueSA9IGF4aW9zLmNyZWF0ZSh7XG4gICAgYmFzZVVSTDogJy9ua2JyZWxhdGlvbi9hcGkvY29tcGFueSdcbn0pO1xuXG5leHBvcnQgY2xhc3MgU2VhcmNoUmVzb3VyY2Uge1xuICAgIHF1ZXVlO1xuICAgIGRlYm91bmNlO1xuICAgIHBlbmRpbmc7XG4gICAgcXVldWVSZXN1bHRzO1xuICAgIGNhY2hlO1xuICAgIHF1ZXVlVHlwZTtcbiAgICBxdWV1ZVBhcmFtcztcbiAgICBtZXRhU2VydmljZTogTWV0YTtcbiAgICBjb25zdHJ1Y3RvcihtZXRhU2VydmljZSkge1xuICAgICAgICBPYmplY3QuYXNzaWduKHRoaXMsIHtcbiAgICAgICAgICAgIGNhY2hlOiBuZXcgQ2FjaGUoXG4gICAgICAgICAgICAgICAgLy8ge2tleUZ1bmN0aW9uOiAodHlwZSwgaWRGaWVsZCwgaWQpID0+IGAvbmticmVsYXRpb24vYXBpL25vZGVzLyR7dHlwZX0/JHtpZEZpZWxkfS5lcXVhbHM9JHtpZH1gfVxuICAgICAgICAgICAgKSxcbiAgICAgICAgICAgIHF1ZXVlOiBbXSxcbiAgICAgICAgICAgIHF1ZXVlVHlwZTogbnVsbCxcbiAgICAgICAgICAgIHF1ZXVlUmVzdWx0czogW10sXG4gICAgICAgICAgICBxdWV1ZVBhcmFtczoge30sXG4gICAgICAgICAgICBwZWRuaW5nOiBmYWxzZSxcbiAgICAgICAgICAgIGRlYm91bmNlOiBkZWJvdW5jZSh0aGlzLnNlYXJjaEJ5VHlwZUFuZElkc0Zyb21RdWV1ZSwgNTAwLCB7IG1heFdhaXQ6IDEwMDAgfSksXG4gICAgICAgICAgICBtZXRhU2VydmljZTogbWV0YVNlcnZpY2VcbiAgICAgICAgfSk7XG5cbiAgICAgICAgbWV0YVNlcnZpY2UuaW5pdCgpO1xuICAgIH1cblxuICAgIHVwZGF0ZVF1ZXVlU2VhcmNoQnlUeXBlQW5kSWRzKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5xdWV1ZS5sZW5ndGggPCAxMCA/IHRoaXMuZGVib3VuY2UoKSA6IHRoaXMuc2VhcmNoQnlUeXBlQW5kSWRzRnJvbVF1ZXVlKCk7XG4gICAgfVxuXG4gICAgYXN5bmMgc2VhcmNoUmVxdWVzdChvcHRpb25zKSB7XG4gICAgICAgIGNvbnN0IHBhcmFtcyA9IGFzc2lnbkluKHt9LFxuICAgICAgICAgICAgb3B0aW9ucy5maWx0ZXIsXG4gICAgICAgICAgICBvcHRpb25zLnBhZ2VDb25maWcsXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgcTogb3B0aW9ucy5xLFxuICAgICAgICAgICAgICAgIGV4Y2x1ZGU6ICdfcmVsYXRpb25zJ1xuICAgICAgICAgICAgfVxuICAgICAgICApO1xuICAgICAgICBjb25zdCByZXMgPSBhd2FpdCBzZWFyY2guZ2V0KGAvJHtvcHRpb25zLm5vZGVUeXBlfWAsIHsgcGFyYW1zIH0pO1xuICAgICAgICByZXR1cm4gZ2V0KHJlcywgJ2RhdGEnLCBbXSk7XG4gICAgfVxuXG4gICAgYXN5bmMgc2VhcmNoQnlUeXBlQW5kSWQob3B0aW9ucykge1xuICAgICAgICBpZiAoIXRoaXMubWV0YVNlcnZpY2UuaW5pdGVkKSBhd2FpdCB0aGlzLm1ldGFTZXJ2aWNlLmluaXQoKTtcbiAgICAgICAgY29uc3QgaWRGaWVsZCA9IHRoaXMubWV0YVNlcnZpY2UuZ2V0SWRGaWVsZEZvclR5cGUob3B0aW9ucy50eXBlKTtcbiAgICAgICAgcmV0dXJuIHNlYXJjaC5nZXQoYCR7b3B0aW9ucy50eXBlfT8ke2lkRmllbGR9LmVxdWFscz0ke29wdGlvbnMuaWR9JmV4Y2x1ZGU9X3JlbGF0aW9uc2AsIHsgcGFyYW1zOiBvcHRpb25zIH0pLnRoZW4ocmVzID0+IGhlYWQocmVzLmRhdGEubGlzdCkpO1xuICAgIH1cblxuICAgIGFzeW5jIHNlYXJjaEJ5VHlwZUFuZElkUXVldWVkKG9wdGlvbnMgPSB7IHR5cGU6IFRZUEUuQ09NUEFOWSwgaWQ6IG51bGwsIHBhcmFtczoge30gfSkge1xuICAgICAgICByZXR1cm4gbmV3IFByb21pc2UoYXN5bmMgKHJlc29sdmUpID0+IHtcbiAgICAgICAgICAgIGNvbnN0IGRvSW50ZXJ2YWwgPSAoKSA9PiB7XG4gICAgICAgICAgICAgICAgY29uc3QgaW50ZXJ2YWxJRCA9IHNldEludGVydmFsKCgpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKCF0aGlzLnBlbmRpbmcpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGxldCByZXMgPSBmaW5kKHRoaXMucXVldWVSZXN1bHRzLCAoY29tcGFueSkgPT4gaXNFcXVhbChgJHtjb21wYW55W2lkRmllbGRdfWAsIGAke29wdGlvbnMuaWR9YCkpXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAocmVzKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY2xlYXJJbnRlcnZhbChpbnRlcnZhbElEKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXNvbHZlKHJlcyk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9LCAxMDApO1xuICAgICAgICAgICAgfTtcblxuICAgICAgICAgICAgY29uc3QgaWRGaWVsZCA9IHRoaXMubWV0YVNlcnZpY2UuZ2V0SWRGaWVsZEZvclR5cGUob3B0aW9ucy50eXBlKTtcbiAgICAgICAgICAgIGNvbnN0IGluQ2FjaGUgPSB0aGlzLmNhY2hlLmdldChgL25rYnJlbGF0aW9uL2FwaS9ub2Rlcy8ke29wdGlvbnMudHlwZX0/JHtpZEZpZWxkfS5lcXVhbHM9JHtvcHRpb25zLmlkfWApO1xuICAgICAgICAgICAgaWYgKGluQ2FjaGUpIHtcbiAgICAgICAgICAgICAgICByZXNvbHZlKGluQ2FjaGUuaGVhZCgpKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgY29uc3QgaXNJbnF1ZXVlID0gdGhpcy5xdWV1ZS5pbmNsdWRlcyhvcHRpb25zLmlkKTtcbiAgICAgICAgICAgIGlmICghaXNJbnF1ZXVlKSB7XG4gICAgICAgICAgICAgICAgaWYgKHRoaXMucXVldWVUeXBlID09PSBvcHRpb25zLnR5cGUgfHwgdGhpcy5xdWV1ZVR5cGUgPT09IG51bGwpIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5xdWV1ZVR5cGUgPSBvcHRpb25zLnR5cGU7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMucXVldWVQYXJhbXMgPSBvcHRpb25zLnBhcmFtcztcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5xdWV1ZS5wdXNoKG9wdGlvbnMuaWQpO1xuICAgICAgICAgICAgICAgICAgICB0aGlzLnVwZGF0ZVF1ZXVlU2VhcmNoQnlUeXBlQW5kSWRzKCk7XG4gICAgICAgICAgICAgICAgICAgIGRvSW50ZXJ2YWwoKTtcbiAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKHRoaXMucXVldWVUeXBlICE9PSBvcHRpb25zLnR5cGUpIHtcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgcmVzID0gYXdhaXQgdGhpcy5jYWNoZS50cnkoYC9ua2JyZWxhdGlvbi9hcGkvbm9kZXMvJHtvcHRpb25zLnR5cGV9PyR7aWRGaWVsZH0uZXF1YWxzPSR7b3B0aW9ucy5pZH1gKS50aGVuKHIgPT4gaGVhZChyKSk7XG4gICAgICAgICAgICAgICAgICAgIHJlc29sdmUocmVzKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIGRvSW50ZXJ2YWwoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSlcbiAgICB9XG5cbiAgICBhc3luYyBzZWFyY2hCeVR5cGVBbmRJZHNGcm9tUXVldWUoKSB7XG4gICAgICAgIHRoaXMucGVuZGluZyA9IHRydWU7XG4gICAgICAgIGNvbnN0IHR5cGUgPSB0aGlzLnF1ZXVlVHlwZTtcbiAgICAgICAgY29uc3QgaWRGaWVsZCA9IHRoaXMubWV0YVNlcnZpY2UuZ2V0SWRGaWVsZEZvclR5cGUodHlwZSk7XG4gICAgICAgIHRoaXMuc2VhcmNoQnlUeXBlQW5kSWRzKHsgdHlwZSwgaWRzOiB0aGlzLnF1ZXVlLCBwYXJhbXM6IHRoaXMucXVldWVQYXJhbXMgfSkudGhlbihyZXMgPT4ge1xuICAgICAgICAgICAgdGhpcy5xdWV1ZVJlc3VsdHMgPSB0aGlzLnF1ZXVlUmVzdWx0cy5jb25jYXQocmVzKTtcblxuICAgICAgICAgICAgcmVzLmZvckVhY2goZWwgPT4ge1xuICAgICAgICAgICAgICAgIHRoaXMuY2FjaGUuc2V0KGAvbmticmVsYXRpb24vYXBpL25vZGVzLyR7dHlwZX0/JHtpZEZpZWxkfS5lcXVhbHM9JHtlbFtpZEZpZWxkXX1gLCBuZXcgQ2FjaGVFbGVtZW50KFByb21pc2UucmVzb2x2ZShlbCkpKVxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB0aGlzLnBlbmRpbmcgPSBmYWxzZTtcbiAgICAgICAgfSk7XG4gICAgICAgIHRoaXMucXVldWVUeXBlID0gbnVsbDtcbiAgICAgICAgdGhpcy5xdWV1ZSA9IFtdO1xuICAgIH1cblxuICAgIGFzeW5jIHNlYXJjaEJ5VHlwZUFuZElkcyhvcHRpb25zID0geyB0eXBlOiBUWVBFLkNPTVBBTlksIGlkczogW10sIHBhcmFtczoge30gfSkge1xuICAgICAgICBsZXQgcmVzdWx0O1xuICAgICAgICBhd2FpdCB0aGlzLm1ldGFTZXJ2aWNlLmluaXQoKTtcbiAgICAgICAgaWYgKG9wdGlvbnMuaWRzLmxlbmd0aCA8IDEpIHtcbiAgICAgICAgICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUoe30pXG4gICAgICAgIH1cbiAgICAgICAgY29uc3QgdXJscGFyYW1zID0gbmV3IFVSTFNlYXJjaFBhcmFtcygpO1xuICAgICAgICBjb25zdCBpZEZpZWxkID0gdGhpcy5tZXRhU2VydmljZS5nZXRJZEZpZWxkRm9yVHlwZShvcHRpb25zLnR5cGUpO1xuICAgICAgICBvcHRpb25zLmlkcy5mb3JFYWNoKGlkID0+IHVybHBhcmFtcy5hcHBlbmQoYCR7aWRGaWVsZH0uZXF1YWxzYCwgaWQpKTtcbiAgICAgICAgaWYgKG9wdGlvbnMucGFyYW1zKSBPYmplY3Qua2V5cyhvcHRpb25zLnBhcmFtcykuZm9yRWFjaChrZXkgPT4gdXJscGFyYW1zLmFwcGVuZChrZXksIG9wdGlvbnMucGFyYW1zW2tleV0pKTtcbiAgICAgICAgY29uc3QgdXJsID0gYC9ua2JyZWxhdGlvbi9hcGkvbm9kZXMvJHtvcHRpb25zLnR5cGV9PyR7dXJscGFyYW1zLnRvU3RyaW5nKCl9YDtcbiAgICAgICAgcmVzdWx0ID0gYXdhaXQgdGhpcy5jYWNoZS50cnkodXJsKS50aGVuKChyZXMpID0+IHJlcy5saXN0KCkpO1xuICAgICAgICByZXR1cm4gcmVzdWx0XG4gICAgfVxuXG4gICAgYXN5bmMgc2VhcmNoQ29tcGFueShwYXJhbXMpIHtcbiAgICAgICAgY29uc3QgcmVzID0gYXdhaXQgY29tcGFueS5nZXQoJy8nLCB7IHBhcmFtcyB9KTtcbiAgICAgICAgcmV0dXJuIGdldChyZXMsICdkYXRhJywgW10pO1xuICAgIH1cbn0iXX0=