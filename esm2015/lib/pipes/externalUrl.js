/**
 * @fileoverview added by tsickle
 * Generated from: lib/pipes/externalUrl.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Pipe } from '@angular/core';
export class ExternalUrlPipe {
    /**
     * @param {?} url
     * @return {?}
     */
    transform(url) {
        return '/siteapp/url/external' + '?url=' + encodeURIComponent(url);
    }
    ;
}
ExternalUrlPipe.decorators = [
    { type: Pipe, args: [{ name: 'externalUrl' },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZXh0ZXJuYWxVcmwuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AbmtiL2FuZ3VsYXItbmtiLWxpYi8iLCJzb3VyY2VzIjpbImxpYi9waXBlcy9leHRlcm5hbFVybC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxJQUFJLEVBQWlCLE1BQU0sZUFBZSxDQUFDO0FBR3BELE1BQU0sT0FBTyxlQUFlOzs7OztJQUNqQixTQUFTLENBQUMsR0FBVTtRQUN2QixPQUFPLHVCQUF1QixHQUFHLE9BQU8sR0FBRyxrQkFBa0IsQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUN2RSxDQUFDO0lBQUEsQ0FBQzs7O1lBSkwsSUFBSSxTQUFDLEVBQUMsSUFBSSxFQUFFLGFBQWEsRUFBQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFBpcGUsIFBpcGVUcmFuc2Zvcm0gfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQFBpcGUoe25hbWU6ICdleHRlcm5hbFVybCd9KVxuZXhwb3J0IGNsYXNzIEV4dGVybmFsVXJsUGlwZSBpbXBsZW1lbnRzIFBpcGVUcmFuc2Zvcm0ge1xuICAgIHB1YmxpYyB0cmFuc2Zvcm0odXJsOnN0cmluZyk6IHN0cmluZyB7XG4gICAgICAgIHJldHVybiAnL3NpdGVhcHAvdXJsL2V4dGVybmFsJyArICc/dXJsPScgKyBlbmNvZGVVUklDb21wb25lbnQodXJsKTtcbiAgICB9O1xuICAgICAgXG59XG4iXX0=