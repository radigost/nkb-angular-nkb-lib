/**
 * @fileoverview added by tsickle
 * Generated from: lib/cache/CacheElement.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import * as _ from 'lodash';
export class CacheElement {
    /**
     * @param {?} el
     */
    constructor(el) {
        Object.assign(this, { promise: el });
    }
    /**
     * @return {?}
     */
    head() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const res = yield this.promise;
            return _.head(_.get(res, 'data.list', [res]));
        });
    }
    /**
     * @return {?}
     */
    list() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const res = yield this.promise;
            return _.get(res, 'data.list');
        });
    }
    /**
     * @return {?}
     */
    data() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const res = yield this.promise;
            return _.get(res, 'data');
        });
    }
    /**
     * @return {?}
     */
    get() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            return yield this.promise;
        });
    }
}
if (false) {
    /** @type {?} */
    CacheElement.prototype.promise;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQ2FjaGVFbGVtZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQG5rYi9hbmd1bGFyLW5rYi1saWIvIiwic291cmNlcyI6WyJsaWIvY2FjaGUvQ2FjaGVFbGVtZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLE9BQU8sS0FBSyxDQUFDLE1BQU0sUUFBUSxDQUFDO0FBRzVCLE1BQU0sT0FBTyxZQUFZOzs7O0lBRXJCLFlBQVksRUFBRTtRQUNWLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLEVBQUUsT0FBTyxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUM7SUFDekMsQ0FBQzs7OztJQUVLLElBQUk7OztrQkFDQSxHQUFHLEdBQUcsTUFBTSxJQUFJLENBQUMsT0FBTztZQUM5QixPQUFPLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxHQUFHLEVBQUUsV0FBVyxFQUFFLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ2xELENBQUM7S0FBQTs7OztJQUVLLElBQUk7OztrQkFDQSxHQUFHLEdBQUcsTUFBTSxJQUFJLENBQUMsT0FBTztZQUM5QixPQUFPLENBQUMsQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLFdBQVcsQ0FBQyxDQUFDO1FBQ25DLENBQUM7S0FBQTs7OztJQUVLLElBQUk7OztrQkFDQSxHQUFHLEdBQUcsTUFBTSxJQUFJLENBQUMsT0FBTztZQUM5QixPQUFPLENBQUMsQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLE1BQU0sQ0FBQyxDQUFDO1FBQzlCLENBQUM7S0FBQTs7OztJQUVLLEdBQUc7O1lBQ0wsT0FBTyxNQUFNLElBQUksQ0FBQyxPQUFPLENBQUM7UUFDOUIsQ0FBQztLQUFBO0NBQ0o7OztJQXZCRywrQkFBZ0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgKiBhcyBfIGZyb20gJ2xvZGFzaCc7XHJcbmltcG9ydCB7IEF4aW9zUmVzcG9uc2UgfSBmcm9tICdheGlvcyc7XHJcblxyXG5leHBvcnQgY2xhc3MgQ2FjaGVFbGVtZW50IHtcclxuICAgIHByb21pc2U6IFByb21pc2U8QXhpb3NSZXNwb25zZT47XHJcbiAgICBjb25zdHJ1Y3RvcihlbCkge1xyXG4gICAgICAgIE9iamVjdC5hc3NpZ24odGhpcywgeyBwcm9taXNlOiBlbCB9KTtcclxuICAgIH1cclxuXHJcbiAgICBhc3luYyBoZWFkKCkge1xyXG4gICAgICAgIGNvbnN0IHJlcyA9IGF3YWl0IHRoaXMucHJvbWlzZTtcclxuICAgICAgICByZXR1cm4gXy5oZWFkKF8uZ2V0KHJlcywgJ2RhdGEubGlzdCcsIFtyZXNdKSk7XHJcbiAgICB9XHJcblxyXG4gICAgYXN5bmMgbGlzdCgpIHtcclxuICAgICAgICBjb25zdCByZXMgPSBhd2FpdCB0aGlzLnByb21pc2U7XHJcbiAgICAgICAgcmV0dXJuIF8uZ2V0KHJlcywgJ2RhdGEubGlzdCcpO1xyXG4gICAgfVxyXG5cclxuICAgIGFzeW5jIGRhdGEoKSB7XHJcbiAgICAgICAgY29uc3QgcmVzID0gYXdhaXQgdGhpcy5wcm9taXNlO1xyXG4gICAgICAgIHJldHVybiBfLmdldChyZXMsICdkYXRhJyk7XHJcbiAgICB9XHJcblxyXG4gICAgYXN5bmMgZ2V0KCkge1xyXG4gICAgICAgIHJldHVybiBhd2FpdCB0aGlzLnByb21pc2U7XHJcbiAgICB9XHJcbn1cclxuIl19