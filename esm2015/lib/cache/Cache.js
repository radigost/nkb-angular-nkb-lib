/**
 * @fileoverview added by tsickle
 * Generated from: lib/cache/Cache.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import axios from 'axios';
import { CacheElement } from './CacheElement';
/** @type {?} */
const METHODS = {
    'GET': 'GET',
    'POST': 'POST',
    'PUT': 'PUT',
    'DELETE': 'DELETE',
};
export class Cache {
    constructor() {
        this.obj = {};
    }
    /**
     * @param {?} key
     * @return {?}
     */
    get(key) {
        return this.obj[key];
    }
    /**
     * @param {?} key
     * @param {?} value
     * @return {?}
     */
    set(key, value) {
        this.obj[key] = value;
    }
    /**
     * @param {?} url
     * @param {?=} options
     * @return {?}
     */
    try(url, options = { params: {}, method: METHODS.GET }) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            let result = this.get(url);
            if (result === undefined) {
                // @ts-ignore
                result = new CacheElement(axios({ url, method: options.method }));
                this.set(url, result);
            }
            return result;
            //1. Если есть таймер - добавляем параметры, меняет таймаут
            //2. по прошествии таймаута делаем множественный запрос
            //3. разбираем запрос по элементам, кладем каждый из них в соответсвующих кеш
        });
    }
}
if (false) {
    /** @type {?} */
    Cache.prototype.obj;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQ2FjaGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AbmtiL2FuZ3VsYXItbmtiLWxpYi8iLCJzb3VyY2VzIjpbImxpYi9jYWNoZS9DYWNoZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7QUFFQSxPQUFPLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDMUIsT0FBTyxFQUFDLFlBQVksRUFBQyxNQUFNLGdCQUFnQixDQUFDOztNQUd0QyxPQUFPLEdBQUc7SUFDWixLQUFLLEVBQUUsS0FBSztJQUNaLE1BQU0sRUFBRSxNQUFNO0lBQ2QsS0FBSyxFQUFFLEtBQUs7SUFDWixRQUFRLEVBQUUsUUFBUTtDQUNyQjtBQUVELE1BQU0sT0FBTyxLQUFLO0lBRWQ7UUFDSSxJQUFJLENBQUMsR0FBRyxHQUFHLEVBQUUsQ0FBQztJQUNsQixDQUFDOzs7OztJQUdELEdBQUcsQ0FBQyxHQUFHO1FBQ0gsT0FBTyxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQ3pCLENBQUM7Ozs7OztJQUdELEdBQUcsQ0FBQyxHQUFHLEVBQUUsS0FBSztRQUNWLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLEdBQUcsS0FBSyxDQUFDO0lBQzFCLENBQUM7Ozs7OztJQUVLLEdBQUcsQ0FBQyxHQUFHLEVBQUUsT0FBTyxHQUFHLEVBQUMsTUFBTSxFQUFFLEVBQUUsRUFBRSxNQUFNLEVBQUUsT0FBTyxDQUFDLEdBQUcsRUFBQzs7O2dCQUNsRCxNQUFNLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUM7WUFDMUIsSUFBSSxNQUFNLEtBQUssU0FBUyxFQUFFO2dCQUN0QixhQUFhO2dCQUNiLE1BQU0sR0FBRyxJQUFJLFlBQVksQ0FBQyxLQUFLLENBQUMsRUFBQyxHQUFHLEVBQUUsTUFBTSxFQUFFLE9BQU8sQ0FBQyxNQUFNLEVBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ2hFLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLE1BQU0sQ0FBQyxDQUFBO2FBQ3hCO1lBQ0QsT0FBTyxNQUFNLENBQUM7WUFFZCwyREFBMkQ7WUFDM0QsdURBQXVEO1lBQ3ZELDZFQUE2RTtRQUdqRixDQUFDO0tBQUE7Q0FDSjs7O0lBOUJHLG9CQUFJIiwic291cmNlc0NvbnRlbnQiOlsiXHJcblxyXG5pbXBvcnQgYXhpb3MgZnJvbSAnYXhpb3MnO1xyXG5pbXBvcnQge0NhY2hlRWxlbWVudH0gZnJvbSAnLi9DYWNoZUVsZW1lbnQnO1xyXG5cclxuXHJcbmNvbnN0IE1FVEhPRFMgPSB7XHJcbiAgICAnR0VUJzogJ0dFVCcsXHJcbiAgICAnUE9TVCc6ICdQT1NUJyxcclxuICAgICdQVVQnOiAnUFVUJyxcclxuICAgICdERUxFVEUnOiAnREVMRVRFJyxcclxufTtcclxuXHJcbmV4cG9ydCBjbGFzcyBDYWNoZSB7XHJcbiAgICBvYmo7XHJcbiAgICBjb25zdHJ1Y3RvcigpIHtcclxuICAgICAgICB0aGlzLm9iaiA9IHt9O1xyXG4gICAgfVxyXG5cclxuXHJcbiAgICBnZXQoa2V5KSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMub2JqW2tleV07XHJcbiAgICB9XHJcblxyXG5cclxuICAgIHNldChrZXksIHZhbHVlKSB7XHJcbiAgICAgICAgdGhpcy5vYmpba2V5XSA9IHZhbHVlO1xyXG4gICAgfVxyXG5cclxuICAgIGFzeW5jIHRyeSh1cmwsIG9wdGlvbnMgPSB7cGFyYW1zOiB7fSwgbWV0aG9kOiBNRVRIT0RTLkdFVH0pIHtcclxuICAgICAgICBsZXQgcmVzdWx0ID0gdGhpcy5nZXQodXJsKTtcclxuICAgICAgICBpZiAocmVzdWx0ID09PSB1bmRlZmluZWQpIHtcclxuICAgICAgICAgICAgLy8gQHRzLWlnbm9yZVxyXG4gICAgICAgICAgICByZXN1bHQgPSBuZXcgQ2FjaGVFbGVtZW50KGF4aW9zKHt1cmwsIG1ldGhvZDogb3B0aW9ucy5tZXRob2R9KSk7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0KHVybCwgcmVzdWx0KVxyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gcmVzdWx0O1xyXG4gICAgICAgIFxyXG4gICAgICAgIC8vMS4g0JXRgdC70Lgg0LXRgdGC0Ywg0YLQsNC50LzQtdGAIC0g0LTQvtCx0LDQstC70Y/QtdC8INC/0LDRgNCw0LzQtdGC0YDRiywg0LzQtdC90Y/QtdGCINGC0LDQudC80LDRg9GCXHJcbiAgICAgICAgLy8yLiDQv9C+INC/0YDQvtGI0LXRgdGC0LLQuNC4INGC0LDQudC80LDRg9GC0LAg0LTQtdC70LDQtdC8INC80L3QvtC20LXRgdGC0LLQtdC90L3Ri9C5INC30LDQv9GA0L7RgVxyXG4gICAgICAgIC8vMy4g0YDQsNC30LHQuNGA0LDQtdC8INC30LDQv9GA0L7RgSDQv9C+INGN0LvQtdC80LXQvdGC0LDQvCwg0LrQu9Cw0LTQtdC8INC60LDQttC00YvQuSDQuNC3INC90LjRhSDQsiDRgdC+0L7RgtCy0LXRgtGB0LLRg9GO0YnQuNGFINC60LXRiFxyXG5cclxuXHJcbiAgICB9XHJcbn1cclxuIl19