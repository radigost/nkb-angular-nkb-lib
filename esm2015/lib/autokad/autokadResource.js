/**
 * @fileoverview added by tsickle
 * Generated from: lib/autokad/autokadResource.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/**
 * Created by radigost on 24.04.17.
 */
import axios from 'axios';
import { Cache } from '../cache/Cache';
import { head, get } from 'lodash';
import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
/** @type {?} */
const seldon = axios.create({
    baseURL: '/api/v1/companies/',
});
/**
 * @record
 */
function IRequestParams() { }
if (false) {
    /** @type {?|undefined} */
    IRequestParams.prototype.ogrn;
    /** @type {?|undefined} */
    IRequestParams.prototype.inn;
    /** @type {?} */
    IRequestParams.prototype.type;
    /** @type {?} */
    IRequestParams.prototype.pageSize;
    /** @type {?} */
    IRequestParams.prototype.pageNo;
}
export class AutokadResource {
    constructor() {
        this.cache = new Cache();
    }
    /**
     * @param {?} arg0
     * @return {?}
     */
    kadSearch(arg0) {
        throw new Error("Method not implemented.");
    }
    /**
     * @return {?}
     */
    init() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
        });
    }
    /**
     * @param {?} options
     * @return {?}
     */
    search(options) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            let result;
            /** @type {?} */
            const req = buildRequestParams(options);
            result = this.cache.get(`${req.ogrn}_${req.type}_${req.pageSize}_${req.pageNo}`);
            if (result === undefined) {
                result = seldon.get(`ogrn-${req.ogrn}/cases`, { params: req });
                this.cache.set(`${req.ogrn}_${req.type}_${req.pageSize}_${req.pageNo}`, result);
            }
            return result;
            /**
             * @param {?} options
             * @return {?}
             */
            function buildRequestParams(options) {
                /** @type {?} */
                const req = {
                    type: options.type,
                    pageSize: options.pageSize || 100,
                    pageNo: options.pageIndex || 1,
                };
                options.ogrn !== undefined ? req.ogrn = options.ogrn :
                    options.inn !== undefined ? req.inn = options.inn :
                        console.warn('there is no inn or ogrn in request!', options);
                return req;
            }
        });
    }
    /**
     * @param {?} options
     * @return {?}
     */
    aggregates(options) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            let res;
            if (options.ogrn) {
                res = seldon.get(`ogrn-${options.ogrn}/aggregates`);
            }
            return res;
        });
    }
    /**
     * @param {?} ogrn
     * @return {?}
     */
    getAggregates(ogrn) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            try {
                /** @type {?} */
                const res = yield this.aggregates({ ogrn });
                return head(get(res, 'data.entries'));
            }
            catch (e) {
                return Promise.reject();
            }
        });
    }
}
AutokadResource.decorators = [
    { type: Injectable, args: [{ providedIn: 'root' },] }
];
/** @nocollapse */ AutokadResource.ngInjectableDef = i0.defineInjectable({ factory: function AutokadResource_Factory() { return new AutokadResource(); }, token: AutokadResource, providedIn: "root" });
if (false) {
    /** @type {?} */
    AutokadResource.prototype.cache;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0b2thZFJlc291cmNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQG5rYi9hbmd1bGFyLW5rYi1saWIvIiwic291cmNlcyI6WyJsaWIvYXV0b2thZC9hdXRva2FkUmVzb3VyY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7O0FBSUEsT0FBTyxLQUFLLE1BQU0sT0FBTyxDQUFDO0FBQzFCLE9BQU8sRUFBQyxLQUFLLEVBQUMsTUFBTSxnQkFBZ0IsQ0FBQztBQUNyQyxPQUFPLEVBQUUsSUFBSSxFQUFFLEdBQUcsRUFBRSxNQUFNLFFBQVEsQ0FBQztBQUNuQyxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDOzs7TUFFckMsTUFBTSxHQUFHLEtBQUssQ0FBQyxNQUFNLENBQUM7SUFDeEIsT0FBTyxFQUFFLG9CQUFvQjtDQUNoQyxDQUFDOzs7O0FBRUYsNkJBTUM7OztJQUxHLDhCQUFNOztJQUNOLDZCQUFLOztJQUNMLDhCQUFLOztJQUNMLGtDQUFTOztJQUNULGdDQUFPOztBQUlYLE1BQU0sT0FBTyxlQUFlO0lBRDVCO1FBS0ksVUFBSyxHQUFHLElBQUksS0FBSyxFQUFFLENBQUM7S0E0Q3ZCOzs7OztJQS9DRyxTQUFTLENBQUMsSUFBMEU7UUFDaEYsTUFBTSxJQUFJLEtBQUssQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDO0lBQy9DLENBQUM7Ozs7SUFHSyxJQUFJOztRQUNWLENBQUM7S0FBQTs7Ozs7SUFFSyxNQUFNLENBQUMsT0FBTzs7O2dCQUNaLE1BQU07O2tCQUNKLEdBQUcsR0FBRyxrQkFBa0IsQ0FBQyxPQUFPLENBQUM7WUFDdkMsTUFBTSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLEdBQUcsR0FBRyxDQUFDLElBQUksSUFBSSxHQUFHLENBQUMsSUFBSSxJQUFJLEdBQUcsQ0FBQyxRQUFRLElBQUksR0FBRyxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUM7WUFDakYsSUFBSSxNQUFNLEtBQUssU0FBUyxFQUFFO2dCQUN0QixNQUFNLEdBQUcsTUFBTSxDQUFDLEdBQUcsQ0FBQyxRQUFRLEdBQUcsQ0FBQyxJQUFJLFFBQVEsRUFBRSxFQUFFLE1BQU0sRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFDO2dCQUMvRCxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxHQUFHLEdBQUcsQ0FBQyxJQUFJLElBQUksR0FBRyxDQUFDLElBQUksSUFBSSxHQUFHLENBQUMsUUFBUSxJQUFJLEdBQUcsQ0FBQyxNQUFNLEVBQUUsRUFBRSxNQUFNLENBQUMsQ0FBQzthQUNuRjtZQUNELE9BQU8sTUFBTSxDQUFDOzs7OztZQUVkLFNBQVMsa0JBQWtCLENBQUMsT0FBb0Q7O3NCQUN0RSxHQUFHLEdBQW1CO29CQUN4QixJQUFJLEVBQUUsT0FBTyxDQUFDLElBQUk7b0JBQ2xCLFFBQVEsRUFBRSxPQUFPLENBQUMsUUFBUSxJQUFJLEdBQUc7b0JBQ2pDLE1BQU0sRUFBRSxPQUFPLENBQUMsU0FBUyxJQUFJLENBQUM7aUJBQ2pDO2dCQUNELE9BQU8sQ0FBQyxJQUFJLEtBQUssU0FBUyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsSUFBSSxHQUFHLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDbEQsT0FBTyxDQUFDLEdBQUcsS0FBSyxTQUFTLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxHQUFHLEdBQUcsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDO3dCQUMvQyxPQUFPLENBQUMsSUFBSSxDQUFDLHFDQUFxQyxFQUFFLE9BQU8sQ0FBQyxDQUFDO2dCQUNyRSxPQUFPLEdBQUcsQ0FBQztZQUNmLENBQUM7UUFDTCxDQUFDO0tBQUE7Ozs7O0lBRUssVUFBVSxDQUFDLE9BQU87OztnQkFDaEIsR0FBRztZQUNQLElBQUksT0FBTyxDQUFDLElBQUksRUFBRTtnQkFDZCxHQUFHLEdBQUcsTUFBTSxDQUFDLEdBQUcsQ0FBQyxRQUFRLE9BQU8sQ0FBQyxJQUFJLGFBQWEsQ0FBQyxDQUFDO2FBQ3ZEO1lBQ0QsT0FBTyxHQUFHLENBQUM7UUFDZixDQUFDO0tBQUE7Ozs7O0lBRUssYUFBYSxDQUFDLElBQUk7O1lBQ3BCLElBQUk7O3NCQUNNLEdBQUcsR0FBRyxNQUFNLElBQUksQ0FBQyxVQUFVLENBQUMsRUFBRSxJQUFJLEVBQUUsQ0FBQztnQkFDM0MsT0FBTyxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsRUFBRSxjQUFjLENBQUMsQ0FBQyxDQUFDO2FBQ3pDO1lBQUMsT0FBTyxDQUFDLEVBQUU7Z0JBQ1IsT0FBTyxPQUFPLENBQUMsTUFBTSxFQUFFLENBQUM7YUFDM0I7UUFDTCxDQUFDO0tBQUE7OztZQWhESixVQUFVLFNBQUMsRUFBRSxVQUFVLEVBQUUsTUFBTSxFQUFFOzs7OztJQUs5QixnQ0FBb0IiLCJzb3VyY2VzQ29udGVudCI6WyIvKipcclxuICogQ3JlYXRlZCBieSByYWRpZ29zdCBvbiAyNC4wNC4xNy5cclxuICovXHJcblxyXG5pbXBvcnQgYXhpb3MgZnJvbSAnYXhpb3MnO1xyXG5pbXBvcnQge0NhY2hlfSBmcm9tICcuLi9jYWNoZS9DYWNoZSc7XHJcbmltcG9ydCB7IGhlYWQsIGdldCB9IGZyb20gJ2xvZGFzaCc7XHJcbmltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbmNvbnN0IHNlbGRvbiA9IGF4aW9zLmNyZWF0ZSh7XHJcbiAgICBiYXNlVVJMOiAnL2FwaS92MS9jb21wYW5pZXMvJyxcclxufSk7XHJcblxyXG5pbnRlcmZhY2UgSVJlcXVlc3RQYXJhbXMge1xyXG4gICAgb2dybj87XHJcbiAgICBpbm4/O1xyXG4gICAgdHlwZTtcclxuICAgIHBhZ2VTaXplO1xyXG4gICAgcGFnZU5vO1xyXG59XHJcblxyXG5ASW5qZWN0YWJsZSh7IHByb3ZpZGVkSW46ICdyb290JyB9KVxyXG5leHBvcnQgY2xhc3MgQXV0b2thZFJlc291cmNlIHtcclxuICAgIGthZFNlYXJjaChhcmcwOiB7IHI6IHsgcTogYW55OyB9OyBzdWNjZXNzOiAoZGF0YTogYW55KSA9PiB2b2lkOyBlcnJvcjogKCkgPT4gdm9pZDsgfSk6IGFueSB7XHJcbiAgICAgICAgdGhyb3cgbmV3IEVycm9yKFwiTWV0aG9kIG5vdCBpbXBsZW1lbnRlZC5cIik7XHJcbiAgICB9XHJcbiAgICBjYWNoZSA9IG5ldyBDYWNoZSgpO1xyXG5cclxuICAgIGFzeW5jIGluaXQoKSB7XHJcbiAgICB9XHJcblxyXG4gICAgYXN5bmMgc2VhcmNoKG9wdGlvbnMpIHtcclxuICAgICAgICBsZXQgcmVzdWx0O1xyXG4gICAgICAgIGNvbnN0IHJlcSA9IGJ1aWxkUmVxdWVzdFBhcmFtcyhvcHRpb25zKTtcclxuICAgICAgICByZXN1bHQgPSB0aGlzLmNhY2hlLmdldChgJHtyZXEub2dybn1fJHtyZXEudHlwZX1fJHtyZXEucGFnZVNpemV9XyR7cmVxLnBhZ2VOb31gKTtcclxuICAgICAgICBpZiAocmVzdWx0ID09PSB1bmRlZmluZWQpIHtcclxuICAgICAgICAgICAgcmVzdWx0ID0gc2VsZG9uLmdldChgb2dybi0ke3JlcS5vZ3JufS9jYXNlc2AsIHsgcGFyYW1zOiByZXEgfSk7XHJcbiAgICAgICAgICAgIHRoaXMuY2FjaGUuc2V0KGAke3JlcS5vZ3JufV8ke3JlcS50eXBlfV8ke3JlcS5wYWdlU2l6ZX1fJHtyZXEucGFnZU5vfWAsIHJlc3VsdCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiByZXN1bHQ7XHJcblxyXG4gICAgICAgIGZ1bmN0aW9uIGJ1aWxkUmVxdWVzdFBhcmFtcyhvcHRpb25zOiB7IG9ncm4/LCBpbm4/LCB0eXBlLCBwYWdlU2l6ZT8sIHBhZ2VJbmRleD99KTogSVJlcXVlc3RQYXJhbXMge1xyXG4gICAgICAgICAgICBjb25zdCByZXE6IElSZXF1ZXN0UGFyYW1zID0ge1xyXG4gICAgICAgICAgICAgICAgdHlwZTogb3B0aW9ucy50eXBlLFxyXG4gICAgICAgICAgICAgICAgcGFnZVNpemU6IG9wdGlvbnMucGFnZVNpemUgfHwgMTAwLFxyXG4gICAgICAgICAgICAgICAgcGFnZU5vOiBvcHRpb25zLnBhZ2VJbmRleCB8fCAxLFxyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICBvcHRpb25zLm9ncm4gIT09IHVuZGVmaW5lZCA/IHJlcS5vZ3JuID0gb3B0aW9ucy5vZ3JuIDpcclxuICAgICAgICAgICAgICAgIG9wdGlvbnMuaW5uICE9PSB1bmRlZmluZWQgPyByZXEuaW5uID0gb3B0aW9ucy5pbm4gOlxyXG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUud2FybigndGhlcmUgaXMgbm8gaW5uIG9yIG9ncm4gaW4gcmVxdWVzdCEnLCBvcHRpb25zKTtcclxuICAgICAgICAgICAgcmV0dXJuIHJlcTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgYXN5bmMgYWdncmVnYXRlcyhvcHRpb25zKSB7XHJcbiAgICAgICAgbGV0IHJlcztcclxuICAgICAgICBpZiAob3B0aW9ucy5vZ3JuKSB7XHJcbiAgICAgICAgICAgIHJlcyA9IHNlbGRvbi5nZXQoYG9ncm4tJHtvcHRpb25zLm9ncm59L2FnZ3JlZ2F0ZXNgKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHJlcztcclxuICAgIH1cclxuXHJcbiAgICBhc3luYyBnZXRBZ2dyZWdhdGVzKG9ncm4pIHtcclxuICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICBjb25zdCByZXMgPSBhd2FpdCB0aGlzLmFnZ3JlZ2F0ZXMoeyBvZ3JuIH0pO1xyXG4gICAgICAgICAgICByZXR1cm4gaGVhZChnZXQocmVzLCAnZGF0YS5lbnRyaWVzJykpO1xyXG4gICAgICAgIH0gY2F0Y2ggKGUpIHtcclxuICAgICAgICAgICAgcmV0dXJuIFByb21pc2UucmVqZWN0KCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcblxyXG4iXX0=