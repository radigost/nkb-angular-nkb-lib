/**
 * @fileoverview added by tsickle
 * Generated from: lib/autokad/autokadService.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/**
 * Created by radigost on 24.04.17.
 */
import { assignIn, isObject, isArray, isNumber, isNaN, size, isNull, isFunction, isEmpty, forEach, get } from 'lodash';
import { AutokadResource } from './autokadResource';
import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
import * as i1 from "./autokadResource";
/** @type {?} */
const Search = (/**
 * @return {?}
 */
() => {
    return {
        params: null,
        requestData: null,
        request: null,
        result: {
            // В формате kad.arbitr.ru
            TotalCount: null,
            PagesCount: null,
            Items: [],
            entries: []
        },
        noResult: true,
        error: null,
        watch: true,
        sources: undefined,
    };
});
const ɵ0 = Search;
/** @type {?} */
const DEFAULT_SEARCH_PARAMS = {
    side: 4
};
/** @type {?} */
const DEFAULT_SEARCH_REQUEST_DATA = {
    q: null,
    dateFrom: null,
    dateTo: null,
    page: 1,
    pageIndex: 1
};
export class AutokadService {
    /**
     * @param {?} autokadResourceService
     */
    constructor(autokadResourceService) {
        this.autokadResourceService = autokadResourceService;
        /** @type {?} */
        let node = null;
        /** @type {?} */
        const autokadService = {
            search: Search(),
            loading: false,
            pager: Pager(),
            initSearch,
            doSearch,
            buildSearchRequestData,
            getResultTotal,
            getCaseCount,
            hasError,
            isLoading,
            isNoResult,
        };
        Object.assign(this, autokadService);
        /**
         * @param {?} newNode
         * @param {?} params
         * @return {?}
         */
        function initSearch(newNode, params) {
            node = newNode;
            autokadService.search.params = assignIn({}, DEFAULT_SEARCH_PARAMS, params, {
                source: 0
            });
        }
        /**
         * @return {?}
         */
        function resetSearchRequest() {
            if (autokadService.search.request && autokadService.search.request.abort) {
                autokadService.search.request.abort();
            }
            autokadService.search.requestData = getDefaultSearchRequestData();
        }
        /**
         * @return {?}
         */
        function doSearch() {
            autokadService.search.result = {
                TotalCount: null,
                PagesCount: null,
                Items: [],
                entries: []
            };
            resetSearchRequest();
            autokadService.search.requestData = autokadService.buildSearchRequestData(autokadService.search.params);
            return new Promise((/**
             * @param {?} resolve
             * @return {?}
             */
            (resolve) => {
                if (isBlank(autokadService.search.requestData.inn)) {
                    console.warn('search.requestData.inn is empty ');
                    resolve();
                }
                else {
                    searchRequestPromise.call(this).then((/**
                     * @param {?} data
                     * @return {?}
                     */
                    (data) => {
                        autokadService.search.result = data;
                        autokadService.pager.reset(data);
                        hideLoading();
                        resolve();
                    }), (/**
                     * @param {?} data
                     * @return {?}
                     */
                    (data) => {
                        autokadService.search.result = data;
                        autokadService.pager.reset(data);
                        hideLoading();
                        resolve();
                    }));
                }
            }));
        }
        /**
         * @return {?}
         */
        function searchRequestPromise() {
            return new Promise((/**
             * @param {?} resolve
             * @param {?} reject
             * @return {?}
             */
            function (resolve, reject) {
                showLoading();
                autokadService.search.noResult = true;
                autokadService.search.request = autokadResourceService.search(autokadService.search.requestData).then(success, error);
                /**
                 * @param {?} data
                 * @return {?}
                 */
                function success(data) {
                    data = data.data;
                    autokadService.search.noResult = (data.total === 0);
                    autokadService.search.error = null;
                    if (!hasResultItems(data)) {
                        autokadService.search.error = true;
                        hideLoading();
                        reject(data);
                    }
                    hideLoading();
                    resolve(data);
                }
                /**
                 * @param {?} err
                 * @return {?}
                 */
                function error(err) {
                    autokadService.search.noResult = false;
                    autokadService.search.error = true;
                    hideLoading();
                    reject(err);
                }
            }));
        }
        /**
         * @param {?} result
         * @return {?}
         */
        function hasResultItems(result) {
            return isObject(result) && isArray(result['entries']);
        }
        /**
         * @param {?} value
         * @return {?}
         */
        function isBlank(value) {
            return isEmpty(value) && !isNumber(value) || isNaN(value);
        }
        /**
         * @return {?}
         */
        function hasError() {
            return autokadService.search.error;
        }
        /**
         * @param {?} searchParams
         * @return {?}
         */
        function buildSearchRequestData(searchParams) {
            /** @type {?} */
            const requestData = assignIn({}, DEFAULT_SEARCH_REQUEST_DATA);
            requestData['type'] = (searchParams['side'] === -1 ? 4 : searchParams['side']);
            requestData['inn'] = node['inn'];
            requestData['ogrn'] = node['ogrn'];
            return requestData;
        }
        /*
                    * case
                    *
                    */
        /**
         * @param {?} search
         * @param {?} success
         * @param {?} error
         * @param {?} complete
         * @return {?}
         */
        function getCaseCount(search, success, error, complete) {
            if (!isArray(autokadService.search.sources) || !size(autokadService.search.sources)) {
                console.warn('getCaseCount... error: search.sources is blank');
                errorCallback();
                return null;
            }
            /** @type {?} */
            let sourceIndex = 0;
            /** @type {?} */
            let sourceCount = size(autokadService.search.sources);
            /** @type {?} */
            let caseCountRequest = CaseCountRequest();
            /** @type {?} */
            let result = null;
            /** @type {?} */
            let source;
            /** @type {?} */
            let request;
            nextRequest();
            return caseCountRequest;
            /**
             * @return {?}
             */
            function nextRequest() {
                /** @type {?} */
                let error = false;
                /** @type {?} */
                let next = false;
                /** @type {?} */
                let q;
                source = autokadService.search.sources[sourceIndex++];
                q = source.value;
                if (isBlank(q)) {
                    request = null;
                    result = 0;
                    next = true;
                    caseCountRequest._setRequest(request);
                    check();
                }
                else {
                    request = autokadResourceService.kadSearch({
                        r: {
                            q: q
                        },
                        success: (/**
                         * @param {?} data
                         * @return {?}
                         */
                        function (data) {
                            result = getResultTotal(data['Result']);
                            if (isNull(result)) {
                                error = true;
                            }
                            else if (result === 0) {
                                next = true;
                            }
                        }),
                        error: (/**
                         * @return {?}
                         */
                        function () {
                            error = true;
                        })
                    });
                    caseCountRequest._setRequest(request);
                    request.completePromise.then((/**
                     * @return {?}
                     */
                    function () {
                        check();
                    }));
                }
                /**
                 * @return {?}
                 */
                function check() {
                    if (error) {
                        errorCallback();
                    }
                    else if (!next || sourceIndex === sourceCount) {
                        successCallback();
                    }
                    else if (next) {
                        nextRequest();
                    }
                }
            }
            /**
             * @return {?}
             */
            function successCallback() {
                if (isFunction(success)) {
                    success(result, sourceIndex - 1);
                }
                completeCallback();
            }
            /**
             * @return {?}
             */
            function errorCallback() {
                if (isFunction(error)) {
                    error();
                }
                completeCallback();
            }
            /**
             * @return {?}
             */
            function completeCallback() {
                if (isFunction(complete)) {
                    complete();
                }
            }
            /**
             * @param {?} value
             * @return {?}
             */
            function isBlank(value) {
                return isEmpty(value) && !isNumber(value) || isNaN(value);
            }
            /**
             * @return {?}
             */
            function CaseCountRequest() {
                /** @type {?} */
                let request;
                return {
                    _setRequest: (/**
                     * @param {?} r
                     * @return {?}
                     */
                    function (r) {
                        request = r;
                    }),
                    abort: (/**
                     * @return {?}
                     */
                    function () {
                        if (request) {
                            request.abort();
                        }
                    })
                };
            }
        }
        /**
         * @param {?} result
         * @return {?}
         */
        function getResultTotal(result) {
            /** @type {?} */
            let r = result && result['TotalCount'];
            if (!isNumber(r)) {
                r = null;
            }
            return r;
        }
        /**
         * @return {?}
         */
        function showLoading() {
            autokadService.loading = true;
        }
        /**
         * @return {?}
         */
        function hideLoading() {
            autokadService.loading = false;
        }
        /**
         * @return {?}
         */
        function getDefaultSearchRequestData() {
            return DEFAULT_SEARCH_REQUEST_DATA;
        }
        /**
         * @return {?}
         */
        function isLoading() {
            return autokadService.loading;
        }
        /**
         * @return {?}
         */
        function isNoResult() {
            return get(autokadService.search, 'result.status.itemsFound', 0) === 0;
        }
        //Pager
        /**
         * @return {?}
         * @this {*}
         */
        function Pager() {
            /** @type {?} */
            let internalDisabled = false;
            /** @type {?} */
            let noNextPage = false;
            return {
                reset: reset,
                nextPage: nextPage,
                isDisabled: isDisabled
            };
            /**
             * @return {?}
             */
            function isDisabled() {
                return internalDisabled || noNextPage;
            }
            /**
             * @param {?} result
             * @return {?}
             */
            function reset(result) {
                internalDisabled = false;
                noNextPage = noMore(false, result);
            }
            /**
             * @param {?} hasError
             * @param {?} result
             * @return {?}
             */
            function noMore(hasError, result) {
                return hasError || (autokadService.search.requestData.pageIndex * result.size >= result.total);
            }
            /**
             * @return {?}
             * @this {*}
             */
            function nextPage() {
                return tslib_1.__awaiter(this, void 0, void 0, function* () {
                    return new Promise((/**
                     * @param {?} resolve
                     * @return {?}
                     */
                    (resolve) => {
                        showLoading();
                        if (!isDisabled() && autokadService.search.requestData) {
                            internalDisabled = true;
                            autokadService.search.requestData.pageIndex++;
                            searchRequestPromise().then((/**
                             * @param {?} result
                             * @return {?}
                             */
                            function (result) {
                                /** @type {?} */
                                const hasError = !hasResultItems(result);
                                if (!hasError) {
                                    forEach(result.entries, (/**
                                     * @param {?} item
                                     * @return {?}
                                     */
                                    function (item) {
                                        autokadService.search.result.entries.push(item);
                                    }));
                                    internalDisabled = false;
                                    noNextPage = noMore(false, result);
                                }
                                else {
                                    internalDisabled = false;
                                    noNextPage = true;
                                }
                                hideLoading();
                                resolve();
                            }));
                        }
                        resolve();
                    }));
                });
            }
        }
    }
}
AutokadService.decorators = [
    { type: Injectable, args: [{ providedIn: 'root' },] }
];
/** @nocollapse */
AutokadService.ctorParameters = () => [
    { type: AutokadResource }
];
/** @nocollapse */ AutokadService.ngInjectableDef = i0.defineInjectable({ factory: function AutokadService_Factory() { return new AutokadService(i0.inject(i1.AutokadResource)); }, token: AutokadService, providedIn: "root" });
if (false) {
    /** @type {?} */
    AutokadService.prototype.search;
    /** @type {?} */
    AutokadService.prototype.loading;
    /** @type {?} */
    AutokadService.prototype.pager;
    /** @type {?} */
    AutokadService.prototype.initSearch;
    /** @type {?} */
    AutokadService.prototype.doSearch;
    /** @type {?} */
    AutokadService.prototype.buildSearchRequestData;
    /** @type {?} */
    AutokadService.prototype.getResultTotal;
    /** @type {?} */
    AutokadService.prototype.getCaseCount;
    /** @type {?} */
    AutokadService.prototype.hasError;
    /** @type {?} */
    AutokadService.prototype.isLoading;
    /** @type {?} */
    AutokadService.prototype.isNoResult;
    /**
     * @type {?}
     * @private
     */
    AutokadService.prototype.autokadResourceService;
}
export { ɵ0 };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0b2thZFNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AbmtiL2FuZ3VsYXItbmtiLWxpYi8iLCJzb3VyY2VzIjpbImxpYi9hdXRva2FkL2F1dG9rYWRTZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7OztBQUlBLE9BQU8sRUFBRSxRQUFRLEVBQUUsUUFBUSxFQUFFLE9BQU8sRUFBRSxRQUFRLEVBQUUsS0FBSyxFQUFFLElBQUksRUFBRSxNQUFNLEVBQUUsVUFBVSxFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsR0FBRyxFQUFFLE1BQU0sUUFBUSxDQUFDO0FBQ3ZILE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUNwRCxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDOzs7O01BRXJDLE1BQU07OztBQUFHLEdBQUcsRUFBRTtJQUNoQixPQUFPO1FBQ0gsTUFBTSxFQUFFLElBQUk7UUFDWixXQUFXLEVBQUUsSUFBSTtRQUNqQixPQUFPLEVBQUUsSUFBSTtRQUNiLE1BQU0sRUFBRTs7WUFDSixVQUFVLEVBQUUsSUFBSTtZQUNoQixVQUFVLEVBQUUsSUFBSTtZQUNoQixLQUFLLEVBQUUsRUFBRTtZQUNULE9BQU8sRUFBRSxFQUFFO1NBQ2Q7UUFDRCxRQUFRLEVBQUUsSUFBSTtRQUNkLEtBQUssRUFBRSxJQUFJO1FBQ1gsS0FBSyxFQUFFLElBQUk7UUFDWCxPQUFPLEVBQUUsU0FBUztLQUNyQixDQUFDO0FBQ04sQ0FBQyxDQUFBOzs7TUFFSyxxQkFBcUIsR0FBRztJQUMxQixJQUFJLEVBQUUsQ0FBQztDQUNWOztNQUVLLDJCQUEyQixHQUFHO0lBQ2hDLENBQUMsRUFBRSxJQUFJO0lBQ1AsUUFBUSxFQUFFLElBQUk7SUFDZCxNQUFNLEVBQUUsSUFBSTtJQUNaLElBQUksRUFBRSxDQUFDO0lBQ1AsU0FBUyxFQUFFLENBQUM7Q0FDZjtBQUdELE1BQU0sT0FBTyxjQUFjOzs7O0lBWXZCLFlBQ1ksc0JBQXVDO1FBQXZDLDJCQUFzQixHQUF0QixzQkFBc0IsQ0FBaUI7O1lBRzNDLElBQUksR0FBRyxJQUFJOztjQUVULGNBQWMsR0FBRztZQUNuQixNQUFNLEVBQUUsTUFBTSxFQUFFO1lBQ2hCLE9BQU8sRUFBRSxLQUFLO1lBQ2QsS0FBSyxFQUFFLEtBQUssRUFBRTtZQUVkLFVBQVU7WUFDVixRQUFRO1lBQ1Isc0JBQXNCO1lBQ3RCLGNBQWM7WUFDZCxZQUFZO1lBQ1osUUFBUTtZQUNSLFNBQVM7WUFDVCxVQUFVO1NBRWI7UUFDRCxNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxjQUFjLENBQUMsQ0FBQzs7Ozs7O1FBRXBDLFNBQVMsVUFBVSxDQUFDLE9BQU8sRUFBRSxNQUFNO1lBQy9CLElBQUksR0FBRyxPQUFPLENBQUM7WUFDZixjQUFjLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBRyxRQUFRLENBQUMsRUFBRSxFQUFFLHFCQUFxQixFQUFFLE1BQU0sRUFBRTtnQkFDdkUsTUFBTSxFQUFFLENBQUM7YUFDWixDQUFDLENBQUM7UUFDUCxDQUFDOzs7O1FBRUQsU0FBUyxrQkFBa0I7WUFDdkIsSUFBSSxjQUFjLENBQUMsTUFBTSxDQUFDLE9BQU8sSUFBSSxjQUFjLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUU7Z0JBQ3RFLGNBQWMsQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLEtBQUssRUFBRSxDQUFDO2FBQ3pDO1lBQ0QsY0FBYyxDQUFDLE1BQU0sQ0FBQyxXQUFXLEdBQUcsMkJBQTJCLEVBQUUsQ0FBQztRQUN0RSxDQUFDOzs7O1FBRUQsU0FBUyxRQUFRO1lBQ2IsY0FBYyxDQUFDLE1BQU0sQ0FBQyxNQUFNLEdBQUc7Z0JBQzNCLFVBQVUsRUFBRSxJQUFJO2dCQUNoQixVQUFVLEVBQUUsSUFBSTtnQkFDaEIsS0FBSyxFQUFFLEVBQUU7Z0JBQ1QsT0FBTyxFQUFFLEVBQUU7YUFDZCxDQUFDO1lBQ0Ysa0JBQWtCLEVBQUUsQ0FBQztZQUNyQixjQUFjLENBQUMsTUFBTSxDQUFDLFdBQVcsR0FBRyxjQUFjLENBQUMsc0JBQXNCLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUN4RyxPQUFPLElBQUksT0FBTzs7OztZQUFDLENBQUMsT0FBTyxFQUFFLEVBQUU7Z0JBQzNCLElBQUksT0FBTyxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxFQUFFO29CQUNoRCxPQUFPLENBQUMsSUFBSSxDQUFDLGtDQUFrQyxDQUFDLENBQUM7b0JBQ2pELE9BQU8sRUFBRSxDQUFDO2lCQUNiO3FCQUFNO29CQUNILG9CQUFvQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJOzs7O29CQUFDLENBQUMsSUFBSSxFQUFFLEVBQUU7d0JBQzFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQzt3QkFDcEMsY0FBYyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7d0JBQ2pDLFdBQVcsRUFBRSxDQUFDO3dCQUNkLE9BQU8sRUFBRSxDQUFDO29CQUVkLENBQUM7Ozs7b0JBQUUsQ0FBQyxJQUFJLEVBQUUsRUFBRTt3QkFDUixjQUFjLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7d0JBQ3BDLGNBQWMsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO3dCQUNqQyxXQUFXLEVBQUUsQ0FBQzt3QkFDZCxPQUFPLEVBQUUsQ0FBQztvQkFDZCxDQUFDLEVBQUMsQ0FBQztpQkFDTjtZQUNMLENBQUMsRUFBQyxDQUFDO1FBQ1AsQ0FBQzs7OztRQUVELFNBQVMsb0JBQW9CO1lBQ3pCLE9BQU8sSUFBSSxPQUFPOzs7OztZQUFDLFVBQVUsT0FBTyxFQUFFLE1BQU07Z0JBQ3hDLFdBQVcsRUFBRSxDQUFDO2dCQUNkLGNBQWMsQ0FBQyxNQUFNLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztnQkFFdEMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxPQUFPLEdBQUcsc0JBQXNCLENBQUMsTUFBTSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxLQUFLLENBQUMsQ0FBQzs7Ozs7Z0JBRXRILFNBQVMsT0FBTyxDQUFDLElBQUk7b0JBQ2pCLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO29CQUNqQixjQUFjLENBQUMsTUFBTSxDQUFDLFFBQVEsR0FBRyxDQUFDLElBQUksQ0FBQyxLQUFLLEtBQUssQ0FBQyxDQUFDLENBQUM7b0JBQ3BELGNBQWMsQ0FBQyxNQUFNLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQztvQkFDbkMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsRUFBRTt3QkFDdkIsY0FBYyxDQUFDLE1BQU0sQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDO3dCQUNuQyxXQUFXLEVBQUUsQ0FBQzt3QkFDZCxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7cUJBQ2hCO29CQUNELFdBQVcsRUFBRSxDQUFDO29CQUNkLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDbEIsQ0FBQzs7Ozs7Z0JBRUQsU0FBUyxLQUFLLENBQUMsR0FBRztvQkFDZCxjQUFjLENBQUMsTUFBTSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7b0JBQ3ZDLGNBQWMsQ0FBQyxNQUFNLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQztvQkFDbkMsV0FBVyxFQUFFLENBQUM7b0JBQ2QsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUNoQixDQUFDO1lBRUwsQ0FBQyxFQUFDLENBQUM7UUFDUCxDQUFDOzs7OztRQUVELFNBQVMsY0FBYyxDQUFDLE1BQU07WUFDMUIsT0FBTyxRQUFRLENBQUMsTUFBTSxDQUFDLElBQUksT0FBTyxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO1FBQzFELENBQUM7Ozs7O1FBRUQsU0FBUyxPQUFPLENBQUMsS0FBSztZQUNsQixPQUFPLE9BQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsSUFBSSxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDOUQsQ0FBQzs7OztRQUVELFNBQVMsUUFBUTtZQUNiLE9BQU8sY0FBYyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUM7UUFFdkMsQ0FBQzs7Ozs7UUFFRCxTQUFTLHNCQUFzQixDQUFDLFlBQVk7O2tCQUNsQyxXQUFXLEdBQUcsUUFBUSxDQUFDLEVBQUUsRUFBRSwyQkFBMkIsQ0FBQztZQUM3RCxXQUFXLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7WUFDL0UsV0FBVyxDQUFDLEtBQUssQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUNqQyxXQUFXLENBQUMsTUFBTSxDQUFDLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQ25DLE9BQU8sV0FBVyxDQUFDO1FBQ3ZCLENBQUM7Ozs7Ozs7Ozs7OztRQU9ELFNBQVMsWUFBWSxDQUFDLE1BQU0sRUFBRSxPQUFPLEVBQUUsS0FBSyxFQUFFLFFBQVE7WUFDbEQsSUFBSSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLEVBQUU7Z0JBQ2pGLE9BQU8sQ0FBQyxJQUFJLENBQUMsZ0RBQWdELENBQUMsQ0FBQztnQkFDL0QsYUFBYSxFQUFFLENBQUM7Z0JBQ2hCLE9BQU8sSUFBSSxDQUFDO2FBQ2Y7O2dCQUVHLFdBQVcsR0FBRyxDQUFDOztnQkFDZixXQUFXLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDOztnQkFDakQsZ0JBQWdCLEdBQUcsZ0JBQWdCLEVBQUU7O2dCQUNyQyxNQUFNLEdBQUcsSUFBSTs7Z0JBQ2IsTUFBTTs7Z0JBQUUsT0FBTztZQUVuQixXQUFXLEVBQUUsQ0FBQztZQUNkLE9BQU8sZ0JBQWdCLENBQUM7Ozs7WUFFeEIsU0FBUyxXQUFXOztvQkFDWixLQUFLLEdBQUcsS0FBSzs7b0JBQ2IsSUFBSSxHQUFHLEtBQUs7O29CQUNaLENBQUM7Z0JBRUwsTUFBTSxHQUFHLGNBQWMsQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUM7Z0JBQ3RELENBQUMsR0FBRyxNQUFNLENBQUMsS0FBSyxDQUFDO2dCQUVqQixJQUFJLE9BQU8sQ0FBQyxDQUFDLENBQUMsRUFBRTtvQkFDWixPQUFPLEdBQUcsSUFBSSxDQUFDO29CQUNmLE1BQU0sR0FBRyxDQUFDLENBQUM7b0JBQ1gsSUFBSSxHQUFHLElBQUksQ0FBQztvQkFDWixnQkFBZ0IsQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLENBQUM7b0JBQ3RDLEtBQUssRUFBRSxDQUFDO2lCQUNYO3FCQUFNO29CQUNILE9BQU8sR0FBRyxzQkFBc0IsQ0FBQyxTQUFTLENBQUM7d0JBQ3ZDLENBQUMsRUFBRTs0QkFDQyxDQUFDLEVBQUUsQ0FBQzt5QkFDUDt3QkFDRCxPQUFPOzs7O3dCQUFFLFVBQVUsSUFBSTs0QkFDbkIsTUFBTSxHQUFHLGNBQWMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQzs0QkFFeEMsSUFBSSxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUU7Z0NBQ2hCLEtBQUssR0FBRyxJQUFJLENBQUM7NkJBQ2hCO2lDQUFNLElBQUksTUFBTSxLQUFLLENBQUMsRUFBRTtnQ0FDckIsSUFBSSxHQUFHLElBQUksQ0FBQzs2QkFDZjt3QkFDTCxDQUFDLENBQUE7d0JBQ0QsS0FBSzs7O3dCQUFFOzRCQUNILEtBQUssR0FBRyxJQUFJLENBQUM7d0JBQ2pCLENBQUMsQ0FBQTtxQkFDSixDQUFDLENBQUM7b0JBRUgsZ0JBQWdCLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxDQUFDO29CQUV0QyxPQUFPLENBQUMsZUFBZSxDQUFDLElBQUk7OztvQkFBQzt3QkFDekIsS0FBSyxFQUFFLENBQUM7b0JBQ1osQ0FBQyxFQUFDLENBQUM7aUJBQ047Ozs7Z0JBRUQsU0FBUyxLQUFLO29CQUNWLElBQUksS0FBSyxFQUFFO3dCQUNQLGFBQWEsRUFBRSxDQUFDO3FCQUNuQjt5QkFBTSxJQUFJLENBQUMsSUFBSSxJQUFJLFdBQVcsS0FBSyxXQUFXLEVBQUU7d0JBQzdDLGVBQWUsRUFBRSxDQUFDO3FCQUNyQjt5QkFBTSxJQUFJLElBQUksRUFBRTt3QkFDYixXQUFXLEVBQUUsQ0FBQztxQkFDakI7Z0JBQ0wsQ0FBQztZQUNMLENBQUM7Ozs7WUFFRCxTQUFTLGVBQWU7Z0JBQ3BCLElBQUksVUFBVSxDQUFDLE9BQU8sQ0FBQyxFQUFFO29CQUNyQixPQUFPLENBQUMsTUFBTSxFQUFFLFdBQVcsR0FBRyxDQUFDLENBQUMsQ0FBQztpQkFDcEM7Z0JBQ0QsZ0JBQWdCLEVBQUUsQ0FBQztZQUN2QixDQUFDOzs7O1lBRUQsU0FBUyxhQUFhO2dCQUNsQixJQUFJLFVBQVUsQ0FBQyxLQUFLLENBQUMsRUFBRTtvQkFDbkIsS0FBSyxFQUFFLENBQUM7aUJBQ1g7Z0JBQ0QsZ0JBQWdCLEVBQUUsQ0FBQztZQUN2QixDQUFDOzs7O1lBRUQsU0FBUyxnQkFBZ0I7Z0JBQ3JCLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO29CQUN0QixRQUFRLEVBQUUsQ0FBQztpQkFDZDtZQUNMLENBQUM7Ozs7O1lBRUQsU0FBUyxPQUFPLENBQUMsS0FBSztnQkFDbEIsT0FBTyxPQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLElBQUksS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzlELENBQUM7Ozs7WUFFRCxTQUFTLGdCQUFnQjs7b0JBQ2pCLE9BQU87Z0JBRVgsT0FBTztvQkFDSCxXQUFXOzs7O29CQUFFLFVBQVUsQ0FBQzt3QkFDcEIsT0FBTyxHQUFHLENBQUMsQ0FBQztvQkFDaEIsQ0FBQyxDQUFBO29CQUNELEtBQUs7OztvQkFBRTt3QkFDSCxJQUFJLE9BQU8sRUFBRTs0QkFDVCxPQUFPLENBQUMsS0FBSyxFQUFFLENBQUM7eUJBQ25CO29CQUNMLENBQUMsQ0FBQTtpQkFDSixDQUFDO1lBQ04sQ0FBQztRQUNMLENBQUM7Ozs7O1FBRUQsU0FBUyxjQUFjLENBQUMsTUFBTTs7Z0JBQ3RCLENBQUMsR0FBRyxNQUFNLElBQUksTUFBTSxDQUFDLFlBQVksQ0FBQztZQUV0QyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxFQUFFO2dCQUNkLENBQUMsR0FBRyxJQUFJLENBQUM7YUFDWjtZQUVELE9BQU8sQ0FBQyxDQUFDO1FBQ2IsQ0FBQzs7OztRQUdELFNBQVMsV0FBVztZQUNoQixjQUFjLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztRQUNsQyxDQUFDOzs7O1FBRUQsU0FBUyxXQUFXO1lBQ2hCLGNBQWMsQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO1FBQ25DLENBQUM7Ozs7UUFFRCxTQUFTLDJCQUEyQjtZQUNoQyxPQUFPLDJCQUEyQixDQUFDO1FBQ3ZDLENBQUM7Ozs7UUFFRCxTQUFTLFNBQVM7WUFDZCxPQUFPLGNBQWMsQ0FBQyxPQUFPLENBQUM7UUFDbEMsQ0FBQzs7OztRQUVELFNBQVMsVUFBVTtZQUNmLE9BQU8sR0FBRyxDQUFDLGNBQWMsQ0FBQyxNQUFNLEVBQUUsMEJBQTBCLEVBQUUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzNFLENBQUM7Ozs7OztRQUdELFNBQVMsS0FBSzs7Z0JBQ04sZ0JBQWdCLEdBQUcsS0FBSzs7Z0JBQ3hCLFVBQVUsR0FBRyxLQUFLO1lBQ3RCLE9BQU87Z0JBQ0gsS0FBSyxFQUFFLEtBQUs7Z0JBQ1osUUFBUSxFQUFFLFFBQVE7Z0JBQ2xCLFVBQVUsRUFBRSxVQUFVO2FBQ3pCLENBQUM7Ozs7WUFFRixTQUFTLFVBQVU7Z0JBQ2YsT0FBTyxnQkFBZ0IsSUFBSSxVQUFVLENBQUM7WUFDMUMsQ0FBQzs7Ozs7WUFFRCxTQUFTLEtBQUssQ0FBQyxNQUFNO2dCQUNqQixnQkFBZ0IsR0FBRyxLQUFLLENBQUM7Z0JBQ3pCLFVBQVUsR0FBRyxNQUFNLENBQUMsS0FBSyxFQUFFLE1BQU0sQ0FBQyxDQUFDO1lBQ3ZDLENBQUM7Ozs7OztZQUVELFNBQVMsTUFBTSxDQUFDLFFBQVEsRUFBRSxNQUFNO2dCQUM1QixPQUFPLFFBQVEsSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLFNBQVMsR0FBRyxNQUFNLENBQUMsSUFBSSxJQUFJLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUNuRyxDQUFDOzs7OztZQUdELFNBQWUsUUFBUTs7b0JBQ25CLE9BQU8sSUFBSSxPQUFPOzs7O29CQUFDLENBQUMsT0FBTyxFQUFFLEVBQUU7d0JBQzNCLFdBQVcsRUFBRSxDQUFDO3dCQUNkLElBQUksQ0FBQyxVQUFVLEVBQUUsSUFBSSxjQUFjLENBQUMsTUFBTSxDQUFDLFdBQVcsRUFBRTs0QkFDcEQsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDOzRCQUN4QixjQUFjLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxTQUFTLEVBQUUsQ0FBQzs0QkFFOUMsb0JBQW9CLEVBQUUsQ0FBQyxJQUFJOzs7OzRCQUFDLFVBQVUsTUFBbUI7O3NDQUMvQyxRQUFRLEdBQUcsQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDO2dDQUN4QyxJQUFJLENBQUMsUUFBUSxFQUFFO29DQUNYLE9BQU8sQ0FBQyxNQUFNLENBQUMsT0FBTzs7OztvQ0FBRSxVQUFVLElBQUk7d0NBQ2xDLGNBQWMsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7b0NBQ3BELENBQUMsRUFBQyxDQUFDO29DQUNILGdCQUFnQixHQUFHLEtBQUssQ0FBQztvQ0FDekIsVUFBVSxHQUFHLE1BQU0sQ0FBQyxLQUFLLEVBQUUsTUFBTSxDQUFDLENBQUM7aUNBQ3RDO3FDQUFNO29DQUNILGdCQUFnQixHQUFHLEtBQUssQ0FBQztvQ0FDekIsVUFBVSxHQUFHLElBQUksQ0FBQztpQ0FDckI7Z0NBQ0QsV0FBVyxFQUFFLENBQUM7Z0NBQ2QsT0FBTyxFQUFFLENBQUM7NEJBQ2QsQ0FBQyxFQUFDLENBQUM7eUJBQ047d0JBQ0QsT0FBTyxFQUFFLENBQUM7b0JBQ2QsQ0FBQyxFQUFDLENBQUE7Z0JBQ04sQ0FBQzthQUFBO1FBQ0wsQ0FBQztJQUdMLENBQUM7OztZQXZVSixVQUFVLFNBQUMsRUFBRSxVQUFVLEVBQUUsTUFBTSxFQUFFOzs7O1lBakN6QixlQUFlOzs7OztJQW1DcEIsZ0NBQU87O0lBQ1AsaUNBQVE7O0lBQ1IsK0JBQU07O0lBQ04sb0NBQVc7O0lBQ1gsa0NBQVM7O0lBQ1QsZ0RBQXVCOztJQUN2Qix3Q0FBZTs7SUFDZixzQ0FBYTs7SUFDYixrQ0FBUzs7SUFDVCxtQ0FBVTs7SUFDVixvQ0FBVzs7Ozs7SUFFUCxnREFBK0MiLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIENyZWF0ZWQgYnkgcmFkaWdvc3Qgb24gMjQuMDQuMTcuXG4gKi9cblxuaW1wb3J0IHsgYXNzaWduSW4sIGlzT2JqZWN0LCBpc0FycmF5LCBpc051bWJlciwgaXNOYU4sIHNpemUsIGlzTnVsbCwgaXNGdW5jdGlvbiwgaXNFbXB0eSwgZm9yRWFjaCwgZ2V0IH0gZnJvbSAnbG9kYXNoJztcbmltcG9ydCB7IEF1dG9rYWRSZXNvdXJjZSB9IGZyb20gJy4vYXV0b2thZFJlc291cmNlJztcbmltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuY29uc3QgU2VhcmNoID0gKCkgPT4ge1xuICAgIHJldHVybiB7XG4gICAgICAgIHBhcmFtczogbnVsbCxcbiAgICAgICAgcmVxdWVzdERhdGE6IG51bGwsXG4gICAgICAgIHJlcXVlc3Q6IG51bGwsXG4gICAgICAgIHJlc3VsdDogeyAvLyDQkiDRhNC+0YDQvNCw0YLQtSBrYWQuYXJiaXRyLnJ1XG4gICAgICAgICAgICBUb3RhbENvdW50OiBudWxsLFxuICAgICAgICAgICAgUGFnZXNDb3VudDogbnVsbCxcbiAgICAgICAgICAgIEl0ZW1zOiBbXSxcbiAgICAgICAgICAgIGVudHJpZXM6IFtdXG4gICAgICAgIH0sXG4gICAgICAgIG5vUmVzdWx0OiB0cnVlLFxuICAgICAgICBlcnJvcjogbnVsbCxcbiAgICAgICAgd2F0Y2g6IHRydWUsXG4gICAgICAgIHNvdXJjZXM6IHVuZGVmaW5lZCxcbiAgICB9O1xufTtcblxuY29uc3QgREVGQVVMVF9TRUFSQ0hfUEFSQU1TID0ge1xuICAgIHNpZGU6IDRcbn07XG5cbmNvbnN0IERFRkFVTFRfU0VBUkNIX1JFUVVFU1RfREFUQSA9IHtcbiAgICBxOiBudWxsLFxuICAgIGRhdGVGcm9tOiBudWxsLFxuICAgIGRhdGVUbzogbnVsbCxcbiAgICBwYWdlOiAxLFxuICAgIHBhZ2VJbmRleDogMVxufTtcblxuQEluamVjdGFibGUoeyBwcm92aWRlZEluOiAncm9vdCcgfSlcbmV4cG9ydCBjbGFzcyBBdXRva2FkU2VydmljZSB7XG4gICAgc2VhcmNoO1xuICAgIGxvYWRpbmc7XG4gICAgcGFnZXI7XG4gICAgaW5pdFNlYXJjaDtcbiAgICBkb1NlYXJjaDtcbiAgICBidWlsZFNlYXJjaFJlcXVlc3REYXRhO1xuICAgIGdldFJlc3VsdFRvdGFsO1xuICAgIGdldENhc2VDb3VudDtcbiAgICBoYXNFcnJvcjtcbiAgICBpc0xvYWRpbmc7XG4gICAgaXNOb1Jlc3VsdDtcbiAgICBjb25zdHJ1Y3RvcihcbiAgICAgICAgcHJpdmF0ZSBhdXRva2FkUmVzb3VyY2VTZXJ2aWNlOiBBdXRva2FkUmVzb3VyY2VcbiAgICApIHtcblxuICAgICAgICBsZXQgbm9kZSA9IG51bGw7XG5cbiAgICAgICAgY29uc3QgYXV0b2thZFNlcnZpY2UgPSB7XG4gICAgICAgICAgICBzZWFyY2g6IFNlYXJjaCgpLFxuICAgICAgICAgICAgbG9hZGluZzogZmFsc2UsXG4gICAgICAgICAgICBwYWdlcjogUGFnZXIoKSxcblxuICAgICAgICAgICAgaW5pdFNlYXJjaCxcbiAgICAgICAgICAgIGRvU2VhcmNoLFxuICAgICAgICAgICAgYnVpbGRTZWFyY2hSZXF1ZXN0RGF0YSxcbiAgICAgICAgICAgIGdldFJlc3VsdFRvdGFsLFxuICAgICAgICAgICAgZ2V0Q2FzZUNvdW50LFxuICAgICAgICAgICAgaGFzRXJyb3IsXG4gICAgICAgICAgICBpc0xvYWRpbmcsXG4gICAgICAgICAgICBpc05vUmVzdWx0LFxuXG4gICAgICAgIH07XG4gICAgICAgIE9iamVjdC5hc3NpZ24odGhpcywgYXV0b2thZFNlcnZpY2UpO1xuXG4gICAgICAgIGZ1bmN0aW9uIGluaXRTZWFyY2gobmV3Tm9kZSwgcGFyYW1zKSB7XG4gICAgICAgICAgICBub2RlID0gbmV3Tm9kZTtcbiAgICAgICAgICAgIGF1dG9rYWRTZXJ2aWNlLnNlYXJjaC5wYXJhbXMgPSBhc3NpZ25Jbih7fSwgREVGQVVMVF9TRUFSQ0hfUEFSQU1TLCBwYXJhbXMsIHtcbiAgICAgICAgICAgICAgICBzb3VyY2U6IDBcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9XG5cbiAgICAgICAgZnVuY3Rpb24gcmVzZXRTZWFyY2hSZXF1ZXN0KCkge1xuICAgICAgICAgICAgaWYgKGF1dG9rYWRTZXJ2aWNlLnNlYXJjaC5yZXF1ZXN0ICYmIGF1dG9rYWRTZXJ2aWNlLnNlYXJjaC5yZXF1ZXN0LmFib3J0KSB7XG4gICAgICAgICAgICAgICAgYXV0b2thZFNlcnZpY2Uuc2VhcmNoLnJlcXVlc3QuYWJvcnQoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGF1dG9rYWRTZXJ2aWNlLnNlYXJjaC5yZXF1ZXN0RGF0YSA9IGdldERlZmF1bHRTZWFyY2hSZXF1ZXN0RGF0YSgpO1xuICAgICAgICB9XG5cbiAgICAgICAgZnVuY3Rpb24gZG9TZWFyY2goKSB7XG4gICAgICAgICAgICBhdXRva2FkU2VydmljZS5zZWFyY2gucmVzdWx0ID0ge1xuICAgICAgICAgICAgICAgIFRvdGFsQ291bnQ6IG51bGwsXG4gICAgICAgICAgICAgICAgUGFnZXNDb3VudDogbnVsbCxcbiAgICAgICAgICAgICAgICBJdGVtczogW10sXG4gICAgICAgICAgICAgICAgZW50cmllczogW11cbiAgICAgICAgICAgIH07XG4gICAgICAgICAgICByZXNldFNlYXJjaFJlcXVlc3QoKTtcbiAgICAgICAgICAgIGF1dG9rYWRTZXJ2aWNlLnNlYXJjaC5yZXF1ZXN0RGF0YSA9IGF1dG9rYWRTZXJ2aWNlLmJ1aWxkU2VhcmNoUmVxdWVzdERhdGEoYXV0b2thZFNlcnZpY2Uuc2VhcmNoLnBhcmFtcyk7XG4gICAgICAgICAgICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUpID0+IHtcbiAgICAgICAgICAgICAgICBpZiAoaXNCbGFuayhhdXRva2FkU2VydmljZS5zZWFyY2gucmVxdWVzdERhdGEuaW5uKSkge1xuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLndhcm4oJ3NlYXJjaC5yZXF1ZXN0RGF0YS5pbm4gaXMgZW1wdHkgJyk7XG4gICAgICAgICAgICAgICAgICAgIHJlc29sdmUoKTtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICBzZWFyY2hSZXF1ZXN0UHJvbWlzZS5jYWxsKHRoaXMpLnRoZW4oKGRhdGEpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGF1dG9rYWRTZXJ2aWNlLnNlYXJjaC5yZXN1bHQgPSBkYXRhO1xuICAgICAgICAgICAgICAgICAgICAgICAgYXV0b2thZFNlcnZpY2UucGFnZXIucmVzZXQoZGF0YSk7XG4gICAgICAgICAgICAgICAgICAgICAgICBoaWRlTG9hZGluZygpO1xuICAgICAgICAgICAgICAgICAgICAgICAgcmVzb2x2ZSgpO1xuXG4gICAgICAgICAgICAgICAgICAgIH0sIChkYXRhKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBhdXRva2FkU2VydmljZS5zZWFyY2gucmVzdWx0ID0gZGF0YTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGF1dG9rYWRTZXJ2aWNlLnBhZ2VyLnJlc2V0KGRhdGEpO1xuICAgICAgICAgICAgICAgICAgICAgICAgaGlkZUxvYWRpbmcoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc29sdmUoKTtcbiAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cblxuICAgICAgICBmdW5jdGlvbiBzZWFyY2hSZXF1ZXN0UHJvbWlzZSgpIHtcbiAgICAgICAgICAgIHJldHVybiBuZXcgUHJvbWlzZShmdW5jdGlvbiAocmVzb2x2ZSwgcmVqZWN0KSB7XG4gICAgICAgICAgICAgICAgc2hvd0xvYWRpbmcoKTtcbiAgICAgICAgICAgICAgICBhdXRva2FkU2VydmljZS5zZWFyY2gubm9SZXN1bHQgPSB0cnVlO1xuXG4gICAgICAgICAgICAgICAgYXV0b2thZFNlcnZpY2Uuc2VhcmNoLnJlcXVlc3QgPSBhdXRva2FkUmVzb3VyY2VTZXJ2aWNlLnNlYXJjaChhdXRva2FkU2VydmljZS5zZWFyY2gucmVxdWVzdERhdGEpLnRoZW4oc3VjY2VzcywgZXJyb3IpO1xuXG4gICAgICAgICAgICAgICAgZnVuY3Rpb24gc3VjY2VzcyhkYXRhKSB7XG4gICAgICAgICAgICAgICAgICAgIGRhdGEgPSBkYXRhLmRhdGE7XG4gICAgICAgICAgICAgICAgICAgIGF1dG9rYWRTZXJ2aWNlLnNlYXJjaC5ub1Jlc3VsdCA9IChkYXRhLnRvdGFsID09PSAwKTtcbiAgICAgICAgICAgICAgICAgICAgYXV0b2thZFNlcnZpY2Uuc2VhcmNoLmVycm9yID0gbnVsbDtcbiAgICAgICAgICAgICAgICAgICAgaWYgKCFoYXNSZXN1bHRJdGVtcyhkYXRhKSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgYXV0b2thZFNlcnZpY2Uuc2VhcmNoLmVycm9yID0gdHJ1ZTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGhpZGVMb2FkaW5nKCk7XG4gICAgICAgICAgICAgICAgICAgICAgICByZWplY3QoZGF0YSk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgaGlkZUxvYWRpbmcoKTtcbiAgICAgICAgICAgICAgICAgICAgcmVzb2x2ZShkYXRhKTtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICBmdW5jdGlvbiBlcnJvcihlcnIpIHtcbiAgICAgICAgICAgICAgICAgICAgYXV0b2thZFNlcnZpY2Uuc2VhcmNoLm5vUmVzdWx0ID0gZmFsc2U7XG4gICAgICAgICAgICAgICAgICAgIGF1dG9rYWRTZXJ2aWNlLnNlYXJjaC5lcnJvciA9IHRydWU7XG4gICAgICAgICAgICAgICAgICAgIGhpZGVMb2FkaW5nKCk7XG4gICAgICAgICAgICAgICAgICAgIHJlamVjdChlcnIpO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cblxuICAgICAgICBmdW5jdGlvbiBoYXNSZXN1bHRJdGVtcyhyZXN1bHQpIHtcbiAgICAgICAgICAgIHJldHVybiBpc09iamVjdChyZXN1bHQpICYmIGlzQXJyYXkocmVzdWx0WydlbnRyaWVzJ10pO1xuICAgICAgICB9XG5cbiAgICAgICAgZnVuY3Rpb24gaXNCbGFuayh2YWx1ZSkge1xuICAgICAgICAgICAgcmV0dXJuIGlzRW1wdHkodmFsdWUpICYmICFpc051bWJlcih2YWx1ZSkgfHwgaXNOYU4odmFsdWUpO1xuICAgICAgICB9XG5cbiAgICAgICAgZnVuY3Rpb24gaGFzRXJyb3IoKSB7XG4gICAgICAgICAgICByZXR1cm4gYXV0b2thZFNlcnZpY2Uuc2VhcmNoLmVycm9yO1xuXG4gICAgICAgIH1cblxuICAgICAgICBmdW5jdGlvbiBidWlsZFNlYXJjaFJlcXVlc3REYXRhKHNlYXJjaFBhcmFtcykge1xuICAgICAgICAgICAgY29uc3QgcmVxdWVzdERhdGEgPSBhc3NpZ25Jbih7fSwgREVGQVVMVF9TRUFSQ0hfUkVRVUVTVF9EQVRBKTtcbiAgICAgICAgICAgIHJlcXVlc3REYXRhWyd0eXBlJ10gPSAoc2VhcmNoUGFyYW1zWydzaWRlJ10gPT09IC0xID8gNCA6IHNlYXJjaFBhcmFtc1snc2lkZSddKTtcbiAgICAgICAgICAgIHJlcXVlc3REYXRhWydpbm4nXSA9IG5vZGVbJ2lubiddO1xuICAgICAgICAgICAgcmVxdWVzdERhdGFbJ29ncm4nXSA9IG5vZGVbJ29ncm4nXTtcbiAgICAgICAgICAgIHJldHVybiByZXF1ZXN0RGF0YTtcbiAgICAgICAgfVxuXG4gICAgICAgIC8qXG4gICAgICAgICAgICAqIGNhc2VcbiAgICAgICAgICAgICpcbiAgICAgICAgICAgICovXG5cbiAgICAgICAgZnVuY3Rpb24gZ2V0Q2FzZUNvdW50KHNlYXJjaCwgc3VjY2VzcywgZXJyb3IsIGNvbXBsZXRlKSB7XG4gICAgICAgICAgICBpZiAoIWlzQXJyYXkoYXV0b2thZFNlcnZpY2Uuc2VhcmNoLnNvdXJjZXMpIHx8ICFzaXplKGF1dG9rYWRTZXJ2aWNlLnNlYXJjaC5zb3VyY2VzKSkge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUud2FybignZ2V0Q2FzZUNvdW50Li4uIGVycm9yOiBzZWFyY2guc291cmNlcyBpcyBibGFuaycpO1xuICAgICAgICAgICAgICAgIGVycm9yQ2FsbGJhY2soKTtcbiAgICAgICAgICAgICAgICByZXR1cm4gbnVsbDtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgbGV0IHNvdXJjZUluZGV4ID0gMCxcbiAgICAgICAgICAgICAgICBzb3VyY2VDb3VudCA9IHNpemUoYXV0b2thZFNlcnZpY2Uuc2VhcmNoLnNvdXJjZXMpLFxuICAgICAgICAgICAgICAgIGNhc2VDb3VudFJlcXVlc3QgPSBDYXNlQ291bnRSZXF1ZXN0KCksXG4gICAgICAgICAgICAgICAgcmVzdWx0ID0gbnVsbCxcbiAgICAgICAgICAgICAgICBzb3VyY2UsIHJlcXVlc3Q7XG5cbiAgICAgICAgICAgIG5leHRSZXF1ZXN0KCk7XG4gICAgICAgICAgICByZXR1cm4gY2FzZUNvdW50UmVxdWVzdDtcblxuICAgICAgICAgICAgZnVuY3Rpb24gbmV4dFJlcXVlc3QoKSB7XG4gICAgICAgICAgICAgICAgbGV0IGVycm9yID0gZmFsc2UsXG4gICAgICAgICAgICAgICAgICAgIG5leHQgPSBmYWxzZSxcbiAgICAgICAgICAgICAgICAgICAgcTtcblxuICAgICAgICAgICAgICAgIHNvdXJjZSA9IGF1dG9rYWRTZXJ2aWNlLnNlYXJjaC5zb3VyY2VzW3NvdXJjZUluZGV4KytdO1xuICAgICAgICAgICAgICAgIHEgPSBzb3VyY2UudmFsdWU7XG5cbiAgICAgICAgICAgICAgICBpZiAoaXNCbGFuayhxKSkge1xuICAgICAgICAgICAgICAgICAgICByZXF1ZXN0ID0gbnVsbDtcbiAgICAgICAgICAgICAgICAgICAgcmVzdWx0ID0gMDtcbiAgICAgICAgICAgICAgICAgICAgbmV4dCA9IHRydWU7XG4gICAgICAgICAgICAgICAgICAgIGNhc2VDb3VudFJlcXVlc3QuX3NldFJlcXVlc3QocmVxdWVzdCk7XG4gICAgICAgICAgICAgICAgICAgIGNoZWNrKCk7XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgcmVxdWVzdCA9IGF1dG9rYWRSZXNvdXJjZVNlcnZpY2Uua2FkU2VhcmNoKHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBxOiBxXG4gICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgc3VjY2VzczogZnVuY3Rpb24gKGRhdGEpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXN1bHQgPSBnZXRSZXN1bHRUb3RhbChkYXRhWydSZXN1bHQnXSk7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoaXNOdWxsKHJlc3VsdCkpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZXJyb3IgPSB0cnVlO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAocmVzdWx0ID09PSAwKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5leHQgPSB0cnVlO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICBlcnJvcjogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGVycm9yID0gdHJ1ZTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgICAgICAgICAgY2FzZUNvdW50UmVxdWVzdC5fc2V0UmVxdWVzdChyZXF1ZXN0KTtcblxuICAgICAgICAgICAgICAgICAgICByZXF1ZXN0LmNvbXBsZXRlUHJvbWlzZS50aGVuKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGNoZWNrKCk7XG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIGZ1bmN0aW9uIGNoZWNrKCkge1xuICAgICAgICAgICAgICAgICAgICBpZiAoZXJyb3IpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGVycm9yQ2FsbGJhY2soKTtcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmICghbmV4dCB8fCBzb3VyY2VJbmRleCA9PT0gc291cmNlQ291bnQpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHN1Y2Nlc3NDYWxsYmFjaygpO1xuICAgICAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKG5leHQpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIG5leHRSZXF1ZXN0KCk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGZ1bmN0aW9uIHN1Y2Nlc3NDYWxsYmFjaygpIHtcbiAgICAgICAgICAgICAgICBpZiAoaXNGdW5jdGlvbihzdWNjZXNzKSkge1xuICAgICAgICAgICAgICAgICAgICBzdWNjZXNzKHJlc3VsdCwgc291cmNlSW5kZXggLSAxKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgY29tcGxldGVDYWxsYmFjaygpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBmdW5jdGlvbiBlcnJvckNhbGxiYWNrKCkge1xuICAgICAgICAgICAgICAgIGlmIChpc0Z1bmN0aW9uKGVycm9yKSkge1xuICAgICAgICAgICAgICAgICAgICBlcnJvcigpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBjb21wbGV0ZUNhbGxiYWNrKCk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGZ1bmN0aW9uIGNvbXBsZXRlQ2FsbGJhY2soKSB7XG4gICAgICAgICAgICAgICAgaWYgKGlzRnVuY3Rpb24oY29tcGxldGUpKSB7XG4gICAgICAgICAgICAgICAgICAgIGNvbXBsZXRlKCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBmdW5jdGlvbiBpc0JsYW5rKHZhbHVlKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIGlzRW1wdHkodmFsdWUpICYmICFpc051bWJlcih2YWx1ZSkgfHwgaXNOYU4odmFsdWUpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBmdW5jdGlvbiBDYXNlQ291bnRSZXF1ZXN0KCkge1xuICAgICAgICAgICAgICAgIGxldCByZXF1ZXN0O1xuXG4gICAgICAgICAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgICAgICAgICAgX3NldFJlcXVlc3Q6IGZ1bmN0aW9uIChyKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXF1ZXN0ID0gcjtcbiAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgYWJvcnQ6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChyZXF1ZXN0KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmVxdWVzdC5hYm9ydCgpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIGZ1bmN0aW9uIGdldFJlc3VsdFRvdGFsKHJlc3VsdCkge1xuICAgICAgICAgICAgbGV0IHIgPSByZXN1bHQgJiYgcmVzdWx0WydUb3RhbENvdW50J107XG5cbiAgICAgICAgICAgIGlmICghaXNOdW1iZXIocikpIHtcbiAgICAgICAgICAgICAgICByID0gbnVsbDtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgcmV0dXJuIHI7XG4gICAgICAgIH1cblxuXG4gICAgICAgIGZ1bmN0aW9uIHNob3dMb2FkaW5nKCkge1xuICAgICAgICAgICAgYXV0b2thZFNlcnZpY2UubG9hZGluZyA9IHRydWU7XG4gICAgICAgIH1cblxuICAgICAgICBmdW5jdGlvbiBoaWRlTG9hZGluZygpIHtcbiAgICAgICAgICAgIGF1dG9rYWRTZXJ2aWNlLmxvYWRpbmcgPSBmYWxzZTtcbiAgICAgICAgfVxuXG4gICAgICAgIGZ1bmN0aW9uIGdldERlZmF1bHRTZWFyY2hSZXF1ZXN0RGF0YSgpIHtcbiAgICAgICAgICAgIHJldHVybiBERUZBVUxUX1NFQVJDSF9SRVFVRVNUX0RBVEE7XG4gICAgICAgIH1cblxuICAgICAgICBmdW5jdGlvbiBpc0xvYWRpbmcoKSB7XG4gICAgICAgICAgICByZXR1cm4gYXV0b2thZFNlcnZpY2UubG9hZGluZztcbiAgICAgICAgfVxuXG4gICAgICAgIGZ1bmN0aW9uIGlzTm9SZXN1bHQoKSB7XG4gICAgICAgICAgICByZXR1cm4gZ2V0KGF1dG9rYWRTZXJ2aWNlLnNlYXJjaCwgJ3Jlc3VsdC5zdGF0dXMuaXRlbXNGb3VuZCcsIDApID09PSAwO1xuICAgICAgICB9XG5cbiAgICAgICAgLy9QYWdlclxuICAgICAgICBmdW5jdGlvbiBQYWdlcigpIHtcbiAgICAgICAgICAgIGxldCBpbnRlcm5hbERpc2FibGVkID0gZmFsc2UsXG4gICAgICAgICAgICAgICAgbm9OZXh0UGFnZSA9IGZhbHNlO1xuICAgICAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgICAgICByZXNldDogcmVzZXQsXG4gICAgICAgICAgICAgICAgbmV4dFBhZ2U6IG5leHRQYWdlLFxuICAgICAgICAgICAgICAgIGlzRGlzYWJsZWQ6IGlzRGlzYWJsZWRcbiAgICAgICAgICAgIH07XG5cbiAgICAgICAgICAgIGZ1bmN0aW9uIGlzRGlzYWJsZWQoKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIGludGVybmFsRGlzYWJsZWQgfHwgbm9OZXh0UGFnZTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgZnVuY3Rpb24gcmVzZXQocmVzdWx0KSB7XG4gICAgICAgICAgICAgICAgaW50ZXJuYWxEaXNhYmxlZCA9IGZhbHNlO1xuICAgICAgICAgICAgICAgIG5vTmV4dFBhZ2UgPSBub01vcmUoZmFsc2UsIHJlc3VsdCk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGZ1bmN0aW9uIG5vTW9yZShoYXNFcnJvciwgcmVzdWx0KSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIGhhc0Vycm9yIHx8IChhdXRva2FkU2VydmljZS5zZWFyY2gucmVxdWVzdERhdGEucGFnZUluZGV4ICogcmVzdWx0LnNpemUgPj0gcmVzdWx0LnRvdGFsKTtcbiAgICAgICAgICAgIH1cblxuXG4gICAgICAgICAgICBhc3luYyBmdW5jdGlvbiBuZXh0UGFnZSgpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgc2hvd0xvYWRpbmcoKTtcbiAgICAgICAgICAgICAgICAgICAgaWYgKCFpc0Rpc2FibGVkKCkgJiYgYXV0b2thZFNlcnZpY2Uuc2VhcmNoLnJlcXVlc3REYXRhKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBpbnRlcm5hbERpc2FibGVkID0gdHJ1ZTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGF1dG9rYWRTZXJ2aWNlLnNlYXJjaC5yZXF1ZXN0RGF0YS5wYWdlSW5kZXgrKztcblxuICAgICAgICAgICAgICAgICAgICAgICAgc2VhcmNoUmVxdWVzdFByb21pc2UoKS50aGVuKGZ1bmN0aW9uIChyZXN1bHQ6IHsgZW50cmllcyB9KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgaGFzRXJyb3IgPSAhaGFzUmVzdWx0SXRlbXMocmVzdWx0KTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoIWhhc0Vycm9yKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZvckVhY2gocmVzdWx0LmVudHJpZXMsIGZ1bmN0aW9uIChpdGVtKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhdXRva2FkU2VydmljZS5zZWFyY2gucmVzdWx0LmVudHJpZXMucHVzaChpdGVtKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGludGVybmFsRGlzYWJsZWQgPSBmYWxzZTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbm9OZXh0UGFnZSA9IG5vTW9yZShmYWxzZSwgcmVzdWx0KTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpbnRlcm5hbERpc2FibGVkID0gZmFsc2U7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5vTmV4dFBhZ2UgPSB0cnVlO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBoaWRlTG9hZGluZygpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlc29sdmUoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIHJlc29sdmUoKTtcbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgfVxuICAgICAgICB9XG5cblxuICAgIH1cblxufVxuXG5cblxuIl19