/**
 * @fileoverview added by tsickle
 * Generated from: lib/autokad/autokadComponent.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/**
 * Created by radigost on 24.04.17.
 */
import { isEmpty, get, filter } from 'lodash';
import { AutokadService } from './autokadService';
import { Node } from '../domain/Node';
import { Component, Input, Output, EventEmitter } from '@angular/core';
export class AutokadComponent {
    /**
     * @param {?} autokadService
     */
    constructor(autokadService) {
        this.autokadService = autokadService;
        this.onAddArbitrationBreadcrumb = new EventEmitter();
        this.loading = true;
        Object.assign(this, {
            pager: autokadService.pager,
        });
    }
    /**
     * @return {?}
     */
    ngOnChanges() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            this.autokadService.initSearch(this.node);
            if (this.side) {
                this.autokadService.search.params.side = this.side;
            }
            try {
                yield this.autokadService.doSearch();
            }
            finally {
                this.loading = false;
                if (this.scrollTop) {
                    window.scrollTo(0, this.scrollTop);
                }
            }
        });
    }
    /**
     * @return {?}
     */
    get list() {
        return this.autokadService.search.result.entries;
    }
    /**
     * @param {?} type
     * @return {?}
     */
    changeSide(type) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            this.loading = true;
            try {
                this.autokadService.search.params.side = type;
                yield this.autokadService.doSearch();
            }
            finally {
                this.loading = false;
            }
        });
    }
    /**
     * @param {?} type
     * @return {?}
     */
    isSelectedSide(type) {
        return this.autokadService.search.params
            && (parseInt(type) === parseInt(this.autokadService.search.params.side));
    }
    /**
     * @param {?} caseSides
     * @return {?}
     */
    getPlaintiff(caseSides) {
        return filter(caseSides, (/**
         * @param {?} item
         * @return {?}
         */
        (item) => item.type === 0));
    }
    /**
     * @param {?} caseSides
     * @return {?}
     */
    getDefendant(caseSides) {
        return filter(caseSides, (/**
         * @param {?} item
         * @return {?}
         */
        (item) => item.type === 1));
    }
    /**
     * @param {?} caseSides
     * @return {?}
     */
    getOthers(caseSides) {
        return filter(caseSides, (/**
         * @param {?} item
         * @return {?}
         */
        (item) => item.type !== 1 && item.type !== 0));
    }
    /**
     * @return {?}
     */
    getTotal() {
        /** @type {?} */
        const hasSearchResult = (/**
         * @return {?}
         */
        () => {
            return !isEmpty(this.autokadService.search.result);
        });
        return hasSearchResult() ? get(this.autokadService, 'search.result.total', 0) : 0;
    }
    /**
     * @return {?}
     */
    isEmptyResult() {
        return !this.getTotal();
    }
    /**
     * @return {?}
     */
    isNoResult() {
        return this.autokadService.search.noResult;
    }
    /**
     * @return {?}
     */
    hasError() {
        return this.autokadService.search.error;
    }
    /**
     * @return {?}
     */
    get isLoading() {
        return this.loading;
    }
    /**
     * @param {?} id
     * @return {?}
     */
    addArbitrationBreadcrumb(id) {
        this.onAddArbitrationBreadcrumb.emit(id);
    }
    /**
     * @return {?}
     */
    nextPage() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            this.loading = true;
            try {
                /** @type {?} */
                const promise = this.pager.nextPage().then((/**
                 * @return {?}
                 */
                () => {
                    this.loading = false;
                }));
                console.log(promise);
                yield promise;
            }
            finally {
            }
        });
    }
}
AutokadComponent.decorators = [
    { type: Component, args: [{
                selector: 'nkb-autokad',
                template: `
        <div class="autokad autokad__inner">
            <div class="autokad__filters">
                <div class="">
                    <span>Сторона дела</span><br>
                    <button class="btn  btn-sm flat" [ngClass]="isSelectedSide(4) ? 'active':''"
                            (click)="changeSide(4)">любое
                    </button>
                    <button class="btn  btn-sm flat" [ngClass]="isSelectedSide(0) ? 'active':''"
                            (click)="changeSide(0)">истец
                    </button>
                    <button class="btn  btn-sm flat" [ngClass]="isSelectedSide(1) ? 'active':''"
                            (click)="changeSide(1)">ответчик
                    </button>
                    <button class="btn  btn-sm flat" [ngClass]="isSelectedSide(2) ? 'active':''"
                            (click)="changeSide(2)">иное лицо
                    </button>
                    <button class="btn  btn-sm flat" [ngClass]="isSelectedSide(3) ? 'active':''"
                            (click)="changeSide(3)">третье лицо
                    </button>
                </div>
                <div class="autokad__total">
                    <a class="underline" [attr.href]="'http://kad.arbitr.ru/ ' | externalUrl" target="_blank">Картотека
                        арбитражных дел</a>
                    <div>
                        <span [hidden]="getTotal()===0 || isLoading">
                            <span>{{getTotal()}}</span>
                            <!-- | translate: {number: getTotal()} -->
                            <!-- {{'AUTOKAD.KAD_SEARCH_TOTAL' | async }} -->
                            &nbsp;
                        </span>
                    </div>
                </div>
            </div>
            <div class="autokad__result">
                <div class="autokad__result-list"
                    infiniteScroll
                    (scrolled)="nextPage()"
                >
                    <div
                        class="result-list-element autokad__list-element autokad-case autokad-case__inner"
                        *ngFor="let item of list"
                    >
                        <div class="autokad-case__header">
                            <div class="autokad-case__caption">
                                <div class="autokad-case__number">
                                    {{item.id}}
                                </div>
                                <div class="autokad-case__instance-container">
                                    <div>{{item.filledMillis * 1000 | date : 'dd.MM.yyyy'}}</div>
                                    <div class="autokad-case__instance">{{item.currentInstance}}</div>
                                </div>
                                <div class="autokad-case__type">{{item.type.name}}</div>
                            </div>
                            <div
                                class="autokad-case__process-container autokad-case__process-container--1"
                            >
                                <span
                                    class="autokad-case__process autokad-case__process--1"
                                    title="Банкротное дело"
                                    *ngIf="item.type.code===1"
                                >Б</span>
                            </div>

                            <div class="autokad-case__process-container">
                                <span class="autokad-case__process" *ngIf="item.active===true">на рассмотрении</span>
                            </div>
                            <div class="autokad-case__sum">
                                <span *ngIf="item.claimAmount !== 0">{{item.claimAmount | number}}р.</span>
                            </div>
                        </div>

                        <div class="autokad-sides autokad-sides__inner">
                            <div class="autokad-sides__row">
                                <div class="autokad-sides__caption">
                                    Истец:
                                </div>
                                <div class="autokad-sides__companies">
                                    <div *ngFor="let company of getPlaintiff(item.parties) ; last as isLast">
                                        <nkb-company-name-with-link
                                            (addBreadcrumb)="addArbitrationBreadcrumb(id)"
                                            [node]="company"
                                            [ogrn]="node.ogrn"
                                        ></nkb-company-name-with-link>
                                        <span [hidden]="isLast">, &nbsp;</span>
                                    </div>
                                </div>
                            </div>
                            <div class="autokad-sides__row">
                                <div class="autokad-sides__caption">
                                    Ответчик:
                                </div>
                                <div class="autokad-sides__companies">
                                    <div *ngFor="let company of getDefendant(item.parties); last as isLast">
                                        <nkb-company-name-with-link
                                            [ogrn]="node.ogrn"
                                            (addBreadcrumb)="addArbitrationBreadcrumb(id)"
                                            [node]="company"></nkb-company-name-with-link>
                                        <span [hidden]="isLast">, &nbsp;</span>
                                    </div>
                                </div>
                            </div>
                            <div class="autokad-sides__row" *ngIf="getOthers(item.parties).length>0">
                                <div class="autokad-sides__caption">
                                    Прочие лица:
                                </div>
                                <div class="autokad-sides__companies">
                                    <div *ngFor="let company of getOthers(item.parties); last as isLast">
                                        <nkb-company-name-with-link
                                            [ogrn]="node.ogrn"
                                            (addBreadcrumb)="addArbitrationBreadcrumb(id)"
                                            [node]="company"
                                        ></nkb-company-name-with-link>
                                        <span [hidden]="isLast">, &nbsp;</span>
                                    </div>
                                </div>
                            </div>

                            <div class="autokad-documents" *ngIf="item.documents">
                                <h5>Документы по делу:</h5>
                                <ul>
                                    <li *ngFor=" let document of item.documents">
                                        <a target="_blank" [attr.href]="document.url">
                                            <span>{{document.date * 1000 | date:'dd.MM.yyyy'}}</span>
                                            .
                                            {{document.name}}
                                        </a>
                                    </li>
                                </ul>

                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="align-center empty-result muted"
                *ngIf="isNoResult()  && !hasError() && !isLoading" style="margin-top: 4em;">
                Арбитражных дел не найдено
            </div>
            <div class="align-center empty-result" *ngIf="hasError() && !isLoading">
                Поиск в
                <a class="underline" [attr.href]="'http://kad.arbitr.ru/' | externalUrl" target="_blank">
                    картотеке арбитражных дел
                </a>
                временно недоступен
            </div>
            <div class="loader" [hidden]="!loading" style="margin-top:1em; "></div>
        </div>
    `,
                styles: [".autokad__filters{display:-webkit-box;display:flex;-webkit-box-pack:justify;justify-content:space-between}.autokad__total{display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column;-webkit-box-pack:justify;justify-content:space-between}.autokad__result-list{margin-top:1em}.autokad-case__inner{padding:1em;border-color:#444;cursor:default}.autokad-case__inner:hover{border-color:#444;cursor:default}.autokad-case__header{display:-webkit-box;display:flex;-webkit-box-pack:justify;justify-content:space-between;margin-bottom:1em}.autokad-case__caption{flex-basis:60%;display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column}.autokad-case__process{background-color:#2f96b4;color:#fff;display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column;-webkit-box-pack:center;justify-content:center;text-align:center;font-weight:700;border-radius:3px;height:2em;width:10em;align-self:center}.autokad-case__process--1{width:2em;background-color:#bd362f;margin-left:.5em;margin-right:.5em}.autokad-case__process-container{flex-basis:10%}.autokad-case__process-container--1{flex-basis:1em}.autokad-case__sum{font-size:large;font-weight:700;flex-basis:20%;display:-webkit-box;display:flex;-webkit-box-pack:end;justify-content:flex-end}.autokad-case__number{font-size:large;font-weight:700}.autokad-case__instance-container{display:-webkit-box;display:flex;-webkit-box-orient:horizontal;-webkit-box-direction:normal;flex-direction:row}.autokad-case__instance-container div{margin:0 1em}.autokad-case__instance-container div:first-of-type{margin:0}.autokad-case__type{font-style:italic}.autokad-sides__row{display:-webkit-box;display:flex;-webkit-box-orient:horizontal;-webkit-box-direction:normal;flex-direction:row}.autokad-sides__caption{flex-basis:20%;font-weight:700}.autokad-sides__companies{flex-basis:80%;display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column}.autokad-sides__companies div{margin:.2em 0}.autokad-documents h5{text-align:center;font-weight:700}.autokad__company-name--highlight{background-color:#ff0}"]
            }] }
];
/** @nocollapse */
AutokadComponent.ctorParameters = () => [
    { type: AutokadService }
];
AutokadComponent.propDecorators = {
    scrollTop: [{ type: Input }],
    node: [{ type: Input }],
    side: [{ type: Input }],
    onAddArbitrationBreadcrumb: [{ type: Output }]
};
if (false) {
    /** @type {?} */
    AutokadComponent.prototype.scrollTop;
    /** @type {?} */
    AutokadComponent.prototype.node;
    /** @type {?} */
    AutokadComponent.prototype.side;
    /** @type {?} */
    AutokadComponent.prototype.onAddArbitrationBreadcrumb;
    /** @type {?} */
    AutokadComponent.prototype.id;
    /** @type {?} */
    AutokadComponent.prototype.pager;
    /** @type {?} */
    AutokadComponent.prototype.loading;
    /**
     * @type {?}
     * @private
     */
    AutokadComponent.prototype.autokadService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0b2thZENvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bua2IvYW5ndWxhci1ua2ItbGliLyIsInNvdXJjZXMiOlsibGliL2F1dG9rYWQvYXV0b2thZENvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7QUFJQSxPQUFPLEVBQUUsT0FBTyxFQUFFLEdBQUcsRUFBRSxNQUFNLEVBQUUsTUFBTSxRQUFRLENBQUM7QUFDOUMsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLGtCQUFrQixDQUFDO0FBQ2xELE9BQU8sRUFBRSxJQUFJLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUN0QyxPQUFPLEVBQUUsU0FBUyxFQUFhLEtBQUssRUFBRSxNQUFNLEVBQUUsWUFBWSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBOEpsRixNQUFNLE9BQU8sZ0JBQWdCOzs7O0lBU3pCLFlBQ1ksY0FBOEI7UUFBOUIsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBTmhDLCtCQUEwQixHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFJbkQsWUFBTyxHQUFHLElBQUksQ0FBQztRQUlsQixNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRTtZQUNoQixLQUFLLEVBQUUsY0FBYyxDQUFDLEtBQUs7U0FDOUIsQ0FBQyxDQUFDO0lBQ1AsQ0FBQzs7OztJQUVLLFdBQVc7O1lBQ2IsSUFBSSxDQUFDLGNBQWMsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzFDLElBQUksSUFBSSxDQUFDLElBQUksRUFBRTtnQkFDWCxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUM7YUFDdEQ7WUFDRCxJQUFJO2dCQUNBLE1BQU0sSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLEVBQUUsQ0FBQzthQUN4QztvQkFDTztnQkFDSixJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztnQkFDckIsSUFBSSxJQUFJLENBQUMsU0FBUyxFQUFFO29CQUNoQixNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7aUJBQ3RDO2FBQ0o7UUFDTCxDQUFDO0tBQUE7Ozs7SUFFRCxJQUFJLElBQUk7UUFDSixPQUFPLElBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUM7SUFDckQsQ0FBQzs7Ozs7SUFFSyxVQUFVLENBQUMsSUFBSTs7WUFDakIsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7WUFDcEIsSUFBSTtnQkFDQSxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQztnQkFDOUMsTUFBTSxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsRUFBRSxDQUFDO2FBQ3hDO29CQUNPO2dCQUNKLElBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO2FBQ3hCO1FBQ0wsQ0FBQztLQUFBOzs7OztJQUVELGNBQWMsQ0FBQyxJQUFJO1FBQ2YsT0FBTyxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxNQUFNO2VBQ2pDLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLFFBQVEsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztJQUNqRixDQUFDOzs7OztJQUVELFlBQVksQ0FBQyxTQUFTO1FBQ2xCLE9BQU8sTUFBTSxDQUFDLFNBQVM7Ozs7UUFBRSxDQUFDLElBQUksRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksS0FBSyxDQUFDLEVBQUMsQ0FBQztJQUN4RCxDQUFDOzs7OztJQUVELFlBQVksQ0FBQyxTQUFTO1FBQ2xCLE9BQU8sTUFBTSxDQUFDLFNBQVM7Ozs7UUFBRSxDQUFDLElBQUksRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksS0FBSyxDQUFDLEVBQUMsQ0FBQztJQUN4RCxDQUFDOzs7OztJQUVELFNBQVMsQ0FBQyxTQUFTO1FBQ2YsT0FBTyxNQUFNLENBQUMsU0FBUzs7OztRQUFFLENBQUMsSUFBSSxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxLQUFLLENBQUMsSUFBSSxJQUFJLENBQUMsSUFBSSxLQUFLLENBQUMsRUFBQyxDQUFDO0lBQzNFLENBQUM7Ozs7SUFFRCxRQUFROztjQUNFLGVBQWU7OztRQUFHLEdBQUcsRUFBRTtZQUN6QixPQUFPLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ3ZELENBQUMsQ0FBQTtRQUNELE9BQU8sZUFBZSxFQUFFLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsY0FBYyxFQUFFLHFCQUFxQixFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDdEYsQ0FBQzs7OztJQUVELGFBQWE7UUFDVCxPQUFPLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO0lBQzVCLENBQUM7Ozs7SUFFRCxVQUFVO1FBQ04sT0FBTyxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUM7SUFDL0MsQ0FBQzs7OztJQUVELFFBQVE7UUFDSixPQUFPLElBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQztJQUM1QyxDQUFDOzs7O0lBRUQsSUFBSSxTQUFTO1FBQ1QsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDO0lBQ3hCLENBQUM7Ozs7O0lBRUQsd0JBQXdCLENBQUMsRUFBRTtRQUN2QixJQUFJLENBQUMsMEJBQTBCLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO0lBQzdDLENBQUM7Ozs7SUFFSyxRQUFROztZQUNWLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO1lBQ3BCLElBQUk7O3NCQUNNLE9BQU8sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsRUFBRSxDQUFDLElBQUk7OztnQkFBQyxHQUFHLEVBQUU7b0JBQzVDLElBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO2dCQUN6QixDQUFDLEVBQUM7Z0JBQ0YsT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQztnQkFDckIsTUFBTSxPQUFPLENBQUM7YUFDakI7b0JBQ087YUFFUDtRQUNMLENBQUM7S0FBQTs7O1lBcFFKLFNBQVMsU0FBQztnQkFDUCxRQUFRLEVBQUUsYUFBYTtnQkFFdkIsUUFBUSxFQUFFOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7S0FzSlQ7O2FBQ0o7Ozs7WUE5SlEsY0FBYzs7O3dCQWlLbEIsS0FBSzttQkFDTCxLQUFLO21CQUNMLEtBQUs7eUNBQ0wsTUFBTTs7OztJQUhQLHFDQUEyQjs7SUFDM0IsZ0NBQW9COztJQUNwQixnQ0FBbUI7O0lBQ25CLHNEQUEwRDs7SUFDMUQsOEJBQVE7O0lBRVIsaUNBQVc7O0lBQ1gsbUNBQXNCOzs7OztJQUVsQiwwQ0FBc0MiLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIENyZWF0ZWQgYnkgcmFkaWdvc3Qgb24gMjQuMDQuMTcuXG4gKi9cblxuaW1wb3J0IHsgaXNFbXB0eSwgZ2V0LCBmaWx0ZXIgfSBmcm9tICdsb2Rhc2gnO1xuaW1wb3J0IHsgQXV0b2thZFNlcnZpY2UgfSBmcm9tICcuL2F1dG9rYWRTZXJ2aWNlJztcbmltcG9ydCB7IE5vZGUgfSBmcm9tICcuLi9kb21haW4vTm9kZSc7XG5pbXBvcnQgeyBDb21wb25lbnQsIE9uQ2hhbmdlcywgSW5wdXQsIE91dHB1dCwgRXZlbnRFbWl0dGVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBDb21wb25lbnQoe1xuICAgIHNlbGVjdG9yOiAnbmtiLWF1dG9rYWQnLFxuICAgIHN0eWxlVXJsczogWycuL2F1dG9rYWQubGVzcyddLFxuICAgIHRlbXBsYXRlOiBgXG4gICAgICAgIDxkaXYgY2xhc3M9XCJhdXRva2FkIGF1dG9rYWRfX2lubmVyXCI+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwiYXV0b2thZF9fZmlsdGVyc1wiPlxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJcIj5cbiAgICAgICAgICAgICAgICAgICAgPHNwYW4+0KHRgtC+0YDQvtC90LAg0LTQtdC70LA8L3NwYW4+PGJyPlxuICAgICAgICAgICAgICAgICAgICA8YnV0dG9uIGNsYXNzPVwiYnRuICBidG4tc20gZmxhdFwiIFtuZ0NsYXNzXT1cImlzU2VsZWN0ZWRTaWRlKDQpID8gJ2FjdGl2ZSc6JydcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIChjbGljayk9XCJjaGFuZ2VTaWRlKDQpXCI+0LvRjtCx0L7QtVxuICAgICAgICAgICAgICAgICAgICA8L2J1dHRvbj5cbiAgICAgICAgICAgICAgICAgICAgPGJ1dHRvbiBjbGFzcz1cImJ0biAgYnRuLXNtIGZsYXRcIiBbbmdDbGFzc109XCJpc1NlbGVjdGVkU2lkZSgwKSA/ICdhY3RpdmUnOicnXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAoY2xpY2spPVwiY2hhbmdlU2lkZSgwKVwiPtC40YHRgtC10YZcbiAgICAgICAgICAgICAgICAgICAgPC9idXR0b24+XG4gICAgICAgICAgICAgICAgICAgIDxidXR0b24gY2xhc3M9XCJidG4gIGJ0bi1zbSBmbGF0XCIgW25nQ2xhc3NdPVwiaXNTZWxlY3RlZFNpZGUoMSkgPyAnYWN0aXZlJzonJ1wiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKGNsaWNrKT1cImNoYW5nZVNpZGUoMSlcIj7QvtGC0LLQtdGC0YfQuNC6XG4gICAgICAgICAgICAgICAgICAgIDwvYnV0dG9uPlxuICAgICAgICAgICAgICAgICAgICA8YnV0dG9uIGNsYXNzPVwiYnRuICBidG4tc20gZmxhdFwiIFtuZ0NsYXNzXT1cImlzU2VsZWN0ZWRTaWRlKDIpID8gJ2FjdGl2ZSc6JydcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIChjbGljayk9XCJjaGFuZ2VTaWRlKDIpXCI+0LjQvdC+0LUg0LvQuNGG0L5cbiAgICAgICAgICAgICAgICAgICAgPC9idXR0b24+XG4gICAgICAgICAgICAgICAgICAgIDxidXR0b24gY2xhc3M9XCJidG4gIGJ0bi1zbSBmbGF0XCIgW25nQ2xhc3NdPVwiaXNTZWxlY3RlZFNpZGUoMykgPyAnYWN0aXZlJzonJ1wiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKGNsaWNrKT1cImNoYW5nZVNpZGUoMylcIj7RgtGA0LXRgtGM0LUg0LvQuNGG0L5cbiAgICAgICAgICAgICAgICAgICAgPC9idXR0b24+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImF1dG9rYWRfX3RvdGFsXCI+XG4gICAgICAgICAgICAgICAgICAgIDxhIGNsYXNzPVwidW5kZXJsaW5lXCIgW2F0dHIuaHJlZl09XCInaHR0cDovL2thZC5hcmJpdHIucnUvICcgfCBleHRlcm5hbFVybFwiIHRhcmdldD1cIl9ibGFua1wiPtCa0LDRgNGC0L7RgtC10LrQsFxuICAgICAgICAgICAgICAgICAgICAgICAg0LDRgNCx0LjRgtGA0LDQttC90YvRhSDQtNC10Ls8L2E+XG4gICAgICAgICAgICAgICAgICAgIDxkaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBbaGlkZGVuXT1cImdldFRvdGFsKCk9PT0wIHx8IGlzTG9hZGluZ1wiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuPnt7Z2V0VG90YWwoKX19PC9zcGFuPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwhLS0gfCB0cmFuc2xhdGU6IHtudW1iZXI6IGdldFRvdGFsKCl9IC0tPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwhLS0ge3snQVVUT0tBRC5LQURfU0VBUkNIX1RPVEFMJyB8IGFzeW5jIH19IC0tPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICZuYnNwO1xuICAgICAgICAgICAgICAgICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cImF1dG9rYWRfX3Jlc3VsdFwiPlxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJhdXRva2FkX19yZXN1bHQtbGlzdFwiXG4gICAgICAgICAgICAgICAgICAgIGluZmluaXRlU2Nyb2xsXG4gICAgICAgICAgICAgICAgICAgIChzY3JvbGxlZCk9XCJuZXh0UGFnZSgpXCJcbiAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgIDxkaXZcbiAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzPVwicmVzdWx0LWxpc3QtZWxlbWVudCBhdXRva2FkX19saXN0LWVsZW1lbnQgYXV0b2thZC1jYXNlIGF1dG9rYWQtY2FzZV9faW5uZXJcIlxuICAgICAgICAgICAgICAgICAgICAgICAgKm5nRm9yPVwibGV0IGl0ZW0gb2YgbGlzdFwiXG4gICAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJhdXRva2FkLWNhc2VfX2hlYWRlclwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJhdXRva2FkLWNhc2VfX2NhcHRpb25cIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImF1dG9rYWQtY2FzZV9fbnVtYmVyXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7e2l0ZW0uaWR9fVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImF1dG9rYWQtY2FzZV9faW5zdGFuY2UtY29udGFpbmVyXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2Pnt7aXRlbS5maWxsZWRNaWxsaXMgKiAxMDAwIHwgZGF0ZSA6ICdkZC5NTS55eXl5J319PC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiYXV0b2thZC1jYXNlX19pbnN0YW5jZVwiPnt7aXRlbS5jdXJyZW50SW5zdGFuY2V9fTwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImF1dG9rYWQtY2FzZV9fdHlwZVwiPnt7aXRlbS50eXBlLm5hbWV9fTwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXZcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2xhc3M9XCJhdXRva2FkLWNhc2VfX3Byb2Nlc3MtY29udGFpbmVyIGF1dG9rYWQtY2FzZV9fcHJvY2Vzcy1jb250YWluZXItLTFcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzPVwiYXV0b2thZC1jYXNlX19wcm9jZXNzIGF1dG9rYWQtY2FzZV9fcHJvY2Vzcy0tMVwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aXRsZT1cItCR0LDQvdC60YDQvtGC0L3QvtC1INC00LXQu9C+XCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICpuZ0lmPVwiaXRlbS50eXBlLmNvZGU9PT0xXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPtCRPC9zcGFuPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImF1dG9rYWQtY2FzZV9fcHJvY2Vzcy1jb250YWluZXJcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJhdXRva2FkLWNhc2VfX3Byb2Nlc3NcIiAqbmdJZj1cIml0ZW0uYWN0aXZlPT09dHJ1ZVwiPtC90LAg0YDQsNGB0YHQvNC+0YLRgNC10L3QuNC4PC9zcGFuPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJhdXRva2FkLWNhc2VfX3N1bVwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiAqbmdJZj1cIml0ZW0uY2xhaW1BbW91bnQgIT09IDBcIj57e2l0ZW0uY2xhaW1BbW91bnQgfCBudW1iZXJ9fdGALjwvc3Bhbj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiYXV0b2thZC1zaWRlcyBhdXRva2FkLXNpZGVzX19pbm5lclwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJhdXRva2FkLXNpZGVzX19yb3dcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImF1dG9rYWQtc2lkZXNfX2NhcHRpb25cIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgINCY0YHRgtC10YY6XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiYXV0b2thZC1zaWRlc19fY29tcGFuaWVzXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2ICpuZ0Zvcj1cImxldCBjb21wYW55IG9mIGdldFBsYWludGlmZihpdGVtLnBhcnRpZXMpIDsgbGFzdCBhcyBpc0xhc3RcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bmtiLWNvbXBhbnktbmFtZS13aXRoLWxpbmtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKGFkZEJyZWFkY3J1bWIpPVwiYWRkQXJiaXRyYXRpb25CcmVhZGNydW1iKGlkKVwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFtub2RlXT1cImNvbXBhbnlcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBbb2dybl09XCJub2RlLm9ncm5cIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgID48L25rYi1jb21wYW55LW5hbWUtd2l0aC1saW5rPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuIFtoaWRkZW5dPVwiaXNMYXN0XCI+LCAmbmJzcDs8L3NwYW4+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImF1dG9rYWQtc2lkZXNfX3Jvd1wiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiYXV0b2thZC1zaWRlc19fY2FwdGlvblwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg0J7RgtCy0LXRgtGH0LjQujpcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJhdXRva2FkLXNpZGVzX19jb21wYW5pZXNcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgKm5nRm9yPVwibGV0IGNvbXBhbnkgb2YgZ2V0RGVmZW5kYW50KGl0ZW0ucGFydGllcyk7IGxhc3QgYXMgaXNMYXN0XCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPG5rYi1jb21wYW55LW5hbWUtd2l0aC1saW5rXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFtvZ3JuXT1cIm5vZGUub2dyblwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIChhZGRCcmVhZGNydW1iKT1cImFkZEFyYml0cmF0aW9uQnJlYWRjcnVtYihpZClcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBbbm9kZV09XCJjb21wYW55XCI+PC9ua2ItY29tcGFueS1uYW1lLXdpdGgtbGluaz5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBbaGlkZGVuXT1cImlzTGFzdFwiPiwgJm5ic3A7PC9zcGFuPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJhdXRva2FkLXNpZGVzX19yb3dcIiAqbmdJZj1cImdldE90aGVycyhpdGVtLnBhcnRpZXMpLmxlbmd0aD4wXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJhdXRva2FkLXNpZGVzX19jYXB0aW9uXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICDQn9GA0L7Rh9C40LUg0LvQuNGG0LA6XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiYXV0b2thZC1zaWRlc19fY29tcGFuaWVzXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2ICpuZ0Zvcj1cImxldCBjb21wYW55IG9mIGdldE90aGVycyhpdGVtLnBhcnRpZXMpOyBsYXN0IGFzIGlzTGFzdFwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxua2ItY29tcGFueS1uYW1lLXdpdGgtbGlua1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBbb2dybl09XCJub2RlLm9ncm5cIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAoYWRkQnJlYWRjcnVtYik9XCJhZGRBcmJpdHJhdGlvbkJyZWFkY3J1bWIoaWQpXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgW25vZGVdPVwiY29tcGFueVwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPjwvbmtiLWNvbXBhbnktbmFtZS13aXRoLWxpbms+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gW2hpZGRlbl09XCJpc0xhc3RcIj4sICZuYnNwOzwvc3Bhbj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJhdXRva2FkLWRvY3VtZW50c1wiICpuZ0lmPVwiaXRlbS5kb2N1bWVudHNcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGg1PtCU0L7QutGD0LzQtdC90YLRiyDQv9C+INC00LXQu9GDOjwvaDU+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDx1bD5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxsaSAqbmdGb3I9XCIgbGV0IGRvY3VtZW50IG9mIGl0ZW0uZG9jdW1lbnRzXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGEgdGFyZ2V0PVwiX2JsYW5rXCIgW2F0dHIuaHJlZl09XCJkb2N1bWVudC51cmxcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4+e3tkb2N1bWVudC5kYXRlICogMTAwMCB8IGRhdGU6J2RkLk1NLnl5eXknfX08L3NwYW4+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge3tkb2N1bWVudC5uYW1lfX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2E+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2xpPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L3VsPlxuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDwvZGl2PlxuXG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwiYWxpZ24tY2VudGVyIGVtcHR5LXJlc3VsdCBtdXRlZFwiXG4gICAgICAgICAgICAgICAgKm5nSWY9XCJpc05vUmVzdWx0KCkgICYmICFoYXNFcnJvcigpICYmICFpc0xvYWRpbmdcIiBzdHlsZT1cIm1hcmdpbi10b3A6IDRlbTtcIj5cbiAgICAgICAgICAgICAgICDQkNGA0LHQuNGC0YDQsNC20L3Ri9GFINC00LXQuyDQvdC1INC90LDQudC00LXQvdC+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJhbGlnbi1jZW50ZXIgZW1wdHktcmVzdWx0XCIgKm5nSWY9XCJoYXNFcnJvcigpICYmICFpc0xvYWRpbmdcIj5cbiAgICAgICAgICAgICAgICDQn9C+0LjRgdC6INCyXG4gICAgICAgICAgICAgICAgPGEgY2xhc3M9XCJ1bmRlcmxpbmVcIiBbYXR0ci5ocmVmXT1cIidodHRwOi8va2FkLmFyYml0ci5ydS8nIHwgZXh0ZXJuYWxVcmxcIiB0YXJnZXQ9XCJfYmxhbmtcIj5cbiAgICAgICAgICAgICAgICAgICAg0LrQsNGA0YLQvtGC0LXQutC1INCw0YDQsdC40YLRgNCw0LbQvdGL0YUg0LTQtdC7XG4gICAgICAgICAgICAgICAgPC9hPlxuICAgICAgICAgICAgICAgINCy0YDQtdC80LXQvdC90L4g0L3QtdC00L7RgdGC0YPQv9C10L1cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cImxvYWRlclwiIFtoaWRkZW5dPVwiIWxvYWRpbmdcIiBzdHlsZT1cIm1hcmdpbi10b3A6MWVtOyBcIj48L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgYFxufSlcblxuZXhwb3J0IGNsYXNzIEF1dG9rYWRDb21wb25lbnQgaW1wbGVtZW50cyBPbkNoYW5nZXMge1xuICAgIEBJbnB1dCgpIHNjcm9sbFRvcDogbnVtYmVyO1xuICAgIEBJbnB1dCgpIG5vZGU6IE5vZGU7XG4gICAgQElucHV0KCkgc2lkZTogYW55O1xuICAgIEBPdXRwdXQoKSBvbkFkZEFyYml0cmF0aW9uQnJlYWRjcnVtYiA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiAgICBpZDogYW55O1xuXG4gICAgcGFnZXI6IGFueTtcbiAgICBwdWJsaWMgbG9hZGluZyA9IHRydWU7XG4gICAgY29uc3RydWN0b3IoXG4gICAgICAgIHByaXZhdGUgYXV0b2thZFNlcnZpY2U6IEF1dG9rYWRTZXJ2aWNlLFxuICAgICkge1xuICAgICAgICBPYmplY3QuYXNzaWduKHRoaXMsIHtcbiAgICAgICAgICAgIHBhZ2VyOiBhdXRva2FkU2VydmljZS5wYWdlcixcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgYXN5bmMgbmdPbkNoYW5nZXMoKSB7XG4gICAgICAgIHRoaXMuYXV0b2thZFNlcnZpY2UuaW5pdFNlYXJjaCh0aGlzLm5vZGUpO1xuICAgICAgICBpZiAodGhpcy5zaWRlKSB7XG4gICAgICAgICAgICB0aGlzLmF1dG9rYWRTZXJ2aWNlLnNlYXJjaC5wYXJhbXMuc2lkZSA9IHRoaXMuc2lkZTtcbiAgICAgICAgfVxuICAgICAgICB0cnkge1xuICAgICAgICAgICAgYXdhaXQgdGhpcy5hdXRva2FkU2VydmljZS5kb1NlYXJjaCgpO1xuICAgICAgICB9XG4gICAgICAgIGZpbmFsbHkge1xuICAgICAgICAgICAgdGhpcy5sb2FkaW5nID0gZmFsc2U7XG4gICAgICAgICAgICBpZiAodGhpcy5zY3JvbGxUb3ApIHtcbiAgICAgICAgICAgICAgICB3aW5kb3cuc2Nyb2xsVG8oMCwgdGhpcy5zY3JvbGxUb3ApO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxuXG4gICAgZ2V0IGxpc3QoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmF1dG9rYWRTZXJ2aWNlLnNlYXJjaC5yZXN1bHQuZW50cmllcztcbiAgICB9XG5cbiAgICBhc3luYyBjaGFuZ2VTaWRlKHR5cGUpIHtcbiAgICAgICAgdGhpcy5sb2FkaW5nID0gdHJ1ZTtcbiAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgIHRoaXMuYXV0b2thZFNlcnZpY2Uuc2VhcmNoLnBhcmFtcy5zaWRlID0gdHlwZTtcbiAgICAgICAgICAgIGF3YWl0IHRoaXMuYXV0b2thZFNlcnZpY2UuZG9TZWFyY2goKTtcbiAgICAgICAgfVxuICAgICAgICBmaW5hbGx5IHtcbiAgICAgICAgICAgIHRoaXMubG9hZGluZyA9IGZhbHNlO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgaXNTZWxlY3RlZFNpZGUodHlwZSkge1xuICAgICAgICByZXR1cm4gdGhpcy5hdXRva2FkU2VydmljZS5zZWFyY2gucGFyYW1zXG4gICAgICAgICAgICAmJiAocGFyc2VJbnQodHlwZSkgPT09IHBhcnNlSW50KHRoaXMuYXV0b2thZFNlcnZpY2Uuc2VhcmNoLnBhcmFtcy5zaWRlKSk7XG4gICAgfVxuXG4gICAgZ2V0UGxhaW50aWZmKGNhc2VTaWRlcykge1xuICAgICAgICByZXR1cm4gZmlsdGVyKGNhc2VTaWRlcywgKGl0ZW0pID0+IGl0ZW0udHlwZSA9PT0gMCk7XG4gICAgfVxuXG4gICAgZ2V0RGVmZW5kYW50KGNhc2VTaWRlcykge1xuICAgICAgICByZXR1cm4gZmlsdGVyKGNhc2VTaWRlcywgKGl0ZW0pID0+IGl0ZW0udHlwZSA9PT0gMSk7XG4gICAgfVxuXG4gICAgZ2V0T3RoZXJzKGNhc2VTaWRlcykge1xuICAgICAgICByZXR1cm4gZmlsdGVyKGNhc2VTaWRlcywgKGl0ZW0pID0+IGl0ZW0udHlwZSAhPT0gMSAmJiBpdGVtLnR5cGUgIT09IDApO1xuICAgIH1cblxuICAgIGdldFRvdGFsKCkge1xuICAgICAgICBjb25zdCBoYXNTZWFyY2hSZXN1bHQgPSAoKSA9PiB7XG4gICAgICAgICAgICByZXR1cm4gIWlzRW1wdHkodGhpcy5hdXRva2FkU2VydmljZS5zZWFyY2gucmVzdWx0KTtcbiAgICAgICAgfTtcbiAgICAgICAgcmV0dXJuIGhhc1NlYXJjaFJlc3VsdCgpID8gZ2V0KHRoaXMuYXV0b2thZFNlcnZpY2UsICdzZWFyY2gucmVzdWx0LnRvdGFsJywgMCkgOiAwO1xuICAgIH1cblxuICAgIGlzRW1wdHlSZXN1bHQoKSB7XG4gICAgICAgIHJldHVybiAhdGhpcy5nZXRUb3RhbCgpO1xuICAgIH1cblxuICAgIGlzTm9SZXN1bHQoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmF1dG9rYWRTZXJ2aWNlLnNlYXJjaC5ub1Jlc3VsdDtcbiAgICB9XG5cbiAgICBoYXNFcnJvcigpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuYXV0b2thZFNlcnZpY2Uuc2VhcmNoLmVycm9yO1xuICAgIH1cblxuICAgIGdldCBpc0xvYWRpbmcoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmxvYWRpbmc7XG4gICAgfVxuXG4gICAgYWRkQXJiaXRyYXRpb25CcmVhZGNydW1iKGlkKSB7XG4gICAgICAgIHRoaXMub25BZGRBcmJpdHJhdGlvbkJyZWFkY3J1bWIuZW1pdChpZCk7XG4gICAgfVxuXG4gICAgYXN5bmMgbmV4dFBhZ2UoKSB7XG4gICAgICAgIHRoaXMubG9hZGluZyA9IHRydWU7XG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgICBjb25zdCBwcm9taXNlID0gdGhpcy5wYWdlci5uZXh0UGFnZSgpLnRoZW4oKCkgPT4ge1xuICAgICAgICAgICAgICAgIHRoaXMubG9hZGluZyA9IGZhbHNlO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICBjb25zb2xlLmxvZyhwcm9taXNlKTtcbiAgICAgICAgICAgIGF3YWl0IHByb21pc2U7XG4gICAgICAgIH1cbiAgICAgICAgZmluYWxseSB7XG5cbiAgICAgICAgfVxuICAgIH1cbn1cblxuIl19