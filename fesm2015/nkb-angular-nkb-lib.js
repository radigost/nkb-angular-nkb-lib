import { __awaiter } from 'tslib';
import axios from 'axios';
import { forEach, extend, get, find, each, sortBy, includes, keyBy, upperFirst, lowerCase, map, head, capitalize, forOwn, filter, isEmpty, assignIn, isObject, isArray, isNumber, isNaN, size, isNull, isFunction, toNumber, toString, debounce, isEqual } from 'lodash';
import { Injectable, Component, Pipe, defineInjectable, NgModule, EventEmitter, Input, Output, inject } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';

/**
 * @fileoverview added by tsickle
 * Generated from: lib/domain/TYPES.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {string} */
const NODE_TYPE = {
    'INDIVIDUAL_IDENTITY': "INDIVIDUAL_IDENTITY",
    'INDIVIDUAL': "INDIVIDUAL",
    'COMPANY': "COMPANY",
    'EGRUL': "EGRUL",
    'ADDRESS': "ADDRESS",
    'PHONE': "PHONE",
};
/** @enum {string} */
const LINK_TYPES = {
    'FOUNDER_INDIVIDUAL': "FOUNDER_INDIVIDUAL",
    'FOUNDER_COMPANY': "FOUNDER_COMPANY",
    'EXECUTIVE_COMPANY': "EXECUTIVE_COMPANY",
    'EXECUTIVE_INDIVIDUAL': "EXECUTIVE_INDIVIDUAL",
    'AFFILIATED_COMPANY': "AFFILIATED_COMPANY",
    'AFFILIATED_INDIVIDUAL': "AFFILIATED_INDIVIDUAL",
    'EMPLOYEE': "EMPLOYEE",
    'PARTICIPANT_COMPANY': "PARTICIPANT_COMPANY",
    'PARTICIPANT_INDIVIDUAL': "PARTICIPANT_INDIVIDUAL",
    'PREDECESSOR_COMPANY': "PREDECESSOR_COMPANY",
    'HEAD_COMPANY': "HEAD_COMPANY",
    'COMMISSION_MEMBER': "COMMISSION_MEMBER",
    'CUSTOMER_COMPANY': "CUSTOMER_COMPANY",
    'ADDRESS': "ADDRESS",
    'PHONE': "PHONE",
    'REGISTER_HOLDER': "REGISTER_HOLDER",
};
/** @type {?} */
const LINK_TYPE_GROUPS = {
    FOUNDER: [LINK_TYPES.FOUNDER_INDIVIDUAL, LINK_TYPES.FOUNDER_COMPANY],
    EXECUTIVE: [LINK_TYPES.EXECUTIVE_COMPANY, LINK_TYPES.EXECUTIVE_INDIVIDUAL],
    PURCHASE: [LINK_TYPES.EMPLOYEE, LINK_TYPES.PARTICIPANT_COMPANY, LINK_TYPES.COMMISSION_MEMBER, LINK_TYPES.CUSTOMER_COMPANY]
};

/**
 * @fileoverview added by tsickle
 * Generated from: lib/meta/rsearchMeta.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const rsearchMeta = {
    // additional properties for nodes
    nodeTypes: {
        'COMPANY': {
            search: true,
            searchResultPriority: 60,
            accentedResultPriority: 30
        },
        'INDIVIDUAL_IDENTITY': {
            search: true,
            searchResultPriority: 50,
            accentedResultPriority: 10
        },
        'INDIVIDUAL': {
            search: true,
            searchResultPriority: 40,
            accentedResultPriority: 20
        },
        'ADDRESS': {
            search: true,
            searchResultPriority: 30,
            accentedResultPriority: 0
        },
        'PHONE': {
            search: true,
            searchResultPriority: 20,
            accentedResultPriority: 0
        },
        'PURCHASE': {
            search: false,
            searchResultPriority: 10,
            accentedResultPriority: 0
        }
    },
    // Дополнительные свойства для связей
    //
    // TODO автоматическое формирование свойств для объединенных типов на основе базовых типов
    relationTypes: {
        'FOUNDER': {
            name: 'FOUNDER',
            'parents': {
                order: 100,
                top: true,
                group: 'BASE',
                pageSize: 'all',
                history: {
                    opposite: false
                }
            }
        },
        'FOUNDER_INDIVIDUAL': {
            'children': {
                order: 250,
                top: true,
                group: 'BASE',
                history: {
                    opposite: true
                }
            },
            'parents': {
                order: 200,
                top: true,
                group: 'BASE',
                pageSize: 'all',
                history: {
                    opposite: false
                }
            }
        },
        'FOUNDER_COMPANY': {
            'children': {
                order: 350,
                top: true,
                group: 'BASE',
                history: {
                    opposite: true
                }
            },
            'parents': {
                order: 300,
                top: true,
                group: 'BASE',
                pageSize: 'all',
                history: {
                    opposite: false
                }
            }
        },
        'HEAD_COMPANY': {
            'children': {
                order: 400,
                top: true,
                group: 'BASE'
            },
            'parents': {
                order: 450,
                group: 'BASE'
            }
        },
        'PREDECESSOR_COMPANY': {
            'children': {
                order: 550,
                top: true,
                group: 'BASE'
            },
            'parents': {
                order: 500,
                top: true,
                group: 'BASE'
            }
        },
        'REGISTER_HOLDER': {
            'children': {
                order: 650,
                group: 'BASE'
            },
            'parents': {
                order: 600,
                group: 'BASE'
            }
        },
        'EXECUTIVE_COMPANY': {
            'children': {
                order: 750,
                group: 'BASE'
            },
            'parents': {
                order: 700,
                group: 'BASE'
            }
        },
        'EXECUTIVE_INDIVIDUAL': {
            'children': {
                order: 850,
                top: true,
                group: 'BASE',
                history: {
                    opposite: true
                }
            },
            'parents': {
                order: 800,
                group: 'BASE',
                pageSize: 'all',
                history: {
                    opposite: false
                }
            }
        },
        'AFFILIATED_INDIVIDUAL': {
            'children': {
                order: 950,
                top: true,
                group: 'AFFILIATED'
            },
            'parents': {
                order: 900,
                top: true,
                group: 'AFFILIATED'
            }
        },
        'AFFILIATED_COMPANY': {
            'children': {
                order: 1050,
                top: true,
                group: 'AFFILIATED'
            },
            'parents': {
                order: 1000,
                top: true,
                group: 'AFFILIATED'
            }
        },
        'ADDRESS': {
            'children': {
                order: 1150,
                top: true,
                group: 'BASE'
            },
            'parents': {
                order: 1100,
                group: 'CONTACTS'
            }
        },
        'PHONE': {
            'children': {
                order: 1250,
                top: true,
                group: 'BASE'
            },
            'parents': {
                order: 1200,
                group: 'CONTACTS'
            }
        },
        'CUSTOMER_COMPANY': {
            'children': {
                order: 1300,
                group: 'PURCHASE'
            },
            'parents': {
                order: 1350,
                top: true,
                group: 'BASE'
            }
        },
        'PARTICIPANT_COMPANY': {
            'children': {
                order: 1400,
                group: 'PURCHASE'
            },
            'parents': {
                order: 1670,
                top: true,
                group: 'BASE'
            }
        },
        'EMPLOYEE': {
            'children': {
                order: 1720,
                group: 'PURCHASE'
            },
            'parents': {
                order: 1500,
                group: 'PURCHASE'
            }
        },
        'PARTICIPANT_INDIVIDUAL': {
            'children': {
                order: 1600,
                group: 'PURCHASE'
            },
            'parents': {
                order: 1650,
                top: true,
                group: 'BASE'
            }
        },
        'COMMISSION_MEMBER': {
            'children': {
                order: 1700,
                group: 'PURCHASE'
            },
            'parents': {
                order: 1750,
                group: 'BASE'
            }
        },
        'kinsmen': {
            name: 'kinsmen',
            sourceNodeType: 'INDIVIDUAL',
            destinationNodeType: 'INDIVIDUAL'
        }
    },
    mergedRelationTypes: {
        'FOUNDER': {
            'parents': ['FOUNDER_INDIVIDUAL', 'FOUNDER_COMPANY']
        }
    },
    relationGroups: {
        'BASE': {
            order: 10
        },
        'AFFILIATED': {
            order: 20
        },
        'CONTACTS': {
            order: 30
        },
        'PURCHASE': {
            order: 40
        }
    }
};

/**
 * @fileoverview added by tsickle
 * Generated from: lib/meta/metaService.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
//todo покрыть тестами
class Meta {
    constructor() {
        this.SEARCHABLE_TYPES = [NODE_TYPE.COMPANY, NODE_TYPE.INDIVIDUAL, NODE_TYPE.ADDRESS, NODE_TYPE.PHONE];
        this.state = {
            inited: false,
            pending: false,
            error: false,
        };
        // @Deprecated
        this.nodeTypesMeta = {};
        this.relationTypesMeta = {};
    }
    /**
     * @return {?}
     */
    get inited() {
        return this.state.inited;
    }
    /**
     * @return {?}
     */
    init() {
        return __awaiter(this, void 0, void 0, function* () {
            if (!this.state.inited) {
                if (!this.state.pending) {
                    this.state.pending = true;
                    try {
                        this.relationTypes = yield axios.get('/nkbrelation/api/meta/relation/types').then((/**
                         * @param {?} res
                         * @return {?}
                         */
                        res => res.data));
                        this.nodeTypes = yield axios.get('/nkbrelation/api/meta/node/types').then((/**
                         * @param {?} res
                         * @return {?}
                         */
                        res => res.data));
                        // TODO legacy - refactor this - a lot of additional not needed work
                        forEach(this.nodeTypes, (/**
                         * @param {?} nodeType
                         * @return {?}
                         */
                        (nodeType) => {
                            this.nodeTypesMeta[nodeType.name] = nodeType;
                        }));
                        forEach(rsearchMeta.nodeTypes, (/**
                         * @param {?} nodeType
                         * @param {?} nodeTypeName
                         * @return {?}
                         */
                        (nodeType, nodeTypeName) => {
                            this.nodeTypesMeta[nodeTypeName] = extend({}, this.nodeTypesMeta[nodeTypeName], nodeType);
                        }));
                        // relationTypesMeta
                        forEach(this.relationTypes, (/**
                         * @param {?} relationType
                         * @return {?}
                         */
                        (relationType) => {
                            this.relationTypesMeta[relationType.name] = relationType;
                        }));
                        forEach(rsearchMeta.relationTypes, (/**
                         * @param {?} relationType
                         * @param {?} relationTypeName
                         * @return {?}
                         */
                        (relationType, relationTypeName) => {
                            this.relationTypesMeta[relationTypeName] = extend({}, this.relationTypesMeta[relationTypeName], relationType);
                        }));
                        // end TODO legacy - refactor this - a lot of additional not needed work
                        this.state.inited = true;
                    }
                    catch (err) {
                        console.error(err);
                        this.state.error = err;
                    }
                    finally {
                        console.log("meta service inited");
                        this.state.pending = false;
                    }
                }
                else {
                    return new Promise((/**
                     * @param {?} resolve
                     * @return {?}
                     */
                    (resolve) => {
                        /** @type {?} */
                        const interval = setInterval((/**
                         * @return {?}
                         */
                        () => {
                            if (this.state.inited) {
                                resolve();
                                clearInterval(interval);
                            }
                        }), 1000);
                    }));
                }
            }
        });
    }
    /**
     * @param {?} node
     * @return {?}
     */
    getNodeName(node) {
        /** @type {?} */
        const nameField = get(find(this.nodeTypes, { name: get(node, '_type') }), 'nameField');
        return get(node, [nameField], get(node, '_name', ''));
    }
    /**
     * @param {?} node
     * @return {?}
     */
    getNodeId(node) {
        if (this.inited) {
            /** @type {?} */
            const idField = get(find(this.nodeTypes, { name: get(node, '_type') }), 'idField');
            return idField === 'id' ? get(node, '_id') : get(node, idField, get(node, '_id'));
        }
    }
    /**
     * @param {?} type
     * @return {?}
     */
    getIdFieldForType(type) {
        return get(find(this.nodeTypes, { name: type }), 'idField', '_id');
    }
    /**
     * @param {?} node
     * @return {?}
     */
    buildRelationDataByNodeInfo(node) {
        /** @type {?} */
        const relationCountMap = {};
        /** @type {?} */
        let relationCounts = [];
        /** @type {?} */
        let groups = {};
        each(['in', 'out'], (/**
         * @param {?} infoDirection
         * @return {?}
         */
        (infoDirection) => {
            each(get(node, ['_info', infoDirection]), (/**
             * @param {?} info
             * @param {?} relationType
             * @return {?}
             */
            (info, relationType) => {
                /** @type {?} */
                const historyRelationCounts = this.getHistoryRelationCountsByInfo(info);
                /** @type {?} */
                const relationCount = historyRelationCounts.all;
                /** @type {?} */
                const direction = this.getDirection(infoDirection);
                /** @type {?} */
                const relationTypeMeta = this.getRelationTypeMeta(relationType, direction);
                /** @type {?} */
                const groupKey = relationTypeMeta.group;
                /** @type {?} */
                const relationGroupMeta = this.getRelationGroupMeta(groupKey);
                /** @type {?} */
                const mergedTypeInfo = this.getMergedTypeInfoByRelationType(relationType, direction);
                /** @type {?} */
                const mergedTypeKey = mergedTypeInfo.mergedType && this.buildNodeRelationKey(direction, mergedTypeInfo.mergedType);
                /** @type {?} */
                let mergedTypeCountData = relationCountMap[mergedTypeKey];
                /** @type {?} */
                const relationKey = this.buildNodeRelationKey(direction, relationType);
                /** @type {?} */
                const relationPluralKey = this.buildNodeRelationPluralKey(direction, relationType);
                /** @type {?} */
                let mergedTypeRelationCount = 0;
                /** @type {?} */
                const group = groups[groupKey] = groups[groupKey] || {
                    key: groupKey,
                    order: relationGroupMeta.order,
                    relationCounts: {},
                };
                // TODO Объединить код с mergedTypeCountData
                /** @type {?} */
                const countData = {
                    key: relationKey,
                    order: relationTypeMeta.order,
                    top: relationTypeMeta.top,
                    history: relationTypeMeta.history,
                    relationType,
                    direction,
                    relationCount,
                    historyRelationCounts,
                    pluralKey: relationPluralKey,
                };
                /** @type {?} */
                const mergedTypeHistoryRelationCounts = {
                    actual: 0,
                    outdated: 0,
                    all: 0,
                };
                forEach(!mergedTypeCountData && mergedTypeInfo.relationTypes, (/**
                 * @param {?} t
                 * @return {?}
                 */
                (t) => {
                    /** @type {?} */
                    const info = get(node, ['_info', infoDirection, t]);
                    /** @type {?} */
                    const counts = info ? this.getHistoryRelationCountsByInfo(info) : null;
                    if (!counts) {
                        return;
                    }
                    mergedTypeHistoryRelationCounts.actual += counts.actual;
                    mergedTypeHistoryRelationCounts.outdated += counts.outdated;
                    mergedTypeHistoryRelationCounts.all += counts.all;
                }));
                mergedTypeRelationCount = mergedTypeHistoryRelationCounts.all;
                if (!mergedTypeCountData && mergedTypeRelationCount > relationCount) {
                    /** @type {?} */
                    const mergedType = mergedTypeInfo.mergedType;
                    /** @type {?} */
                    const mergedTypeMeta = this.getRelationTypeMeta(mergedType, direction);
                    // TODO Объединить код с countData
                    mergedTypeCountData = {
                        key: mergedTypeKey,
                        order: mergedTypeMeta.order,
                        top: mergedTypeMeta.top,
                        history: mergedTypeMeta.history,
                        relationType: mergedType,
                        direction,
                        relationCount: mergedTypeRelationCount,
                        historyRelationCounts: mergedTypeHistoryRelationCounts,
                        pluralKey: this.buildNodeRelationPluralKey(direction, mergedType),
                    };
                    group.relationCounts[mergedTypeKey] = mergedTypeCountData;
                    relationCountMap[mergedTypeKey] = mergedTypeCountData;
                }
                if (!relationCountMap[mergedTypeKey]) {
                    group.relationCounts[relationKey] = countData;
                }
                relationCountMap[relationKey] = countData;
            }));
        }));
        groups = sortBy(groups, 'order');
        forEach(groups, (/**
         * @param {?} group
         * @return {?}
         */
        (group) => {
            group.relationCounts = sortBy(group.relationCounts, 'order');
            relationCounts = relationCounts.concat(group.relationCounts);
        }));
        return {
            relationCountMap,
            relationCounts,
            groups,
        };
    }
    /**
     * @param {?} info
     * @return {?}
     */
    getHistoryRelationCountsByInfo(info) {
        info = info || {};
        return {
            actual: info.actual || 0,
            all: (info.actual || 0) + (info.outdated || 0),
            outdated: info.outdated || 0,
        };
    }
    /**
     * @param {?} infoDirection
     * @return {?}
     */
    getDirection(infoDirection) {
        return infoDirection === 'in' ? 'parents' : 'children';
    }
    /**
     * @param {?} relationType
     * @param {?} direction
     * @return {?}
     */
    getRelationTypeMeta(relationType, direction) {
        return get(rsearchMeta.relationTypes, [relationType, direction]);
    }
    /**
     * @param {?} relationGroup
     * @return {?}
     */
    getRelationGroupMeta(relationGroup) {
        return rsearchMeta.relationGroups[relationGroup];
    }
    /**
     * @param {?} relationType
     * @param {?} direction
     * @return {?}
     */
    getMergedTypeInfoByRelationType(relationType, direction) {
        /** @type {?} */
        const info = {
            mergedType: null,
            relationTypes: null,
        };
        forEach(rsearchMeta.mergedRelationTypes, (/**
         * @param {?} byDirections
         * @param {?} mergedType
         * @return {?}
         */
        (byDirections, mergedType) => {
            /** @type {?} */
            const relationTypes = byDirections[direction];
            if (includes(relationTypes, relationType)) {
                info.mergedType = mergedType;
                info.relationTypes = relationTypes;
                return false;
            }
        }));
        return info;
    }
    /**
     * @param {?} direction
     * @param {?} relationType
     * @return {?}
     */
    buildNodeRelationKey(direction, relationType) {
        return direction + '::' + relationType;
    }
    /**
     * @param {?} direction
     * @param {?} relationType
     * @return {?}
     */
    buildNodeRelationPluralKey(direction, relationType) {
        return 'NG_PLURALIZE::RELATION_' + relationType + '-' + direction;
    }
    /**
     * @param {?} node
     * @return {?}
     */
    buildNodeExtraMeta(node) {
        if (!node) {
            return;
        }
        // uid
        this.buildNodeUID(node);
        //
        node.__relationData = metaService.buildRelationDataByNodeInfo(node);
        //
        node.__idField = get(this.nodeTypesMeta[node._type], 'idField');
        if (node._type === 'COMPANY' || node._type === 'EGRUL') { // компания
            this.buildCompanyState(node);
            node.__isFavoritesSupported = true;
        }
        else if (node._type === 'INDIVIDUAL') { // физик
            node.__isFavoritesSupported = true;
        }
        else if (node._type === 'PURCHASE') { // закупка
            node.currency = node.currency || 'RUB';
            node.__lotMap = keyBy(node.lots, 'lot');
            this.resolvePurchaseHref(node);
        }
    }
    /**
     * @param {?} node
     * @return {?}
     */
    buildNodeUID(node) {
        node.__uid = node.$$hashKey = node.__uid || this.buildNodeUIDByType(node._id, node._type);
        return node.__uid;
    }
    /**
     * @param {?} id
     * @param {?} type
     * @return {?}
     */
    buildNodeUIDByType(id, type) {
        return ('node-' + type + '-' + id);
    }
    /**
     * @param {?} node
     * @return {?}
     */
    buildCompanyState(node) {
        // юридическое состояние
        /** @type {?} */
        let egrulState = node.egrul_state;
        /** @type {?} */
        let aliveCode = '5';
        // действующее
        /** @type {?} */
        let intermediateCodes = ['6', '111', '121', '122', '123', '124', '131', '132'];
        // коды промежуточных состояний
        /** @type {?} */
        let _liquidate;
        if (node.egrul_liq) {
            _liquidate = {
                state: {
                    _actual: null,
                    _since: node.dead_dt,
                    type: upperFirst(lowerCase(node.egrul_liq.type)),
                },
            };
        }
        else if (node.dead_dt) {
            _liquidate = {
                state: {
                    _actual: null,
                    _since: node.dead_dt,
                    type: 'Ликвидировано',
                },
            };
        }
        else if (egrulState) {
            if (egrulState.code !== aliveCode) {
                _liquidate = {
                    state: {
                        _actual: egrulState._actual,
                        _since: egrulState._since,
                        type: egrulState.type,
                        intermediate: includes(intermediateCodes, egrulState.code),
                    },
                };
            }
        }
        node._liquidate = _liquidate;
        // способ организации компании
        /** @type {?} */
        const egrulReg = node.egrul_reg;
        /** @type {?} */
        const baseCodes = ['11', '01', '03'];
        if (egrulReg && !includes(baseCodes, egrulReg.code)) {
            node._reg = {
                state: {
                    _actual: egrulReg._actual,
                    _since: egrulReg._since,
                    type: egrulReg.type,
                },
            };
        }
    }
    /**
     * @param {?} node
     * @return {?}
     */
    resolvePurchaseHref(node) {
        if (node.law === 'FZ_44') {
            node.__href = node.href;
        }
        else {
            node.__href = 'http://zakupki.gov.ru/epz/order/quicksearch/search_eis.html?strictEqual=on&pageNumber=1&sortDirection=false&recordsPerPage=_10&showLotsInfoHidden=false&fz44=on&fz223=on&fz94=on&regions=&priceFrom=&priceTo=&currencyId=-1&publishDateFrom=&publishDateTo=&updateDateFrom=&updateDateTo=&sortBy=UPDATE_DATE&searchString=' + node._id;
        }
    }
}
/** @type {?} */
const metaService = new Meta();

/**
 * @fileoverview added by tsickle
 * Generated from: lib/domain/Node.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class Node {
    /**
     * @param {?} node
     * @return {?}
     */
    static nodeWrapper(node) {
        if (node) {
            return node instanceof Node ? node : new Node(node);
        }
        else {
            return new Node(node);
        }
    }
    /**
     * @return {?}
     */
    get idField() {
        /** @type {?} */
        const node = this;
        return metaService.getIdFieldForType(node.type);
    }
    /**
     * @return {?}
     */
    get aggregates() {
        return get(this, '_aggregates', {});
    }
    /**
     * @return {?}
     */
    get name() {
        /** @type {?} */
        const node = this;
        return node._name;
    }
    /**
     * @return {?}
     */
    get fullName() {
        return get(this, 'namesort', '');
    }
    /**
     * @return {?}
     */
    get chiefName() {
        return get(this, 'chief_name', '');
    }
    /**
     * @return {?}
     */
    get type() {
        return get(this, '_type', '');
    }
    /**
     * @return {?}
     */
    get debts() {
        return get(this, '_debts', null);
    }
    /**
     * @return {?}
     */
    get licencies() {
        return map(get(this._egrulProfile, 'infoLicense'), (/**
         * @param {?} licence
         * @return {?}
         */
        (licence) => {
            return {
                terminationDate: get(head(get(licence, 'profile')), 'terminationDate', false),
                number: get(licence, 'data.number'),
                type: map(get(licence, 'data.type'), (/**
                 * @param {?} type
                 * @return {?}
                 */
                (type) => upperFirst(lowerCase(type)))),
                issued: upperFirst(lowerCase(get(licence, 'data.issued'))),
                dateBegin: get(licence, 'data.dateBegin'),
                date: get(licence, 'data.date'),
            };
        }));
    }
    /**
     * @return {?}
     */
    get relations() {
        return get(this, '_relations', []);
    }
    /**
     * @return {?}
     */
    get disqualification() {
        /** @type {?} */
        const disqualification = head(get(head(get(this._egrulProfile, 'infoPersonOfficial')), 'data.disqualification'));
        return disqualification;
    }
    /**
     * @param {?} newNode
     */
    constructor(newNode) {
        /** @type {?} */
        const node = Object.assign({}, newNode);
        // TODO add wrapper for this - need somehow add this info to node, when needed
        // if (nkbSearchMetaService) {
        metaService.buildNodeExtraMeta(node);
        // }
        node.id = metaService.getNodeId(node);
        node._name = metaService.getNodeName(node);
        delete node.name;
        Object.assign(this, node, { _metaService: metaService });
    }
    /**
     * @return {?}
     */
    getFullName() {
        return get(this, 'namesort', '');
    }
    /**
     * @return {?}
     */
    getChiefName() {
        return get(this, 'chief_name', '');
    }
    /**
     * @return {?}
     */
    getType() {
        return get(this, '_type', '');
    }
    /**
     * @return {?}
     */
    getCity() {
        if (this.type === NODE_TYPE.INDIVIDUAL_IDENTITY) {
            return capitalize(get(head(get(this, 'selfemployedInfo.infoAddress')), 'data.адресРФ.город.наимГород', ''));
        }
        if (this.type === NODE_TYPE.COMPANY) {
            return capitalize(get(head(get(this, '_egrulProfile.infoAddress')), 'data.address.regionName', ''));
        }
    }
    /**
     * @return {?}
     */
    getOgrnName() {
        return this.ogrn ? 'ОГРН' : this.selfemployedInfo ? 'ОГРНИП' : '';
    }
    /**
     * @return {?}
     */
    getEgrulName() {
        return this.ogrn ? 'ЕГРЮЛ' : this.selfemployedInfo ? 'ЕГРИП' : '';
    }
    /**
     * @return {?}
     */
    getOgrn() {
        return this.ogrn ? this.ogrn : this.selfemployedInfo ? this.selfemployedInfo.ogrnip : '';
    }
    /**
     * @param {?} selfemployedInfo
     * @return {?}
     */
    setSelfemployedInfo(selfemployedInfo) {
        if (selfemployedInfo !== undefined) {
            this.selfemployedInfo = selfemployedInfo;
        }
    }
    /**
     * @param {?} egrulProfile
     * @return {?}
     */
    setEgrulProfile(egrulProfile) {
        this._egrulProfile = egrulProfile;
    }
    /**
     * @param {?} balance
     * @return {?}
     */
    setBalance(balance) {
        this._balance = balance;
    }
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/domain/SearchFilter.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
//Класс Фильтров для каждого из типов поиска
/**
 * @return {?}
 */
function Filter() {
    return {
        regions: [],
        inns: [],
        makeActive
    };
    /**
     * @param {?} elementToMake
     * @return {?}
     */
    function makeActive(elementToMake) {
        forEach(this.regions, (/**
         * @param {?} region
         * @return {?}
         */
        function (region) {
            region.active = (elementToMake && region.regionCode === elementToMake.regionCode);
        }));
    }
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/domain/SearchType.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
//Класс типов поиска
/**
 * @param {?} nodeType
 * @param {?} data
 * @return {?}
 */
function SearchType(nodeType, data) {
    /** @type {?} */
    var searchType = {
        nodeType: nodeType,
        resultPriority: data.searchResultPriority,
        accentedResultPriority: data.accentedResultPriority,
        pageConfig: null,
        filters: null,
        filter: Filter(),
        request: null,
        result: null,
        nodeList: null,
        active: false,
        setResultRegions,
        setResultInns,
        resetActive
    };
    return searchType;
    /**
     * @return {?}
     */
    function resetActive() {
        searchType.active = false;
    }
    /**
     * @return {?}
     */
    function setResultRegions() {
        /** @type {?} */
        const regions = [];
        if (hasRegionCodes.call(this)) {
            /** @type {?} */
            const data = this.result['info']['nodeFacet']['region_code'];
            forOwn(data, (/**
             * @param {?} number
             * @param {?} regionCode
             * @return {?}
             */
            function (number, regionCode) {
                /** @type {?} */
                const res = {
                    number: number,
                    regionCode: regionCode,
                    active: false
                };
                regions.push(res);
            }));
        }
        this.filter.regions = regions;
        /**
         * @return {?}
         */
        function hasRegionCodes() {
            return get(this, 'result.info.nodeFacet.region_code', false);
        }
    }
    /**
     * @param {?} searchType
     * @return {?}
     */
    function setResultInns(searchType) {
        /** @type {?} */
        const inns = [];
        hasResult(searchType) ? forEach(searchType.result.list, (/**
         * @param {?} element
         * @return {?}
         */
        (element) => {
            element.inn ? inns.push(element.inn) : '';
        })) : '';
        searchType.filter.inns = inns;
        /**
         * @param {?} searchType
         * @return {?}
         */
        function hasResult(searchType) {
            return get(searchType, 'result.list', undefined);
        }
    }
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/angular-nkb-lib.service.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class AngularNkbLibService {
    constructor() { }
}
AngularNkbLibService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
AngularNkbLibService.ctorParameters = () => [];
/** @nocollapse */ AngularNkbLibService.ngInjectableDef = defineInjectable({ factory: function AngularNkbLibService_Factory() { return new AngularNkbLibService(); }, token: AngularNkbLibService, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * Generated from: lib/angular-nkb-lib.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class AngularNkbLibComponent {
    constructor() { }
    /**
     * @return {?}
     */
    ngOnInit() {
        console.log(this);
    }
}
AngularNkbLibComponent.decorators = [
    { type: Component, args: [{
                selector: 'lib-angular-nkb-lib',
                template: `
    <p>
      angular-nkb-lib works!
    </p>
  `
            }] }
];
/** @nocollapse */
AngularNkbLibComponent.ctorParameters = () => [];

/**
 * @fileoverview added by tsickle
 * Generated from: lib/cache/CacheElement.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class CacheElement {
    /**
     * @param {?} el
     */
    constructor(el) {
        Object.assign(this, { promise: el });
    }
    /**
     * @return {?}
     */
    head() {
        return __awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const res = yield this.promise;
            return head(get(res, 'data.list', [res]));
        });
    }
    /**
     * @return {?}
     */
    list() {
        return __awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const res = yield this.promise;
            return get(res, 'data.list');
        });
    }
    /**
     * @return {?}
     */
    data() {
        return __awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const res = yield this.promise;
            return get(res, 'data');
        });
    }
    /**
     * @return {?}
     */
    get() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.promise;
        });
    }
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/cache/Cache.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const METHODS = {
    'GET': 'GET',
    'POST': 'POST',
    'PUT': 'PUT',
    'DELETE': 'DELETE',
};
class Cache {
    constructor() {
        this.obj = {};
    }
    /**
     * @param {?} key
     * @return {?}
     */
    get(key) {
        return this.obj[key];
    }
    /**
     * @param {?} key
     * @param {?} value
     * @return {?}
     */
    set(key, value) {
        this.obj[key] = value;
    }
    /**
     * @param {?} url
     * @param {?=} options
     * @return {?}
     */
    try(url, options = { params: {}, method: METHODS.GET }) {
        return __awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            let result = this.get(url);
            if (result === undefined) {
                // @ts-ignore
                result = new CacheElement(axios({ url, method: options.method }));
                this.set(url, result);
            }
            return result;
            //1. Если есть таймер - добавляем параметры, меняет таймаут
            //2. по прошествии таймаута делаем множественный запрос
            //3. разбираем запрос по элементам, кладем каждый из них в соответсвующих кеш
        });
    }
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/autokad/autokadResource.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const seldon = axios.create({
    baseURL: '/api/v1/companies/',
});
class AutokadResource {
    constructor() {
        this.cache = new Cache();
    }
    /**
     * @param {?} arg0
     * @return {?}
     */
    kadSearch(arg0) {
        throw new Error("Method not implemented.");
    }
    /**
     * @return {?}
     */
    init() {
        return __awaiter(this, void 0, void 0, function* () {
        });
    }
    /**
     * @param {?} options
     * @return {?}
     */
    search(options) {
        return __awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            let result;
            /** @type {?} */
            const req = buildRequestParams(options);
            result = this.cache.get(`${req.ogrn}_${req.type}_${req.pageSize}_${req.pageNo}`);
            if (result === undefined) {
                result = seldon.get(`ogrn-${req.ogrn}/cases`, { params: req });
                this.cache.set(`${req.ogrn}_${req.type}_${req.pageSize}_${req.pageNo}`, result);
            }
            return result;
            /**
             * @param {?} options
             * @return {?}
             */
            function buildRequestParams(options) {
                /** @type {?} */
                const req = {
                    type: options.type,
                    pageSize: options.pageSize || 100,
                    pageNo: options.pageIndex || 1,
                };
                options.ogrn !== undefined ? req.ogrn = options.ogrn :
                    options.inn !== undefined ? req.inn = options.inn :
                        console.warn('there is no inn or ogrn in request!', options);
                return req;
            }
        });
    }
    /**
     * @param {?} options
     * @return {?}
     */
    aggregates(options) {
        return __awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            let res;
            if (options.ogrn) {
                res = seldon.get(`ogrn-${options.ogrn}/aggregates`);
            }
            return res;
        });
    }
    /**
     * @param {?} ogrn
     * @return {?}
     */
    getAggregates(ogrn) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                /** @type {?} */
                const res = yield this.aggregates({ ogrn });
                return head(get(res, 'data.entries'));
            }
            catch (e) {
                return Promise.reject();
            }
        });
    }
}
AutokadResource.decorators = [
    { type: Injectable, args: [{ providedIn: 'root' },] }
];
/** @nocollapse */ AutokadResource.ngInjectableDef = defineInjectable({ factory: function AutokadResource_Factory() { return new AutokadResource(); }, token: AutokadResource, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * Generated from: lib/autokad/autokadService.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const Search = (/**
 * @return {?}
 */
() => {
    return {
        params: null,
        requestData: null,
        request: null,
        result: {
            // В формате kad.arbitr.ru
            TotalCount: null,
            PagesCount: null,
            Items: [],
            entries: []
        },
        noResult: true,
        error: null,
        watch: true,
        sources: undefined,
    };
});
/** @type {?} */
const DEFAULT_SEARCH_PARAMS = {
    side: 4
};
/** @type {?} */
const DEFAULT_SEARCH_REQUEST_DATA = {
    q: null,
    dateFrom: null,
    dateTo: null,
    page: 1,
    pageIndex: 1
};
class AutokadService {
    /**
     * @param {?} autokadResourceService
     */
    constructor(autokadResourceService) {
        this.autokadResourceService = autokadResourceService;
        /** @type {?} */
        let node = null;
        /** @type {?} */
        const autokadService = {
            search: Search(),
            loading: false,
            pager: Pager(),
            initSearch,
            doSearch,
            buildSearchRequestData,
            getResultTotal,
            getCaseCount,
            hasError,
            isLoading,
            isNoResult,
        };
        Object.assign(this, autokadService);
        /**
         * @param {?} newNode
         * @param {?} params
         * @return {?}
         */
        function initSearch(newNode, params) {
            node = newNode;
            autokadService.search.params = assignIn({}, DEFAULT_SEARCH_PARAMS, params, {
                source: 0
            });
        }
        /**
         * @return {?}
         */
        function resetSearchRequest() {
            if (autokadService.search.request && autokadService.search.request.abort) {
                autokadService.search.request.abort();
            }
            autokadService.search.requestData = getDefaultSearchRequestData();
        }
        /**
         * @return {?}
         */
        function doSearch() {
            autokadService.search.result = {
                TotalCount: null,
                PagesCount: null,
                Items: [],
                entries: []
            };
            resetSearchRequest();
            autokadService.search.requestData = autokadService.buildSearchRequestData(autokadService.search.params);
            return new Promise((/**
             * @param {?} resolve
             * @return {?}
             */
            (resolve) => {
                if (isBlank(autokadService.search.requestData.inn)) {
                    console.warn('search.requestData.inn is empty ');
                    resolve();
                }
                else {
                    searchRequestPromise.call(this).then((/**
                     * @param {?} data
                     * @return {?}
                     */
                    (data) => {
                        autokadService.search.result = data;
                        autokadService.pager.reset(data);
                        hideLoading();
                        resolve();
                    }), (/**
                     * @param {?} data
                     * @return {?}
                     */
                    (data) => {
                        autokadService.search.result = data;
                        autokadService.pager.reset(data);
                        hideLoading();
                        resolve();
                    }));
                }
            }));
        }
        /**
         * @return {?}
         */
        function searchRequestPromise() {
            return new Promise((/**
             * @param {?} resolve
             * @param {?} reject
             * @return {?}
             */
            function (resolve, reject) {
                showLoading();
                autokadService.search.noResult = true;
                autokadService.search.request = autokadResourceService.search(autokadService.search.requestData).then(success, error);
                /**
                 * @param {?} data
                 * @return {?}
                 */
                function success(data) {
                    data = data.data;
                    autokadService.search.noResult = (data.total === 0);
                    autokadService.search.error = null;
                    if (!hasResultItems(data)) {
                        autokadService.search.error = true;
                        hideLoading();
                        reject(data);
                    }
                    hideLoading();
                    resolve(data);
                }
                /**
                 * @param {?} err
                 * @return {?}
                 */
                function error(err) {
                    autokadService.search.noResult = false;
                    autokadService.search.error = true;
                    hideLoading();
                    reject(err);
                }
            }));
        }
        /**
         * @param {?} result
         * @return {?}
         */
        function hasResultItems(result) {
            return isObject(result) && isArray(result['entries']);
        }
        /**
         * @param {?} value
         * @return {?}
         */
        function isBlank(value) {
            return isEmpty(value) && !isNumber(value) || isNaN(value);
        }
        /**
         * @return {?}
         */
        function hasError() {
            return autokadService.search.error;
        }
        /**
         * @param {?} searchParams
         * @return {?}
         */
        function buildSearchRequestData(searchParams) {
            /** @type {?} */
            const requestData = assignIn({}, DEFAULT_SEARCH_REQUEST_DATA);
            requestData['type'] = (searchParams['side'] === -1 ? 4 : searchParams['side']);
            requestData['inn'] = node['inn'];
            requestData['ogrn'] = node['ogrn'];
            return requestData;
        }
        /*
                    * case
                    *
                    */
        /**
         * @param {?} search
         * @param {?} success
         * @param {?} error
         * @param {?} complete
         * @return {?}
         */
        function getCaseCount(search, success, error, complete) {
            if (!isArray(autokadService.search.sources) || !size(autokadService.search.sources)) {
                console.warn('getCaseCount... error: search.sources is blank');
                errorCallback();
                return null;
            }
            /** @type {?} */
            let sourceIndex = 0;
            /** @type {?} */
            let sourceCount = size(autokadService.search.sources);
            /** @type {?} */
            let caseCountRequest = CaseCountRequest();
            /** @type {?} */
            let result = null;
            /** @type {?} */
            let source;
            /** @type {?} */
            let request;
            nextRequest();
            return caseCountRequest;
            /**
             * @return {?}
             */
            function nextRequest() {
                /** @type {?} */
                let error = false;
                /** @type {?} */
                let next = false;
                /** @type {?} */
                let q;
                source = autokadService.search.sources[sourceIndex++];
                q = source.value;
                if (isBlank(q)) {
                    request = null;
                    result = 0;
                    next = true;
                    caseCountRequest._setRequest(request);
                    check();
                }
                else {
                    request = autokadResourceService.kadSearch({
                        r: {
                            q: q
                        },
                        success: (/**
                         * @param {?} data
                         * @return {?}
                         */
                        function (data) {
                            result = getResultTotal(data['Result']);
                            if (isNull(result)) {
                                error = true;
                            }
                            else if (result === 0) {
                                next = true;
                            }
                        }),
                        error: (/**
                         * @return {?}
                         */
                        function () {
                            error = true;
                        })
                    });
                    caseCountRequest._setRequest(request);
                    request.completePromise.then((/**
                     * @return {?}
                     */
                    function () {
                        check();
                    }));
                }
                /**
                 * @return {?}
                 */
                function check() {
                    if (error) {
                        errorCallback();
                    }
                    else if (!next || sourceIndex === sourceCount) {
                        successCallback();
                    }
                    else if (next) {
                        nextRequest();
                    }
                }
            }
            /**
             * @return {?}
             */
            function successCallback() {
                if (isFunction(success)) {
                    success(result, sourceIndex - 1);
                }
                completeCallback();
            }
            /**
             * @return {?}
             */
            function errorCallback() {
                if (isFunction(error)) {
                    error();
                }
                completeCallback();
            }
            /**
             * @return {?}
             */
            function completeCallback() {
                if (isFunction(complete)) {
                    complete();
                }
            }
            /**
             * @param {?} value
             * @return {?}
             */
            function isBlank(value) {
                return isEmpty(value) && !isNumber(value) || isNaN(value);
            }
            /**
             * @return {?}
             */
            function CaseCountRequest() {
                /** @type {?} */
                let request;
                return {
                    _setRequest: (/**
                     * @param {?} r
                     * @return {?}
                     */
                    function (r) {
                        request = r;
                    }),
                    abort: (/**
                     * @return {?}
                     */
                    function () {
                        if (request) {
                            request.abort();
                        }
                    })
                };
            }
        }
        /**
         * @param {?} result
         * @return {?}
         */
        function getResultTotal(result) {
            /** @type {?} */
            let r = result && result['TotalCount'];
            if (!isNumber(r)) {
                r = null;
            }
            return r;
        }
        /**
         * @return {?}
         */
        function showLoading() {
            autokadService.loading = true;
        }
        /**
         * @return {?}
         */
        function hideLoading() {
            autokadService.loading = false;
        }
        /**
         * @return {?}
         */
        function getDefaultSearchRequestData() {
            return DEFAULT_SEARCH_REQUEST_DATA;
        }
        /**
         * @return {?}
         */
        function isLoading() {
            return autokadService.loading;
        }
        /**
         * @return {?}
         */
        function isNoResult() {
            return get(autokadService.search, 'result.status.itemsFound', 0) === 0;
        }
        //Pager
        /**
         * @return {?}
         * @this {*}
         */
        function Pager() {
            /** @type {?} */
            let internalDisabled = false;
            /** @type {?} */
            let noNextPage = false;
            return {
                reset: reset,
                nextPage: nextPage,
                isDisabled: isDisabled
            };
            /**
             * @return {?}
             */
            function isDisabled() {
                return internalDisabled || noNextPage;
            }
            /**
             * @param {?} result
             * @return {?}
             */
            function reset(result) {
                internalDisabled = false;
                noNextPage = noMore(false, result);
            }
            /**
             * @param {?} hasError
             * @param {?} result
             * @return {?}
             */
            function noMore(hasError, result) {
                return hasError || (autokadService.search.requestData.pageIndex * result.size >= result.total);
            }
            /**
             * @return {?}
             * @this {*}
             */
            function nextPage() {
                return __awaiter(this, void 0, void 0, function* () {
                    return new Promise((/**
                     * @param {?} resolve
                     * @return {?}
                     */
                    (resolve) => {
                        showLoading();
                        if (!isDisabled() && autokadService.search.requestData) {
                            internalDisabled = true;
                            autokadService.search.requestData.pageIndex++;
                            searchRequestPromise().then((/**
                             * @param {?} result
                             * @return {?}
                             */
                            function (result) {
                                /** @type {?} */
                                const hasError = !hasResultItems(result);
                                if (!hasError) {
                                    forEach(result.entries, (/**
                                     * @param {?} item
                                     * @return {?}
                                     */
                                    function (item) {
                                        autokadService.search.result.entries.push(item);
                                    }));
                                    internalDisabled = false;
                                    noNextPage = noMore(false, result);
                                }
                                else {
                                    internalDisabled = false;
                                    noNextPage = true;
                                }
                                hideLoading();
                                resolve();
                            }));
                        }
                        resolve();
                    }));
                });
            }
        }
    }
}
AutokadService.decorators = [
    { type: Injectable, args: [{ providedIn: 'root' },] }
];
/** @nocollapse */
AutokadService.ctorParameters = () => [
    { type: AutokadResource }
];
/** @nocollapse */ AutokadService.ngInjectableDef = defineInjectable({ factory: function AutokadService_Factory() { return new AutokadService(inject(AutokadResource)); }, token: AutokadService, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * Generated from: lib/autokad/autokadComponent.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class AutokadComponent {
    /**
     * @param {?} autokadService
     */
    constructor(autokadService) {
        this.autokadService = autokadService;
        this.onAddArbitrationBreadcrumb = new EventEmitter();
        this.loading = true;
        Object.assign(this, {
            pager: autokadService.pager,
        });
    }
    /**
     * @return {?}
     */
    ngOnChanges() {
        return __awaiter(this, void 0, void 0, function* () {
            this.autokadService.initSearch(this.node);
            if (this.side) {
                this.autokadService.search.params.side = this.side;
            }
            try {
                yield this.autokadService.doSearch();
            }
            finally {
                this.loading = false;
                if (this.scrollTop) {
                    window.scrollTo(0, this.scrollTop);
                }
            }
        });
    }
    /**
     * @return {?}
     */
    get list() {
        return this.autokadService.search.result.entries;
    }
    /**
     * @param {?} type
     * @return {?}
     */
    changeSide(type) {
        return __awaiter(this, void 0, void 0, function* () {
            this.loading = true;
            try {
                this.autokadService.search.params.side = type;
                yield this.autokadService.doSearch();
            }
            finally {
                this.loading = false;
            }
        });
    }
    /**
     * @param {?} type
     * @return {?}
     */
    isSelectedSide(type) {
        return this.autokadService.search.params
            && (parseInt(type) === parseInt(this.autokadService.search.params.side));
    }
    /**
     * @param {?} caseSides
     * @return {?}
     */
    getPlaintiff(caseSides) {
        return filter(caseSides, (/**
         * @param {?} item
         * @return {?}
         */
        (item) => item.type === 0));
    }
    /**
     * @param {?} caseSides
     * @return {?}
     */
    getDefendant(caseSides) {
        return filter(caseSides, (/**
         * @param {?} item
         * @return {?}
         */
        (item) => item.type === 1));
    }
    /**
     * @param {?} caseSides
     * @return {?}
     */
    getOthers(caseSides) {
        return filter(caseSides, (/**
         * @param {?} item
         * @return {?}
         */
        (item) => item.type !== 1 && item.type !== 0));
    }
    /**
     * @return {?}
     */
    getTotal() {
        /** @type {?} */
        const hasSearchResult = (/**
         * @return {?}
         */
        () => {
            return !isEmpty(this.autokadService.search.result);
        });
        return hasSearchResult() ? get(this.autokadService, 'search.result.total', 0) : 0;
    }
    /**
     * @return {?}
     */
    isEmptyResult() {
        return !this.getTotal();
    }
    /**
     * @return {?}
     */
    isNoResult() {
        return this.autokadService.search.noResult;
    }
    /**
     * @return {?}
     */
    hasError() {
        return this.autokadService.search.error;
    }
    /**
     * @return {?}
     */
    get isLoading() {
        return this.loading;
    }
    /**
     * @param {?} id
     * @return {?}
     */
    addArbitrationBreadcrumb(id) {
        this.onAddArbitrationBreadcrumb.emit(id);
    }
    /**
     * @return {?}
     */
    nextPage() {
        return __awaiter(this, void 0, void 0, function* () {
            this.loading = true;
            try {
                /** @type {?} */
                const promise = this.pager.nextPage().then((/**
                 * @return {?}
                 */
                () => {
                    this.loading = false;
                }));
                console.log(promise);
                yield promise;
            }
            finally {
            }
        });
    }
}
AutokadComponent.decorators = [
    { type: Component, args: [{
                selector: 'nkb-autokad',
                template: `
        <div class="autokad autokad__inner">
            <div class="autokad__filters">
                <div class="">
                    <span>Сторона дела</span><br>
                    <button class="btn  btn-sm flat" [ngClass]="isSelectedSide(4) ? 'active':''"
                            (click)="changeSide(4)">любое
                    </button>
                    <button class="btn  btn-sm flat" [ngClass]="isSelectedSide(0) ? 'active':''"
                            (click)="changeSide(0)">истец
                    </button>
                    <button class="btn  btn-sm flat" [ngClass]="isSelectedSide(1) ? 'active':''"
                            (click)="changeSide(1)">ответчик
                    </button>
                    <button class="btn  btn-sm flat" [ngClass]="isSelectedSide(2) ? 'active':''"
                            (click)="changeSide(2)">иное лицо
                    </button>
                    <button class="btn  btn-sm flat" [ngClass]="isSelectedSide(3) ? 'active':''"
                            (click)="changeSide(3)">третье лицо
                    </button>
                </div>
                <div class="autokad__total">
                    <a class="underline" [attr.href]="'http://kad.arbitr.ru/ ' | externalUrl" target="_blank">Картотека
                        арбитражных дел</a>
                    <div>
                        <span [hidden]="getTotal()===0 || isLoading">
                            <span>{{getTotal()}}</span>
                            <!-- | translate: {number: getTotal()} -->
                            <!-- {{'AUTOKAD.KAD_SEARCH_TOTAL' | async }} -->
                            &nbsp;
                        </span>
                    </div>
                </div>
            </div>
            <div class="autokad__result">
                <div class="autokad__result-list"
                    infiniteScroll
                    (scrolled)="nextPage()"
                >
                    <div
                        class="result-list-element autokad__list-element autokad-case autokad-case__inner"
                        *ngFor="let item of list"
                    >
                        <div class="autokad-case__header">
                            <div class="autokad-case__caption">
                                <div class="autokad-case__number">
                                    {{item.id}}
                                </div>
                                <div class="autokad-case__instance-container">
                                    <div>{{item.filledMillis * 1000 | date : 'dd.MM.yyyy'}}</div>
                                    <div class="autokad-case__instance">{{item.currentInstance}}</div>
                                </div>
                                <div class="autokad-case__type">{{item.type.name}}</div>
                            </div>
                            <div
                                class="autokad-case__process-container autokad-case__process-container--1"
                            >
                                <span
                                    class="autokad-case__process autokad-case__process--1"
                                    title="Банкротное дело"
                                    *ngIf="item.type.code===1"
                                >Б</span>
                            </div>

                            <div class="autokad-case__process-container">
                                <span class="autokad-case__process" *ngIf="item.active===true">на рассмотрении</span>
                            </div>
                            <div class="autokad-case__sum">
                                <span *ngIf="item.claimAmount !== 0">{{item.claimAmount | number}}р.</span>
                            </div>
                        </div>

                        <div class="autokad-sides autokad-sides__inner">
                            <div class="autokad-sides__row">
                                <div class="autokad-sides__caption">
                                    Истец:
                                </div>
                                <div class="autokad-sides__companies">
                                    <div *ngFor="let company of getPlaintiff(item.parties) ; last as isLast">
                                        <nkb-company-name-with-link
                                            (addBreadcrumb)="addArbitrationBreadcrumb(id)"
                                            [node]="company"
                                            [ogrn]="node.ogrn"
                                        ></nkb-company-name-with-link>
                                        <span [hidden]="isLast">, &nbsp;</span>
                                    </div>
                                </div>
                            </div>
                            <div class="autokad-sides__row">
                                <div class="autokad-sides__caption">
                                    Ответчик:
                                </div>
                                <div class="autokad-sides__companies">
                                    <div *ngFor="let company of getDefendant(item.parties); last as isLast">
                                        <nkb-company-name-with-link
                                            [ogrn]="node.ogrn"
                                            (addBreadcrumb)="addArbitrationBreadcrumb(id)"
                                            [node]="company"></nkb-company-name-with-link>
                                        <span [hidden]="isLast">, &nbsp;</span>
                                    </div>
                                </div>
                            </div>
                            <div class="autokad-sides__row" *ngIf="getOthers(item.parties).length>0">
                                <div class="autokad-sides__caption">
                                    Прочие лица:
                                </div>
                                <div class="autokad-sides__companies">
                                    <div *ngFor="let company of getOthers(item.parties); last as isLast">
                                        <nkb-company-name-with-link
                                            [ogrn]="node.ogrn"
                                            (addBreadcrumb)="addArbitrationBreadcrumb(id)"
                                            [node]="company"
                                        ></nkb-company-name-with-link>
                                        <span [hidden]="isLast">, &nbsp;</span>
                                    </div>
                                </div>
                            </div>

                            <div class="autokad-documents" *ngIf="item.documents">
                                <h5>Документы по делу:</h5>
                                <ul>
                                    <li *ngFor=" let document of item.documents">
                                        <a target="_blank" [attr.href]="document.url">
                                            <span>{{document.date * 1000 | date:'dd.MM.yyyy'}}</span>
                                            .
                                            {{document.name}}
                                        </a>
                                    </li>
                                </ul>

                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="align-center empty-result muted"
                *ngIf="isNoResult()  && !hasError() && !isLoading" style="margin-top: 4em;">
                Арбитражных дел не найдено
            </div>
            <div class="align-center empty-result" *ngIf="hasError() && !isLoading">
                Поиск в
                <a class="underline" [attr.href]="'http://kad.arbitr.ru/' | externalUrl" target="_blank">
                    картотеке арбитражных дел
                </a>
                временно недоступен
            </div>
            <div class="loader" [hidden]="!loading" style="margin-top:1em; "></div>
        </div>
    `,
                styles: [".autokad__filters{display:-webkit-box;display:flex;-webkit-box-pack:justify;justify-content:space-between}.autokad__total{display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column;-webkit-box-pack:justify;justify-content:space-between}.autokad__result-list{margin-top:1em}.autokad-case__inner{padding:1em;border-color:#444;cursor:default}.autokad-case__inner:hover{border-color:#444;cursor:default}.autokad-case__header{display:-webkit-box;display:flex;-webkit-box-pack:justify;justify-content:space-between;margin-bottom:1em}.autokad-case__caption{flex-basis:60%;display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column}.autokad-case__process{background-color:#2f96b4;color:#fff;display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column;-webkit-box-pack:center;justify-content:center;text-align:center;font-weight:700;border-radius:3px;height:2em;width:10em;align-self:center}.autokad-case__process--1{width:2em;background-color:#bd362f;margin-left:.5em;margin-right:.5em}.autokad-case__process-container{flex-basis:10%}.autokad-case__process-container--1{flex-basis:1em}.autokad-case__sum{font-size:large;font-weight:700;flex-basis:20%;display:-webkit-box;display:flex;-webkit-box-pack:end;justify-content:flex-end}.autokad-case__number{font-size:large;font-weight:700}.autokad-case__instance-container{display:-webkit-box;display:flex;-webkit-box-orient:horizontal;-webkit-box-direction:normal;flex-direction:row}.autokad-case__instance-container div{margin:0 1em}.autokad-case__instance-container div:first-of-type{margin:0}.autokad-case__type{font-style:italic}.autokad-sides__row{display:-webkit-box;display:flex;-webkit-box-orient:horizontal;-webkit-box-direction:normal;flex-direction:row}.autokad-sides__caption{flex-basis:20%;font-weight:700}.autokad-sides__companies{flex-basis:80%;display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column}.autokad-sides__companies div{margin:.2em 0}.autokad-documents h5{text-align:center;font-weight:700}.autokad__company-name--highlight{background-color:#ff0}"]
            }] }
];
/** @nocollapse */
AutokadComponent.ctorParameters = () => [
    { type: AutokadService }
];
AutokadComponent.propDecorators = {
    scrollTop: [{ type: Input }],
    node: [{ type: Input }],
    side: [{ type: Input }],
    onAddArbitrationBreadcrumb: [{ type: Output }]
};

/**
 * @fileoverview added by tsickle
 * Generated from: lib/search/SearchResourceClass.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const search = axios.create({
    baseURL: '/nkbrelation/api/nodes'
});
//old but fast api , maybe after _relations=false new is the same on speed - TODO check speen and if neer is faster - move to it
/** @type {?} */
const company = axios.create({
    baseURL: '/nkbrelation/api/company'
});
class SearchResource {
    /**
     * @param {?} metaService
     */
    constructor(metaService) {
        Object.assign(this, {
            cache: new Cache(
            // {keyFunction: (type, idField, id) => `/nkbrelation/api/nodes/${type}?${idField}.equals=${id}`}
            ),
            queue: [],
            queueType: null,
            queueResults: [],
            queueParams: {},
            pedning: false,
            debounce: debounce(this.searchByTypeAndIdsFromQueue, 500, { maxWait: 1000 }),
            metaService: metaService
        });
        metaService.init();
    }
    /**
     * @return {?}
     */
    updateQueueSearchByTypeAndIds() {
        return this.queue.length < 10 ? this.debounce() : this.searchByTypeAndIdsFromQueue();
    }
    /**
     * @param {?} options
     * @return {?}
     */
    searchRequest(options) {
        return __awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const params = assignIn({}, options.filter, options.pageConfig, {
                q: options.q,
                exclude: '_relations'
            });
            /** @type {?} */
            const res = yield search.get(`/${options.nodeType}`, { params });
            return get(res, 'data', []);
        });
    }
    /**
     * @param {?} options
     * @return {?}
     */
    searchByTypeAndId(options) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!this.metaService.inited)
                yield this.metaService.init();
            /** @type {?} */
            const idField = this.metaService.getIdFieldForType(options.type);
            return search.get(`${options.type}?${idField}.equals=${options.id}&exclude=_relations`, { params: options }).then((/**
             * @param {?} res
             * @return {?}
             */
            res => head(res.data.list)));
        });
    }
    /**
     * @param {?=} options
     * @return {?}
     */
    searchByTypeAndIdQueued(options = { type: NODE_TYPE.COMPANY, id: null, params: {} }) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((/**
             * @param {?} resolve
             * @return {?}
             */
            (resolve) => __awaiter(this, void 0, void 0, function* () {
                /** @type {?} */
                const doInterval = (/**
                 * @return {?}
                 */
                () => {
                    /** @type {?} */
                    const intervalID = setInterval((/**
                     * @return {?}
                     */
                    () => {
                        if (!this.pending) {
                            /** @type {?} */
                            let res = find(this.queueResults, (/**
                             * @param {?} company
                             * @return {?}
                             */
                            (company) => isEqual(`${company[idField]}`, `${options.id}`)));
                            if (res) {
                                clearInterval(intervalID);
                                resolve(res);
                            }
                        }
                    }), 100);
                });
                /** @type {?} */
                const idField = this.metaService.getIdFieldForType(options.type);
                /** @type {?} */
                const inCache = this.cache.get(`/nkbrelation/api/nodes/${options.type}?${idField}.equals=${options.id}`);
                if (inCache) {
                    resolve(inCache.head());
                }
                /** @type {?} */
                const isInqueue = this.queue.includes(options.id);
                if (!isInqueue) {
                    if (this.queueType === options.type || this.queueType === null) {
                        this.queueType = options.type;
                        this.queueParams = options.params;
                        this.queue.push(options.id);
                        this.updateQueueSearchByTypeAndIds();
                        doInterval();
                    }
                    else if (this.queueType !== options.type) {
                        /** @type {?} */
                        const res = yield this.cache.try(`/nkbrelation/api/nodes/${options.type}?${idField}.equals=${options.id}`).then((/**
                         * @param {?} r
                         * @return {?}
                         */
                        r => head(r)));
                        resolve(res);
                    }
                }
                else {
                    doInterval();
                }
            })));
        });
    }
    /**
     * @return {?}
     */
    searchByTypeAndIdsFromQueue() {
        return __awaiter(this, void 0, void 0, function* () {
            this.pending = true;
            /** @type {?} */
            const type = this.queueType;
            /** @type {?} */
            const idField = this.metaService.getIdFieldForType(type);
            this.searchByTypeAndIds({ type, ids: this.queue, params: this.queueParams }).then((/**
             * @param {?} res
             * @return {?}
             */
            res => {
                this.queueResults = this.queueResults.concat(res);
                res.forEach((/**
                 * @param {?} el
                 * @return {?}
                 */
                el => {
                    this.cache.set(`/nkbrelation/api/nodes/${type}?${idField}.equals=${el[idField]}`, new CacheElement(Promise.resolve(el)));
                }));
                this.pending = false;
            }));
            this.queueType = null;
            this.queue = [];
        });
    }
    /**
     * @param {?=} options
     * @return {?}
     */
    searchByTypeAndIds(options = { type: NODE_TYPE.COMPANY, ids: [], params: {} }) {
        return __awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            let result;
            yield this.metaService.init();
            if (options.ids.length < 1) {
                return Promise.resolve({});
            }
            /** @type {?} */
            const urlparams = new URLSearchParams();
            /** @type {?} */
            const idField = this.metaService.getIdFieldForType(options.type);
            options.ids.forEach((/**
             * @param {?} id
             * @return {?}
             */
            id => urlparams.append(`${idField}.equals`, id)));
            if (options.params)
                Object.keys(options.params).forEach((/**
                 * @param {?} key
                 * @return {?}
                 */
                key => urlparams.append(key, options.params[key])));
            /** @type {?} */
            const url = `/nkbrelation/api/nodes/${options.type}?${urlparams.toString()}`;
            result = yield this.cache.try(url).then((/**
             * @param {?} res
             * @return {?}
             */
            (res) => res.list()));
            return result;
        });
    }
    /**
     * @param {?} params
     * @return {?}
     */
    searchCompany(params) {
        return __awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const res = yield company.get('/', { params });
            return get(res, 'data', []);
        });
    }
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/search/searchResource.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const searchResource = new SearchResource(metaService);

/**
 * @fileoverview added by tsickle
 * Generated from: lib/search/searchService.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class SearchService {
    /**
     * @param {?} metaService
     */
    constructor(metaService$$1) {
        this.metaService = metaService$$1;
    }
    /**
     * @return {?}
     */
    init() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield this.metaService.init();
            }
            catch (_a) {
                console.error("cannot init SearchService");
            }
        });
    }
    /**
     * @return {?}
     */
    getSearchableNodeTypes() {
        /** @type {?} */
        const searchableNodeTypes = {};
        // TODO rewrite to make rsearchMeta, metaService one service
        /** @type {?} */
        const nodeTypes = rsearchMeta.nodeTypes;
        forEach(nodeTypes, (/**
         * @param {?} data
         * @param {?} nodeType
         * @return {?}
         */
        (data, nodeType) => {
            // TODO if nodeType is serachABLE
            if (data.search) {
                searchableNodeTypes[nodeType] = SearchType(nodeType, data);
            }
        }));
        return searchableNodeTypes;
    }
    /**
     * @param {?} searchType
     * @param {?} query
     * @return {?}
     */
    searchByTypeAndText(searchType, query) {
        return __awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const filter$$1 = {};
            if (searchType && searchType.filter) {
                /** @type {?} */
                const activeRegion = find(searchType.filter.regions, { active: true });
                if (activeRegion && activeRegion.regionCode) {
                    /** @type {?} */
                    const filterRegion = { 'region_code.equals': activeRegion.regionCode };
                    assignIn(filter$$1, filterRegion);
                }
            }
            return new Promise((/**
             * @param {?} resolve
             * @return {?}
             */
            (resolve) => __awaiter(this, void 0, void 0, function* () {
                try {
                    searchType.result = yield searchResource.searchRequest({
                        q: query,
                        nodeType: searchType.nodeType,
                        pageConfig: searchType.pageConfig,
                        filter: filter$$1,
                    });
                    resolve(searchType);
                }
                catch (err) {
                    console.log(err);
                    resolve({ result: null });
                }
            })));
        });
    }
    /**
     * @param {?} options
     * @return {?}
     */
    searchByTypeAndId(options) {
        return __awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const params = {
                id: options.id,
                type: options.searchtype || options.type,
            };
            try {
                return yield searchResource.searchByTypeAndId(params);
            }
            catch (err) {
                console.error(err);
                return {};
            }
        });
    }
}
/** @type {?} */
const searchService = new SearchService(metaService);
searchService.init();

/**
 * @fileoverview added by tsickle
 * Generated from: lib/ui/CompanyNameWithLink.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class CompanyNameWithLinkComponent {
    constructor() {
        this.addBreadcrumb = new EventEmitter();
        this.onCompanyClick = new EventEmitter();
        this.loading = false;
    }
    /**
     * @return {?}
     */
    get title() {
        return this.node.state ? `${this.node.state._since} : ${this.node.state.type}` : '';
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        if (this.showParams === undefined) {
            this.showParams = {
                ogrn: true,
                inn: false,
                created: false,
            };
        }
    }
    /**
     * @param {?} company
     * @return {?}
     */
    goToDetails(company) {
        return __awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            let nodeType;
            /** @type {?} */
            let node;
            /** @type {?} */
            const getNodeTypeByOgrn = (/**
             * @param {?} ogrn
             * @return {?}
             */
            (ogrn) => {
                return this.isCompanyOgrn(ogrn) ? NODE_TYPE.COMPANY : this.isIndividualOgrn(ogrn) ? NODE_TYPE.INDIVIDUAL_IDENTITY : false;
            });
            /** @type {?} */
            const getNodeTypeByInn = (/**
             * @param {?} inn
             * @return {?}
             */
            (inn) => {
                return this.isCompanyInn(inn) ? NODE_TYPE.COMPANY : this.isIndividualInn(inn) ? NODE_TYPE.INDIVIDUAL_IDENTITY : false;
            });
            /** @type {?} */
            const getNode = (/**
             * @param {?} nodeType
             * @param {?} id
             * @return {?}
             */
            (nodeType, id) => __awaiter(this, void 0, void 0, function* () {
                /** @type {?} */
                const res = yield searchService.searchByTypeAndText({ nodeType }, id);
                return head(get(res, 'result.list', get(res, 'list')));
            }));
            company = this.node;
            this.loading = true;
            if (company.id && company.type) {
                this.addBreadcrumb.emit({ id: company.id });
                /** @type {?} */
                const params = {
                    searchtype: company.type,
                    text: company.name,
                    id: company.id,
                };
                this.onCompanyClick.emit({ node: company });
                // this.$state.go('main.search.details', params);
            }
            else {
                if (company.ogrn) {
                    nodeType = getNodeTypeByOgrn(company.ogrn);
                    node = yield getNode(nodeType, company.ogrn);
                }
                else if (company.inn) {
                    nodeType = getNodeTypeByInn(company.inn);
                    node = yield getNode(nodeType, company.inn);
                }
                node = Node.nodeWrapper(node);
                this.addBreadcrumb.emit({ id: node.id });
                this.onCompanyClick.emit({ node });
                // const params = {
                //     searchtype: node.type,
                //     text: node.name,
                //     id: node.id,
                // };
                // this.$state.go('main.search.details', params);
            }
        });
    }
    /**
     * @param {?} node
     * @return {?}
     */
    isNotActive(node) {
        return toNumber(get(node, 'state.code')) === 0;
    }
    /**
     * @param {?} textOgrn
     * @param {?} queryOgrn
     * @return {?}
     */
    isHighlightSearch(textOgrn, queryOgrn) {
        return toString(textOgrn) === queryOgrn;
    }
    /**
     * @param {?} inn
     * @return {?}
     */
    isIndividualInn(inn) {
        return String(inn).length === 12;
    }
    /**
     * @param {?} ogrn
     * @return {?}
     */
    isCompanyOgrn(ogrn) {
        return String(ogrn).length === 13;
    }
    /**
     * @param {?} ogrn
     * @return {?}
     */
    isIndividualOgrn(ogrn) {
        return String(ogrn).length === 15;
    }
    /**
     * @param {?} inn
     * @return {?}
     */
    isCompanyInn(inn) {
        return String(inn).length === 10;
    }
}
CompanyNameWithLinkComponent.decorators = [
    { type: Component, args: [{
                selector: 'nkb-company-name-with-link',
                template: `
        <div *ngIf="node.shortname">{{node.shortname}}</div>
        <a
                *ngIf="node.ogrn || node.inn"
                (click)="goToDetails(node)"
                [ngClass]="{danger:isNotActive(node),'autokad__company-name--highlight':isHighlightSearch(node.ogrn,ogrn)}"
                class="autokad__company-name"
                [title]="title"
        >
            {{node.name}}
        </a>
        <span
                *ngIf="!node.ogrn && !node.inn"
                [ngClass]="{danger:isNotActive(node),'autokad__company-name--highlight':isHighlightSearch(node.ogrn,ogrn)}"
                [title]="title"
        >
        {{node.name}}
    </span>

        <span *ngIf="node.ogrn!==undefined && showParams.ogrn">
        <span *ngIf="isCompanyOgrn(node.ogrn)">ОГРН:</span>
        <span *ngIf="isIndividualOgrn(node.ogrn)">ОГРНИП:</span>
            {{node.ogrn}}
    </span>
        <span *ngIf="node.ogrnip!==undefined">
        <span>ОГРНИП:</span>
            {{node.ogrnip}}
    </span>
        <span *ngIf="node.inn && showParams.inn">
        <span>ИНН:</span>
            {{node.inn}}
    </span>

        <span *ngIf="node && node.founded_dt && showParams.created">
          c      {{ node.founded_dt | date: 'dd.MM.yyyy'}}
          <!-- c      {{ node.founded_dt | date}} -->
            </span>
        <div class="loader" *ngIf="loading"></div>

    `
            }] }
];
/** @nocollapse */
CompanyNameWithLinkComponent.ctorParameters = () => [];
CompanyNameWithLinkComponent.propDecorators = {
    node: [{ type: Input }],
    type: [{ type: Input }],
    showParams: [{ type: Input }],
    ogrn: [{ type: Input }],
    addBreadcrumb: [{ type: Output }],
    onCompanyClick: [{ type: Output }]
};

/**
 * @fileoverview added by tsickle
 * Generated from: lib/pipes/externalUrl.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class ExternalUrlPipe {
    /**
     * @param {?} url
     * @return {?}
     */
    transform(url) {
        return '/siteapp/url/external' + '?url=' + encodeURIComponent(url);
    }
    ;
}
ExternalUrlPipe.decorators = [
    { type: Pipe, args: [{ name: 'externalUrl' },] }
];

/**
 * @fileoverview added by tsickle
 * Generated from: lib/angular-nkb-lib.module.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class AngularNkbLibModule {
}
AngularNkbLibModule.decorators = [
    { type: NgModule, args: [{
                imports: [
                    CommonModule
                ],
                declarations: [
                    AngularNkbLibComponent,
                    AutokadComponent,
                    CompanyNameWithLinkComponent,
                    ExternalUrlPipe,
                ],
                providers: [
                    AutokadResource,
                    AutokadService,
                    DatePipe,
                ],
                exports: [
                    AngularNkbLibComponent,
                    AutokadComponent,
                    CompanyNameWithLinkComponent,
                    ExternalUrlPipe,
                ]
            },] }
];

/**
 * @fileoverview added by tsickle
 * Generated from: public-api.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * Generated from: nkb-angular-nkb-lib.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

export { Meta, metaService, rsearchMeta, Node, NODE_TYPE, LINK_TYPES, LINK_TYPE_GROUPS, AngularNkbLibService, AngularNkbLibComponent, AngularNkbLibModule, AutokadComponent, AutokadResource, AutokadService, ExternalUrlPipe, CompanyNameWithLinkComponent, CacheElement, Cache, searchService, searchResource, SearchResource };

//# sourceMappingURL=nkb-angular-nkb-lib.js.map