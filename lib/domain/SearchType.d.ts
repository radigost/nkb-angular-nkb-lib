export default function SearchType(nodeType: any, data: any): {
    nodeType: any;
    resultPriority: any;
    accentedResultPriority: any;
    pageConfig: any;
    filters: any;
    filter: {
        regions: any[];
        inns: any[];
        makeActive: (elementToMake: any) => void;
    };
    request: any;
    result: any;
    nodeList: any;
    active: boolean;
    setResultRegions: () => void;
    setResultInns: (searchType: any) => void;
    resetActive: () => void;
};
