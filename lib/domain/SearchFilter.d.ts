export default function Filter(): {
    regions: any[];
    inns: any[];
    makeActive: (elementToMake: any) => void;
};
