/**
 * Created by radigost on 27.09.17.
 */
export interface IEgrulProfile {
}
interface IGrn {
    date: string;
    grn: number;
}
export interface IDisqualifiedChef {
    dateBegin: string;
    dateEnd: string;
    grn: IGrn;
}
export declare class Node {
    static nodeWrapper(node: any): Node;
    _id: string;
    _type?: any;
    _aggregates?: any;
    _name?: any;
    ogrn?: any;
    _balance?: any;
    _egrulProfile?: any;
    ogrnip?: any;
    selfemployedInfo?: any;
    _debts?: any;
    inn?: any;
    id: any;
    state: {
        _since: any;
        type: any;
    };
    balance?: number[];
    chief_name?: string;
    chief_gender: string;
    shortname: string;
    founded_dt: any;
    readonly idField: any;
    readonly aggregates: {} | Exclude<this["_aggregates"], undefined>;
    readonly name: any;
    readonly fullName: any;
    readonly chiefName: "" | Exclude<this["chief_name"], undefined>;
    readonly type: "" | Exclude<this["_type"], undefined>;
    readonly debts: any;
    readonly licencies: {
        terminationDate: any;
        number: any;
        type: string[];
        issued: string;
        dateBegin: any;
        date: any;
    }[];
    readonly relations: any;
    readonly disqualification: IDisqualifiedChef | undefined;
    constructor(newNode: any);
    getFullName(): any;
    getChiefName(): "" | Exclude<this["chief_name"], undefined>;
    getType(): "" | Exclude<this["_type"], undefined>;
    getCity(): string;
    getOgrnName(): "" | "ОГРН" | "ОГРНИП";
    getEgrulName(): "" | "ЕГРЮЛ" | "ЕГРИП";
    getOgrn(): any;
    setSelfemployedInfo(selfemployedInfo: any): void;
    setEgrulProfile(egrulProfile: any): void;
    setBalance(balance: any): void;
}
export {};
