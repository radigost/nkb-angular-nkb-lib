import { PipeTransform } from '@angular/core';
export declare class ExternalUrlPipe implements PipeTransform {
    transform(url: string): string;
}
