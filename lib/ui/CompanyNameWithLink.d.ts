import { Node } from '../domain/Node';
import { OnInit, EventEmitter } from '@angular/core';
export declare class CompanyNameWithLinkComponent implements OnInit {
    readonly title: string;
    node: Node;
    type: any;
    showParams: {
        inn?: boolean;
        ogrn?: boolean;
        created?: boolean;
    };
    ogrn: string;
    addBreadcrumb: EventEmitter<{}>;
    onCompanyClick: EventEmitter<{}>;
    loading: boolean;
    constructor();
    ngOnInit(): void;
    goToDetails(company: any): Promise<void>;
    isNotActive(node: any): boolean;
    isHighlightSearch(textOgrn: any, queryOgrn: any): boolean;
    isIndividualInn(inn: any): boolean;
    isCompanyOgrn(ogrn: any): boolean;
    isIndividualOgrn(ogrn: any): boolean;
    isCompanyInn(inn: any): boolean;
}
