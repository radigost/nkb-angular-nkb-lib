export declare class Cache {
    obj: any;
    constructor();
    get(key: any): any;
    set(key: any, value: any): void;
    try(url: any, options?: {
        params: {};
        method: string;
    }): Promise<any>;
}
