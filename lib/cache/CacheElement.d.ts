import { AxiosResponse } from 'axios';
export declare class CacheElement {
    promise: Promise<AxiosResponse>;
    constructor(el: any);
    head(): Promise<{}>;
    list(): Promise<any>;
    data(): Promise<any>;
    get(): Promise<AxiosResponse<any>>;
}
