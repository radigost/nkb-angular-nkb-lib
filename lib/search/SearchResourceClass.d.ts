import { NODE_TYPE as TYPE } from '../domain/TYPES';
import { Meta } from '../meta/metaService';
export declare class SearchResource {
    queue: any;
    debounce: any;
    pending: any;
    queueResults: any;
    cache: any;
    queueType: any;
    queueParams: any;
    metaService: Meta;
    constructor(metaService: any);
    updateQueueSearchByTypeAndIds(): any;
    searchRequest(options: any): Promise<any>;
    searchByTypeAndId(options: any): Promise<never>;
    searchByTypeAndIdQueued(options?: {
        type: TYPE;
        id: any;
        params: {};
    }): Promise<{}>;
    searchByTypeAndIdsFromQueue(): Promise<void>;
    searchByTypeAndIds(options?: {
        type: TYPE;
        ids: any[];
        params: {};
    }): Promise<any>;
    searchCompany(params: any): Promise<any>;
}
