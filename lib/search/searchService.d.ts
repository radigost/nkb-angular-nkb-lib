import { Meta } from '../meta/metaService';
declare class SearchService {
    metaService: Meta;
    constructor(metaService: any);
    init(): Promise<void>;
    getSearchableNodeTypes(): {};
    searchByTypeAndText(searchType: any, query: any): Promise<{}>;
    searchByTypeAndId(options: any): Promise<{}>;
}
declare const searchService: SearchService;
export { searchService };
