/**
 * Created by radigost on 24.04.17.
 */
import { AutokadResource } from './autokadResource';
export declare class AutokadService {
    private autokadResourceService;
    search: any;
    loading: any;
    pager: any;
    initSearch: any;
    doSearch: any;
    buildSearchRequestData: any;
    getResultTotal: any;
    getCaseCount: any;
    hasError: any;
    isLoading: any;
    isNoResult: any;
    constructor(autokadResourceService: AutokadResource);
}
