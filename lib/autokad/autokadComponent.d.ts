/**
 * Created by radigost on 24.04.17.
 */
import { AutokadService } from './autokadService';
import { Node } from '../domain/Node';
import { OnChanges, EventEmitter } from '@angular/core';
export declare class AutokadComponent implements OnChanges {
    private autokadService;
    scrollTop: number;
    node: Node;
    side: any;
    onAddArbitrationBreadcrumb: EventEmitter<{}>;
    id: any;
    pager: any;
    loading: boolean;
    constructor(autokadService: AutokadService);
    ngOnChanges(): Promise<void>;
    readonly list: any;
    changeSide(type: any): Promise<void>;
    isSelectedSide(type: any): boolean;
    getPlaintiff(caseSides: any): any[];
    getDefendant(caseSides: any): any[];
    getOthers(caseSides: any): any[];
    getTotal(): any;
    isEmptyResult(): boolean;
    isNoResult(): any;
    hasError(): any;
    readonly isLoading: boolean;
    addArbitrationBreadcrumb(id: any): void;
    nextPage(): Promise<void>;
}
