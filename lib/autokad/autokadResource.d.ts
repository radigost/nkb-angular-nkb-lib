/**
 * Created by radigost on 24.04.17.
 */
import { Cache } from '../cache/Cache';
export declare class AutokadResource {
    kadSearch(arg0: {
        r: {
            q: any;
        };
        success: (data: any) => void;
        error: () => void;
    }): any;
    cache: Cache;
    init(): Promise<void>;
    search(options: any): Promise<any>;
    aggregates(options: any): Promise<any>;
    getAggregates(ogrn: any): Promise<{}>;
}
