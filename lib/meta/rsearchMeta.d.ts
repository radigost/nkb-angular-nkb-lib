export declare const rsearchMeta: {
    nodeTypes: {
        'COMPANY': {
            search: boolean;
            searchResultPriority: number;
            accentedResultPriority: number;
        };
        'INDIVIDUAL_IDENTITY': {
            search: boolean;
            searchResultPriority: number;
            accentedResultPriority: number;
        };
        'INDIVIDUAL': {
            search: boolean;
            searchResultPriority: number;
            accentedResultPriority: number;
        };
        'ADDRESS': {
            search: boolean;
            searchResultPriority: number;
            accentedResultPriority: number;
        };
        'PHONE': {
            search: boolean;
            searchResultPriority: number;
            accentedResultPriority: number;
        };
        'PURCHASE': {
            search: boolean;
            searchResultPriority: number;
            accentedResultPriority: number;
        };
    };
    relationTypes: {
        'FOUNDER': {
            name: string;
            'parents': {
                order: number;
                top: boolean;
                group: string;
                pageSize: string;
                history: {
                    opposite: boolean;
                };
            };
        };
        'FOUNDER_INDIVIDUAL': {
            'children': {
                order: number;
                top: boolean;
                group: string;
                history: {
                    opposite: boolean;
                };
            };
            'parents': {
                order: number;
                top: boolean;
                group: string;
                pageSize: string;
                history: {
                    opposite: boolean;
                };
            };
        };
        'FOUNDER_COMPANY': {
            'children': {
                order: number;
                top: boolean;
                group: string;
                history: {
                    opposite: boolean;
                };
            };
            'parents': {
                order: number;
                top: boolean;
                group: string;
                pageSize: string;
                history: {
                    opposite: boolean;
                };
            };
        };
        'HEAD_COMPANY': {
            'children': {
                order: number;
                top: boolean;
                group: string;
            };
            'parents': {
                order: number;
                group: string;
            };
        };
        'PREDECESSOR_COMPANY': {
            'children': {
                order: number;
                top: boolean;
                group: string;
            };
            'parents': {
                order: number;
                top: boolean;
                group: string;
            };
        };
        'REGISTER_HOLDER': {
            'children': {
                order: number;
                group: string;
            };
            'parents': {
                order: number;
                group: string;
            };
        };
        'EXECUTIVE_COMPANY': {
            'children': {
                order: number;
                group: string;
            };
            'parents': {
                order: number;
                group: string;
            };
        };
        'EXECUTIVE_INDIVIDUAL': {
            'children': {
                order: number;
                top: boolean;
                group: string;
                history: {
                    opposite: boolean;
                };
            };
            'parents': {
                order: number;
                group: string;
                pageSize: string;
                history: {
                    opposite: boolean;
                };
            };
        };
        'AFFILIATED_INDIVIDUAL': {
            'children': {
                order: number;
                top: boolean;
                group: string;
            };
            'parents': {
                order: number;
                top: boolean;
                group: string;
            };
        };
        'AFFILIATED_COMPANY': {
            'children': {
                order: number;
                top: boolean;
                group: string;
            };
            'parents': {
                order: number;
                top: boolean;
                group: string;
            };
        };
        'ADDRESS': {
            'children': {
                order: number;
                top: boolean;
                group: string;
            };
            'parents': {
                order: number;
                group: string;
            };
        };
        'PHONE': {
            'children': {
                order: number;
                top: boolean;
                group: string;
            };
            'parents': {
                order: number;
                group: string;
            };
        };
        'CUSTOMER_COMPANY': {
            'children': {
                order: number;
                group: string;
            };
            'parents': {
                order: number;
                top: boolean;
                group: string;
            };
        };
        'PARTICIPANT_COMPANY': {
            'children': {
                order: number;
                group: string;
            };
            'parents': {
                order: number;
                top: boolean;
                group: string;
            };
        };
        'EMPLOYEE': {
            'children': {
                order: number;
                group: string;
            };
            'parents': {
                order: number;
                group: string;
            };
        };
        'PARTICIPANT_INDIVIDUAL': {
            'children': {
                order: number;
                group: string;
            };
            'parents': {
                order: number;
                top: boolean;
                group: string;
            };
        };
        'COMMISSION_MEMBER': {
            'children': {
                order: number;
                group: string;
            };
            'parents': {
                order: number;
                group: string;
            };
        };
        'kinsmen': {
            name: string;
            sourceNodeType: string;
            destinationNodeType: string;
        };
    };
    mergedRelationTypes: {
        'FOUNDER': {
            'parents': string[];
        };
    };
    relationGroups: {
        'BASE': {
            order: number;
        };
        'AFFILIATED': {
            order: number;
        };
        'CONTACTS': {
            order: number;
        };
        'PURCHASE': {
            order: number;
        };
    };
};
