/**
 * Created by radigost on 07.09.17.
 */
import { NODE_TYPE } from '../domain/TYPES';
export interface NodeType {
    name: NODE_TYPE;
    sortField: any;
    sortAscending: any;
    idField: any;
    nameField: any;
    properties: [{
        [property: string]: string;
    }];
}
export declare class Meta {
    relationTypes: any;
    nodeTypes: NodeType[];
    SEARCHABLE_TYPES: NODE_TYPE[];
    state: {
        inited: boolean;
        pending: boolean;
        error: boolean;
    };
    nodeTypesMeta: {};
    relationTypesMeta: {};
    readonly inited: boolean;
    init(): Promise<{}>;
    getNodeName(node: any): any;
    getNodeId(node: any): any;
    getIdFieldForType(type: any): any;
    buildRelationDataByNodeInfo(node: any): {
        relationCountMap: {};
        relationCounts: any[];
        groups: any;
    };
    getHistoryRelationCountsByInfo(info: any): {
        actual: any;
        all: any;
        outdated: any;
    };
    getDirection(infoDirection: any): "parents" | "children";
    getRelationTypeMeta(relationType: any, direction: any): any;
    getRelationGroupMeta(relationGroup: any): any;
    getMergedTypeInfoByRelationType(relationType: any, direction: any): {
        mergedType: any;
        relationTypes: any;
    };
    buildNodeRelationKey(direction: any, relationType: any): string;
    buildNodeRelationPluralKey(direction: any, relationType: any): string;
    buildNodeExtraMeta(node: any): void;
    buildNodeUID(node: any): any;
    buildNodeUIDByType(id: any, type: any): string;
    buildCompanyState(node: any): void;
    resolvePurchaseHref(node: any): void;
}
declare const metaService: Meta;
export { metaService };
