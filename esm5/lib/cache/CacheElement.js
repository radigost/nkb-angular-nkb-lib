/**
 * @fileoverview added by tsickle
 * Generated from: lib/cache/CacheElement.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import * as _ from 'lodash';
var CacheElement = /** @class */ (function () {
    function CacheElement(el) {
        Object.assign(this, { promise: el });
    }
    /**
     * @return {?}
     */
    CacheElement.prototype.head = /**
     * @return {?}
     */
    function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var res;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.promise];
                    case 1:
                        res = _a.sent();
                        return [2 /*return*/, _.head(_.get(res, 'data.list', [res]))];
                }
            });
        });
    };
    /**
     * @return {?}
     */
    CacheElement.prototype.list = /**
     * @return {?}
     */
    function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var res;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.promise];
                    case 1:
                        res = _a.sent();
                        return [2 /*return*/, _.get(res, 'data.list')];
                }
            });
        });
    };
    /**
     * @return {?}
     */
    CacheElement.prototype.data = /**
     * @return {?}
     */
    function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var res;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.promise];
                    case 1:
                        res = _a.sent();
                        return [2 /*return*/, _.get(res, 'data')];
                }
            });
        });
    };
    /**
     * @return {?}
     */
    CacheElement.prototype.get = /**
     * @return {?}
     */
    function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.promise];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    return CacheElement;
}());
export { CacheElement };
if (false) {
    /** @type {?} */
    CacheElement.prototype.promise;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQ2FjaGVFbGVtZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQG5rYi9hbmd1bGFyLW5rYi1saWIvIiwic291cmNlcyI6WyJsaWIvY2FjaGUvQ2FjaGVFbGVtZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLE9BQU8sS0FBSyxDQUFDLE1BQU0sUUFBUSxDQUFDO0FBRzVCO0lBRUksc0JBQVksRUFBRTtRQUNWLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLEVBQUUsT0FBTyxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUM7SUFDekMsQ0FBQzs7OztJQUVLLDJCQUFJOzs7SUFBVjs7Ozs7NEJBQ2dCLHFCQUFNLElBQUksQ0FBQyxPQUFPLEVBQUE7O3dCQUF4QixHQUFHLEdBQUcsU0FBa0I7d0JBQzlCLHNCQUFPLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxHQUFHLEVBQUUsV0FBVyxFQUFFLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxFQUFDOzs7O0tBQ2pEOzs7O0lBRUssMkJBQUk7OztJQUFWOzs7Ozs0QkFDZ0IscUJBQU0sSUFBSSxDQUFDLE9BQU8sRUFBQTs7d0JBQXhCLEdBQUcsR0FBRyxTQUFrQjt3QkFDOUIsc0JBQU8sQ0FBQyxDQUFDLEdBQUcsQ0FBQyxHQUFHLEVBQUUsV0FBVyxDQUFDLEVBQUM7Ozs7S0FDbEM7Ozs7SUFFSywyQkFBSTs7O0lBQVY7Ozs7OzRCQUNnQixxQkFBTSxJQUFJLENBQUMsT0FBTyxFQUFBOzt3QkFBeEIsR0FBRyxHQUFHLFNBQWtCO3dCQUM5QixzQkFBTyxDQUFDLENBQUMsR0FBRyxDQUFDLEdBQUcsRUFBRSxNQUFNLENBQUMsRUFBQzs7OztLQUM3Qjs7OztJQUVLLDBCQUFHOzs7SUFBVDs7Ozs0QkFDVyxxQkFBTSxJQUFJLENBQUMsT0FBTyxFQUFBOzRCQUF6QixzQkFBTyxTQUFrQixFQUFDOzs7O0tBQzdCO0lBQ0wsbUJBQUM7QUFBRCxDQUFDLEFBeEJELElBd0JDOzs7O0lBdkJHLCtCQUFnQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCAqIGFzIF8gZnJvbSAnbG9kYXNoJztcclxuaW1wb3J0IHsgQXhpb3NSZXNwb25zZSB9IGZyb20gJ2F4aW9zJztcclxuXHJcbmV4cG9ydCBjbGFzcyBDYWNoZUVsZW1lbnQge1xyXG4gICAgcHJvbWlzZTogUHJvbWlzZTxBeGlvc1Jlc3BvbnNlPjtcclxuICAgIGNvbnN0cnVjdG9yKGVsKSB7XHJcbiAgICAgICAgT2JqZWN0LmFzc2lnbih0aGlzLCB7IHByb21pc2U6IGVsIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGFzeW5jIGhlYWQoKSB7XHJcbiAgICAgICAgY29uc3QgcmVzID0gYXdhaXQgdGhpcy5wcm9taXNlO1xyXG4gICAgICAgIHJldHVybiBfLmhlYWQoXy5nZXQocmVzLCAnZGF0YS5saXN0JywgW3Jlc10pKTtcclxuICAgIH1cclxuXHJcbiAgICBhc3luYyBsaXN0KCkge1xyXG4gICAgICAgIGNvbnN0IHJlcyA9IGF3YWl0IHRoaXMucHJvbWlzZTtcclxuICAgICAgICByZXR1cm4gXy5nZXQocmVzLCAnZGF0YS5saXN0Jyk7XHJcbiAgICB9XHJcblxyXG4gICAgYXN5bmMgZGF0YSgpIHtcclxuICAgICAgICBjb25zdCByZXMgPSBhd2FpdCB0aGlzLnByb21pc2U7XHJcbiAgICAgICAgcmV0dXJuIF8uZ2V0KHJlcywgJ2RhdGEnKTtcclxuICAgIH1cclxuXHJcbiAgICBhc3luYyBnZXQoKSB7XHJcbiAgICAgICAgcmV0dXJuIGF3YWl0IHRoaXMucHJvbWlzZTtcclxuICAgIH1cclxufVxyXG4iXX0=