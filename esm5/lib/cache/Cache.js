/**
 * @fileoverview added by tsickle
 * Generated from: lib/cache/Cache.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import axios from 'axios';
import { CacheElement } from './CacheElement';
/** @type {?} */
var METHODS = {
    'GET': 'GET',
    'POST': 'POST',
    'PUT': 'PUT',
    'DELETE': 'DELETE',
};
var Cache = /** @class */ (function () {
    function Cache() {
        this.obj = {};
    }
    /**
     * @param {?} key
     * @return {?}
     */
    Cache.prototype.get = /**
     * @param {?} key
     * @return {?}
     */
    function (key) {
        return this.obj[key];
    };
    /**
     * @param {?} key
     * @param {?} value
     * @return {?}
     */
    Cache.prototype.set = /**
     * @param {?} key
     * @param {?} value
     * @return {?}
     */
    function (key, value) {
        this.obj[key] = value;
    };
    /**
     * @param {?} url
     * @param {?=} options
     * @return {?}
     */
    Cache.prototype.try = /**
     * @param {?} url
     * @param {?=} options
     * @return {?}
     */
    function (url, options) {
        if (options === void 0) { options = { params: {}, method: METHODS.GET }; }
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var result;
            return tslib_1.__generator(this, function (_a) {
                result = this.get(url);
                if (result === undefined) {
                    // @ts-ignore
                    result = new CacheElement(axios({ url: url, method: options.method }));
                    this.set(url, result);
                }
                return [2 /*return*/, result];
            });
        });
    };
    return Cache;
}());
export { Cache };
if (false) {
    /** @type {?} */
    Cache.prototype.obj;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQ2FjaGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AbmtiL2FuZ3VsYXItbmtiLWxpYi8iLCJzb3VyY2VzIjpbImxpYi9jYWNoZS9DYWNoZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7QUFFQSxPQUFPLEtBQUssTUFBTSxPQUFPLENBQUM7QUFDMUIsT0FBTyxFQUFDLFlBQVksRUFBQyxNQUFNLGdCQUFnQixDQUFDOztJQUd0QyxPQUFPLEdBQUc7SUFDWixLQUFLLEVBQUUsS0FBSztJQUNaLE1BQU0sRUFBRSxNQUFNO0lBQ2QsS0FBSyxFQUFFLEtBQUs7SUFDWixRQUFRLEVBQUUsUUFBUTtDQUNyQjtBQUVEO0lBRUk7UUFDSSxJQUFJLENBQUMsR0FBRyxHQUFHLEVBQUUsQ0FBQztJQUNsQixDQUFDOzs7OztJQUdELG1CQUFHOzs7O0lBQUgsVUFBSSxHQUFHO1FBQ0gsT0FBTyxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQ3pCLENBQUM7Ozs7OztJQUdELG1CQUFHOzs7OztJQUFILFVBQUksR0FBRyxFQUFFLEtBQUs7UUFDVixJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxHQUFHLEtBQUssQ0FBQztJQUMxQixDQUFDOzs7Ozs7SUFFSyxtQkFBRzs7Ozs7SUFBVCxVQUFVLEdBQUcsRUFBRSxPQUEyQztRQUEzQyx3QkFBQSxFQUFBLFlBQVcsTUFBTSxFQUFFLEVBQUUsRUFBRSxNQUFNLEVBQUUsT0FBTyxDQUFDLEdBQUcsRUFBQzs7OztnQkFDbEQsTUFBTSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDO2dCQUMxQixJQUFJLE1BQU0sS0FBSyxTQUFTLEVBQUU7b0JBQ3RCLGFBQWE7b0JBQ2IsTUFBTSxHQUFHLElBQUksWUFBWSxDQUFDLEtBQUssQ0FBQyxFQUFDLEdBQUcsS0FBQSxFQUFFLE1BQU0sRUFBRSxPQUFPLENBQUMsTUFBTSxFQUFDLENBQUMsQ0FBQyxDQUFDO29CQUNoRSxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsRUFBRSxNQUFNLENBQUMsQ0FBQTtpQkFDeEI7Z0JBQ0Qsc0JBQU8sTUFBTSxFQUFDOzs7S0FPakI7SUFDTCxZQUFDO0FBQUQsQ0FBQyxBQS9CRCxJQStCQzs7OztJQTlCRyxvQkFBSSIsInNvdXJjZXNDb250ZW50IjpbIlxyXG5cclxuaW1wb3J0IGF4aW9zIGZyb20gJ2F4aW9zJztcclxuaW1wb3J0IHtDYWNoZUVsZW1lbnR9IGZyb20gJy4vQ2FjaGVFbGVtZW50JztcclxuXHJcblxyXG5jb25zdCBNRVRIT0RTID0ge1xyXG4gICAgJ0dFVCc6ICdHRVQnLFxyXG4gICAgJ1BPU1QnOiAnUE9TVCcsXHJcbiAgICAnUFVUJzogJ1BVVCcsXHJcbiAgICAnREVMRVRFJzogJ0RFTEVURScsXHJcbn07XHJcblxyXG5leHBvcnQgY2xhc3MgQ2FjaGUge1xyXG4gICAgb2JqO1xyXG4gICAgY29uc3RydWN0b3IoKSB7XHJcbiAgICAgICAgdGhpcy5vYmogPSB7fTtcclxuICAgIH1cclxuXHJcblxyXG4gICAgZ2V0KGtleSkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLm9ialtrZXldO1xyXG4gICAgfVxyXG5cclxuXHJcbiAgICBzZXQoa2V5LCB2YWx1ZSkge1xyXG4gICAgICAgIHRoaXMub2JqW2tleV0gPSB2YWx1ZTtcclxuICAgIH1cclxuXHJcbiAgICBhc3luYyB0cnkodXJsLCBvcHRpb25zID0ge3BhcmFtczoge30sIG1ldGhvZDogTUVUSE9EUy5HRVR9KSB7XHJcbiAgICAgICAgbGV0IHJlc3VsdCA9IHRoaXMuZ2V0KHVybCk7XHJcbiAgICAgICAgaWYgKHJlc3VsdCA9PT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgICAgICAgIC8vIEB0cy1pZ25vcmVcclxuICAgICAgICAgICAgcmVzdWx0ID0gbmV3IENhY2hlRWxlbWVudChheGlvcyh7dXJsLCBtZXRob2Q6IG9wdGlvbnMubWV0aG9kfSkpO1xyXG4gICAgICAgICAgICB0aGlzLnNldCh1cmwsIHJlc3VsdClcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHJlc3VsdDtcclxuICAgICAgICBcclxuICAgICAgICAvLzEuINCV0YHQu9C4INC10YHRgtGMINGC0LDQudC80LXRgCAtINC00L7QsdCw0LLQu9GP0LXQvCDQv9Cw0YDQsNC80LXRgtGA0YssINC80LXQvdGP0LXRgiDRgtCw0LnQvNCw0YPRglxyXG4gICAgICAgIC8vMi4g0L/QviDQv9GA0L7RiNC10YHRgtCy0LjQuCDRgtCw0LnQvNCw0YPRgtCwINC00LXQu9Cw0LXQvCDQvNC90L7QttC10YHRgtCy0LXQvdC90YvQuSDQt9Cw0L/RgNC+0YFcclxuICAgICAgICAvLzMuINGA0LDQt9Cx0LjRgNCw0LXQvCDQt9Cw0L/RgNC+0YEg0L/QviDRjdC70LXQvNC10L3RgtCw0LwsINC60LvQsNC00LXQvCDQutCw0LbQtNGL0Lkg0LjQtyDQvdC40YUg0LIg0YHQvtC+0YLQstC10YLRgdCy0YPRjtGJ0LjRhSDQutC10YhcclxuXHJcblxyXG4gICAgfVxyXG59XHJcbiJdfQ==