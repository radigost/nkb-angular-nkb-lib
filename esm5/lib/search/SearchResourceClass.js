/**
 * @fileoverview added by tsickle
 * Generated from: lib/search/SearchResourceClass.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import axios from 'axios';
import { Cache } from '../cache/Cache';
import { NODE_TYPE as TYPE } from '../domain/TYPES';
import { head, get, debounce, assignIn, find, isEqual } from 'lodash';
import { CacheElement } from '../cache/CacheElement';
/** @type {?} */
var search = axios.create({
    baseURL: '/nkbrelation/api/nodes'
});
//old but fast api , maybe after _relations=false new is the same on speed - TODO check speen and if neer is faster - move to it
/** @type {?} */
var company = axios.create({
    baseURL: '/nkbrelation/api/company'
});
var SearchResource = /** @class */ (function () {
    function SearchResource(metaService) {
        Object.assign(this, {
            cache: new Cache(
            // {keyFunction: (type, idField, id) => `/nkbrelation/api/nodes/${type}?${idField}.equals=${id}`}
            ),
            queue: [],
            queueType: null,
            queueResults: [],
            queueParams: {},
            pedning: false,
            debounce: debounce(this.searchByTypeAndIdsFromQueue, 500, { maxWait: 1000 }),
            metaService: metaService
        });
        metaService.init();
    }
    /**
     * @return {?}
     */
    SearchResource.prototype.updateQueueSearchByTypeAndIds = /**
     * @return {?}
     */
    function () {
        return this.queue.length < 10 ? this.debounce() : this.searchByTypeAndIdsFromQueue();
    };
    /**
     * @param {?} options
     * @return {?}
     */
    SearchResource.prototype.searchRequest = /**
     * @param {?} options
     * @return {?}
     */
    function (options) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var params, res;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        params = assignIn({}, options.filter, options.pageConfig, {
                            q: options.q,
                            exclude: '_relations'
                        });
                        return [4 /*yield*/, search.get("/" + options.nodeType, { params: params })];
                    case 1:
                        res = _a.sent();
                        return [2 /*return*/, get(res, 'data', [])];
                }
            });
        });
    };
    /**
     * @param {?} options
     * @return {?}
     */
    SearchResource.prototype.searchByTypeAndId = /**
     * @param {?} options
     * @return {?}
     */
    function (options) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var idField;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!!this.metaService.inited) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.metaService.init()];
                    case 1:
                        _a.sent();
                        _a.label = 2;
                    case 2:
                        idField = this.metaService.getIdFieldForType(options.type);
                        return [2 /*return*/, search.get(options.type + "?" + idField + ".equals=" + options.id + "&exclude=_relations", { params: options }).then((/**
                             * @param {?} res
                             * @return {?}
                             */
                            function (res) { return head(res.data.list); }))];
                }
            });
        });
    };
    /**
     * @param {?=} options
     * @return {?}
     */
    SearchResource.prototype.searchByTypeAndIdQueued = /**
     * @param {?=} options
     * @return {?}
     */
    function (options) {
        if (options === void 0) { options = { type: TYPE.COMPANY, id: null, params: {} }; }
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                return [2 /*return*/, new Promise((/**
                     * @param {?} resolve
                     * @return {?}
                     */
                    function (resolve) { return tslib_1.__awaiter(_this, void 0, void 0, function () {
                        var doInterval, idField, inCache, isInqueue, res;
                        var _this = this;
                        return tslib_1.__generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    doInterval = (/**
                                     * @return {?}
                                     */
                                    function () {
                                        /** @type {?} */
                                        var intervalID = setInterval((/**
                                         * @return {?}
                                         */
                                        function () {
                                            if (!_this.pending) {
                                                /** @type {?} */
                                                var res = find(_this.queueResults, (/**
                                                 * @param {?} company
                                                 * @return {?}
                                                 */
                                                function (company) { return isEqual("" + company[idField], "" + options.id); }));
                                                if (res) {
                                                    clearInterval(intervalID);
                                                    resolve(res);
                                                }
                                            }
                                        }), 100);
                                    });
                                    idField = this.metaService.getIdFieldForType(options.type);
                                    inCache = this.cache.get("/nkbrelation/api/nodes/" + options.type + "?" + idField + ".equals=" + options.id);
                                    if (inCache) {
                                        resolve(inCache.head());
                                    }
                                    isInqueue = this.queue.includes(options.id);
                                    if (!!isInqueue) return [3 /*break*/, 4];
                                    if (!(this.queueType === options.type || this.queueType === null)) return [3 /*break*/, 1];
                                    this.queueType = options.type;
                                    this.queueParams = options.params;
                                    this.queue.push(options.id);
                                    this.updateQueueSearchByTypeAndIds();
                                    doInterval();
                                    return [3 /*break*/, 3];
                                case 1:
                                    if (!(this.queueType !== options.type)) return [3 /*break*/, 3];
                                    return [4 /*yield*/, this.cache.try("/nkbrelation/api/nodes/" + options.type + "?" + idField + ".equals=" + options.id).then((/**
                                         * @param {?} r
                                         * @return {?}
                                         */
                                        function (r) { return head(r); }))];
                                case 2:
                                    res = _a.sent();
                                    resolve(res);
                                    _a.label = 3;
                                case 3: return [3 /*break*/, 5];
                                case 4:
                                    doInterval();
                                    _a.label = 5;
                                case 5: return [2 /*return*/];
                            }
                        });
                    }); }))];
            });
        });
    };
    /**
     * @return {?}
     */
    SearchResource.prototype.searchByTypeAndIdsFromQueue = /**
     * @return {?}
     */
    function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var type, idField;
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                this.pending = true;
                type = this.queueType;
                idField = this.metaService.getIdFieldForType(type);
                this.searchByTypeAndIds({ type: type, ids: this.queue, params: this.queueParams }).then((/**
                 * @param {?} res
                 * @return {?}
                 */
                function (res) {
                    _this.queueResults = _this.queueResults.concat(res);
                    res.forEach((/**
                     * @param {?} el
                     * @return {?}
                     */
                    function (el) {
                        _this.cache.set("/nkbrelation/api/nodes/" + type + "?" + idField + ".equals=" + el[idField], new CacheElement(Promise.resolve(el)));
                    }));
                    _this.pending = false;
                }));
                this.queueType = null;
                this.queue = [];
                return [2 /*return*/];
            });
        });
    };
    /**
     * @param {?=} options
     * @return {?}
     */
    SearchResource.prototype.searchByTypeAndIds = /**
     * @param {?=} options
     * @return {?}
     */
    function (options) {
        if (options === void 0) { options = { type: TYPE.COMPANY, ids: [], params: {} }; }
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var result, urlparams, idField, url;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.metaService.init()];
                    case 1:
                        _a.sent();
                        if (options.ids.length < 1) {
                            return [2 /*return*/, Promise.resolve({})];
                        }
                        urlparams = new URLSearchParams();
                        idField = this.metaService.getIdFieldForType(options.type);
                        options.ids.forEach((/**
                         * @param {?} id
                         * @return {?}
                         */
                        function (id) { return urlparams.append(idField + ".equals", id); }));
                        if (options.params)
                            Object.keys(options.params).forEach((/**
                             * @param {?} key
                             * @return {?}
                             */
                            function (key) { return urlparams.append(key, options.params[key]); }));
                        url = "/nkbrelation/api/nodes/" + options.type + "?" + urlparams.toString();
                        return [4 /*yield*/, this.cache.try(url).then((/**
                             * @param {?} res
                             * @return {?}
                             */
                            function (res) { return res.list(); }))];
                    case 2:
                        result = _a.sent();
                        return [2 /*return*/, result];
                }
            });
        });
    };
    /**
     * @param {?} params
     * @return {?}
     */
    SearchResource.prototype.searchCompany = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var res;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, company.get('/', { params: params })];
                    case 1:
                        res = _a.sent();
                        return [2 /*return*/, get(res, 'data', [])];
                }
            });
        });
    };
    return SearchResource;
}());
export { SearchResource };
if (false) {
    /** @type {?} */
    SearchResource.prototype.queue;
    /** @type {?} */
    SearchResource.prototype.debounce;
    /** @type {?} */
    SearchResource.prototype.pending;
    /** @type {?} */
    SearchResource.prototype.queueResults;
    /** @type {?} */
    SearchResource.prototype.cache;
    /** @type {?} */
    SearchResource.prototype.queueType;
    /** @type {?} */
    SearchResource.prototype.queueParams;
    /** @type {?} */
    SearchResource.prototype.metaService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiU2VhcmNoUmVzb3VyY2VDbGFzcy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bua2IvYW5ndWxhci1ua2ItbGliLyIsInNvdXJjZXMiOlsibGliL3NlYXJjaC9TZWFyY2hSZXNvdXJjZUNsYXNzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLE9BQU8sS0FBSyxNQUFNLE9BQU8sQ0FBQTtBQUN6QixPQUFPLEVBQUUsS0FBSyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDdkMsT0FBTyxFQUFFLFNBQVMsSUFBSSxJQUFJLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUNwRCxPQUFPLEVBQUUsSUFBSSxFQUFFLEdBQUcsRUFBRSxRQUFRLEVBQUUsUUFBUSxFQUFFLElBQUksRUFBRSxPQUFPLEVBQUUsTUFBTSxRQUFRLENBQUM7QUFDdEUsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLHVCQUF1QixDQUFDOztJQUcvQyxNQUFNLEdBQUcsS0FBSyxDQUFDLE1BQU0sQ0FBQztJQUN4QixPQUFPLEVBQUUsd0JBQXdCO0NBQ3BDLENBQUM7OztJQUlJLE9BQU8sR0FBRyxLQUFLLENBQUMsTUFBTSxDQUFDO0lBQ3pCLE9BQU8sRUFBRSwwQkFBMEI7Q0FDdEMsQ0FBQztBQUVGO0lBU0ksd0JBQVksV0FBVztRQUNuQixNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRTtZQUNoQixLQUFLLEVBQUUsSUFBSSxLQUFLO1lBQ1osaUdBQWlHO2FBQ3BHO1lBQ0QsS0FBSyxFQUFFLEVBQUU7WUFDVCxTQUFTLEVBQUUsSUFBSTtZQUNmLFlBQVksRUFBRSxFQUFFO1lBQ2hCLFdBQVcsRUFBRSxFQUFFO1lBQ2YsT0FBTyxFQUFFLEtBQUs7WUFDZCxRQUFRLEVBQUUsUUFBUSxDQUFDLElBQUksQ0FBQywyQkFBMkIsRUFBRSxHQUFHLEVBQUUsRUFBRSxPQUFPLEVBQUUsSUFBSSxFQUFFLENBQUM7WUFDNUUsV0FBVyxFQUFFLFdBQVc7U0FDM0IsQ0FBQyxDQUFDO1FBRUgsV0FBVyxDQUFDLElBQUksRUFBRSxDQUFDO0lBQ3ZCLENBQUM7Ozs7SUFFRCxzREFBNkI7OztJQUE3QjtRQUNJLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQywyQkFBMkIsRUFBRSxDQUFDO0lBQ3pGLENBQUM7Ozs7O0lBRUssc0NBQWE7Ozs7SUFBbkIsVUFBb0IsT0FBTzs7Ozs7O3dCQUNqQixNQUFNLEdBQUcsUUFBUSxDQUFDLEVBQUUsRUFDdEIsT0FBTyxDQUFDLE1BQU0sRUFDZCxPQUFPLENBQUMsVUFBVSxFQUNsQjs0QkFDSSxDQUFDLEVBQUUsT0FBTyxDQUFDLENBQUM7NEJBQ1osT0FBTyxFQUFFLFlBQVk7eUJBQ3hCLENBQ0o7d0JBQ1cscUJBQU0sTUFBTSxDQUFDLEdBQUcsQ0FBQyxNQUFJLE9BQU8sQ0FBQyxRQUFVLEVBQUUsRUFBRSxNQUFNLFFBQUEsRUFBRSxDQUFDLEVBQUE7O3dCQUExRCxHQUFHLEdBQUcsU0FBb0Q7d0JBQ2hFLHNCQUFPLEdBQUcsQ0FBQyxHQUFHLEVBQUUsTUFBTSxFQUFFLEVBQUUsQ0FBQyxFQUFDOzs7O0tBQy9COzs7OztJQUVLLDBDQUFpQjs7OztJQUF2QixVQUF3QixPQUFPOzs7Ozs7NkJBQ3ZCLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEVBQXhCLHdCQUF3Qjt3QkFBRSxxQkFBTSxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksRUFBRSxFQUFBOzt3QkFBN0IsU0FBNkIsQ0FBQzs7O3dCQUN0RCxPQUFPLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxpQkFBaUIsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDO3dCQUNoRSxzQkFBTyxNQUFNLENBQUMsR0FBRyxDQUFJLE9BQU8sQ0FBQyxJQUFJLFNBQUksT0FBTyxnQkFBVyxPQUFPLENBQUMsRUFBRSx3QkFBcUIsRUFBRSxFQUFFLE1BQU0sRUFBRSxPQUFPLEVBQUUsQ0FBQyxDQUFDLElBQUk7Ozs7NEJBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBbkIsQ0FBbUIsRUFBQyxFQUFDOzs7O0tBQ2pKOzs7OztJQUVLLGdEQUF1Qjs7OztJQUE3QixVQUE4QixPQUFzRDtRQUF0RCx3QkFBQSxFQUFBLFlBQVksSUFBSSxFQUFFLElBQUksQ0FBQyxPQUFPLEVBQUUsRUFBRSxFQUFFLElBQUksRUFBRSxNQUFNLEVBQUUsRUFBRSxFQUFFOzs7O2dCQUNoRixzQkFBTyxJQUFJLE9BQU87Ozs7b0JBQUMsVUFBTyxPQUFPOzs7Ozs7b0NBQ3ZCLFVBQVU7OztvQ0FBRzs7NENBQ1QsVUFBVSxHQUFHLFdBQVc7Ozt3Q0FBQzs0Q0FDM0IsSUFBSSxDQUFDLEtBQUksQ0FBQyxPQUFPLEVBQUU7O29EQUNYLEdBQUcsR0FBRyxJQUFJLENBQUMsS0FBSSxDQUFDLFlBQVk7Ozs7Z0RBQUUsVUFBQyxPQUFPLElBQUssT0FBQSxPQUFPLENBQUMsS0FBRyxPQUFPLENBQUMsT0FBTyxDQUFHLEVBQUUsS0FBRyxPQUFPLENBQUMsRUFBSSxDQUFDLEVBQS9DLENBQStDLEVBQUM7Z0RBQy9GLElBQUksR0FBRyxFQUFFO29EQUNMLGFBQWEsQ0FBQyxVQUFVLENBQUMsQ0FBQztvREFDMUIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDO2lEQUNoQjs2Q0FDSjt3Q0FDTCxDQUFDLEdBQUUsR0FBRyxDQUFDO29DQUNYLENBQUMsQ0FBQTtvQ0FFSyxPQUFPLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxpQkFBaUIsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDO29DQUMxRCxPQUFPLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsNEJBQTBCLE9BQU8sQ0FBQyxJQUFJLFNBQUksT0FBTyxnQkFBVyxPQUFPLENBQUMsRUFBSSxDQUFDO29DQUN4RyxJQUFJLE9BQU8sRUFBRTt3Q0FDVCxPQUFPLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxDQUFDLENBQUM7cUNBQzNCO29DQUVLLFNBQVMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDO3lDQUM3QyxDQUFDLFNBQVMsRUFBVix3QkFBVTt5Q0FDTixDQUFBLElBQUksQ0FBQyxTQUFTLEtBQUssT0FBTyxDQUFDLElBQUksSUFBSSxJQUFJLENBQUMsU0FBUyxLQUFLLElBQUksQ0FBQSxFQUExRCx3QkFBMEQ7b0NBQzFELElBQUksQ0FBQyxTQUFTLEdBQUcsT0FBTyxDQUFDLElBQUksQ0FBQztvQ0FDOUIsSUFBSSxDQUFDLFdBQVcsR0FBRyxPQUFPLENBQUMsTUFBTSxDQUFDO29DQUNsQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDLENBQUM7b0NBQzVCLElBQUksQ0FBQyw2QkFBNkIsRUFBRSxDQUFDO29DQUNyQyxVQUFVLEVBQUUsQ0FBQzs7O3lDQUNOLENBQUEsSUFBSSxDQUFDLFNBQVMsS0FBSyxPQUFPLENBQUMsSUFBSSxDQUFBLEVBQS9CLHdCQUErQjtvQ0FDMUIscUJBQU0sSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsNEJBQTBCLE9BQU8sQ0FBQyxJQUFJLFNBQUksT0FBTyxnQkFBVyxPQUFPLENBQUMsRUFBSSxDQUFDLENBQUMsSUFBSTs7Ozt3Q0FBQyxVQUFBLENBQUMsSUFBSSxPQUFBLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBUCxDQUFPLEVBQUMsRUFBQTs7b0NBQXZILEdBQUcsR0FBRyxTQUFpSDtvQ0FDN0gsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDOzs7O29DQUdqQixVQUFVLEVBQUUsQ0FBQzs7Ozs7eUJBRXBCLEVBQUMsRUFBQTs7O0tBQ0w7Ozs7SUFFSyxvREFBMkI7OztJQUFqQzs7Ozs7Z0JBQ0ksSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7Z0JBQ2QsSUFBSSxHQUFHLElBQUksQ0FBQyxTQUFTO2dCQUNyQixPQUFPLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUM7Z0JBQ3hELElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxFQUFFLElBQUksTUFBQSxFQUFFLEdBQUcsRUFBRSxJQUFJLENBQUMsS0FBSyxFQUFFLE1BQU0sRUFBRSxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQyxJQUFJOzs7O2dCQUFDLFVBQUEsR0FBRztvQkFDakYsS0FBSSxDQUFDLFlBQVksR0FBRyxLQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQztvQkFFbEQsR0FBRyxDQUFDLE9BQU87Ozs7b0JBQUMsVUFBQSxFQUFFO3dCQUNWLEtBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLDRCQUEwQixJQUFJLFNBQUksT0FBTyxnQkFBVyxFQUFFLENBQUMsT0FBTyxDQUFHLEVBQUUsSUFBSSxZQUFZLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUE7b0JBQzVILENBQUMsRUFBQyxDQUFDO29CQUNILEtBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO2dCQUN6QixDQUFDLEVBQUMsQ0FBQztnQkFDSCxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztnQkFDdEIsSUFBSSxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUM7Ozs7S0FDbkI7Ozs7O0lBRUssMkNBQWtCOzs7O0lBQXhCLFVBQXlCLE9BQXFEO1FBQXJELHdCQUFBLEVBQUEsWUFBWSxJQUFJLEVBQUUsSUFBSSxDQUFDLE9BQU8sRUFBRSxHQUFHLEVBQUUsRUFBRSxFQUFFLE1BQU0sRUFBRSxFQUFFLEVBQUU7Ozs7OzRCQUUxRSxxQkFBTSxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksRUFBRSxFQUFBOzt3QkFBN0IsU0FBNkIsQ0FBQzt3QkFDOUIsSUFBSSxPQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7NEJBQ3hCLHNCQUFPLE9BQU8sQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDLEVBQUE7eUJBQzdCO3dCQUNLLFNBQVMsR0FBRyxJQUFJLGVBQWUsRUFBRTt3QkFDakMsT0FBTyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsaUJBQWlCLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQzt3QkFDaEUsT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPOzs7O3dCQUFDLFVBQUEsRUFBRSxJQUFJLE9BQUEsU0FBUyxDQUFDLE1BQU0sQ0FBSSxPQUFPLFlBQVMsRUFBRSxFQUFFLENBQUMsRUFBekMsQ0FBeUMsRUFBQyxDQUFDO3dCQUNyRSxJQUFJLE9BQU8sQ0FBQyxNQUFNOzRCQUFFLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLE9BQU87Ozs7NEJBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxTQUFTLENBQUMsTUFBTSxDQUFDLEdBQUcsRUFBRSxPQUFPLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQTFDLENBQTBDLEVBQUMsQ0FBQzt3QkFDckcsR0FBRyxHQUFHLDRCQUEwQixPQUFPLENBQUMsSUFBSSxTQUFJLFNBQVMsQ0FBQyxRQUFRLEVBQUk7d0JBQ25FLHFCQUFNLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUk7Ozs7NEJBQUMsVUFBQyxHQUFHLElBQUssT0FBQSxHQUFHLENBQUMsSUFBSSxFQUFFLEVBQVYsQ0FBVSxFQUFDLEVBQUE7O3dCQUE1RCxNQUFNLEdBQUcsU0FBbUQsQ0FBQzt3QkFDN0Qsc0JBQU8sTUFBTSxFQUFBOzs7O0tBQ2hCOzs7OztJQUVLLHNDQUFhOzs7O0lBQW5CLFVBQW9CLE1BQU07Ozs7OzRCQUNWLHFCQUFNLE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLEVBQUUsTUFBTSxRQUFBLEVBQUUsQ0FBQyxFQUFBOzt3QkFBeEMsR0FBRyxHQUFHLFNBQWtDO3dCQUM5QyxzQkFBTyxHQUFHLENBQUMsR0FBRyxFQUFFLE1BQU0sRUFBRSxFQUFFLENBQUMsRUFBQzs7OztLQUMvQjtJQUNMLHFCQUFDO0FBQUQsQ0FBQyxBQTFIRCxJQTBIQzs7OztJQXpIRywrQkFBTTs7SUFDTixrQ0FBUzs7SUFDVCxpQ0FBUTs7SUFDUixzQ0FBYTs7SUFDYiwrQkFBTTs7SUFDTixtQ0FBVTs7SUFDVixxQ0FBWTs7SUFDWixxQ0FBa0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgYXhpb3MgZnJvbSAnYXhpb3MnXG5pbXBvcnQgeyBDYWNoZSB9IGZyb20gJy4uL2NhY2hlL0NhY2hlJztcbmltcG9ydCB7IE5PREVfVFlQRSBhcyBUWVBFIH0gZnJvbSAnLi4vZG9tYWluL1RZUEVTJztcbmltcG9ydCB7IGhlYWQsIGdldCwgZGVib3VuY2UsIGFzc2lnbkluLCBmaW5kLCBpc0VxdWFsIH0gZnJvbSAnbG9kYXNoJztcbmltcG9ydCB7IENhY2hlRWxlbWVudCB9IGZyb20gJy4uL2NhY2hlL0NhY2hlRWxlbWVudCc7XG5pbXBvcnQgeyBNZXRhIH0gZnJvbSAnLi4vbWV0YS9tZXRhU2VydmljZSc7XG5cbmNvbnN0IHNlYXJjaCA9IGF4aW9zLmNyZWF0ZSh7XG4gICAgYmFzZVVSTDogJy9ua2JyZWxhdGlvbi9hcGkvbm9kZXMnXG59KTtcblxuXG4vL29sZCBidXQgZmFzdCBhcGkgLCBtYXliZSBhZnRlciBfcmVsYXRpb25zPWZhbHNlIG5ldyBpcyB0aGUgc2FtZSBvbiBzcGVlZCAtIFRPRE8gY2hlY2sgc3BlZW4gYW5kIGlmIG5lZXIgaXMgZmFzdGVyIC0gbW92ZSB0byBpdFxuY29uc3QgY29tcGFueSA9IGF4aW9zLmNyZWF0ZSh7XG4gICAgYmFzZVVSTDogJy9ua2JyZWxhdGlvbi9hcGkvY29tcGFueSdcbn0pO1xuXG5leHBvcnQgY2xhc3MgU2VhcmNoUmVzb3VyY2Uge1xuICAgIHF1ZXVlO1xuICAgIGRlYm91bmNlO1xuICAgIHBlbmRpbmc7XG4gICAgcXVldWVSZXN1bHRzO1xuICAgIGNhY2hlO1xuICAgIHF1ZXVlVHlwZTtcbiAgICBxdWV1ZVBhcmFtcztcbiAgICBtZXRhU2VydmljZTogTWV0YTtcbiAgICBjb25zdHJ1Y3RvcihtZXRhU2VydmljZSkge1xuICAgICAgICBPYmplY3QuYXNzaWduKHRoaXMsIHtcbiAgICAgICAgICAgIGNhY2hlOiBuZXcgQ2FjaGUoXG4gICAgICAgICAgICAgICAgLy8ge2tleUZ1bmN0aW9uOiAodHlwZSwgaWRGaWVsZCwgaWQpID0+IGAvbmticmVsYXRpb24vYXBpL25vZGVzLyR7dHlwZX0/JHtpZEZpZWxkfS5lcXVhbHM9JHtpZH1gfVxuICAgICAgICAgICAgKSxcbiAgICAgICAgICAgIHF1ZXVlOiBbXSxcbiAgICAgICAgICAgIHF1ZXVlVHlwZTogbnVsbCxcbiAgICAgICAgICAgIHF1ZXVlUmVzdWx0czogW10sXG4gICAgICAgICAgICBxdWV1ZVBhcmFtczoge30sXG4gICAgICAgICAgICBwZWRuaW5nOiBmYWxzZSxcbiAgICAgICAgICAgIGRlYm91bmNlOiBkZWJvdW5jZSh0aGlzLnNlYXJjaEJ5VHlwZUFuZElkc0Zyb21RdWV1ZSwgNTAwLCB7IG1heFdhaXQ6IDEwMDAgfSksXG4gICAgICAgICAgICBtZXRhU2VydmljZTogbWV0YVNlcnZpY2VcbiAgICAgICAgfSk7XG5cbiAgICAgICAgbWV0YVNlcnZpY2UuaW5pdCgpO1xuICAgIH1cblxuICAgIHVwZGF0ZVF1ZXVlU2VhcmNoQnlUeXBlQW5kSWRzKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5xdWV1ZS5sZW5ndGggPCAxMCA/IHRoaXMuZGVib3VuY2UoKSA6IHRoaXMuc2VhcmNoQnlUeXBlQW5kSWRzRnJvbVF1ZXVlKCk7XG4gICAgfVxuXG4gICAgYXN5bmMgc2VhcmNoUmVxdWVzdChvcHRpb25zKSB7XG4gICAgICAgIGNvbnN0IHBhcmFtcyA9IGFzc2lnbkluKHt9LFxuICAgICAgICAgICAgb3B0aW9ucy5maWx0ZXIsXG4gICAgICAgICAgICBvcHRpb25zLnBhZ2VDb25maWcsXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgcTogb3B0aW9ucy5xLFxuICAgICAgICAgICAgICAgIGV4Y2x1ZGU6ICdfcmVsYXRpb25zJ1xuICAgICAgICAgICAgfVxuICAgICAgICApO1xuICAgICAgICBjb25zdCByZXMgPSBhd2FpdCBzZWFyY2guZ2V0KGAvJHtvcHRpb25zLm5vZGVUeXBlfWAsIHsgcGFyYW1zIH0pO1xuICAgICAgICByZXR1cm4gZ2V0KHJlcywgJ2RhdGEnLCBbXSk7XG4gICAgfVxuXG4gICAgYXN5bmMgc2VhcmNoQnlUeXBlQW5kSWQob3B0aW9ucykge1xuICAgICAgICBpZiAoIXRoaXMubWV0YVNlcnZpY2UuaW5pdGVkKSBhd2FpdCB0aGlzLm1ldGFTZXJ2aWNlLmluaXQoKTtcbiAgICAgICAgY29uc3QgaWRGaWVsZCA9IHRoaXMubWV0YVNlcnZpY2UuZ2V0SWRGaWVsZEZvclR5cGUob3B0aW9ucy50eXBlKTtcbiAgICAgICAgcmV0dXJuIHNlYXJjaC5nZXQoYCR7b3B0aW9ucy50eXBlfT8ke2lkRmllbGR9LmVxdWFscz0ke29wdGlvbnMuaWR9JmV4Y2x1ZGU9X3JlbGF0aW9uc2AsIHsgcGFyYW1zOiBvcHRpb25zIH0pLnRoZW4ocmVzID0+IGhlYWQocmVzLmRhdGEubGlzdCkpO1xuICAgIH1cblxuICAgIGFzeW5jIHNlYXJjaEJ5VHlwZUFuZElkUXVldWVkKG9wdGlvbnMgPSB7IHR5cGU6IFRZUEUuQ09NUEFOWSwgaWQ6IG51bGwsIHBhcmFtczoge30gfSkge1xuICAgICAgICByZXR1cm4gbmV3IFByb21pc2UoYXN5bmMgKHJlc29sdmUpID0+IHtcbiAgICAgICAgICAgIGNvbnN0IGRvSW50ZXJ2YWwgPSAoKSA9PiB7XG4gICAgICAgICAgICAgICAgY29uc3QgaW50ZXJ2YWxJRCA9IHNldEludGVydmFsKCgpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKCF0aGlzLnBlbmRpbmcpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGxldCByZXMgPSBmaW5kKHRoaXMucXVldWVSZXN1bHRzLCAoY29tcGFueSkgPT4gaXNFcXVhbChgJHtjb21wYW55W2lkRmllbGRdfWAsIGAke29wdGlvbnMuaWR9YCkpXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAocmVzKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY2xlYXJJbnRlcnZhbChpbnRlcnZhbElEKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXNvbHZlKHJlcyk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9LCAxMDApO1xuICAgICAgICAgICAgfTtcblxuICAgICAgICAgICAgY29uc3QgaWRGaWVsZCA9IHRoaXMubWV0YVNlcnZpY2UuZ2V0SWRGaWVsZEZvclR5cGUob3B0aW9ucy50eXBlKTtcbiAgICAgICAgICAgIGNvbnN0IGluQ2FjaGUgPSB0aGlzLmNhY2hlLmdldChgL25rYnJlbGF0aW9uL2FwaS9ub2Rlcy8ke29wdGlvbnMudHlwZX0/JHtpZEZpZWxkfS5lcXVhbHM9JHtvcHRpb25zLmlkfWApO1xuICAgICAgICAgICAgaWYgKGluQ2FjaGUpIHtcbiAgICAgICAgICAgICAgICByZXNvbHZlKGluQ2FjaGUuaGVhZCgpKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgY29uc3QgaXNJbnF1ZXVlID0gdGhpcy5xdWV1ZS5pbmNsdWRlcyhvcHRpb25zLmlkKTtcbiAgICAgICAgICAgIGlmICghaXNJbnF1ZXVlKSB7XG4gICAgICAgICAgICAgICAgaWYgKHRoaXMucXVldWVUeXBlID09PSBvcHRpb25zLnR5cGUgfHwgdGhpcy5xdWV1ZVR5cGUgPT09IG51bGwpIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5xdWV1ZVR5cGUgPSBvcHRpb25zLnR5cGU7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMucXVldWVQYXJhbXMgPSBvcHRpb25zLnBhcmFtcztcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5xdWV1ZS5wdXNoKG9wdGlvbnMuaWQpO1xuICAgICAgICAgICAgICAgICAgICB0aGlzLnVwZGF0ZVF1ZXVlU2VhcmNoQnlUeXBlQW5kSWRzKCk7XG4gICAgICAgICAgICAgICAgICAgIGRvSW50ZXJ2YWwoKTtcbiAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKHRoaXMucXVldWVUeXBlICE9PSBvcHRpb25zLnR5cGUpIHtcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgcmVzID0gYXdhaXQgdGhpcy5jYWNoZS50cnkoYC9ua2JyZWxhdGlvbi9hcGkvbm9kZXMvJHtvcHRpb25zLnR5cGV9PyR7aWRGaWVsZH0uZXF1YWxzPSR7b3B0aW9ucy5pZH1gKS50aGVuKHIgPT4gaGVhZChyKSk7XG4gICAgICAgICAgICAgICAgICAgIHJlc29sdmUocmVzKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIGRvSW50ZXJ2YWwoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSlcbiAgICB9XG5cbiAgICBhc3luYyBzZWFyY2hCeVR5cGVBbmRJZHNGcm9tUXVldWUoKSB7XG4gICAgICAgIHRoaXMucGVuZGluZyA9IHRydWU7XG4gICAgICAgIGNvbnN0IHR5cGUgPSB0aGlzLnF1ZXVlVHlwZTtcbiAgICAgICAgY29uc3QgaWRGaWVsZCA9IHRoaXMubWV0YVNlcnZpY2UuZ2V0SWRGaWVsZEZvclR5cGUodHlwZSk7XG4gICAgICAgIHRoaXMuc2VhcmNoQnlUeXBlQW5kSWRzKHsgdHlwZSwgaWRzOiB0aGlzLnF1ZXVlLCBwYXJhbXM6IHRoaXMucXVldWVQYXJhbXMgfSkudGhlbihyZXMgPT4ge1xuICAgICAgICAgICAgdGhpcy5xdWV1ZVJlc3VsdHMgPSB0aGlzLnF1ZXVlUmVzdWx0cy5jb25jYXQocmVzKTtcblxuICAgICAgICAgICAgcmVzLmZvckVhY2goZWwgPT4ge1xuICAgICAgICAgICAgICAgIHRoaXMuY2FjaGUuc2V0KGAvbmticmVsYXRpb24vYXBpL25vZGVzLyR7dHlwZX0/JHtpZEZpZWxkfS5lcXVhbHM9JHtlbFtpZEZpZWxkXX1gLCBuZXcgQ2FjaGVFbGVtZW50KFByb21pc2UucmVzb2x2ZShlbCkpKVxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB0aGlzLnBlbmRpbmcgPSBmYWxzZTtcbiAgICAgICAgfSk7XG4gICAgICAgIHRoaXMucXVldWVUeXBlID0gbnVsbDtcbiAgICAgICAgdGhpcy5xdWV1ZSA9IFtdO1xuICAgIH1cblxuICAgIGFzeW5jIHNlYXJjaEJ5VHlwZUFuZElkcyhvcHRpb25zID0geyB0eXBlOiBUWVBFLkNPTVBBTlksIGlkczogW10sIHBhcmFtczoge30gfSkge1xuICAgICAgICBsZXQgcmVzdWx0O1xuICAgICAgICBhd2FpdCB0aGlzLm1ldGFTZXJ2aWNlLmluaXQoKTtcbiAgICAgICAgaWYgKG9wdGlvbnMuaWRzLmxlbmd0aCA8IDEpIHtcbiAgICAgICAgICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUoe30pXG4gICAgICAgIH1cbiAgICAgICAgY29uc3QgdXJscGFyYW1zID0gbmV3IFVSTFNlYXJjaFBhcmFtcygpO1xuICAgICAgICBjb25zdCBpZEZpZWxkID0gdGhpcy5tZXRhU2VydmljZS5nZXRJZEZpZWxkRm9yVHlwZShvcHRpb25zLnR5cGUpO1xuICAgICAgICBvcHRpb25zLmlkcy5mb3JFYWNoKGlkID0+IHVybHBhcmFtcy5hcHBlbmQoYCR7aWRGaWVsZH0uZXF1YWxzYCwgaWQpKTtcbiAgICAgICAgaWYgKG9wdGlvbnMucGFyYW1zKSBPYmplY3Qua2V5cyhvcHRpb25zLnBhcmFtcykuZm9yRWFjaChrZXkgPT4gdXJscGFyYW1zLmFwcGVuZChrZXksIG9wdGlvbnMucGFyYW1zW2tleV0pKTtcbiAgICAgICAgY29uc3QgdXJsID0gYC9ua2JyZWxhdGlvbi9hcGkvbm9kZXMvJHtvcHRpb25zLnR5cGV9PyR7dXJscGFyYW1zLnRvU3RyaW5nKCl9YDtcbiAgICAgICAgcmVzdWx0ID0gYXdhaXQgdGhpcy5jYWNoZS50cnkodXJsKS50aGVuKChyZXMpID0+IHJlcy5saXN0KCkpO1xuICAgICAgICByZXR1cm4gcmVzdWx0XG4gICAgfVxuXG4gICAgYXN5bmMgc2VhcmNoQ29tcGFueShwYXJhbXMpIHtcbiAgICAgICAgY29uc3QgcmVzID0gYXdhaXQgY29tcGFueS5nZXQoJy8nLCB7IHBhcmFtcyB9KTtcbiAgICAgICAgcmV0dXJuIGdldChyZXMsICdkYXRhJywgW10pO1xuICAgIH1cbn0iXX0=