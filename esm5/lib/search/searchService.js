/**
 * @fileoverview added by tsickle
 * Generated from: lib/search/searchService.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/**
 * Created by radigost on 07.09.17.
 */
import * as _ from 'lodash';
import SearchType from '../domain/SearchType';
import { metaService } from '../meta/metaService';
import { searchResource } from './searchResource';
import { rsearchMeta } from '../meta/rsearchMeta';
var SearchService = /** @class */ (function () {
    function SearchService(metaService) {
        this.metaService = metaService;
    }
    /**
     * @return {?}
     */
    SearchService.prototype.init = /**
     * @return {?}
     */
    function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var _a;
            return tslib_1.__generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.metaService.init()];
                    case 1:
                        _b.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        _a = _b.sent();
                        console.error("cannot init SearchService");
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * @return {?}
     */
    SearchService.prototype.getSearchableNodeTypes = /**
     * @return {?}
     */
    function () {
        /** @type {?} */
        var searchableNodeTypes = {};
        // TODO rewrite to make rsearchMeta, metaService one service
        /** @type {?} */
        var nodeTypes = rsearchMeta.nodeTypes;
        _.forEach(nodeTypes, (/**
         * @param {?} data
         * @param {?} nodeType
         * @return {?}
         */
        function (data, nodeType) {
            // TODO if nodeType is serachABLE
            if (data.search) {
                searchableNodeTypes[nodeType] = SearchType(nodeType, data);
            }
        }));
        return searchableNodeTypes;
    };
    /**
     * @param {?} searchType
     * @param {?} query
     * @return {?}
     */
    SearchService.prototype.searchByTypeAndText = /**
     * @param {?} searchType
     * @param {?} query
     * @return {?}
     */
    function (searchType, query) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var filter, activeRegion, filterRegion;
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                filter = {};
                if (searchType && searchType.filter) {
                    activeRegion = _.find(searchType.filter.regions, { active: true });
                    if (activeRegion && activeRegion.regionCode) {
                        filterRegion = { 'region_code.equals': activeRegion.regionCode };
                        _.assignIn(filter, filterRegion);
                    }
                }
                return [2 /*return*/, new Promise((/**
                     * @param {?} resolve
                     * @return {?}
                     */
                    function (resolve) { return tslib_1.__awaiter(_this, void 0, void 0, function () {
                        var _a, err_1;
                        return tslib_1.__generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _b.trys.push([0, 2, , 3]);
                                    _a = searchType;
                                    return [4 /*yield*/, searchResource.searchRequest({
                                            q: query,
                                            nodeType: searchType.nodeType,
                                            pageConfig: searchType.pageConfig,
                                            filter: filter,
                                        })];
                                case 1:
                                    _a.result = _b.sent();
                                    resolve(searchType);
                                    return [3 /*break*/, 3];
                                case 2:
                                    err_1 = _b.sent();
                                    console.log(err_1);
                                    resolve({ result: null });
                                    return [3 /*break*/, 3];
                                case 3: return [2 /*return*/];
                            }
                        });
                    }); }))];
            });
        });
    };
    /**
     * @param {?} options
     * @return {?}
     */
    SearchService.prototype.searchByTypeAndId = /**
     * @param {?} options
     * @return {?}
     */
    function (options) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var params, err_2;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        params = {
                            id: options.id,
                            type: options.searchtype || options.type,
                        };
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, searchResource.searchByTypeAndId(params)];
                    case 2: return [2 /*return*/, _a.sent()];
                    case 3:
                        err_2 = _a.sent();
                        console.error(err_2);
                        return [2 /*return*/, {}];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    return SearchService;
}());
if (false) {
    /** @type {?} */
    SearchService.prototype.metaService;
}
/** @type {?} */
var searchService = new SearchService(metaService);
searchService.init();
export { searchService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VhcmNoU2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bua2IvYW5ndWxhci1ua2ItbGliLyIsInNvdXJjZXMiOlsibGliL3NlYXJjaC9zZWFyY2hTZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7OztBQUdBLE9BQU8sS0FBSyxDQUFDLE1BQU0sUUFBUSxDQUFDO0FBQzVCLE9BQU8sVUFBVSxNQUFNLHNCQUFzQixDQUFDO0FBQzlDLE9BQU8sRUFBRSxXQUFXLEVBQVEsTUFBTSxxQkFBcUIsQ0FBQTtBQUN2RCxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sa0JBQWtCLENBQUM7QUFDbEQsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBRWxEO0lBRUksdUJBQVksV0FBVztRQUNuQixJQUFJLENBQUMsV0FBVyxHQUFHLFdBQVcsQ0FBQztJQUNuQyxDQUFDOzs7O0lBQ0ssNEJBQUk7OztJQUFWOzs7Ozs7O3dCQUVRLHFCQUFNLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxFQUFFLEVBQUE7O3dCQUE3QixTQUE2QixDQUFBOzs7O3dCQUc3QixPQUFPLENBQUMsS0FBSyxDQUFDLDJCQUEyQixDQUFDLENBQUM7Ozs7OztLQUdsRDs7OztJQUNELDhDQUFzQjs7O0lBQXRCOztZQUNVLG1CQUFtQixHQUFPLEVBQUU7OztZQUU1QixTQUFTLEdBQUcsV0FBVyxDQUFDLFNBQVM7UUFDdkMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxTQUFTOzs7OztRQUFFLFVBQUMsSUFBSSxFQUFFLFFBQVE7WUFDaEMsaUNBQWlDO1lBQ2pDLElBQUksSUFBSSxDQUFDLE1BQU0sRUFBRTtnQkFDYixtQkFBbUIsQ0FBQyxRQUFRLENBQUMsR0FBRyxVQUFVLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxDQUFDO2FBQzlEO1FBQ0wsQ0FBQyxFQUFDLENBQUM7UUFDSCxPQUFPLG1CQUFtQixDQUFDO0lBQy9CLENBQUM7Ozs7OztJQUVLLDJDQUFtQjs7Ozs7SUFBekIsVUFBMEIsVUFBVSxFQUFFLEtBQUs7Ozs7O2dCQUNqQyxNQUFNLEdBQUcsRUFBRTtnQkFFakIsSUFBSSxVQUFVLElBQUksVUFBVSxDQUFDLE1BQU0sRUFBRTtvQkFDM0IsWUFBWSxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEVBQUUsRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFFLENBQUM7b0JBQ3hFLElBQUksWUFBWSxJQUFJLFlBQVksQ0FBQyxVQUFVLEVBQUU7d0JBQ25DLFlBQVksR0FBRyxFQUFFLG9CQUFvQixFQUFFLFlBQVksQ0FBQyxVQUFVLEVBQUU7d0JBQ3RFLENBQUMsQ0FBQyxRQUFRLENBQUMsTUFBTSxFQUFFLFlBQVksQ0FBQyxDQUFDO3FCQUNwQztpQkFDSjtnQkFFRCxzQkFBTyxJQUFJLE9BQU87Ozs7b0JBQUMsVUFBTyxPQUFPOzs7Ozs7b0NBRXpCLEtBQUEsVUFBVSxDQUFBO29DQUFVLHFCQUFNLGNBQWMsQ0FBQyxhQUFhLENBQUM7NENBQ25ELENBQUMsRUFBRSxLQUFLOzRDQUNSLFFBQVEsRUFBRSxVQUFVLENBQUMsUUFBUTs0Q0FDN0IsVUFBVSxFQUFFLFVBQVUsQ0FBQyxVQUFVOzRDQUNqQyxNQUFNLFFBQUE7eUNBQ1QsQ0FBQyxFQUFBOztvQ0FMRixHQUFXLE1BQU0sR0FBRyxTQUtsQixDQUFDO29DQUNILE9BQU8sQ0FBQyxVQUFVLENBQUMsQ0FBQzs7OztvQ0FFcEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFHLENBQUMsQ0FBQztvQ0FDakIsT0FBTyxDQUFDLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7Ozs7O3lCQUVqQyxFQUFDLEVBQUM7OztLQUNOOzs7OztJQUVLLHlDQUFpQjs7OztJQUF2QixVQUF3QixPQUFPOzs7Ozs7d0JBQ3JCLE1BQU0sR0FBRzs0QkFDWCxFQUFFLEVBQUUsT0FBTyxDQUFDLEVBQUU7NEJBQ2QsSUFBSSxFQUFFLE9BQU8sQ0FBQyxVQUFVLElBQUksT0FBTyxDQUFDLElBQUk7eUJBQzNDOzs7O3dCQUVVLHFCQUFNLGNBQWMsQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLENBQUMsRUFBQTs0QkFBckQsc0JBQU8sU0FBOEMsRUFBQzs7O3dCQUV0RCxPQUFPLENBQUMsS0FBSyxDQUFDLEtBQUcsQ0FBQyxDQUFDO3dCQUNuQixzQkFBTyxFQUFFLEVBQUM7Ozs7O0tBR2pCO0lBQ0wsb0JBQUM7QUFBRCxDQUFDLEFBbkVELElBbUVDOzs7SUFsRUcsb0NBQWlCOzs7SUFvRWYsYUFBYSxHQUFHLElBQUksYUFBYSxDQUFDLFdBQVcsQ0FBQztBQUNwRCxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUE7QUFDcEIsT0FBTyxFQUFFLGFBQWEsRUFBRSxDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiLyoqXHJcbiAqIENyZWF0ZWQgYnkgcmFkaWdvc3Qgb24gMDcuMDkuMTcuXHJcbiAqL1xyXG5pbXBvcnQgKiBhcyBfIGZyb20gJ2xvZGFzaCc7XHJcbmltcG9ydCBTZWFyY2hUeXBlIGZyb20gJy4uL2RvbWFpbi9TZWFyY2hUeXBlJztcclxuaW1wb3J0IHsgbWV0YVNlcnZpY2UsIE1ldGEgfSBmcm9tICcuLi9tZXRhL21ldGFTZXJ2aWNlJ1xyXG5pbXBvcnQgeyBzZWFyY2hSZXNvdXJjZSB9IGZyb20gJy4vc2VhcmNoUmVzb3VyY2UnO1xyXG5pbXBvcnQgeyByc2VhcmNoTWV0YSB9IGZyb20gJy4uL21ldGEvcnNlYXJjaE1ldGEnO1xyXG5cclxuY2xhc3MgU2VhcmNoU2VydmljZSB7XHJcbiAgICBtZXRhU2VydmljZTogTWV0YVxyXG4gICAgY29uc3RydWN0b3IobWV0YVNlcnZpY2UpIHtcclxuICAgICAgICB0aGlzLm1ldGFTZXJ2aWNlID0gbWV0YVNlcnZpY2U7XHJcbiAgICB9XHJcbiAgICBhc3luYyBpbml0KCkge1xyXG4gICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgIGF3YWl0IHRoaXMubWV0YVNlcnZpY2UuaW5pdCgpXHJcbiAgICAgICAgfVxyXG4gICAgICAgIGNhdGNoe1xyXG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKFwiY2Fubm90IGluaXQgU2VhcmNoU2VydmljZVwiKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgfVxyXG4gICAgZ2V0U2VhcmNoYWJsZU5vZGVUeXBlcygpIHtcclxuICAgICAgICBjb25zdCBzZWFyY2hhYmxlTm9kZVR5cGVzOiB7fSA9IHt9O1xyXG4gICAgICAgIC8vIFRPRE8gcmV3cml0ZSB0byBtYWtlIHJzZWFyY2hNZXRhLCBtZXRhU2VydmljZSBvbmUgc2VydmljZVxyXG4gICAgICAgIGNvbnN0IG5vZGVUeXBlcyA9IHJzZWFyY2hNZXRhLm5vZGVUeXBlcztcclxuICAgICAgICBfLmZvckVhY2gobm9kZVR5cGVzLCAoZGF0YSwgbm9kZVR5cGUpID0+IHtcclxuICAgICAgICAgICAgLy8gVE9ETyBpZiBub2RlVHlwZSBpcyBzZXJhY2hBQkxFXHJcbiAgICAgICAgICAgIGlmIChkYXRhLnNlYXJjaCkge1xyXG4gICAgICAgICAgICAgICAgc2VhcmNoYWJsZU5vZGVUeXBlc1tub2RlVHlwZV0gPSBTZWFyY2hUeXBlKG5vZGVUeXBlLCBkYXRhKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHJldHVybiBzZWFyY2hhYmxlTm9kZVR5cGVzO1xyXG4gICAgfVxyXG5cclxuICAgIGFzeW5jIHNlYXJjaEJ5VHlwZUFuZFRleHQoc2VhcmNoVHlwZSwgcXVlcnkpIHtcclxuICAgICAgICBjb25zdCBmaWx0ZXIgPSB7fTtcclxuXHJcbiAgICAgICAgaWYgKHNlYXJjaFR5cGUgJiYgc2VhcmNoVHlwZS5maWx0ZXIpIHtcclxuICAgICAgICAgICAgY29uc3QgYWN0aXZlUmVnaW9uID0gXy5maW5kKHNlYXJjaFR5cGUuZmlsdGVyLnJlZ2lvbnMsIHsgYWN0aXZlOiB0cnVlIH0pO1xyXG4gICAgICAgICAgICBpZiAoYWN0aXZlUmVnaW9uICYmIGFjdGl2ZVJlZ2lvbi5yZWdpb25Db2RlKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBmaWx0ZXJSZWdpb24gPSB7ICdyZWdpb25fY29kZS5lcXVhbHMnOiBhY3RpdmVSZWdpb24ucmVnaW9uQ29kZSB9O1xyXG4gICAgICAgICAgICAgICAgXy5hc3NpZ25JbihmaWx0ZXIsIGZpbHRlclJlZ2lvbik7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiBuZXcgUHJvbWlzZShhc3luYyAocmVzb2x2ZSkgPT4ge1xyXG4gICAgICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICAgICAgc2VhcmNoVHlwZS5yZXN1bHQgPSBhd2FpdCBzZWFyY2hSZXNvdXJjZS5zZWFyY2hSZXF1ZXN0KHtcclxuICAgICAgICAgICAgICAgICAgICBxOiBxdWVyeSxcclxuICAgICAgICAgICAgICAgICAgICBub2RlVHlwZTogc2VhcmNoVHlwZS5ub2RlVHlwZSxcclxuICAgICAgICAgICAgICAgICAgICBwYWdlQ29uZmlnOiBzZWFyY2hUeXBlLnBhZ2VDb25maWcsXHJcbiAgICAgICAgICAgICAgICAgICAgZmlsdGVyLFxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICByZXNvbHZlKHNlYXJjaFR5cGUpO1xyXG4gICAgICAgICAgICB9IGNhdGNoIChlcnIpIHtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGVycik7XHJcbiAgICAgICAgICAgICAgICByZXNvbHZlKHsgcmVzdWx0OiBudWxsIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgYXN5bmMgc2VhcmNoQnlUeXBlQW5kSWQob3B0aW9ucykge1xyXG4gICAgICAgIGNvbnN0IHBhcmFtcyA9IHtcclxuICAgICAgICAgICAgaWQ6IG9wdGlvbnMuaWQsXHJcbiAgICAgICAgICAgIHR5cGU6IG9wdGlvbnMuc2VhcmNodHlwZSB8fCBvcHRpb25zLnR5cGUsXHJcbiAgICAgICAgfTtcclxuICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICByZXR1cm4gYXdhaXQgc2VhcmNoUmVzb3VyY2Uuc2VhcmNoQnlUeXBlQW5kSWQocGFyYW1zKTtcclxuICAgICAgICB9IGNhdGNoIChlcnIpIHtcclxuICAgICAgICAgICAgY29uc29sZS5lcnJvcihlcnIpO1xyXG4gICAgICAgICAgICByZXR1cm4ge307XHJcbiAgICAgICAgfVxyXG5cclxuICAgIH1cclxufVxyXG5cclxuY29uc3Qgc2VhcmNoU2VydmljZSA9IG5ldyBTZWFyY2hTZXJ2aWNlKG1ldGFTZXJ2aWNlKTtcclxuc2VhcmNoU2VydmljZS5pbml0KClcclxuZXhwb3J0IHsgc2VhcmNoU2VydmljZSB9O1xyXG4iXX0=