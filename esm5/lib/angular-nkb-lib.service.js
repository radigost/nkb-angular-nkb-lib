/**
 * @fileoverview added by tsickle
 * Generated from: lib/angular-nkb-lib.service.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
var AngularNkbLibService = /** @class */ (function () {
    function AngularNkbLibService() {
    }
    AngularNkbLibService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    AngularNkbLibService.ctorParameters = function () { return []; };
    /** @nocollapse */ AngularNkbLibService.ngInjectableDef = i0.defineInjectable({ factory: function AngularNkbLibService_Factory() { return new AngularNkbLibService(); }, token: AngularNkbLibService, providedIn: "root" });
    return AngularNkbLibService;
}());
export { AngularNkbLibService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYW5ndWxhci1ua2ItbGliLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AbmtiL2FuZ3VsYXItbmtiLWxpYi8iLCJzb3VyY2VzIjpbImxpYi9hbmd1bGFyLW5rYi1saWIuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7O0FBRTNDO0lBS0U7SUFBZ0IsQ0FBQzs7Z0JBTGxCLFVBQVUsU0FBQztvQkFDVixVQUFVLEVBQUUsTUFBTTtpQkFDbkI7Ozs7OytCQUpEO0NBUUMsQUFORCxJQU1DO1NBSFksb0JBQW9CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5ASW5qZWN0YWJsZSh7XG4gIHByb3ZpZGVkSW46ICdyb290J1xufSlcbmV4cG9ydCBjbGFzcyBBbmd1bGFyTmtiTGliU2VydmljZSB7XG5cbiAgY29uc3RydWN0b3IoKSB7IH1cbn1cbiJdfQ==