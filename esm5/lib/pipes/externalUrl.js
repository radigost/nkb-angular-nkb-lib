/**
 * @fileoverview added by tsickle
 * Generated from: lib/pipes/externalUrl.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Pipe } from '@angular/core';
var ExternalUrlPipe = /** @class */ (function () {
    function ExternalUrlPipe() {
    }
    /**
     * @param {?} url
     * @return {?}
     */
    ExternalUrlPipe.prototype.transform = /**
     * @param {?} url
     * @return {?}
     */
    function (url) {
        return '/siteapp/url/external' + '?url=' + encodeURIComponent(url);
    };
    ;
    ExternalUrlPipe.decorators = [
        { type: Pipe, args: [{ name: 'externalUrl' },] }
    ];
    return ExternalUrlPipe;
}());
export { ExternalUrlPipe };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZXh0ZXJuYWxVcmwuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AbmtiL2FuZ3VsYXItbmtiLWxpYi8iLCJzb3VyY2VzIjpbImxpYi9waXBlcy9leHRlcm5hbFVybC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxJQUFJLEVBQWlCLE1BQU0sZUFBZSxDQUFDO0FBRXBEO0lBQUE7SUFNQSxDQUFDOzs7OztJQUpVLG1DQUFTOzs7O0lBQWhCLFVBQWlCLEdBQVU7UUFDdkIsT0FBTyx1QkFBdUIsR0FBRyxPQUFPLEdBQUcsa0JBQWtCLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDdkUsQ0FBQztJQUFBLENBQUM7O2dCQUpMLElBQUksU0FBQyxFQUFDLElBQUksRUFBRSxhQUFhLEVBQUM7O0lBTTNCLHNCQUFDO0NBQUEsQUFORCxJQU1DO1NBTFksZUFBZSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFBpcGUsIFBpcGVUcmFuc2Zvcm0gfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQFBpcGUoe25hbWU6ICdleHRlcm5hbFVybCd9KVxuZXhwb3J0IGNsYXNzIEV4dGVybmFsVXJsUGlwZSBpbXBsZW1lbnRzIFBpcGVUcmFuc2Zvcm0ge1xuICAgIHB1YmxpYyB0cmFuc2Zvcm0odXJsOnN0cmluZyk6IHN0cmluZyB7XG4gICAgICAgIHJldHVybiAnL3NpdGVhcHAvdXJsL2V4dGVybmFsJyArICc/dXJsPScgKyBlbmNvZGVVUklDb21wb25lbnQodXJsKTtcbiAgICB9O1xuICAgICAgXG59XG4iXX0=