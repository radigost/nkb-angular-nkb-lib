/**
 * @fileoverview added by tsickle
 * Generated from: lib/angular-nkb-lib.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
var AngularNkbLibComponent = /** @class */ (function () {
    function AngularNkbLibComponent() {
    }
    /**
     * @return {?}
     */
    AngularNkbLibComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        console.log(this);
    };
    AngularNkbLibComponent.decorators = [
        { type: Component, args: [{
                    selector: 'lib-angular-nkb-lib',
                    template: "\n    <p>\n      angular-nkb-lib works!\n    </p>\n  "
                }] }
    ];
    /** @nocollapse */
    AngularNkbLibComponent.ctorParameters = function () { return []; };
    return AngularNkbLibComponent;
}());
export { AngularNkbLibComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYW5ndWxhci1ua2ItbGliLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bua2IvYW5ndWxhci1ua2ItbGliLyIsInNvdXJjZXMiOlsibGliL2FuZ3VsYXItbmtiLWxpYi5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLE1BQU0sZUFBZSxDQUFDO0FBRWxEO0lBV0U7SUFBZ0IsQ0FBQzs7OztJQUVqQix5Q0FBUTs7O0lBQVI7UUFDRSxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFBO0lBQ25CLENBQUM7O2dCQWZGLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUscUJBQXFCO29CQUMvQixRQUFRLEVBQUUsdURBSVQ7aUJBRUY7Ozs7SUFTRCw2QkFBQztDQUFBLEFBakJELElBaUJDO1NBUlksc0JBQXNCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnbGliLWFuZ3VsYXItbmtiLWxpYicsXG4gIHRlbXBsYXRlOiBgXG4gICAgPHA+XG4gICAgICBhbmd1bGFyLW5rYi1saWIgd29ya3MhXG4gICAgPC9wPlxuICBgLFxuICBzdHlsZXM6IFtdXG59KVxuZXhwb3J0IGNsYXNzIEFuZ3VsYXJOa2JMaWJDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gIGNvbnN0cnVjdG9yKCkgeyB9XG5cbiAgbmdPbkluaXQoKSB7XG4gICAgY29uc29sZS5sb2codGhpcylcbiAgfVxuXG59XG4iXX0=