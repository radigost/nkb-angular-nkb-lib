/**
 * @fileoverview added by tsickle
 * Generated from: lib/autokad/autokadService.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/**
 * Created by radigost on 24.04.17.
 */
import { assignIn, isObject, isArray, isNumber, isNaN, size, isNull, isFunction, isEmpty, forEach, get } from 'lodash';
import { AutokadResource } from './autokadResource';
import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
import * as i1 from "./autokadResource";
/** @type {?} */
var Search = (/**
 * @return {?}
 */
function () {
    return {
        params: null,
        requestData: null,
        request: null,
        result: {
            // В формате kad.arbitr.ru
            TotalCount: null,
            PagesCount: null,
            Items: [],
            entries: []
        },
        noResult: true,
        error: null,
        watch: true,
        sources: undefined,
    };
});
var ɵ0 = Search;
/** @type {?} */
var DEFAULT_SEARCH_PARAMS = {
    side: 4
};
/** @type {?} */
var DEFAULT_SEARCH_REQUEST_DATA = {
    q: null,
    dateFrom: null,
    dateTo: null,
    page: 1,
    pageIndex: 1
};
var AutokadService = /** @class */ (function () {
    function AutokadService(autokadResourceService) {
        this.autokadResourceService = autokadResourceService;
        /** @type {?} */
        var node = null;
        /** @type {?} */
        var autokadService = {
            search: Search(),
            loading: false,
            pager: Pager(),
            initSearch: initSearch,
            doSearch: doSearch,
            buildSearchRequestData: buildSearchRequestData,
            getResultTotal: getResultTotal,
            getCaseCount: getCaseCount,
            hasError: hasError,
            isLoading: isLoading,
            isNoResult: isNoResult,
        };
        Object.assign(this, autokadService);
        /**
         * @param {?} newNode
         * @param {?} params
         * @return {?}
         */
        function initSearch(newNode, params) {
            node = newNode;
            autokadService.search.params = assignIn({}, DEFAULT_SEARCH_PARAMS, params, {
                source: 0
            });
        }
        /**
         * @return {?}
         */
        function resetSearchRequest() {
            if (autokadService.search.request && autokadService.search.request.abort) {
                autokadService.search.request.abort();
            }
            autokadService.search.requestData = getDefaultSearchRequestData();
        }
        /**
         * @return {?}
         */
        function doSearch() {
            var _this = this;
            autokadService.search.result = {
                TotalCount: null,
                PagesCount: null,
                Items: [],
                entries: []
            };
            resetSearchRequest();
            autokadService.search.requestData = autokadService.buildSearchRequestData(autokadService.search.params);
            return new Promise((/**
             * @param {?} resolve
             * @return {?}
             */
            function (resolve) {
                if (isBlank(autokadService.search.requestData.inn)) {
                    console.warn('search.requestData.inn is empty ');
                    resolve();
                }
                else {
                    searchRequestPromise.call(_this).then((/**
                     * @param {?} data
                     * @return {?}
                     */
                    function (data) {
                        autokadService.search.result = data;
                        autokadService.pager.reset(data);
                        hideLoading();
                        resolve();
                    }), (/**
                     * @param {?} data
                     * @return {?}
                     */
                    function (data) {
                        autokadService.search.result = data;
                        autokadService.pager.reset(data);
                        hideLoading();
                        resolve();
                    }));
                }
            }));
        }
        /**
         * @return {?}
         */
        function searchRequestPromise() {
            return new Promise((/**
             * @param {?} resolve
             * @param {?} reject
             * @return {?}
             */
            function (resolve, reject) {
                showLoading();
                autokadService.search.noResult = true;
                autokadService.search.request = autokadResourceService.search(autokadService.search.requestData).then(success, error);
                /**
                 * @param {?} data
                 * @return {?}
                 */
                function success(data) {
                    data = data.data;
                    autokadService.search.noResult = (data.total === 0);
                    autokadService.search.error = null;
                    if (!hasResultItems(data)) {
                        autokadService.search.error = true;
                        hideLoading();
                        reject(data);
                    }
                    hideLoading();
                    resolve(data);
                }
                /**
                 * @param {?} err
                 * @return {?}
                 */
                function error(err) {
                    autokadService.search.noResult = false;
                    autokadService.search.error = true;
                    hideLoading();
                    reject(err);
                }
            }));
        }
        /**
         * @param {?} result
         * @return {?}
         */
        function hasResultItems(result) {
            return isObject(result) && isArray(result['entries']);
        }
        /**
         * @param {?} value
         * @return {?}
         */
        function isBlank(value) {
            return isEmpty(value) && !isNumber(value) || isNaN(value);
        }
        /**
         * @return {?}
         */
        function hasError() {
            return autokadService.search.error;
        }
        /**
         * @param {?} searchParams
         * @return {?}
         */
        function buildSearchRequestData(searchParams) {
            /** @type {?} */
            var requestData = assignIn({}, DEFAULT_SEARCH_REQUEST_DATA);
            requestData['type'] = (searchParams['side'] === -1 ? 4 : searchParams['side']);
            requestData['inn'] = node['inn'];
            requestData['ogrn'] = node['ogrn'];
            return requestData;
        }
        /*
                    * case
                    *
                    */
        /**
         * @param {?} search
         * @param {?} success
         * @param {?} error
         * @param {?} complete
         * @return {?}
         */
        function getCaseCount(search, success, error, complete) {
            if (!isArray(autokadService.search.sources) || !size(autokadService.search.sources)) {
                console.warn('getCaseCount... error: search.sources is blank');
                errorCallback();
                return null;
            }
            /** @type {?} */
            var sourceIndex = 0;
            /** @type {?} */
            var sourceCount = size(autokadService.search.sources);
            /** @type {?} */
            var caseCountRequest = CaseCountRequest();
            /** @type {?} */
            var result = null;
            /** @type {?} */
            var source;
            /** @type {?} */
            var request;
            nextRequest();
            return caseCountRequest;
            /**
             * @return {?}
             */
            function nextRequest() {
                /** @type {?} */
                var error = false;
                /** @type {?} */
                var next = false;
                /** @type {?} */
                var q;
                source = autokadService.search.sources[sourceIndex++];
                q = source.value;
                if (isBlank(q)) {
                    request = null;
                    result = 0;
                    next = true;
                    caseCountRequest._setRequest(request);
                    check();
                }
                else {
                    request = autokadResourceService.kadSearch({
                        r: {
                            q: q
                        },
                        success: (/**
                         * @param {?} data
                         * @return {?}
                         */
                        function (data) {
                            result = getResultTotal(data['Result']);
                            if (isNull(result)) {
                                error = true;
                            }
                            else if (result === 0) {
                                next = true;
                            }
                        }),
                        error: (/**
                         * @return {?}
                         */
                        function () {
                            error = true;
                        })
                    });
                    caseCountRequest._setRequest(request);
                    request.completePromise.then((/**
                     * @return {?}
                     */
                    function () {
                        check();
                    }));
                }
                /**
                 * @return {?}
                 */
                function check() {
                    if (error) {
                        errorCallback();
                    }
                    else if (!next || sourceIndex === sourceCount) {
                        successCallback();
                    }
                    else if (next) {
                        nextRequest();
                    }
                }
            }
            /**
             * @return {?}
             */
            function successCallback() {
                if (isFunction(success)) {
                    success(result, sourceIndex - 1);
                }
                completeCallback();
            }
            /**
             * @return {?}
             */
            function errorCallback() {
                if (isFunction(error)) {
                    error();
                }
                completeCallback();
            }
            /**
             * @return {?}
             */
            function completeCallback() {
                if (isFunction(complete)) {
                    complete();
                }
            }
            /**
             * @param {?} value
             * @return {?}
             */
            function isBlank(value) {
                return isEmpty(value) && !isNumber(value) || isNaN(value);
            }
            /**
             * @return {?}
             */
            function CaseCountRequest() {
                /** @type {?} */
                var request;
                return {
                    _setRequest: (/**
                     * @param {?} r
                     * @return {?}
                     */
                    function (r) {
                        request = r;
                    }),
                    abort: (/**
                     * @return {?}
                     */
                    function () {
                        if (request) {
                            request.abort();
                        }
                    })
                };
            }
        }
        /**
         * @param {?} result
         * @return {?}
         */
        function getResultTotal(result) {
            /** @type {?} */
            var r = result && result['TotalCount'];
            if (!isNumber(r)) {
                r = null;
            }
            return r;
        }
        /**
         * @return {?}
         */
        function showLoading() {
            autokadService.loading = true;
        }
        /**
         * @return {?}
         */
        function hideLoading() {
            autokadService.loading = false;
        }
        /**
         * @return {?}
         */
        function getDefaultSearchRequestData() {
            return DEFAULT_SEARCH_REQUEST_DATA;
        }
        /**
         * @return {?}
         */
        function isLoading() {
            return autokadService.loading;
        }
        /**
         * @return {?}
         */
        function isNoResult() {
            return get(autokadService.search, 'result.status.itemsFound', 0) === 0;
        }
        //Pager
        /**
         * @return {?}
         * @this {*}
         */
        function Pager() {
            /** @type {?} */
            var internalDisabled = false;
            /** @type {?} */
            var noNextPage = false;
            return {
                reset: reset,
                nextPage: nextPage,
                isDisabled: isDisabled
            };
            /**
             * @return {?}
             */
            function isDisabled() {
                return internalDisabled || noNextPage;
            }
            /**
             * @param {?} result
             * @return {?}
             */
            function reset(result) {
                internalDisabled = false;
                noNextPage = noMore(false, result);
            }
            /**
             * @param {?} hasError
             * @param {?} result
             * @return {?}
             */
            function noMore(hasError, result) {
                return hasError || (autokadService.search.requestData.pageIndex * result.size >= result.total);
            }
            /**
             * @return {?}
             * @this {*}
             */
            function nextPage() {
                return tslib_1.__awaiter(this, void 0, void 0, function () {
                    return tslib_1.__generator(this, function (_a) {
                        return [2 /*return*/, new Promise((/**
                             * @param {?} resolve
                             * @return {?}
                             */
                            function (resolve) {
                                showLoading();
                                if (!isDisabled() && autokadService.search.requestData) {
                                    internalDisabled = true;
                                    autokadService.search.requestData.pageIndex++;
                                    searchRequestPromise().then((/**
                                     * @param {?} result
                                     * @return {?}
                                     */
                                    function (result) {
                                        /** @type {?} */
                                        var hasError = !hasResultItems(result);
                                        if (!hasError) {
                                            forEach(result.entries, (/**
                                             * @param {?} item
                                             * @return {?}
                                             */
                                            function (item) {
                                                autokadService.search.result.entries.push(item);
                                            }));
                                            internalDisabled = false;
                                            noNextPage = noMore(false, result);
                                        }
                                        else {
                                            internalDisabled = false;
                                            noNextPage = true;
                                        }
                                        hideLoading();
                                        resolve();
                                    }));
                                }
                                resolve();
                            }))];
                    });
                });
            }
        }
    }
    AutokadService.decorators = [
        { type: Injectable, args: [{ providedIn: 'root' },] }
    ];
    /** @nocollapse */
    AutokadService.ctorParameters = function () { return [
        { type: AutokadResource }
    ]; };
    /** @nocollapse */ AutokadService.ngInjectableDef = i0.defineInjectable({ factory: function AutokadService_Factory() { return new AutokadService(i0.inject(i1.AutokadResource)); }, token: AutokadService, providedIn: "root" });
    return AutokadService;
}());
export { AutokadService };
if (false) {
    /** @type {?} */
    AutokadService.prototype.search;
    /** @type {?} */
    AutokadService.prototype.loading;
    /** @type {?} */
    AutokadService.prototype.pager;
    /** @type {?} */
    AutokadService.prototype.initSearch;
    /** @type {?} */
    AutokadService.prototype.doSearch;
    /** @type {?} */
    AutokadService.prototype.buildSearchRequestData;
    /** @type {?} */
    AutokadService.prototype.getResultTotal;
    /** @type {?} */
    AutokadService.prototype.getCaseCount;
    /** @type {?} */
    AutokadService.prototype.hasError;
    /** @type {?} */
    AutokadService.prototype.isLoading;
    /** @type {?} */
    AutokadService.prototype.isNoResult;
    /**
     * @type {?}
     * @private
     */
    AutokadService.prototype.autokadResourceService;
}
export { ɵ0 };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0b2thZFNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AbmtiL2FuZ3VsYXItbmtiLWxpYi8iLCJzb3VyY2VzIjpbImxpYi9hdXRva2FkL2F1dG9rYWRTZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7OztBQUlBLE9BQU8sRUFBRSxRQUFRLEVBQUUsUUFBUSxFQUFFLE9BQU8sRUFBRSxRQUFRLEVBQUUsS0FBSyxFQUFFLElBQUksRUFBRSxNQUFNLEVBQUUsVUFBVSxFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsR0FBRyxFQUFFLE1BQU0sUUFBUSxDQUFDO0FBQ3ZILE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUNwRCxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDOzs7O0lBRXJDLE1BQU07OztBQUFHO0lBQ1gsT0FBTztRQUNILE1BQU0sRUFBRSxJQUFJO1FBQ1osV0FBVyxFQUFFLElBQUk7UUFDakIsT0FBTyxFQUFFLElBQUk7UUFDYixNQUFNLEVBQUU7O1lBQ0osVUFBVSxFQUFFLElBQUk7WUFDaEIsVUFBVSxFQUFFLElBQUk7WUFDaEIsS0FBSyxFQUFFLEVBQUU7WUFDVCxPQUFPLEVBQUUsRUFBRTtTQUNkO1FBQ0QsUUFBUSxFQUFFLElBQUk7UUFDZCxLQUFLLEVBQUUsSUFBSTtRQUNYLEtBQUssRUFBRSxJQUFJO1FBQ1gsT0FBTyxFQUFFLFNBQVM7S0FDckIsQ0FBQztBQUNOLENBQUMsQ0FBQTs7O0lBRUsscUJBQXFCLEdBQUc7SUFDMUIsSUFBSSxFQUFFLENBQUM7Q0FDVjs7SUFFSywyQkFBMkIsR0FBRztJQUNoQyxDQUFDLEVBQUUsSUFBSTtJQUNQLFFBQVEsRUFBRSxJQUFJO0lBQ2QsTUFBTSxFQUFFLElBQUk7SUFDWixJQUFJLEVBQUUsQ0FBQztJQUNQLFNBQVMsRUFBRSxDQUFDO0NBQ2Y7QUFFRDtJQWFJLHdCQUNZLHNCQUF1QztRQUF2QywyQkFBc0IsR0FBdEIsc0JBQXNCLENBQWlCOztZQUczQyxJQUFJLEdBQUcsSUFBSTs7WUFFVCxjQUFjLEdBQUc7WUFDbkIsTUFBTSxFQUFFLE1BQU0sRUFBRTtZQUNoQixPQUFPLEVBQUUsS0FBSztZQUNkLEtBQUssRUFBRSxLQUFLLEVBQUU7WUFFZCxVQUFVLFlBQUE7WUFDVixRQUFRLFVBQUE7WUFDUixzQkFBc0Isd0JBQUE7WUFDdEIsY0FBYyxnQkFBQTtZQUNkLFlBQVksY0FBQTtZQUNaLFFBQVEsVUFBQTtZQUNSLFNBQVMsV0FBQTtZQUNULFVBQVUsWUFBQTtTQUViO1FBQ0QsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUUsY0FBYyxDQUFDLENBQUM7Ozs7OztRQUVwQyxTQUFTLFVBQVUsQ0FBQyxPQUFPLEVBQUUsTUFBTTtZQUMvQixJQUFJLEdBQUcsT0FBTyxDQUFDO1lBQ2YsY0FBYyxDQUFDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsUUFBUSxDQUFDLEVBQUUsRUFBRSxxQkFBcUIsRUFBRSxNQUFNLEVBQUU7Z0JBQ3ZFLE1BQU0sRUFBRSxDQUFDO2FBQ1osQ0FBQyxDQUFDO1FBQ1AsQ0FBQzs7OztRQUVELFNBQVMsa0JBQWtCO1lBQ3ZCLElBQUksY0FBYyxDQUFDLE1BQU0sQ0FBQyxPQUFPLElBQUksY0FBYyxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFO2dCQUN0RSxjQUFjLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsQ0FBQzthQUN6QztZQUNELGNBQWMsQ0FBQyxNQUFNLENBQUMsV0FBVyxHQUFHLDJCQUEyQixFQUFFLENBQUM7UUFDdEUsQ0FBQzs7OztRQUVELFNBQVMsUUFBUTtZQUFqQixpQkE0QkM7WUEzQkcsY0FBYyxDQUFDLE1BQU0sQ0FBQyxNQUFNLEdBQUc7Z0JBQzNCLFVBQVUsRUFBRSxJQUFJO2dCQUNoQixVQUFVLEVBQUUsSUFBSTtnQkFDaEIsS0FBSyxFQUFFLEVBQUU7Z0JBQ1QsT0FBTyxFQUFFLEVBQUU7YUFDZCxDQUFDO1lBQ0Ysa0JBQWtCLEVBQUUsQ0FBQztZQUNyQixjQUFjLENBQUMsTUFBTSxDQUFDLFdBQVcsR0FBRyxjQUFjLENBQUMsc0JBQXNCLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUN4RyxPQUFPLElBQUksT0FBTzs7OztZQUFDLFVBQUMsT0FBTztnQkFDdkIsSUFBSSxPQUFPLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLEVBQUU7b0JBQ2hELE9BQU8sQ0FBQyxJQUFJLENBQUMsa0NBQWtDLENBQUMsQ0FBQztvQkFDakQsT0FBTyxFQUFFLENBQUM7aUJBQ2I7cUJBQU07b0JBQ0gsb0JBQW9CLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxDQUFDLElBQUk7Ozs7b0JBQUMsVUFBQyxJQUFJO3dCQUN0QyxjQUFjLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7d0JBQ3BDLGNBQWMsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO3dCQUNqQyxXQUFXLEVBQUUsQ0FBQzt3QkFDZCxPQUFPLEVBQUUsQ0FBQztvQkFFZCxDQUFDOzs7O29CQUFFLFVBQUMsSUFBSTt3QkFDSixjQUFjLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7d0JBQ3BDLGNBQWMsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO3dCQUNqQyxXQUFXLEVBQUUsQ0FBQzt3QkFDZCxPQUFPLEVBQUUsQ0FBQztvQkFDZCxDQUFDLEVBQUMsQ0FBQztpQkFDTjtZQUNMLENBQUMsRUFBQyxDQUFDO1FBQ1AsQ0FBQzs7OztRQUVELFNBQVMsb0JBQW9CO1lBQ3pCLE9BQU8sSUFBSSxPQUFPOzs7OztZQUFDLFVBQVUsT0FBTyxFQUFFLE1BQU07Z0JBQ3hDLFdBQVcsRUFBRSxDQUFDO2dCQUNkLGNBQWMsQ0FBQyxNQUFNLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztnQkFFdEMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxPQUFPLEdBQUcsc0JBQXNCLENBQUMsTUFBTSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxLQUFLLENBQUMsQ0FBQzs7Ozs7Z0JBRXRILFNBQVMsT0FBTyxDQUFDLElBQUk7b0JBQ2pCLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO29CQUNqQixjQUFjLENBQUMsTUFBTSxDQUFDLFFBQVEsR0FBRyxDQUFDLElBQUksQ0FBQyxLQUFLLEtBQUssQ0FBQyxDQUFDLENBQUM7b0JBQ3BELGNBQWMsQ0FBQyxNQUFNLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQztvQkFDbkMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsRUFBRTt3QkFDdkIsY0FBYyxDQUFDLE1BQU0sQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDO3dCQUNuQyxXQUFXLEVBQUUsQ0FBQzt3QkFDZCxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7cUJBQ2hCO29CQUNELFdBQVcsRUFBRSxDQUFDO29CQUNkLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDbEIsQ0FBQzs7Ozs7Z0JBRUQsU0FBUyxLQUFLLENBQUMsR0FBRztvQkFDZCxjQUFjLENBQUMsTUFBTSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7b0JBQ3ZDLGNBQWMsQ0FBQyxNQUFNLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQztvQkFDbkMsV0FBVyxFQUFFLENBQUM7b0JBQ2QsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUNoQixDQUFDO1lBRUwsQ0FBQyxFQUFDLENBQUM7UUFDUCxDQUFDOzs7OztRQUVELFNBQVMsY0FBYyxDQUFDLE1BQU07WUFDMUIsT0FBTyxRQUFRLENBQUMsTUFBTSxDQUFDLElBQUksT0FBTyxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO1FBQzFELENBQUM7Ozs7O1FBRUQsU0FBUyxPQUFPLENBQUMsS0FBSztZQUNsQixPQUFPLE9BQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsSUFBSSxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDOUQsQ0FBQzs7OztRQUVELFNBQVMsUUFBUTtZQUNiLE9BQU8sY0FBYyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUM7UUFFdkMsQ0FBQzs7Ozs7UUFFRCxTQUFTLHNCQUFzQixDQUFDLFlBQVk7O2dCQUNsQyxXQUFXLEdBQUcsUUFBUSxDQUFDLEVBQUUsRUFBRSwyQkFBMkIsQ0FBQztZQUM3RCxXQUFXLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7WUFDL0UsV0FBVyxDQUFDLEtBQUssQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUNqQyxXQUFXLENBQUMsTUFBTSxDQUFDLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQ25DLE9BQU8sV0FBVyxDQUFDO1FBQ3ZCLENBQUM7Ozs7Ozs7Ozs7OztRQU9ELFNBQVMsWUFBWSxDQUFDLE1BQU0sRUFBRSxPQUFPLEVBQUUsS0FBSyxFQUFFLFFBQVE7WUFDbEQsSUFBSSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLEVBQUU7Z0JBQ2pGLE9BQU8sQ0FBQyxJQUFJLENBQUMsZ0RBQWdELENBQUMsQ0FBQztnQkFDL0QsYUFBYSxFQUFFLENBQUM7Z0JBQ2hCLE9BQU8sSUFBSSxDQUFDO2FBQ2Y7O2dCQUVHLFdBQVcsR0FBRyxDQUFDOztnQkFDZixXQUFXLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDOztnQkFDakQsZ0JBQWdCLEdBQUcsZ0JBQWdCLEVBQUU7O2dCQUNyQyxNQUFNLEdBQUcsSUFBSTs7Z0JBQ2IsTUFBTTs7Z0JBQUUsT0FBTztZQUVuQixXQUFXLEVBQUUsQ0FBQztZQUNkLE9BQU8sZ0JBQWdCLENBQUM7Ozs7WUFFeEIsU0FBUyxXQUFXOztvQkFDWixLQUFLLEdBQUcsS0FBSzs7b0JBQ2IsSUFBSSxHQUFHLEtBQUs7O29CQUNaLENBQUM7Z0JBRUwsTUFBTSxHQUFHLGNBQWMsQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUM7Z0JBQ3RELENBQUMsR0FBRyxNQUFNLENBQUMsS0FBSyxDQUFDO2dCQUVqQixJQUFJLE9BQU8sQ0FBQyxDQUFDLENBQUMsRUFBRTtvQkFDWixPQUFPLEdBQUcsSUFBSSxDQUFDO29CQUNmLE1BQU0sR0FBRyxDQUFDLENBQUM7b0JBQ1gsSUFBSSxHQUFHLElBQUksQ0FBQztvQkFDWixnQkFBZ0IsQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLENBQUM7b0JBQ3RDLEtBQUssRUFBRSxDQUFDO2lCQUNYO3FCQUFNO29CQUNILE9BQU8sR0FBRyxzQkFBc0IsQ0FBQyxTQUFTLENBQUM7d0JBQ3ZDLENBQUMsRUFBRTs0QkFDQyxDQUFDLEVBQUUsQ0FBQzt5QkFDUDt3QkFDRCxPQUFPOzs7O3dCQUFFLFVBQVUsSUFBSTs0QkFDbkIsTUFBTSxHQUFHLGNBQWMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQzs0QkFFeEMsSUFBSSxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUU7Z0NBQ2hCLEtBQUssR0FBRyxJQUFJLENBQUM7NkJBQ2hCO2lDQUFNLElBQUksTUFBTSxLQUFLLENBQUMsRUFBRTtnQ0FDckIsSUFBSSxHQUFHLElBQUksQ0FBQzs2QkFDZjt3QkFDTCxDQUFDLENBQUE7d0JBQ0QsS0FBSzs7O3dCQUFFOzRCQUNILEtBQUssR0FBRyxJQUFJLENBQUM7d0JBQ2pCLENBQUMsQ0FBQTtxQkFDSixDQUFDLENBQUM7b0JBRUgsZ0JBQWdCLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxDQUFDO29CQUV0QyxPQUFPLENBQUMsZUFBZSxDQUFDLElBQUk7OztvQkFBQzt3QkFDekIsS0FBSyxFQUFFLENBQUM7b0JBQ1osQ0FBQyxFQUFDLENBQUM7aUJBQ047Ozs7Z0JBRUQsU0FBUyxLQUFLO29CQUNWLElBQUksS0FBSyxFQUFFO3dCQUNQLGFBQWEsRUFBRSxDQUFDO3FCQUNuQjt5QkFBTSxJQUFJLENBQUMsSUFBSSxJQUFJLFdBQVcsS0FBSyxXQUFXLEVBQUU7d0JBQzdDLGVBQWUsRUFBRSxDQUFDO3FCQUNyQjt5QkFBTSxJQUFJLElBQUksRUFBRTt3QkFDYixXQUFXLEVBQUUsQ0FBQztxQkFDakI7Z0JBQ0wsQ0FBQztZQUNMLENBQUM7Ozs7WUFFRCxTQUFTLGVBQWU7Z0JBQ3BCLElBQUksVUFBVSxDQUFDLE9BQU8sQ0FBQyxFQUFFO29CQUNyQixPQUFPLENBQUMsTUFBTSxFQUFFLFdBQVcsR0FBRyxDQUFDLENBQUMsQ0FBQztpQkFDcEM7Z0JBQ0QsZ0JBQWdCLEVBQUUsQ0FBQztZQUN2QixDQUFDOzs7O1lBRUQsU0FBUyxhQUFhO2dCQUNsQixJQUFJLFVBQVUsQ0FBQyxLQUFLLENBQUMsRUFBRTtvQkFDbkIsS0FBSyxFQUFFLENBQUM7aUJBQ1g7Z0JBQ0QsZ0JBQWdCLEVBQUUsQ0FBQztZQUN2QixDQUFDOzs7O1lBRUQsU0FBUyxnQkFBZ0I7Z0JBQ3JCLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO29CQUN0QixRQUFRLEVBQUUsQ0FBQztpQkFDZDtZQUNMLENBQUM7Ozs7O1lBRUQsU0FBUyxPQUFPLENBQUMsS0FBSztnQkFDbEIsT0FBTyxPQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLElBQUksS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzlELENBQUM7Ozs7WUFFRCxTQUFTLGdCQUFnQjs7b0JBQ2pCLE9BQU87Z0JBRVgsT0FBTztvQkFDSCxXQUFXOzs7O29CQUFFLFVBQVUsQ0FBQzt3QkFDcEIsT0FBTyxHQUFHLENBQUMsQ0FBQztvQkFDaEIsQ0FBQyxDQUFBO29CQUNELEtBQUs7OztvQkFBRTt3QkFDSCxJQUFJLE9BQU8sRUFBRTs0QkFDVCxPQUFPLENBQUMsS0FBSyxFQUFFLENBQUM7eUJBQ25CO29CQUNMLENBQUMsQ0FBQTtpQkFDSixDQUFDO1lBQ04sQ0FBQztRQUNMLENBQUM7Ozs7O1FBRUQsU0FBUyxjQUFjLENBQUMsTUFBTTs7Z0JBQ3RCLENBQUMsR0FBRyxNQUFNLElBQUksTUFBTSxDQUFDLFlBQVksQ0FBQztZQUV0QyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxFQUFFO2dCQUNkLENBQUMsR0FBRyxJQUFJLENBQUM7YUFDWjtZQUVELE9BQU8sQ0FBQyxDQUFDO1FBQ2IsQ0FBQzs7OztRQUdELFNBQVMsV0FBVztZQUNoQixjQUFjLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztRQUNsQyxDQUFDOzs7O1FBRUQsU0FBUyxXQUFXO1lBQ2hCLGNBQWMsQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO1FBQ25DLENBQUM7Ozs7UUFFRCxTQUFTLDJCQUEyQjtZQUNoQyxPQUFPLDJCQUEyQixDQUFDO1FBQ3ZDLENBQUM7Ozs7UUFFRCxTQUFTLFNBQVM7WUFDZCxPQUFPLGNBQWMsQ0FBQyxPQUFPLENBQUM7UUFDbEMsQ0FBQzs7OztRQUVELFNBQVMsVUFBVTtZQUNmLE9BQU8sR0FBRyxDQUFDLGNBQWMsQ0FBQyxNQUFNLEVBQUUsMEJBQTBCLEVBQUUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzNFLENBQUM7Ozs7OztRQUdELFNBQVMsS0FBSzs7Z0JBQ04sZ0JBQWdCLEdBQUcsS0FBSzs7Z0JBQ3hCLFVBQVUsR0FBRyxLQUFLO1lBQ3RCLE9BQU87Z0JBQ0gsS0FBSyxFQUFFLEtBQUs7Z0JBQ1osUUFBUSxFQUFFLFFBQVE7Z0JBQ2xCLFVBQVUsRUFBRSxVQUFVO2FBQ3pCLENBQUM7Ozs7WUFFRixTQUFTLFVBQVU7Z0JBQ2YsT0FBTyxnQkFBZ0IsSUFBSSxVQUFVLENBQUM7WUFDMUMsQ0FBQzs7Ozs7WUFFRCxTQUFTLEtBQUssQ0FBQyxNQUFNO2dCQUNqQixnQkFBZ0IsR0FBRyxLQUFLLENBQUM7Z0JBQ3pCLFVBQVUsR0FBRyxNQUFNLENBQUMsS0FBSyxFQUFFLE1BQU0sQ0FBQyxDQUFDO1lBQ3ZDLENBQUM7Ozs7OztZQUVELFNBQVMsTUFBTSxDQUFDLFFBQVEsRUFBRSxNQUFNO2dCQUM1QixPQUFPLFFBQVEsSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLFNBQVMsR0FBRyxNQUFNLENBQUMsSUFBSSxJQUFJLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUNuRyxDQUFDOzs7OztZQUdELFNBQWUsUUFBUTs7O3dCQUNuQixzQkFBTyxJQUFJLE9BQU87Ozs7NEJBQUMsVUFBQyxPQUFPO2dDQUN2QixXQUFXLEVBQUUsQ0FBQztnQ0FDZCxJQUFJLENBQUMsVUFBVSxFQUFFLElBQUksY0FBYyxDQUFDLE1BQU0sQ0FBQyxXQUFXLEVBQUU7b0NBQ3BELGdCQUFnQixHQUFHLElBQUksQ0FBQztvQ0FDeEIsY0FBYyxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsU0FBUyxFQUFFLENBQUM7b0NBRTlDLG9CQUFvQixFQUFFLENBQUMsSUFBSTs7OztvQ0FBQyxVQUFVLE1BQW1COzs0Q0FDL0MsUUFBUSxHQUFHLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQzt3Q0FDeEMsSUFBSSxDQUFDLFFBQVEsRUFBRTs0Q0FDWCxPQUFPLENBQUMsTUFBTSxDQUFDLE9BQU87Ozs7NENBQUUsVUFBVSxJQUFJO2dEQUNsQyxjQUFjLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDOzRDQUNwRCxDQUFDLEVBQUMsQ0FBQzs0Q0FDSCxnQkFBZ0IsR0FBRyxLQUFLLENBQUM7NENBQ3pCLFVBQVUsR0FBRyxNQUFNLENBQUMsS0FBSyxFQUFFLE1BQU0sQ0FBQyxDQUFDO3lDQUN0Qzs2Q0FBTTs0Q0FDSCxnQkFBZ0IsR0FBRyxLQUFLLENBQUM7NENBQ3pCLFVBQVUsR0FBRyxJQUFJLENBQUM7eUNBQ3JCO3dDQUNELFdBQVcsRUFBRSxDQUFDO3dDQUNkLE9BQU8sRUFBRSxDQUFDO29DQUNkLENBQUMsRUFBQyxDQUFDO2lDQUNOO2dDQUNELE9BQU8sRUFBRSxDQUFDOzRCQUNkLENBQUMsRUFBQyxFQUFBOzs7YUFDTDtRQUNMLENBQUM7SUFHTCxDQUFDOztnQkF2VUosVUFBVSxTQUFDLEVBQUUsVUFBVSxFQUFFLE1BQU0sRUFBRTs7OztnQkFqQ3pCLGVBQWU7Ozt5QkFMeEI7Q0ErV0MsQUF6VUQsSUF5VUM7U0F4VVksY0FBYzs7O0lBQ3ZCLGdDQUFPOztJQUNQLGlDQUFROztJQUNSLCtCQUFNOztJQUNOLG9DQUFXOztJQUNYLGtDQUFTOztJQUNULGdEQUF1Qjs7SUFDdkIsd0NBQWU7O0lBQ2Ysc0NBQWE7O0lBQ2Isa0NBQVM7O0lBQ1QsbUNBQVU7O0lBQ1Ysb0NBQVc7Ozs7O0lBRVAsZ0RBQStDIiwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBDcmVhdGVkIGJ5IHJhZGlnb3N0IG9uIDI0LjA0LjE3LlxuICovXG5cbmltcG9ydCB7IGFzc2lnbkluLCBpc09iamVjdCwgaXNBcnJheSwgaXNOdW1iZXIsIGlzTmFOLCBzaXplLCBpc051bGwsIGlzRnVuY3Rpb24sIGlzRW1wdHksIGZvckVhY2gsIGdldCB9IGZyb20gJ2xvZGFzaCc7XG5pbXBvcnQgeyBBdXRva2FkUmVzb3VyY2UgfSBmcm9tICcuL2F1dG9rYWRSZXNvdXJjZSc7XG5pbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbmNvbnN0IFNlYXJjaCA9ICgpID0+IHtcbiAgICByZXR1cm4ge1xuICAgICAgICBwYXJhbXM6IG51bGwsXG4gICAgICAgIHJlcXVlc3REYXRhOiBudWxsLFxuICAgICAgICByZXF1ZXN0OiBudWxsLFxuICAgICAgICByZXN1bHQ6IHsgLy8g0JIg0YTQvtGA0LzQsNGC0LUga2FkLmFyYml0ci5ydVxuICAgICAgICAgICAgVG90YWxDb3VudDogbnVsbCxcbiAgICAgICAgICAgIFBhZ2VzQ291bnQ6IG51bGwsXG4gICAgICAgICAgICBJdGVtczogW10sXG4gICAgICAgICAgICBlbnRyaWVzOiBbXVxuICAgICAgICB9LFxuICAgICAgICBub1Jlc3VsdDogdHJ1ZSxcbiAgICAgICAgZXJyb3I6IG51bGwsXG4gICAgICAgIHdhdGNoOiB0cnVlLFxuICAgICAgICBzb3VyY2VzOiB1bmRlZmluZWQsXG4gICAgfTtcbn07XG5cbmNvbnN0IERFRkFVTFRfU0VBUkNIX1BBUkFNUyA9IHtcbiAgICBzaWRlOiA0XG59O1xuXG5jb25zdCBERUZBVUxUX1NFQVJDSF9SRVFVRVNUX0RBVEEgPSB7XG4gICAgcTogbnVsbCxcbiAgICBkYXRlRnJvbTogbnVsbCxcbiAgICBkYXRlVG86IG51bGwsXG4gICAgcGFnZTogMSxcbiAgICBwYWdlSW5kZXg6IDFcbn07XG5cbkBJbmplY3RhYmxlKHsgcHJvdmlkZWRJbjogJ3Jvb3QnIH0pXG5leHBvcnQgY2xhc3MgQXV0b2thZFNlcnZpY2Uge1xuICAgIHNlYXJjaDtcbiAgICBsb2FkaW5nO1xuICAgIHBhZ2VyO1xuICAgIGluaXRTZWFyY2g7XG4gICAgZG9TZWFyY2g7XG4gICAgYnVpbGRTZWFyY2hSZXF1ZXN0RGF0YTtcbiAgICBnZXRSZXN1bHRUb3RhbDtcbiAgICBnZXRDYXNlQ291bnQ7XG4gICAgaGFzRXJyb3I7XG4gICAgaXNMb2FkaW5nO1xuICAgIGlzTm9SZXN1bHQ7XG4gICAgY29uc3RydWN0b3IoXG4gICAgICAgIHByaXZhdGUgYXV0b2thZFJlc291cmNlU2VydmljZTogQXV0b2thZFJlc291cmNlXG4gICAgKSB7XG5cbiAgICAgICAgbGV0IG5vZGUgPSBudWxsO1xuXG4gICAgICAgIGNvbnN0IGF1dG9rYWRTZXJ2aWNlID0ge1xuICAgICAgICAgICAgc2VhcmNoOiBTZWFyY2goKSxcbiAgICAgICAgICAgIGxvYWRpbmc6IGZhbHNlLFxuICAgICAgICAgICAgcGFnZXI6IFBhZ2VyKCksXG5cbiAgICAgICAgICAgIGluaXRTZWFyY2gsXG4gICAgICAgICAgICBkb1NlYXJjaCxcbiAgICAgICAgICAgIGJ1aWxkU2VhcmNoUmVxdWVzdERhdGEsXG4gICAgICAgICAgICBnZXRSZXN1bHRUb3RhbCxcbiAgICAgICAgICAgIGdldENhc2VDb3VudCxcbiAgICAgICAgICAgIGhhc0Vycm9yLFxuICAgICAgICAgICAgaXNMb2FkaW5nLFxuICAgICAgICAgICAgaXNOb1Jlc3VsdCxcblxuICAgICAgICB9O1xuICAgICAgICBPYmplY3QuYXNzaWduKHRoaXMsIGF1dG9rYWRTZXJ2aWNlKTtcblxuICAgICAgICBmdW5jdGlvbiBpbml0U2VhcmNoKG5ld05vZGUsIHBhcmFtcykge1xuICAgICAgICAgICAgbm9kZSA9IG5ld05vZGU7XG4gICAgICAgICAgICBhdXRva2FkU2VydmljZS5zZWFyY2gucGFyYW1zID0gYXNzaWduSW4oe30sIERFRkFVTFRfU0VBUkNIX1BBUkFNUywgcGFyYW1zLCB7XG4gICAgICAgICAgICAgICAgc291cmNlOiAwXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfVxuXG4gICAgICAgIGZ1bmN0aW9uIHJlc2V0U2VhcmNoUmVxdWVzdCgpIHtcbiAgICAgICAgICAgIGlmIChhdXRva2FkU2VydmljZS5zZWFyY2gucmVxdWVzdCAmJiBhdXRva2FkU2VydmljZS5zZWFyY2gucmVxdWVzdC5hYm9ydCkge1xuICAgICAgICAgICAgICAgIGF1dG9rYWRTZXJ2aWNlLnNlYXJjaC5yZXF1ZXN0LmFib3J0KCk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBhdXRva2FkU2VydmljZS5zZWFyY2gucmVxdWVzdERhdGEgPSBnZXREZWZhdWx0U2VhcmNoUmVxdWVzdERhdGEoKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGZ1bmN0aW9uIGRvU2VhcmNoKCkge1xuICAgICAgICAgICAgYXV0b2thZFNlcnZpY2Uuc2VhcmNoLnJlc3VsdCA9IHtcbiAgICAgICAgICAgICAgICBUb3RhbENvdW50OiBudWxsLFxuICAgICAgICAgICAgICAgIFBhZ2VzQ291bnQ6IG51bGwsXG4gICAgICAgICAgICAgICAgSXRlbXM6IFtdLFxuICAgICAgICAgICAgICAgIGVudHJpZXM6IFtdXG4gICAgICAgICAgICB9O1xuICAgICAgICAgICAgcmVzZXRTZWFyY2hSZXF1ZXN0KCk7XG4gICAgICAgICAgICBhdXRva2FkU2VydmljZS5zZWFyY2gucmVxdWVzdERhdGEgPSBhdXRva2FkU2VydmljZS5idWlsZFNlYXJjaFJlcXVlc3REYXRhKGF1dG9rYWRTZXJ2aWNlLnNlYXJjaC5wYXJhbXMpO1xuICAgICAgICAgICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlKSA9PiB7XG4gICAgICAgICAgICAgICAgaWYgKGlzQmxhbmsoYXV0b2thZFNlcnZpY2Uuc2VhcmNoLnJlcXVlc3REYXRhLmlubikpIHtcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS53YXJuKCdzZWFyY2gucmVxdWVzdERhdGEuaW5uIGlzIGVtcHR5ICcpO1xuICAgICAgICAgICAgICAgICAgICByZXNvbHZlKCk7XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgc2VhcmNoUmVxdWVzdFByb21pc2UuY2FsbCh0aGlzKS50aGVuKChkYXRhKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBhdXRva2FkU2VydmljZS5zZWFyY2gucmVzdWx0ID0gZGF0YTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGF1dG9rYWRTZXJ2aWNlLnBhZ2VyLnJlc2V0KGRhdGEpO1xuICAgICAgICAgICAgICAgICAgICAgICAgaGlkZUxvYWRpbmcoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc29sdmUoKTtcblxuICAgICAgICAgICAgICAgICAgICB9LCAoZGF0YSkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgYXV0b2thZFNlcnZpY2Uuc2VhcmNoLnJlc3VsdCA9IGRhdGE7XG4gICAgICAgICAgICAgICAgICAgICAgICBhdXRva2FkU2VydmljZS5wYWdlci5yZXNldChkYXRhKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGhpZGVMb2FkaW5nKCk7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXNvbHZlKCk7XG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9XG5cbiAgICAgICAgZnVuY3Rpb24gc2VhcmNoUmVxdWVzdFByb21pc2UoKSB7XG4gICAgICAgICAgICByZXR1cm4gbmV3IFByb21pc2UoZnVuY3Rpb24gKHJlc29sdmUsIHJlamVjdCkge1xuICAgICAgICAgICAgICAgIHNob3dMb2FkaW5nKCk7XG4gICAgICAgICAgICAgICAgYXV0b2thZFNlcnZpY2Uuc2VhcmNoLm5vUmVzdWx0ID0gdHJ1ZTtcblxuICAgICAgICAgICAgICAgIGF1dG9rYWRTZXJ2aWNlLnNlYXJjaC5yZXF1ZXN0ID0gYXV0b2thZFJlc291cmNlU2VydmljZS5zZWFyY2goYXV0b2thZFNlcnZpY2Uuc2VhcmNoLnJlcXVlc3REYXRhKS50aGVuKHN1Y2Nlc3MsIGVycm9yKTtcblxuICAgICAgICAgICAgICAgIGZ1bmN0aW9uIHN1Y2Nlc3MoZGF0YSkge1xuICAgICAgICAgICAgICAgICAgICBkYXRhID0gZGF0YS5kYXRhO1xuICAgICAgICAgICAgICAgICAgICBhdXRva2FkU2VydmljZS5zZWFyY2gubm9SZXN1bHQgPSAoZGF0YS50b3RhbCA9PT0gMCk7XG4gICAgICAgICAgICAgICAgICAgIGF1dG9rYWRTZXJ2aWNlLnNlYXJjaC5lcnJvciA9IG51bGw7XG4gICAgICAgICAgICAgICAgICAgIGlmICghaGFzUmVzdWx0SXRlbXMoZGF0YSkpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGF1dG9rYWRTZXJ2aWNlLnNlYXJjaC5lcnJvciA9IHRydWU7XG4gICAgICAgICAgICAgICAgICAgICAgICBoaWRlTG9hZGluZygpO1xuICAgICAgICAgICAgICAgICAgICAgICAgcmVqZWN0KGRhdGEpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIGhpZGVMb2FkaW5nKCk7XG4gICAgICAgICAgICAgICAgICAgIHJlc29sdmUoZGF0YSk7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgZnVuY3Rpb24gZXJyb3IoZXJyKSB7XG4gICAgICAgICAgICAgICAgICAgIGF1dG9rYWRTZXJ2aWNlLnNlYXJjaC5ub1Jlc3VsdCA9IGZhbHNlO1xuICAgICAgICAgICAgICAgICAgICBhdXRva2FkU2VydmljZS5zZWFyY2guZXJyb3IgPSB0cnVlO1xuICAgICAgICAgICAgICAgICAgICBoaWRlTG9hZGluZygpO1xuICAgICAgICAgICAgICAgICAgICByZWplY3QoZXJyKTtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9XG5cbiAgICAgICAgZnVuY3Rpb24gaGFzUmVzdWx0SXRlbXMocmVzdWx0KSB7XG4gICAgICAgICAgICByZXR1cm4gaXNPYmplY3QocmVzdWx0KSAmJiBpc0FycmF5KHJlc3VsdFsnZW50cmllcyddKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGZ1bmN0aW9uIGlzQmxhbmsodmFsdWUpIHtcbiAgICAgICAgICAgIHJldHVybiBpc0VtcHR5KHZhbHVlKSAmJiAhaXNOdW1iZXIodmFsdWUpIHx8IGlzTmFOKHZhbHVlKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGZ1bmN0aW9uIGhhc0Vycm9yKCkge1xuICAgICAgICAgICAgcmV0dXJuIGF1dG9rYWRTZXJ2aWNlLnNlYXJjaC5lcnJvcjtcblxuICAgICAgICB9XG5cbiAgICAgICAgZnVuY3Rpb24gYnVpbGRTZWFyY2hSZXF1ZXN0RGF0YShzZWFyY2hQYXJhbXMpIHtcbiAgICAgICAgICAgIGNvbnN0IHJlcXVlc3REYXRhID0gYXNzaWduSW4oe30sIERFRkFVTFRfU0VBUkNIX1JFUVVFU1RfREFUQSk7XG4gICAgICAgICAgICByZXF1ZXN0RGF0YVsndHlwZSddID0gKHNlYXJjaFBhcmFtc1snc2lkZSddID09PSAtMSA/IDQgOiBzZWFyY2hQYXJhbXNbJ3NpZGUnXSk7XG4gICAgICAgICAgICByZXF1ZXN0RGF0YVsnaW5uJ10gPSBub2RlWydpbm4nXTtcbiAgICAgICAgICAgIHJlcXVlc3REYXRhWydvZ3JuJ10gPSBub2RlWydvZ3JuJ107XG4gICAgICAgICAgICByZXR1cm4gcmVxdWVzdERhdGE7XG4gICAgICAgIH1cblxuICAgICAgICAvKlxuICAgICAgICAgICAgKiBjYXNlXG4gICAgICAgICAgICAqXG4gICAgICAgICAgICAqL1xuXG4gICAgICAgIGZ1bmN0aW9uIGdldENhc2VDb3VudChzZWFyY2gsIHN1Y2Nlc3MsIGVycm9yLCBjb21wbGV0ZSkge1xuICAgICAgICAgICAgaWYgKCFpc0FycmF5KGF1dG9rYWRTZXJ2aWNlLnNlYXJjaC5zb3VyY2VzKSB8fCAhc2l6ZShhdXRva2FkU2VydmljZS5zZWFyY2guc291cmNlcykpIHtcbiAgICAgICAgICAgICAgICBjb25zb2xlLndhcm4oJ2dldENhc2VDb3VudC4uLiBlcnJvcjogc2VhcmNoLnNvdXJjZXMgaXMgYmxhbmsnKTtcbiAgICAgICAgICAgICAgICBlcnJvckNhbGxiYWNrKCk7XG4gICAgICAgICAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGxldCBzb3VyY2VJbmRleCA9IDAsXG4gICAgICAgICAgICAgICAgc291cmNlQ291bnQgPSBzaXplKGF1dG9rYWRTZXJ2aWNlLnNlYXJjaC5zb3VyY2VzKSxcbiAgICAgICAgICAgICAgICBjYXNlQ291bnRSZXF1ZXN0ID0gQ2FzZUNvdW50UmVxdWVzdCgpLFxuICAgICAgICAgICAgICAgIHJlc3VsdCA9IG51bGwsXG4gICAgICAgICAgICAgICAgc291cmNlLCByZXF1ZXN0O1xuXG4gICAgICAgICAgICBuZXh0UmVxdWVzdCgpO1xuICAgICAgICAgICAgcmV0dXJuIGNhc2VDb3VudFJlcXVlc3Q7XG5cbiAgICAgICAgICAgIGZ1bmN0aW9uIG5leHRSZXF1ZXN0KCkge1xuICAgICAgICAgICAgICAgIGxldCBlcnJvciA9IGZhbHNlLFxuICAgICAgICAgICAgICAgICAgICBuZXh0ID0gZmFsc2UsXG4gICAgICAgICAgICAgICAgICAgIHE7XG5cbiAgICAgICAgICAgICAgICBzb3VyY2UgPSBhdXRva2FkU2VydmljZS5zZWFyY2guc291cmNlc1tzb3VyY2VJbmRleCsrXTtcbiAgICAgICAgICAgICAgICBxID0gc291cmNlLnZhbHVlO1xuXG4gICAgICAgICAgICAgICAgaWYgKGlzQmxhbmsocSkpIHtcbiAgICAgICAgICAgICAgICAgICAgcmVxdWVzdCA9IG51bGw7XG4gICAgICAgICAgICAgICAgICAgIHJlc3VsdCA9IDA7XG4gICAgICAgICAgICAgICAgICAgIG5leHQgPSB0cnVlO1xuICAgICAgICAgICAgICAgICAgICBjYXNlQ291bnRSZXF1ZXN0Ll9zZXRSZXF1ZXN0KHJlcXVlc3QpO1xuICAgICAgICAgICAgICAgICAgICBjaGVjaygpO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIHJlcXVlc3QgPSBhdXRva2FkUmVzb3VyY2VTZXJ2aWNlLmthZFNlYXJjaCh7XG4gICAgICAgICAgICAgICAgICAgICAgICByOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcTogcVxuICAgICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgICAgIHN1Y2Nlc3M6IGZ1bmN0aW9uIChkYXRhKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmVzdWx0ID0gZ2V0UmVzdWx0VG90YWwoZGF0YVsnUmVzdWx0J10pO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGlzTnVsbChyZXN1bHQpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGVycm9yID0gdHJ1ZTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKHJlc3VsdCA9PT0gMCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBuZXh0ID0gdHJ1ZTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgZXJyb3I6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBlcnJvciA9IHRydWU7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAgICAgICAgIGNhc2VDb3VudFJlcXVlc3QuX3NldFJlcXVlc3QocmVxdWVzdCk7XG5cbiAgICAgICAgICAgICAgICAgICAgcmVxdWVzdC5jb21wbGV0ZVByb21pc2UudGhlbihmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBjaGVjaygpO1xuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICBmdW5jdGlvbiBjaGVjaygpIHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKGVycm9yKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBlcnJvckNhbGxiYWNrKCk7XG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoIW5leHQgfHwgc291cmNlSW5kZXggPT09IHNvdXJjZUNvdW50KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBzdWNjZXNzQ2FsbGJhY2soKTtcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmIChuZXh0KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBuZXh0UmVxdWVzdCgpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBmdW5jdGlvbiBzdWNjZXNzQ2FsbGJhY2soKSB7XG4gICAgICAgICAgICAgICAgaWYgKGlzRnVuY3Rpb24oc3VjY2VzcykpIHtcbiAgICAgICAgICAgICAgICAgICAgc3VjY2VzcyhyZXN1bHQsIHNvdXJjZUluZGV4IC0gMSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGNvbXBsZXRlQ2FsbGJhY2soKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgZnVuY3Rpb24gZXJyb3JDYWxsYmFjaygpIHtcbiAgICAgICAgICAgICAgICBpZiAoaXNGdW5jdGlvbihlcnJvcikpIHtcbiAgICAgICAgICAgICAgICAgICAgZXJyb3IoKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgY29tcGxldGVDYWxsYmFjaygpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBmdW5jdGlvbiBjb21wbGV0ZUNhbGxiYWNrKCkge1xuICAgICAgICAgICAgICAgIGlmIChpc0Z1bmN0aW9uKGNvbXBsZXRlKSkge1xuICAgICAgICAgICAgICAgICAgICBjb21wbGV0ZSgpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgZnVuY3Rpb24gaXNCbGFuayh2YWx1ZSkge1xuICAgICAgICAgICAgICAgIHJldHVybiBpc0VtcHR5KHZhbHVlKSAmJiAhaXNOdW1iZXIodmFsdWUpIHx8IGlzTmFOKHZhbHVlKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgZnVuY3Rpb24gQ2FzZUNvdW50UmVxdWVzdCgpIHtcbiAgICAgICAgICAgICAgICBsZXQgcmVxdWVzdDtcblxuICAgICAgICAgICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICAgICAgICAgIF9zZXRSZXF1ZXN0OiBmdW5jdGlvbiAocikge1xuICAgICAgICAgICAgICAgICAgICAgICAgcmVxdWVzdCA9IHI7XG4gICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgIGFib3J0OiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAocmVxdWVzdCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlcXVlc3QuYWJvcnQoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH07XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICBmdW5jdGlvbiBnZXRSZXN1bHRUb3RhbChyZXN1bHQpIHtcbiAgICAgICAgICAgIGxldCByID0gcmVzdWx0ICYmIHJlc3VsdFsnVG90YWxDb3VudCddO1xuXG4gICAgICAgICAgICBpZiAoIWlzTnVtYmVyKHIpKSB7XG4gICAgICAgICAgICAgICAgciA9IG51bGw7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHJldHVybiByO1xuICAgICAgICB9XG5cblxuICAgICAgICBmdW5jdGlvbiBzaG93TG9hZGluZygpIHtcbiAgICAgICAgICAgIGF1dG9rYWRTZXJ2aWNlLmxvYWRpbmcgPSB0cnVlO1xuICAgICAgICB9XG5cbiAgICAgICAgZnVuY3Rpb24gaGlkZUxvYWRpbmcoKSB7XG4gICAgICAgICAgICBhdXRva2FkU2VydmljZS5sb2FkaW5nID0gZmFsc2U7XG4gICAgICAgIH1cblxuICAgICAgICBmdW5jdGlvbiBnZXREZWZhdWx0U2VhcmNoUmVxdWVzdERhdGEoKSB7XG4gICAgICAgICAgICByZXR1cm4gREVGQVVMVF9TRUFSQ0hfUkVRVUVTVF9EQVRBO1xuICAgICAgICB9XG5cbiAgICAgICAgZnVuY3Rpb24gaXNMb2FkaW5nKCkge1xuICAgICAgICAgICAgcmV0dXJuIGF1dG9rYWRTZXJ2aWNlLmxvYWRpbmc7XG4gICAgICAgIH1cblxuICAgICAgICBmdW5jdGlvbiBpc05vUmVzdWx0KCkge1xuICAgICAgICAgICAgcmV0dXJuIGdldChhdXRva2FkU2VydmljZS5zZWFyY2gsICdyZXN1bHQuc3RhdHVzLml0ZW1zRm91bmQnLCAwKSA9PT0gMDtcbiAgICAgICAgfVxuXG4gICAgICAgIC8vUGFnZXJcbiAgICAgICAgZnVuY3Rpb24gUGFnZXIoKSB7XG4gICAgICAgICAgICBsZXQgaW50ZXJuYWxEaXNhYmxlZCA9IGZhbHNlLFxuICAgICAgICAgICAgICAgIG5vTmV4dFBhZ2UgPSBmYWxzZTtcbiAgICAgICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICAgICAgcmVzZXQ6IHJlc2V0LFxuICAgICAgICAgICAgICAgIG5leHRQYWdlOiBuZXh0UGFnZSxcbiAgICAgICAgICAgICAgICBpc0Rpc2FibGVkOiBpc0Rpc2FibGVkXG4gICAgICAgICAgICB9O1xuXG4gICAgICAgICAgICBmdW5jdGlvbiBpc0Rpc2FibGVkKCkge1xuICAgICAgICAgICAgICAgIHJldHVybiBpbnRlcm5hbERpc2FibGVkIHx8IG5vTmV4dFBhZ2U7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGZ1bmN0aW9uIHJlc2V0KHJlc3VsdCkge1xuICAgICAgICAgICAgICAgIGludGVybmFsRGlzYWJsZWQgPSBmYWxzZTtcbiAgICAgICAgICAgICAgICBub05leHRQYWdlID0gbm9Nb3JlKGZhbHNlLCByZXN1bHQpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBmdW5jdGlvbiBub01vcmUoaGFzRXJyb3IsIHJlc3VsdCkge1xuICAgICAgICAgICAgICAgIHJldHVybiBoYXNFcnJvciB8fCAoYXV0b2thZFNlcnZpY2Uuc2VhcmNoLnJlcXVlc3REYXRhLnBhZ2VJbmRleCAqIHJlc3VsdC5zaXplID49IHJlc3VsdC50b3RhbCk7XG4gICAgICAgICAgICB9XG5cblxuICAgICAgICAgICAgYXN5bmMgZnVuY3Rpb24gbmV4dFBhZ2UoKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIHNob3dMb2FkaW5nKCk7XG4gICAgICAgICAgICAgICAgICAgIGlmICghaXNEaXNhYmxlZCgpICYmIGF1dG9rYWRTZXJ2aWNlLnNlYXJjaC5yZXF1ZXN0RGF0YSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgaW50ZXJuYWxEaXNhYmxlZCA9IHRydWU7XG4gICAgICAgICAgICAgICAgICAgICAgICBhdXRva2FkU2VydmljZS5zZWFyY2gucmVxdWVzdERhdGEucGFnZUluZGV4Kys7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIHNlYXJjaFJlcXVlc3RQcm9taXNlKCkudGhlbihmdW5jdGlvbiAocmVzdWx0OiB7IGVudHJpZXMgfSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGhhc0Vycm9yID0gIWhhc1Jlc3VsdEl0ZW1zKHJlc3VsdCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCFoYXNFcnJvcikge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBmb3JFYWNoKHJlc3VsdC5lbnRyaWVzLCBmdW5jdGlvbiAoaXRlbSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYXV0b2thZFNlcnZpY2Uuc2VhcmNoLnJlc3VsdC5lbnRyaWVzLnB1c2goaXRlbSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpbnRlcm5hbERpc2FibGVkID0gZmFsc2U7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5vTmV4dFBhZ2UgPSBub01vcmUoZmFsc2UsIHJlc3VsdCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaW50ZXJuYWxEaXNhYmxlZCA9IGZhbHNlO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBub05leHRQYWdlID0gdHJ1ZTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaGlkZUxvYWRpbmcoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXNvbHZlKCk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICByZXNvbHZlKCk7XG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG5cbiAgICB9XG5cbn1cblxuXG5cbiJdfQ==