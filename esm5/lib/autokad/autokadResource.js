/**
 * @fileoverview added by tsickle
 * Generated from: lib/autokad/autokadResource.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/**
 * Created by radigost on 24.04.17.
 */
import axios from 'axios';
import { Cache } from '../cache/Cache';
import { head, get } from 'lodash';
import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
/** @type {?} */
var seldon = axios.create({
    baseURL: '/api/v1/companies/',
});
/**
 * @record
 */
function IRequestParams() { }
if (false) {
    /** @type {?|undefined} */
    IRequestParams.prototype.ogrn;
    /** @type {?|undefined} */
    IRequestParams.prototype.inn;
    /** @type {?} */
    IRequestParams.prototype.type;
    /** @type {?} */
    IRequestParams.prototype.pageSize;
    /** @type {?} */
    IRequestParams.prototype.pageNo;
}
var AutokadResource = /** @class */ (function () {
    function AutokadResource() {
        this.cache = new Cache();
    }
    /**
     * @param {?} arg0
     * @return {?}
     */
    AutokadResource.prototype.kadSearch = /**
     * @param {?} arg0
     * @return {?}
     */
    function (arg0) {
        throw new Error("Method not implemented.");
    };
    /**
     * @return {?}
     */
    AutokadResource.prototype.init = /**
     * @return {?}
     */
    function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            return tslib_1.__generator(this, function (_a) {
                return [2 /*return*/];
            });
        });
    };
    /**
     * @param {?} options
     * @return {?}
     */
    AutokadResource.prototype.search = /**
     * @param {?} options
     * @return {?}
     */
    function (options) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            /**
             * @param {?} options
             * @return {?}
             */
            function buildRequestParams(options) {
                /** @type {?} */
                var req = {
                    type: options.type,
                    pageSize: options.pageSize || 100,
                    pageNo: options.pageIndex || 1,
                };
                options.ogrn !== undefined ? req.ogrn = options.ogrn :
                    options.inn !== undefined ? req.inn = options.inn :
                        console.warn('there is no inn or ogrn in request!', options);
                return req;
            }
            var result, req;
            return tslib_1.__generator(this, function (_a) {
                req = buildRequestParams(options);
                result = this.cache.get(req.ogrn + "_" + req.type + "_" + req.pageSize + "_" + req.pageNo);
                if (result === undefined) {
                    result = seldon.get("ogrn-" + req.ogrn + "/cases", { params: req });
                    this.cache.set(req.ogrn + "_" + req.type + "_" + req.pageSize + "_" + req.pageNo, result);
                }
                return [2 /*return*/, result];
            });
        });
    };
    /**
     * @param {?} options
     * @return {?}
     */
    AutokadResource.prototype.aggregates = /**
     * @param {?} options
     * @return {?}
     */
    function (options) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var res;
            return tslib_1.__generator(this, function (_a) {
                if (options.ogrn) {
                    res = seldon.get("ogrn-" + options.ogrn + "/aggregates");
                }
                return [2 /*return*/, res];
            });
        });
    };
    /**
     * @param {?} ogrn
     * @return {?}
     */
    AutokadResource.prototype.getAggregates = /**
     * @param {?} ogrn
     * @return {?}
     */
    function (ogrn) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var res, e_1;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.aggregates({ ogrn: ogrn })];
                    case 1:
                        res = _a.sent();
                        return [2 /*return*/, head(get(res, 'data.entries'))];
                    case 2:
                        e_1 = _a.sent();
                        return [2 /*return*/, Promise.reject()];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    AutokadResource.decorators = [
        { type: Injectable, args: [{ providedIn: 'root' },] }
    ];
    /** @nocollapse */ AutokadResource.ngInjectableDef = i0.defineInjectable({ factory: function AutokadResource_Factory() { return new AutokadResource(); }, token: AutokadResource, providedIn: "root" });
    return AutokadResource;
}());
export { AutokadResource };
if (false) {
    /** @type {?} */
    AutokadResource.prototype.cache;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0b2thZFJlc291cmNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQG5rYi9hbmd1bGFyLW5rYi1saWIvIiwic291cmNlcyI6WyJsaWIvYXV0b2thZC9hdXRva2FkUmVzb3VyY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7O0FBSUEsT0FBTyxLQUFLLE1BQU0sT0FBTyxDQUFDO0FBQzFCLE9BQU8sRUFBQyxLQUFLLEVBQUMsTUFBTSxnQkFBZ0IsQ0FBQztBQUNyQyxPQUFPLEVBQUUsSUFBSSxFQUFFLEdBQUcsRUFBRSxNQUFNLFFBQVEsQ0FBQztBQUNuQyxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDOzs7SUFFckMsTUFBTSxHQUFHLEtBQUssQ0FBQyxNQUFNLENBQUM7SUFDeEIsT0FBTyxFQUFFLG9CQUFvQjtDQUNoQyxDQUFDOzs7O0FBRUYsNkJBTUM7OztJQUxHLDhCQUFNOztJQUNOLDZCQUFLOztJQUNMLDhCQUFLOztJQUNMLGtDQUFTOztJQUNULGdDQUFPOztBQUdYO0lBQUE7UUFLSSxVQUFLLEdBQUcsSUFBSSxLQUFLLEVBQUUsQ0FBQztLQTRDdkI7Ozs7O0lBL0NHLG1DQUFTOzs7O0lBQVQsVUFBVSxJQUEwRTtRQUNoRixNQUFNLElBQUksS0FBSyxDQUFDLHlCQUF5QixDQUFDLENBQUM7SUFDL0MsQ0FBQzs7OztJQUdLLDhCQUFJOzs7SUFBVjs7Ozs7O0tBQ0M7Ozs7O0lBRUssZ0NBQU07Ozs7SUFBWixVQUFhLE9BQU87Ozs7OztZQVVoQixTQUFTLGtCQUFrQixDQUFDLE9BQW9EOztvQkFDdEUsR0FBRyxHQUFtQjtvQkFDeEIsSUFBSSxFQUFFLE9BQU8sQ0FBQyxJQUFJO29CQUNsQixRQUFRLEVBQUUsT0FBTyxDQUFDLFFBQVEsSUFBSSxHQUFHO29CQUNqQyxNQUFNLEVBQUUsT0FBTyxDQUFDLFNBQVMsSUFBSSxDQUFDO2lCQUNqQztnQkFDRCxPQUFPLENBQUMsSUFBSSxLQUFLLFNBQVMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksR0FBRyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ2xELE9BQU8sQ0FBQyxHQUFHLEtBQUssU0FBUyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsR0FBRyxHQUFHLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQzt3QkFDL0MsT0FBTyxDQUFDLElBQUksQ0FBQyxxQ0FBcUMsRUFBRSxPQUFPLENBQUMsQ0FBQztnQkFDckUsT0FBTyxHQUFHLENBQUM7WUFDZixDQUFDOzs7Z0JBbEJLLEdBQUcsR0FBRyxrQkFBa0IsQ0FBQyxPQUFPLENBQUM7Z0JBQ3ZDLE1BQU0sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBSSxHQUFHLENBQUMsSUFBSSxTQUFJLEdBQUcsQ0FBQyxJQUFJLFNBQUksR0FBRyxDQUFDLFFBQVEsU0FBSSxHQUFHLENBQUMsTUFBUSxDQUFDLENBQUM7Z0JBQ2pGLElBQUksTUFBTSxLQUFLLFNBQVMsRUFBRTtvQkFDdEIsTUFBTSxHQUFHLE1BQU0sQ0FBQyxHQUFHLENBQUMsVUFBUSxHQUFHLENBQUMsSUFBSSxXQUFRLEVBQUUsRUFBRSxNQUFNLEVBQUUsR0FBRyxFQUFFLENBQUMsQ0FBQztvQkFDL0QsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUksR0FBRyxDQUFDLElBQUksU0FBSSxHQUFHLENBQUMsSUFBSSxTQUFJLEdBQUcsQ0FBQyxRQUFRLFNBQUksR0FBRyxDQUFDLE1BQVEsRUFBRSxNQUFNLENBQUMsQ0FBQztpQkFDbkY7Z0JBQ0Qsc0JBQU8sTUFBTSxFQUFDOzs7S0FhakI7Ozs7O0lBRUssb0NBQVU7Ozs7SUFBaEIsVUFBaUIsT0FBTzs7OztnQkFFcEIsSUFBSSxPQUFPLENBQUMsSUFBSSxFQUFFO29CQUNkLEdBQUcsR0FBRyxNQUFNLENBQUMsR0FBRyxDQUFDLFVBQVEsT0FBTyxDQUFDLElBQUksZ0JBQWEsQ0FBQyxDQUFDO2lCQUN2RDtnQkFDRCxzQkFBTyxHQUFHLEVBQUM7OztLQUNkOzs7OztJQUVLLHVDQUFhOzs7O0lBQW5CLFVBQW9CLElBQUk7Ozs7Ozs7d0JBRUoscUJBQU0sSUFBSSxDQUFDLFVBQVUsQ0FBQyxFQUFFLElBQUksTUFBQSxFQUFFLENBQUMsRUFBQTs7d0JBQXJDLEdBQUcsR0FBRyxTQUErQjt3QkFDM0Msc0JBQU8sSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLEVBQUUsY0FBYyxDQUFDLENBQUMsRUFBQzs7O3dCQUV0QyxzQkFBTyxPQUFPLENBQUMsTUFBTSxFQUFFLEVBQUM7Ozs7O0tBRS9COztnQkFoREosVUFBVSxTQUFDLEVBQUUsVUFBVSxFQUFFLE1BQU0sRUFBRTs7OzBCQXJCbEM7Q0FzRUMsQUFqREQsSUFpREM7U0FoRFksZUFBZTs7O0lBSXhCLGdDQUFvQiIsInNvdXJjZXNDb250ZW50IjpbIi8qKlxyXG4gKiBDcmVhdGVkIGJ5IHJhZGlnb3N0IG9uIDI0LjA0LjE3LlxyXG4gKi9cclxuXHJcbmltcG9ydCBheGlvcyBmcm9tICdheGlvcyc7XHJcbmltcG9ydCB7Q2FjaGV9IGZyb20gJy4uL2NhY2hlL0NhY2hlJztcclxuaW1wb3J0IHsgaGVhZCwgZ2V0IH0gZnJvbSAnbG9kYXNoJztcclxuaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuY29uc3Qgc2VsZG9uID0gYXhpb3MuY3JlYXRlKHtcclxuICAgIGJhc2VVUkw6ICcvYXBpL3YxL2NvbXBhbmllcy8nLFxyXG59KTtcclxuXHJcbmludGVyZmFjZSBJUmVxdWVzdFBhcmFtcyB7XHJcbiAgICBvZ3JuPztcclxuICAgIGlubj87XHJcbiAgICB0eXBlO1xyXG4gICAgcGFnZVNpemU7XHJcbiAgICBwYWdlTm87XHJcbn1cclxuXHJcbkBJbmplY3RhYmxlKHsgcHJvdmlkZWRJbjogJ3Jvb3QnIH0pXHJcbmV4cG9ydCBjbGFzcyBBdXRva2FkUmVzb3VyY2Uge1xyXG4gICAga2FkU2VhcmNoKGFyZzA6IHsgcjogeyBxOiBhbnk7IH07IHN1Y2Nlc3M6IChkYXRhOiBhbnkpID0+IHZvaWQ7IGVycm9yOiAoKSA9PiB2b2lkOyB9KTogYW55IHtcclxuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJNZXRob2Qgbm90IGltcGxlbWVudGVkLlwiKTtcclxuICAgIH1cclxuICAgIGNhY2hlID0gbmV3IENhY2hlKCk7XHJcblxyXG4gICAgYXN5bmMgaW5pdCgpIHtcclxuICAgIH1cclxuXHJcbiAgICBhc3luYyBzZWFyY2gob3B0aW9ucykge1xyXG4gICAgICAgIGxldCByZXN1bHQ7XHJcbiAgICAgICAgY29uc3QgcmVxID0gYnVpbGRSZXF1ZXN0UGFyYW1zKG9wdGlvbnMpO1xyXG4gICAgICAgIHJlc3VsdCA9IHRoaXMuY2FjaGUuZ2V0KGAke3JlcS5vZ3JufV8ke3JlcS50eXBlfV8ke3JlcS5wYWdlU2l6ZX1fJHtyZXEucGFnZU5vfWApO1xyXG4gICAgICAgIGlmIChyZXN1bHQgPT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgICAgICByZXN1bHQgPSBzZWxkb24uZ2V0KGBvZ3JuLSR7cmVxLm9ncm59L2Nhc2VzYCwgeyBwYXJhbXM6IHJlcSB9KTtcclxuICAgICAgICAgICAgdGhpcy5jYWNoZS5zZXQoYCR7cmVxLm9ncm59XyR7cmVxLnR5cGV9XyR7cmVxLnBhZ2VTaXplfV8ke3JlcS5wYWdlTm99YCwgcmVzdWx0KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHJlc3VsdDtcclxuXHJcbiAgICAgICAgZnVuY3Rpb24gYnVpbGRSZXF1ZXN0UGFyYW1zKG9wdGlvbnM6IHsgb2dybj8sIGlubj8sIHR5cGUsIHBhZ2VTaXplPywgcGFnZUluZGV4P30pOiBJUmVxdWVzdFBhcmFtcyB7XHJcbiAgICAgICAgICAgIGNvbnN0IHJlcTogSVJlcXVlc3RQYXJhbXMgPSB7XHJcbiAgICAgICAgICAgICAgICB0eXBlOiBvcHRpb25zLnR5cGUsXHJcbiAgICAgICAgICAgICAgICBwYWdlU2l6ZTogb3B0aW9ucy5wYWdlU2l6ZSB8fCAxMDAsXHJcbiAgICAgICAgICAgICAgICBwYWdlTm86IG9wdGlvbnMucGFnZUluZGV4IHx8IDEsXHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIG9wdGlvbnMub2dybiAhPT0gdW5kZWZpbmVkID8gcmVxLm9ncm4gPSBvcHRpb25zLm9ncm4gOlxyXG4gICAgICAgICAgICAgICAgb3B0aW9ucy5pbm4gIT09IHVuZGVmaW5lZCA/IHJlcS5pbm4gPSBvcHRpb25zLmlubiA6XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS53YXJuKCd0aGVyZSBpcyBubyBpbm4gb3Igb2dybiBpbiByZXF1ZXN0IScsIG9wdGlvbnMpO1xyXG4gICAgICAgICAgICByZXR1cm4gcmVxO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBhc3luYyBhZ2dyZWdhdGVzKG9wdGlvbnMpIHtcclxuICAgICAgICBsZXQgcmVzO1xyXG4gICAgICAgIGlmIChvcHRpb25zLm9ncm4pIHtcclxuICAgICAgICAgICAgcmVzID0gc2VsZG9uLmdldChgb2dybi0ke29wdGlvbnMub2dybn0vYWdncmVnYXRlc2ApO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gcmVzO1xyXG4gICAgfVxyXG5cclxuICAgIGFzeW5jIGdldEFnZ3JlZ2F0ZXMob2dybikge1xyXG4gICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHJlcyA9IGF3YWl0IHRoaXMuYWdncmVnYXRlcyh7IG9ncm4gfSk7XHJcbiAgICAgICAgICAgIHJldHVybiBoZWFkKGdldChyZXMsICdkYXRhLmVudHJpZXMnKSk7XHJcbiAgICAgICAgfSBjYXRjaCAoZSkge1xyXG4gICAgICAgICAgICByZXR1cm4gUHJvbWlzZS5yZWplY3QoKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuXHJcbiJdfQ==