/**
 * @fileoverview added by tsickle
 * Generated from: lib/autokad/autokadComponent.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/**
 * Created by radigost on 24.04.17.
 */
import { isEmpty, get, filter } from 'lodash';
import { AutokadService } from './autokadService';
import { Node } from '../domain/Node';
import { Component, Input, Output, EventEmitter } from '@angular/core';
var AutokadComponent = /** @class */ (function () {
    function AutokadComponent(autokadService) {
        this.autokadService = autokadService;
        this.onAddArbitrationBreadcrumb = new EventEmitter();
        this.loading = true;
        Object.assign(this, {
            pager: autokadService.pager,
        });
    }
    /**
     * @return {?}
     */
    AutokadComponent.prototype.ngOnChanges = /**
     * @return {?}
     */
    function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.autokadService.initSearch(this.node);
                        if (this.side) {
                            this.autokadService.search.params.side = this.side;
                        }
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, , 3, 4]);
                        return [4 /*yield*/, this.autokadService.doSearch()];
                    case 2:
                        _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        this.loading = false;
                        if (this.scrollTop) {
                            window.scrollTo(0, this.scrollTop);
                        }
                        return [7 /*endfinally*/];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    Object.defineProperty(AutokadComponent.prototype, "list", {
        get: /**
         * @return {?}
         */
        function () {
            return this.autokadService.search.result.entries;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @param {?} type
     * @return {?}
     */
    AutokadComponent.prototype.changeSide = /**
     * @param {?} type
     * @return {?}
     */
    function (type) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.loading = true;
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, , 3, 4]);
                        this.autokadService.search.params.side = type;
                        return [4 /*yield*/, this.autokadService.doSearch()];
                    case 2:
                        _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        this.loading = false;
                        return [7 /*endfinally*/];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * @param {?} type
     * @return {?}
     */
    AutokadComponent.prototype.isSelectedSide = /**
     * @param {?} type
     * @return {?}
     */
    function (type) {
        return this.autokadService.search.params
            && (parseInt(type) === parseInt(this.autokadService.search.params.side));
    };
    /**
     * @param {?} caseSides
     * @return {?}
     */
    AutokadComponent.prototype.getPlaintiff = /**
     * @param {?} caseSides
     * @return {?}
     */
    function (caseSides) {
        return filter(caseSides, (/**
         * @param {?} item
         * @return {?}
         */
        function (item) { return item.type === 0; }));
    };
    /**
     * @param {?} caseSides
     * @return {?}
     */
    AutokadComponent.prototype.getDefendant = /**
     * @param {?} caseSides
     * @return {?}
     */
    function (caseSides) {
        return filter(caseSides, (/**
         * @param {?} item
         * @return {?}
         */
        function (item) { return item.type === 1; }));
    };
    /**
     * @param {?} caseSides
     * @return {?}
     */
    AutokadComponent.prototype.getOthers = /**
     * @param {?} caseSides
     * @return {?}
     */
    function (caseSides) {
        return filter(caseSides, (/**
         * @param {?} item
         * @return {?}
         */
        function (item) { return item.type !== 1 && item.type !== 0; }));
    };
    /**
     * @return {?}
     */
    AutokadComponent.prototype.getTotal = /**
     * @return {?}
     */
    function () {
        var _this = this;
        /** @type {?} */
        var hasSearchResult = (/**
         * @return {?}
         */
        function () {
            return !isEmpty(_this.autokadService.search.result);
        });
        return hasSearchResult() ? get(this.autokadService, 'search.result.total', 0) : 0;
    };
    /**
     * @return {?}
     */
    AutokadComponent.prototype.isEmptyResult = /**
     * @return {?}
     */
    function () {
        return !this.getTotal();
    };
    /**
     * @return {?}
     */
    AutokadComponent.prototype.isNoResult = /**
     * @return {?}
     */
    function () {
        return this.autokadService.search.noResult;
    };
    /**
     * @return {?}
     */
    AutokadComponent.prototype.hasError = /**
     * @return {?}
     */
    function () {
        return this.autokadService.search.error;
    };
    Object.defineProperty(AutokadComponent.prototype, "isLoading", {
        get: /**
         * @return {?}
         */
        function () {
            return this.loading;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @param {?} id
     * @return {?}
     */
    AutokadComponent.prototype.addArbitrationBreadcrumb = /**
     * @param {?} id
     * @return {?}
     */
    function (id) {
        this.onAddArbitrationBreadcrumb.emit(id);
    };
    /**
     * @return {?}
     */
    AutokadComponent.prototype.nextPage = /**
     * @return {?}
     */
    function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var promise;
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.loading = true;
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, , 3, 4]);
                        promise = this.pager.nextPage().then((/**
                         * @return {?}
                         */
                        function () {
                            _this.loading = false;
                        }));
                        console.log(promise);
                        return [4 /*yield*/, promise];
                    case 2:
                        _a.sent();
                        return [3 /*break*/, 4];
                    case 3: return [7 /*endfinally*/];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    AutokadComponent.decorators = [
        { type: Component, args: [{
                    selector: 'nkb-autokad',
                    template: "\n        <div class=\"autokad autokad__inner\">\n            <div class=\"autokad__filters\">\n                <div class=\"\">\n                    <span>\u0421\u0442\u043E\u0440\u043E\u043D\u0430 \u0434\u0435\u043B\u0430</span><br>\n                    <button class=\"btn  btn-sm flat\" [ngClass]=\"isSelectedSide(4) ? 'active':''\"\n                            (click)=\"changeSide(4)\">\u043B\u044E\u0431\u043E\u0435\n                    </button>\n                    <button class=\"btn  btn-sm flat\" [ngClass]=\"isSelectedSide(0) ? 'active':''\"\n                            (click)=\"changeSide(0)\">\u0438\u0441\u0442\u0435\u0446\n                    </button>\n                    <button class=\"btn  btn-sm flat\" [ngClass]=\"isSelectedSide(1) ? 'active':''\"\n                            (click)=\"changeSide(1)\">\u043E\u0442\u0432\u0435\u0442\u0447\u0438\u043A\n                    </button>\n                    <button class=\"btn  btn-sm flat\" [ngClass]=\"isSelectedSide(2) ? 'active':''\"\n                            (click)=\"changeSide(2)\">\u0438\u043D\u043E\u0435 \u043B\u0438\u0446\u043E\n                    </button>\n                    <button class=\"btn  btn-sm flat\" [ngClass]=\"isSelectedSide(3) ? 'active':''\"\n                            (click)=\"changeSide(3)\">\u0442\u0440\u0435\u0442\u044C\u0435 \u043B\u0438\u0446\u043E\n                    </button>\n                </div>\n                <div class=\"autokad__total\">\n                    <a class=\"underline\" [attr.href]=\"'http://kad.arbitr.ru/ ' | externalUrl\" target=\"_blank\">\u041A\u0430\u0440\u0442\u043E\u0442\u0435\u043A\u0430\n                        \u0430\u0440\u0431\u0438\u0442\u0440\u0430\u0436\u043D\u044B\u0445 \u0434\u0435\u043B</a>\n                    <div>\n                        <span [hidden]=\"getTotal()===0 || isLoading\">\n                            <span>{{getTotal()}}</span>\n                            <!-- | translate: {number: getTotal()} -->\n                            <!-- {{'AUTOKAD.KAD_SEARCH_TOTAL' | async }} -->\n                            &nbsp;\n                        </span>\n                    </div>\n                </div>\n            </div>\n            <div class=\"autokad__result\">\n                <div class=\"autokad__result-list\"\n                    infiniteScroll\n                    (scrolled)=\"nextPage()\"\n                >\n                    <div\n                        class=\"result-list-element autokad__list-element autokad-case autokad-case__inner\"\n                        *ngFor=\"let item of list\"\n                    >\n                        <div class=\"autokad-case__header\">\n                            <div class=\"autokad-case__caption\">\n                                <div class=\"autokad-case__number\">\n                                    {{item.id}}\n                                </div>\n                                <div class=\"autokad-case__instance-container\">\n                                    <div>{{item.filledMillis * 1000 | date : 'dd.MM.yyyy'}}</div>\n                                    <div class=\"autokad-case__instance\">{{item.currentInstance}}</div>\n                                </div>\n                                <div class=\"autokad-case__type\">{{item.type.name}}</div>\n                            </div>\n                            <div\n                                class=\"autokad-case__process-container autokad-case__process-container--1\"\n                            >\n                                <span\n                                    class=\"autokad-case__process autokad-case__process--1\"\n                                    title=\"\u0411\u0430\u043D\u043A\u0440\u043E\u0442\u043D\u043E\u0435 \u0434\u0435\u043B\u043E\"\n                                    *ngIf=\"item.type.code===1\"\n                                >\u0411</span>\n                            </div>\n\n                            <div class=\"autokad-case__process-container\">\n                                <span class=\"autokad-case__process\" *ngIf=\"item.active===true\">\u043D\u0430 \u0440\u0430\u0441\u0441\u043C\u043E\u0442\u0440\u0435\u043D\u0438\u0438</span>\n                            </div>\n                            <div class=\"autokad-case__sum\">\n                                <span *ngIf=\"item.claimAmount !== 0\">{{item.claimAmount | number}}\u0440.</span>\n                            </div>\n                        </div>\n\n                        <div class=\"autokad-sides autokad-sides__inner\">\n                            <div class=\"autokad-sides__row\">\n                                <div class=\"autokad-sides__caption\">\n                                    \u0418\u0441\u0442\u0435\u0446:\n                                </div>\n                                <div class=\"autokad-sides__companies\">\n                                    <div *ngFor=\"let company of getPlaintiff(item.parties) ; last as isLast\">\n                                        <nkb-company-name-with-link\n                                            (addBreadcrumb)=\"addArbitrationBreadcrumb(id)\"\n                                            [node]=\"company\"\n                                            [ogrn]=\"node.ogrn\"\n                                        ></nkb-company-name-with-link>\n                                        <span [hidden]=\"isLast\">, &nbsp;</span>\n                                    </div>\n                                </div>\n                            </div>\n                            <div class=\"autokad-sides__row\">\n                                <div class=\"autokad-sides__caption\">\n                                    \u041E\u0442\u0432\u0435\u0442\u0447\u0438\u043A:\n                                </div>\n                                <div class=\"autokad-sides__companies\">\n                                    <div *ngFor=\"let company of getDefendant(item.parties); last as isLast\">\n                                        <nkb-company-name-with-link\n                                            [ogrn]=\"node.ogrn\"\n                                            (addBreadcrumb)=\"addArbitrationBreadcrumb(id)\"\n                                            [node]=\"company\"></nkb-company-name-with-link>\n                                        <span [hidden]=\"isLast\">, &nbsp;</span>\n                                    </div>\n                                </div>\n                            </div>\n                            <div class=\"autokad-sides__row\" *ngIf=\"getOthers(item.parties).length>0\">\n                                <div class=\"autokad-sides__caption\">\n                                    \u041F\u0440\u043E\u0447\u0438\u0435 \u043B\u0438\u0446\u0430:\n                                </div>\n                                <div class=\"autokad-sides__companies\">\n                                    <div *ngFor=\"let company of getOthers(item.parties); last as isLast\">\n                                        <nkb-company-name-with-link\n                                            [ogrn]=\"node.ogrn\"\n                                            (addBreadcrumb)=\"addArbitrationBreadcrumb(id)\"\n                                            [node]=\"company\"\n                                        ></nkb-company-name-with-link>\n                                        <span [hidden]=\"isLast\">, &nbsp;</span>\n                                    </div>\n                                </div>\n                            </div>\n\n                            <div class=\"autokad-documents\" *ngIf=\"item.documents\">\n                                <h5>\u0414\u043E\u043A\u0443\u043C\u0435\u043D\u0442\u044B \u043F\u043E \u0434\u0435\u043B\u0443:</h5>\n                                <ul>\n                                    <li *ngFor=\" let document of item.documents\">\n                                        <a target=\"_blank\" [attr.href]=\"document.url\">\n                                            <span>{{document.date * 1000 | date:'dd.MM.yyyy'}}</span>\n                                            .\n                                            {{document.name}}\n                                        </a>\n                                    </li>\n                                </ul>\n\n                            </div>\n\n                        </div>\n                    </div>\n                </div>\n            </div>\n\n            <div class=\"align-center empty-result muted\"\n                *ngIf=\"isNoResult()  && !hasError() && !isLoading\" style=\"margin-top: 4em;\">\n                \u0410\u0440\u0431\u0438\u0442\u0440\u0430\u0436\u043D\u044B\u0445 \u0434\u0435\u043B \u043D\u0435 \u043D\u0430\u0439\u0434\u0435\u043D\u043E\n            </div>\n            <div class=\"align-center empty-result\" *ngIf=\"hasError() && !isLoading\">\n                \u041F\u043E\u0438\u0441\u043A \u0432\n                <a class=\"underline\" [attr.href]=\"'http://kad.arbitr.ru/' | externalUrl\" target=\"_blank\">\n                    \u043A\u0430\u0440\u0442\u043E\u0442\u0435\u043A\u0435 \u0430\u0440\u0431\u0438\u0442\u0440\u0430\u0436\u043D\u044B\u0445 \u0434\u0435\u043B\n                </a>\n                \u0432\u0440\u0435\u043C\u0435\u043D\u043D\u043E \u043D\u0435\u0434\u043E\u0441\u0442\u0443\u043F\u0435\u043D\n            </div>\n            <div class=\"loader\" [hidden]=\"!loading\" style=\"margin-top:1em; \"></div>\n        </div>\n    ",
                    styles: [".autokad__filters{display:-webkit-box;display:flex;-webkit-box-pack:justify;justify-content:space-between}.autokad__total{display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column;-webkit-box-pack:justify;justify-content:space-between}.autokad__result-list{margin-top:1em}.autokad-case__inner{padding:1em;border-color:#444;cursor:default}.autokad-case__inner:hover{border-color:#444;cursor:default}.autokad-case__header{display:-webkit-box;display:flex;-webkit-box-pack:justify;justify-content:space-between;margin-bottom:1em}.autokad-case__caption{flex-basis:60%;display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column}.autokad-case__process{background-color:#2f96b4;color:#fff;display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column;-webkit-box-pack:center;justify-content:center;text-align:center;font-weight:700;border-radius:3px;height:2em;width:10em;align-self:center}.autokad-case__process--1{width:2em;background-color:#bd362f;margin-left:.5em;margin-right:.5em}.autokad-case__process-container{flex-basis:10%}.autokad-case__process-container--1{flex-basis:1em}.autokad-case__sum{font-size:large;font-weight:700;flex-basis:20%;display:-webkit-box;display:flex;-webkit-box-pack:end;justify-content:flex-end}.autokad-case__number{font-size:large;font-weight:700}.autokad-case__instance-container{display:-webkit-box;display:flex;-webkit-box-orient:horizontal;-webkit-box-direction:normal;flex-direction:row}.autokad-case__instance-container div{margin:0 1em}.autokad-case__instance-container div:first-of-type{margin:0}.autokad-case__type{font-style:italic}.autokad-sides__row{display:-webkit-box;display:flex;-webkit-box-orient:horizontal;-webkit-box-direction:normal;flex-direction:row}.autokad-sides__caption{flex-basis:20%;font-weight:700}.autokad-sides__companies{flex-basis:80%;display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column}.autokad-sides__companies div{margin:.2em 0}.autokad-documents h5{text-align:center;font-weight:700}.autokad__company-name--highlight{background-color:#ff0}"]
                }] }
    ];
    /** @nocollapse */
    AutokadComponent.ctorParameters = function () { return [
        { type: AutokadService }
    ]; };
    AutokadComponent.propDecorators = {
        scrollTop: [{ type: Input }],
        node: [{ type: Input }],
        side: [{ type: Input }],
        onAddArbitrationBreadcrumb: [{ type: Output }]
    };
    return AutokadComponent;
}());
export { AutokadComponent };
if (false) {
    /** @type {?} */
    AutokadComponent.prototype.scrollTop;
    /** @type {?} */
    AutokadComponent.prototype.node;
    /** @type {?} */
    AutokadComponent.prototype.side;
    /** @type {?} */
    AutokadComponent.prototype.onAddArbitrationBreadcrumb;
    /** @type {?} */
    AutokadComponent.prototype.id;
    /** @type {?} */
    AutokadComponent.prototype.pager;
    /** @type {?} */
    AutokadComponent.prototype.loading;
    /**
     * @type {?}
     * @private
     */
    AutokadComponent.prototype.autokadService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0b2thZENvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bua2IvYW5ndWxhci1ua2ItbGliLyIsInNvdXJjZXMiOlsibGliL2F1dG9rYWQvYXV0b2thZENvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7QUFJQSxPQUFPLEVBQUUsT0FBTyxFQUFFLEdBQUcsRUFBRSxNQUFNLEVBQUUsTUFBTSxRQUFRLENBQUM7QUFDOUMsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLGtCQUFrQixDQUFDO0FBQ2xELE9BQU8sRUFBRSxJQUFJLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUN0QyxPQUFPLEVBQUUsU0FBUyxFQUFhLEtBQUssRUFBRSxNQUFNLEVBQUUsWUFBWSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRWxGO0lBcUtJLDBCQUNZLGNBQThCO1FBQTlCLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQU5oQywrQkFBMEIsR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBSW5ELFlBQU8sR0FBRyxJQUFJLENBQUM7UUFJbEIsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUU7WUFDaEIsS0FBSyxFQUFFLGNBQWMsQ0FBQyxLQUFLO1NBQzlCLENBQUMsQ0FBQztJQUNQLENBQUM7Ozs7SUFFSyxzQ0FBVzs7O0lBQWpCOzs7Ozt3QkFDSSxJQUFJLENBQUMsY0FBYyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7d0JBQzFDLElBQUksSUFBSSxDQUFDLElBQUksRUFBRTs0QkFDWCxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUM7eUJBQ3REOzs7O3dCQUVHLHFCQUFNLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxFQUFFLEVBQUE7O3dCQUFwQyxTQUFvQyxDQUFDOzs7d0JBR3JDLElBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO3dCQUNyQixJQUFJLElBQUksQ0FBQyxTQUFTLEVBQUU7NEJBQ2hCLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQzt5QkFDdEM7Ozs7OztLQUVSO0lBRUQsc0JBQUksa0NBQUk7Ozs7UUFBUjtZQUNJLE9BQU8sSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQztRQUNyRCxDQUFDOzs7T0FBQTs7Ozs7SUFFSyxxQ0FBVTs7OztJQUFoQixVQUFpQixJQUFJOzs7Ozt3QkFDakIsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7Ozs7d0JBRWhCLElBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO3dCQUM5QyxxQkFBTSxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsRUFBRSxFQUFBOzt3QkFBcEMsU0FBb0MsQ0FBQzs7O3dCQUdyQyxJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQzs7Ozs7O0tBRTVCOzs7OztJQUVELHlDQUFjOzs7O0lBQWQsVUFBZSxJQUFJO1FBQ2YsT0FBTyxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxNQUFNO2VBQ2pDLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLFFBQVEsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztJQUNqRixDQUFDOzs7OztJQUVELHVDQUFZOzs7O0lBQVosVUFBYSxTQUFTO1FBQ2xCLE9BQU8sTUFBTSxDQUFDLFNBQVM7Ozs7UUFBRSxVQUFDLElBQUksSUFBSyxPQUFBLElBQUksQ0FBQyxJQUFJLEtBQUssQ0FBQyxFQUFmLENBQWUsRUFBQyxDQUFDO0lBQ3hELENBQUM7Ozs7O0lBRUQsdUNBQVk7Ozs7SUFBWixVQUFhLFNBQVM7UUFDbEIsT0FBTyxNQUFNLENBQUMsU0FBUzs7OztRQUFFLFVBQUMsSUFBSSxJQUFLLE9BQUEsSUFBSSxDQUFDLElBQUksS0FBSyxDQUFDLEVBQWYsQ0FBZSxFQUFDLENBQUM7SUFDeEQsQ0FBQzs7Ozs7SUFFRCxvQ0FBUzs7OztJQUFULFVBQVUsU0FBUztRQUNmLE9BQU8sTUFBTSxDQUFDLFNBQVM7Ozs7UUFBRSxVQUFDLElBQUksSUFBSyxPQUFBLElBQUksQ0FBQyxJQUFJLEtBQUssQ0FBQyxJQUFJLElBQUksQ0FBQyxJQUFJLEtBQUssQ0FBQyxFQUFsQyxDQUFrQyxFQUFDLENBQUM7SUFDM0UsQ0FBQzs7OztJQUVELG1DQUFROzs7SUFBUjtRQUFBLGlCQUtDOztZQUpTLGVBQWU7OztRQUFHO1lBQ3BCLE9BQU8sQ0FBQyxPQUFPLENBQUMsS0FBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDdkQsQ0FBQyxDQUFBO1FBQ0QsT0FBTyxlQUFlLEVBQUUsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxjQUFjLEVBQUUscUJBQXFCLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUN0RixDQUFDOzs7O0lBRUQsd0NBQWE7OztJQUFiO1FBQ0ksT0FBTyxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUM1QixDQUFDOzs7O0lBRUQscUNBQVU7OztJQUFWO1FBQ0ksT0FBTyxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUM7SUFDL0MsQ0FBQzs7OztJQUVELG1DQUFROzs7SUFBUjtRQUNJLE9BQU8sSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDO0lBQzVDLENBQUM7SUFFRCxzQkFBSSx1Q0FBUzs7OztRQUFiO1lBQ0ksT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDO1FBQ3hCLENBQUM7OztPQUFBOzs7OztJQUVELG1EQUF3Qjs7OztJQUF4QixVQUF5QixFQUFFO1FBQ3ZCLElBQUksQ0FBQywwQkFBMEIsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7SUFDN0MsQ0FBQzs7OztJQUVLLG1DQUFROzs7SUFBZDs7Ozs7Ozt3QkFDSSxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQzs7Ozt3QkFFVixPQUFPLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLEVBQUUsQ0FBQyxJQUFJOzs7d0JBQUM7NEJBQ3ZDLEtBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO3dCQUN6QixDQUFDLEVBQUM7d0JBQ0YsT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQzt3QkFDckIscUJBQU0sT0FBTyxFQUFBOzt3QkFBYixTQUFhLENBQUM7Ozs7Ozs7S0FLckI7O2dCQXBRSixTQUFTLFNBQUM7b0JBQ1AsUUFBUSxFQUFFLGFBQWE7b0JBRXZCLFFBQVEsRUFBRSw2M1NBc0pUOztpQkFDSjs7OztnQkE5SlEsY0FBYzs7OzRCQWlLbEIsS0FBSzt1QkFDTCxLQUFLO3VCQUNMLEtBQUs7NkNBQ0wsTUFBTTs7SUFxR1gsdUJBQUM7Q0FBQSxBQXJRRCxJQXFRQztTQXpHWSxnQkFBZ0I7OztJQUN6QixxQ0FBMkI7O0lBQzNCLGdDQUFvQjs7SUFDcEIsZ0NBQW1COztJQUNuQixzREFBMEQ7O0lBQzFELDhCQUFROztJQUVSLGlDQUFXOztJQUNYLG1DQUFzQjs7Ozs7SUFFbEIsMENBQXNDIiwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBDcmVhdGVkIGJ5IHJhZGlnb3N0IG9uIDI0LjA0LjE3LlxuICovXG5cbmltcG9ydCB7IGlzRW1wdHksIGdldCwgZmlsdGVyIH0gZnJvbSAnbG9kYXNoJztcbmltcG9ydCB7IEF1dG9rYWRTZXJ2aWNlIH0gZnJvbSAnLi9hdXRva2FkU2VydmljZSc7XG5pbXBvcnQgeyBOb2RlIH0gZnJvbSAnLi4vZG9tYWluL05vZGUnO1xuaW1wb3J0IHsgQ29tcG9uZW50LCBPbkNoYW5nZXMsIElucHV0LCBPdXRwdXQsIEV2ZW50RW1pdHRlciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5AQ29tcG9uZW50KHtcbiAgICBzZWxlY3RvcjogJ25rYi1hdXRva2FkJyxcbiAgICBzdHlsZVVybHM6IFsnLi9hdXRva2FkLmxlc3MnXSxcbiAgICB0ZW1wbGF0ZTogYFxuICAgICAgICA8ZGl2IGNsYXNzPVwiYXV0b2thZCBhdXRva2FkX19pbm5lclwiPlxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cImF1dG9rYWRfX2ZpbHRlcnNcIj5cbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiXCI+XG4gICAgICAgICAgICAgICAgICAgIDxzcGFuPtCh0YLQvtGA0L7QvdCwINC00LXQu9CwPC9zcGFuPjxicj5cbiAgICAgICAgICAgICAgICAgICAgPGJ1dHRvbiBjbGFzcz1cImJ0biAgYnRuLXNtIGZsYXRcIiBbbmdDbGFzc109XCJpc1NlbGVjdGVkU2lkZSg0KSA/ICdhY3RpdmUnOicnXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAoY2xpY2spPVwiY2hhbmdlU2lkZSg0KVwiPtC70Y7QsdC+0LVcbiAgICAgICAgICAgICAgICAgICAgPC9idXR0b24+XG4gICAgICAgICAgICAgICAgICAgIDxidXR0b24gY2xhc3M9XCJidG4gIGJ0bi1zbSBmbGF0XCIgW25nQ2xhc3NdPVwiaXNTZWxlY3RlZFNpZGUoMCkgPyAnYWN0aXZlJzonJ1wiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKGNsaWNrKT1cImNoYW5nZVNpZGUoMClcIj7QuNGB0YLQtdGGXG4gICAgICAgICAgICAgICAgICAgIDwvYnV0dG9uPlxuICAgICAgICAgICAgICAgICAgICA8YnV0dG9uIGNsYXNzPVwiYnRuICBidG4tc20gZmxhdFwiIFtuZ0NsYXNzXT1cImlzU2VsZWN0ZWRTaWRlKDEpID8gJ2FjdGl2ZSc6JydcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIChjbGljayk9XCJjaGFuZ2VTaWRlKDEpXCI+0L7RgtCy0LXRgtGH0LjQulxuICAgICAgICAgICAgICAgICAgICA8L2J1dHRvbj5cbiAgICAgICAgICAgICAgICAgICAgPGJ1dHRvbiBjbGFzcz1cImJ0biAgYnRuLXNtIGZsYXRcIiBbbmdDbGFzc109XCJpc1NlbGVjdGVkU2lkZSgyKSA/ICdhY3RpdmUnOicnXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAoY2xpY2spPVwiY2hhbmdlU2lkZSgyKVwiPtC40L3QvtC1INC70LjRhtC+XG4gICAgICAgICAgICAgICAgICAgIDwvYnV0dG9uPlxuICAgICAgICAgICAgICAgICAgICA8YnV0dG9uIGNsYXNzPVwiYnRuICBidG4tc20gZmxhdFwiIFtuZ0NsYXNzXT1cImlzU2VsZWN0ZWRTaWRlKDMpID8gJ2FjdGl2ZSc6JydcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIChjbGljayk9XCJjaGFuZ2VTaWRlKDMpXCI+0YLRgNC10YLRjNC1INC70LjRhtC+XG4gICAgICAgICAgICAgICAgICAgIDwvYnV0dG9uPlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJhdXRva2FkX190b3RhbFwiPlxuICAgICAgICAgICAgICAgICAgICA8YSBjbGFzcz1cInVuZGVybGluZVwiIFthdHRyLmhyZWZdPVwiJ2h0dHA6Ly9rYWQuYXJiaXRyLnJ1LyAnIHwgZXh0ZXJuYWxVcmxcIiB0YXJnZXQ9XCJfYmxhbmtcIj7QmtCw0YDRgtC+0YLQtdC60LBcbiAgICAgICAgICAgICAgICAgICAgICAgINCw0YDQsdC40YLRgNCw0LbQvdGL0YUg0LTQtdC7PC9hPlxuICAgICAgICAgICAgICAgICAgICA8ZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gW2hpZGRlbl09XCJnZXRUb3RhbCgpPT09MCB8fCBpc0xvYWRpbmdcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8c3Bhbj57e2dldFRvdGFsKCl9fTwvc3Bhbj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8IS0tIHwgdHJhbnNsYXRlOiB7bnVtYmVyOiBnZXRUb3RhbCgpfSAtLT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8IS0tIHt7J0FVVE9LQUQuS0FEX1NFQVJDSF9UT1RBTCcgfCBhc3luYyB9fSAtLT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAmbmJzcDtcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJhdXRva2FkX19yZXN1bHRcIj5cbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiYXV0b2thZF9fcmVzdWx0LWxpc3RcIlxuICAgICAgICAgICAgICAgICAgICBpbmZpbml0ZVNjcm9sbFxuICAgICAgICAgICAgICAgICAgICAoc2Nyb2xsZWQpPVwibmV4dFBhZ2UoKVwiXG4gICAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgICAgICA8ZGl2XG4gICAgICAgICAgICAgICAgICAgICAgICBjbGFzcz1cInJlc3VsdC1saXN0LWVsZW1lbnQgYXV0b2thZF9fbGlzdC1lbGVtZW50IGF1dG9rYWQtY2FzZSBhdXRva2FkLWNhc2VfX2lubmVyXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICpuZ0Zvcj1cImxldCBpdGVtIG9mIGxpc3RcIlxuICAgICAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiYXV0b2thZC1jYXNlX19oZWFkZXJcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiYXV0b2thZC1jYXNlX19jYXB0aW9uXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJhdXRva2FkLWNhc2VfX251bWJlclwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge3tpdGVtLmlkfX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJhdXRva2FkLWNhc2VfX2luc3RhbmNlLWNvbnRhaW5lclwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdj57e2l0ZW0uZmlsbGVkTWlsbGlzICogMTAwMCB8IGRhdGUgOiAnZGQuTU0ueXl5eSd9fTwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImF1dG9rYWQtY2FzZV9faW5zdGFuY2VcIj57e2l0ZW0uY3VycmVudEluc3RhbmNlfX08L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJhdXRva2FkLWNhc2VfX3R5cGVcIj57e2l0ZW0udHlwZS5uYW1lfX08L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzPVwiYXV0b2thZC1jYXNlX19wcm9jZXNzLWNvbnRhaW5lciBhdXRva2FkLWNhc2VfX3Byb2Nlc3MtY29udGFpbmVyLS0xXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbGFzcz1cImF1dG9rYWQtY2FzZV9fcHJvY2VzcyBhdXRva2FkLWNhc2VfX3Byb2Nlc3MtLTFcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGl0bGU9XCLQkdCw0L3QutGA0L7RgtC90L7QtSDQtNC10LvQvlwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAqbmdJZj1cIml0ZW0udHlwZS5jb2RlPT09MVwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgID7QkTwvc3Bhbj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJhdXRva2FkLWNhc2VfX3Byb2Nlc3MtY29udGFpbmVyXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwiYXV0b2thZC1jYXNlX19wcm9jZXNzXCIgKm5nSWY9XCJpdGVtLmFjdGl2ZT09PXRydWVcIj7QvdCwINGA0LDRgdGB0LzQvtGC0YDQtdC90LjQuDwvc3Bhbj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiYXV0b2thZC1jYXNlX19zdW1cIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gKm5nSWY9XCJpdGVtLmNsYWltQW1vdW50ICE9PSAwXCI+e3tpdGVtLmNsYWltQW1vdW50IHwgbnVtYmVyfX3RgC48L3NwYW4+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cblxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImF1dG9rYWQtc2lkZXMgYXV0b2thZC1zaWRlc19faW5uZXJcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiYXV0b2thZC1zaWRlc19fcm93XCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJhdXRva2FkLXNpZGVzX19jYXB0aW9uXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICDQmNGB0YLQtdGGOlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImF1dG9rYWQtc2lkZXNfX2NvbXBhbmllc1wiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiAqbmdGb3I9XCJsZXQgY29tcGFueSBvZiBnZXRQbGFpbnRpZmYoaXRlbS5wYXJ0aWVzKSA7IGxhc3QgYXMgaXNMYXN0XCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPG5rYi1jb21wYW55LW5hbWUtd2l0aC1saW5rXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIChhZGRCcmVhZGNydW1iKT1cImFkZEFyYml0cmF0aW9uQnJlYWRjcnVtYihpZClcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBbbm9kZV09XCJjb21wYW55XCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgW29ncm5dPVwibm9kZS5vZ3JuXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA+PC9ua2ItY29tcGFueS1uYW1lLXdpdGgtbGluaz5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBbaGlkZGVuXT1cImlzTGFzdFwiPiwgJm5ic3A7PC9zcGFuPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJhdXRva2FkLXNpZGVzX19yb3dcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImF1dG9rYWQtc2lkZXNfX2NhcHRpb25cIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgINCe0YLQstC10YLRh9C40Lo6XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiYXV0b2thZC1zaWRlc19fY29tcGFuaWVzXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2ICpuZ0Zvcj1cImxldCBjb21wYW55IG9mIGdldERlZmVuZGFudChpdGVtLnBhcnRpZXMpOyBsYXN0IGFzIGlzTGFzdFwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxua2ItY29tcGFueS1uYW1lLXdpdGgtbGlua1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBbb2dybl09XCJub2RlLm9ncm5cIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAoYWRkQnJlYWRjcnVtYik9XCJhZGRBcmJpdHJhdGlvbkJyZWFkY3J1bWIoaWQpXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgW25vZGVdPVwiY29tcGFueVwiPjwvbmtiLWNvbXBhbnktbmFtZS13aXRoLWxpbms+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gW2hpZGRlbl09XCJpc0xhc3RcIj4sICZuYnNwOzwvc3Bhbj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiYXV0b2thZC1zaWRlc19fcm93XCIgKm5nSWY9XCJnZXRPdGhlcnMoaXRlbS5wYXJ0aWVzKS5sZW5ndGg+MFwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiYXV0b2thZC1zaWRlc19fY2FwdGlvblwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg0J/RgNC+0YfQuNC1INC70LjRhtCwOlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImF1dG9rYWQtc2lkZXNfX2NvbXBhbmllc1wiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiAqbmdGb3I9XCJsZXQgY29tcGFueSBvZiBnZXRPdGhlcnMoaXRlbS5wYXJ0aWVzKTsgbGFzdCBhcyBpc0xhc3RcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bmtiLWNvbXBhbnktbmFtZS13aXRoLWxpbmtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgW29ncm5dPVwibm9kZS5vZ3JuXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKGFkZEJyZWFkY3J1bWIpPVwiYWRkQXJiaXRyYXRpb25CcmVhZGNydW1iKGlkKVwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFtub2RlXT1cImNvbXBhbnlcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgID48L25rYi1jb21wYW55LW5hbWUtd2l0aC1saW5rPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuIFtoaWRkZW5dPVwiaXNMYXN0XCI+LCAmbmJzcDs8L3NwYW4+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiYXV0b2thZC1kb2N1bWVudHNcIiAqbmdJZj1cIml0ZW0uZG9jdW1lbnRzXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxoNT7QlNC+0LrRg9C80LXQvdGC0Ysg0L/QviDQtNC10LvRgzo8L2g1PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8dWw+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bGkgKm5nRm9yPVwiIGxldCBkb2N1bWVudCBvZiBpdGVtLmRvY3VtZW50c1wiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxhIHRhcmdldD1cIl9ibGFua1wiIFthdHRyLmhyZWZdPVwiZG9jdW1lbnQudXJsXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuPnt7ZG9jdW1lbnQuZGF0ZSAqIDEwMDAgfCBkYXRlOidkZC5NTS55eXl5J319PC9zcGFuPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHt7ZG9jdW1lbnQubmFtZX19XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9hPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9saT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC91bD5cblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8L2Rpdj5cblxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cImFsaWduLWNlbnRlciBlbXB0eS1yZXN1bHQgbXV0ZWRcIlxuICAgICAgICAgICAgICAgICpuZ0lmPVwiaXNOb1Jlc3VsdCgpICAmJiAhaGFzRXJyb3IoKSAmJiAhaXNMb2FkaW5nXCIgc3R5bGU9XCJtYXJnaW4tdG9wOiA0ZW07XCI+XG4gICAgICAgICAgICAgICAg0JDRgNCx0LjRgtGA0LDQttC90YvRhSDQtNC10Lsg0L3QtSDQvdCw0LnQtNC10L3QvlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwiYWxpZ24tY2VudGVyIGVtcHR5LXJlc3VsdFwiICpuZ0lmPVwiaGFzRXJyb3IoKSAmJiAhaXNMb2FkaW5nXCI+XG4gICAgICAgICAgICAgICAg0J/QvtC40YHQuiDQslxuICAgICAgICAgICAgICAgIDxhIGNsYXNzPVwidW5kZXJsaW5lXCIgW2F0dHIuaHJlZl09XCInaHR0cDovL2thZC5hcmJpdHIucnUvJyB8IGV4dGVybmFsVXJsXCIgdGFyZ2V0PVwiX2JsYW5rXCI+XG4gICAgICAgICAgICAgICAgICAgINC60LDRgNGC0L7RgtC10LrQtSDQsNGA0LHQuNGC0YDQsNC20L3Ri9GFINC00LXQu1xuICAgICAgICAgICAgICAgIDwvYT5cbiAgICAgICAgICAgICAgICDQstGA0LXQvNC10L3QvdC+INC90LXQtNC+0YHRgtGD0L/QtdC9XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJsb2FkZXJcIiBbaGlkZGVuXT1cIiFsb2FkaW5nXCIgc3R5bGU9XCJtYXJnaW4tdG9wOjFlbTsgXCI+PC9kaXY+XG4gICAgICAgIDwvZGl2PlxuICAgIGBcbn0pXG5cbmV4cG9ydCBjbGFzcyBBdXRva2FkQ29tcG9uZW50IGltcGxlbWVudHMgT25DaGFuZ2VzIHtcbiAgICBASW5wdXQoKSBzY3JvbGxUb3A6IG51bWJlcjtcbiAgICBASW5wdXQoKSBub2RlOiBOb2RlO1xuICAgIEBJbnB1dCgpIHNpZGU6IGFueTtcbiAgICBAT3V0cHV0KCkgb25BZGRBcmJpdHJhdGlvbkJyZWFkY3J1bWIgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG4gICAgaWQ6IGFueTtcblxuICAgIHBhZ2VyOiBhbnk7XG4gICAgcHVibGljIGxvYWRpbmcgPSB0cnVlO1xuICAgIGNvbnN0cnVjdG9yKFxuICAgICAgICBwcml2YXRlIGF1dG9rYWRTZXJ2aWNlOiBBdXRva2FkU2VydmljZSxcbiAgICApIHtcbiAgICAgICAgT2JqZWN0LmFzc2lnbih0aGlzLCB7XG4gICAgICAgICAgICBwYWdlcjogYXV0b2thZFNlcnZpY2UucGFnZXIsXG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIGFzeW5jIG5nT25DaGFuZ2VzKCkge1xuICAgICAgICB0aGlzLmF1dG9rYWRTZXJ2aWNlLmluaXRTZWFyY2godGhpcy5ub2RlKTtcbiAgICAgICAgaWYgKHRoaXMuc2lkZSkge1xuICAgICAgICAgICAgdGhpcy5hdXRva2FkU2VydmljZS5zZWFyY2gucGFyYW1zLnNpZGUgPSB0aGlzLnNpZGU7XG4gICAgICAgIH1cbiAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgIGF3YWl0IHRoaXMuYXV0b2thZFNlcnZpY2UuZG9TZWFyY2goKTtcbiAgICAgICAgfVxuICAgICAgICBmaW5hbGx5IHtcbiAgICAgICAgICAgIHRoaXMubG9hZGluZyA9IGZhbHNlO1xuICAgICAgICAgICAgaWYgKHRoaXMuc2Nyb2xsVG9wKSB7XG4gICAgICAgICAgICAgICAgd2luZG93LnNjcm9sbFRvKDAsIHRoaXMuc2Nyb2xsVG9wKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cblxuICAgIGdldCBsaXN0KCkge1xuICAgICAgICByZXR1cm4gdGhpcy5hdXRva2FkU2VydmljZS5zZWFyY2gucmVzdWx0LmVudHJpZXM7XG4gICAgfVxuXG4gICAgYXN5bmMgY2hhbmdlU2lkZSh0eXBlKSB7XG4gICAgICAgIHRoaXMubG9hZGluZyA9IHRydWU7XG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgICB0aGlzLmF1dG9rYWRTZXJ2aWNlLnNlYXJjaC5wYXJhbXMuc2lkZSA9IHR5cGU7XG4gICAgICAgICAgICBhd2FpdCB0aGlzLmF1dG9rYWRTZXJ2aWNlLmRvU2VhcmNoKCk7XG4gICAgICAgIH1cbiAgICAgICAgZmluYWxseSB7XG4gICAgICAgICAgICB0aGlzLmxvYWRpbmcgPSBmYWxzZTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIGlzU2VsZWN0ZWRTaWRlKHR5cGUpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuYXV0b2thZFNlcnZpY2Uuc2VhcmNoLnBhcmFtc1xuICAgICAgICAgICAgJiYgKHBhcnNlSW50KHR5cGUpID09PSBwYXJzZUludCh0aGlzLmF1dG9rYWRTZXJ2aWNlLnNlYXJjaC5wYXJhbXMuc2lkZSkpO1xuICAgIH1cblxuICAgIGdldFBsYWludGlmZihjYXNlU2lkZXMpIHtcbiAgICAgICAgcmV0dXJuIGZpbHRlcihjYXNlU2lkZXMsIChpdGVtKSA9PiBpdGVtLnR5cGUgPT09IDApO1xuICAgIH1cblxuICAgIGdldERlZmVuZGFudChjYXNlU2lkZXMpIHtcbiAgICAgICAgcmV0dXJuIGZpbHRlcihjYXNlU2lkZXMsIChpdGVtKSA9PiBpdGVtLnR5cGUgPT09IDEpO1xuICAgIH1cblxuICAgIGdldE90aGVycyhjYXNlU2lkZXMpIHtcbiAgICAgICAgcmV0dXJuIGZpbHRlcihjYXNlU2lkZXMsIChpdGVtKSA9PiBpdGVtLnR5cGUgIT09IDEgJiYgaXRlbS50eXBlICE9PSAwKTtcbiAgICB9XG5cbiAgICBnZXRUb3RhbCgpIHtcbiAgICAgICAgY29uc3QgaGFzU2VhcmNoUmVzdWx0ID0gKCkgPT4ge1xuICAgICAgICAgICAgcmV0dXJuICFpc0VtcHR5KHRoaXMuYXV0b2thZFNlcnZpY2Uuc2VhcmNoLnJlc3VsdCk7XG4gICAgICAgIH07XG4gICAgICAgIHJldHVybiBoYXNTZWFyY2hSZXN1bHQoKSA/IGdldCh0aGlzLmF1dG9rYWRTZXJ2aWNlLCAnc2VhcmNoLnJlc3VsdC50b3RhbCcsIDApIDogMDtcbiAgICB9XG5cbiAgICBpc0VtcHR5UmVzdWx0KCkge1xuICAgICAgICByZXR1cm4gIXRoaXMuZ2V0VG90YWwoKTtcbiAgICB9XG5cbiAgICBpc05vUmVzdWx0KCkge1xuICAgICAgICByZXR1cm4gdGhpcy5hdXRva2FkU2VydmljZS5zZWFyY2gubm9SZXN1bHQ7XG4gICAgfVxuXG4gICAgaGFzRXJyb3IoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmF1dG9rYWRTZXJ2aWNlLnNlYXJjaC5lcnJvcjtcbiAgICB9XG5cbiAgICBnZXQgaXNMb2FkaW5nKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5sb2FkaW5nO1xuICAgIH1cblxuICAgIGFkZEFyYml0cmF0aW9uQnJlYWRjcnVtYihpZCkge1xuICAgICAgICB0aGlzLm9uQWRkQXJiaXRyYXRpb25CcmVhZGNydW1iLmVtaXQoaWQpO1xuICAgIH1cblxuICAgIGFzeW5jIG5leHRQYWdlKCkge1xuICAgICAgICB0aGlzLmxvYWRpbmcgPSB0cnVlO1xuICAgICAgICB0cnkge1xuICAgICAgICAgICAgY29uc3QgcHJvbWlzZSA9IHRoaXMucGFnZXIubmV4dFBhZ2UoKS50aGVuKCgpID0+IHtcbiAgICAgICAgICAgICAgICB0aGlzLmxvYWRpbmcgPSBmYWxzZTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgY29uc29sZS5sb2cocHJvbWlzZSk7XG4gICAgICAgICAgICBhd2FpdCBwcm9taXNlO1xuICAgICAgICB9XG4gICAgICAgIGZpbmFsbHkge1xuXG4gICAgICAgIH1cbiAgICB9XG59XG5cbiJdfQ==