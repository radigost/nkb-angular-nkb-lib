/**
 * @fileoverview added by tsickle
 * Generated from: lib/angular-nkb-lib.module.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { AngularNkbLibComponent } from './angular-nkb-lib.component';
import { AutokadComponent } from './autokad/autokadComponent';
import { AutokadResource } from './autokad/autokadResource';
import { AutokadService } from './autokad/autokadService';
import { CompanyNameWithLinkComponent } from './ui/CompanyNameWithLink';
import { ExternalUrlPipe } from './pipes/externalUrl';
import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
var AngularNkbLibModule = /** @class */ (function () {
    function AngularNkbLibModule() {
    }
    AngularNkbLibModule.decorators = [
        { type: NgModule, args: [{
                    imports: [
                        CommonModule
                    ],
                    declarations: [
                        AngularNkbLibComponent,
                        AutokadComponent,
                        CompanyNameWithLinkComponent,
                        ExternalUrlPipe,
                    ],
                    providers: [
                        AutokadResource,
                        AutokadService,
                        DatePipe,
                    ],
                    exports: [
                        AngularNkbLibComponent,
                        AutokadComponent,
                        CompanyNameWithLinkComponent,
                        ExternalUrlPipe,
                    ]
                },] }
    ];
    return AngularNkbLibModule;
}());
export { AngularNkbLibModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYW5ndWxhci1ua2ItbGliLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bua2IvYW5ndWxhci1ua2ItbGliLyIsInNvdXJjZXMiOlsibGliL2FuZ3VsYXItbmtiLWxpYi5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUUsc0JBQXNCLEVBQUUsTUFBTSw2QkFBNkIsQ0FBQztBQUNyRSxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQztBQUM5RCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sMkJBQTJCLENBQUM7QUFDNUQsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBQzFELE9BQU8sRUFBRSw0QkFBNEIsRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBQ3hFLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQztBQUN0RCxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBQyxZQUFZLEVBQUUsUUFBUSxFQUFDLE1BQU0saUJBQWlCLENBQUE7QUFFdEQ7SUFBQTtJQXNCbUMsQ0FBQzs7Z0JBdEJuQyxRQUFRLFNBQUM7b0JBQ1IsT0FBTyxFQUFFO3dCQUNQLFlBQVk7cUJBQ2I7b0JBQ0QsWUFBWSxFQUFFO3dCQUNaLHNCQUFzQjt3QkFDdEIsZ0JBQWdCO3dCQUNoQiw0QkFBNEI7d0JBQzVCLGVBQWU7cUJBQ2hCO29CQUNELFNBQVMsRUFBRTt3QkFDVCxlQUFlO3dCQUNmLGNBQWM7d0JBQ2QsUUFBUTtxQkFDVDtvQkFDRCxPQUFPLEVBQUU7d0JBQ1Asc0JBQXNCO3dCQUN0QixnQkFBZ0I7d0JBQ2hCLDRCQUE0Qjt3QkFDNUIsZUFBZTtxQkFDaEI7aUJBQ0Y7O0lBQ2tDLDBCQUFDO0NBQUEsQUF0QnBDLElBc0JvQztTQUF2QixtQkFBbUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBBbmd1bGFyTmtiTGliQ29tcG9uZW50IH0gZnJvbSAnLi9hbmd1bGFyLW5rYi1saWIuY29tcG9uZW50JztcbmltcG9ydCB7IEF1dG9rYWRDb21wb25lbnQgfSBmcm9tICcuL2F1dG9rYWQvYXV0b2thZENvbXBvbmVudCc7XG5pbXBvcnQgeyBBdXRva2FkUmVzb3VyY2UgfSBmcm9tICcuL2F1dG9rYWQvYXV0b2thZFJlc291cmNlJztcbmltcG9ydCB7IEF1dG9rYWRTZXJ2aWNlIH0gZnJvbSAnLi9hdXRva2FkL2F1dG9rYWRTZXJ2aWNlJztcbmltcG9ydCB7IENvbXBhbnlOYW1lV2l0aExpbmtDb21wb25lbnQgfSBmcm9tICcuL3VpL0NvbXBhbnlOYW1lV2l0aExpbmsnO1xuaW1wb3J0IHsgRXh0ZXJuYWxVcmxQaXBlIH0gZnJvbSAnLi9waXBlcy9leHRlcm5hbFVybCc7XG5pbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtDb21tb25Nb2R1bGUsIERhdGVQaXBlfSBmcm9tICdAYW5ndWxhci9jb21tb24nXG5cbkBOZ01vZHVsZSh7XG4gIGltcG9ydHM6IFtcbiAgICBDb21tb25Nb2R1bGVcbiAgXSxcbiAgZGVjbGFyYXRpb25zOiBbXG4gICAgQW5ndWxhck5rYkxpYkNvbXBvbmVudCxcbiAgICBBdXRva2FkQ29tcG9uZW50LFxuICAgIENvbXBhbnlOYW1lV2l0aExpbmtDb21wb25lbnQsXG4gICAgRXh0ZXJuYWxVcmxQaXBlLFxuICBdLFxuICBwcm92aWRlcnM6IFtcbiAgICBBdXRva2FkUmVzb3VyY2UsXG4gICAgQXV0b2thZFNlcnZpY2UsXG4gICAgRGF0ZVBpcGUsXG4gIF0sXG4gIGV4cG9ydHM6IFtcbiAgICBBbmd1bGFyTmtiTGliQ29tcG9uZW50LFxuICAgIEF1dG9rYWRDb21wb25lbnQsXG4gICAgQ29tcGFueU5hbWVXaXRoTGlua0NvbXBvbmVudCxcbiAgICBFeHRlcm5hbFVybFBpcGUsXG4gIF1cbn0pXG5leHBvcnQgY2xhc3MgQW5ndWxhck5rYkxpYk1vZHVsZSB7IH1cbiJdfQ==