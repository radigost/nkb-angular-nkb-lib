/**
 * @fileoverview added by tsickle
 * Generated from: lib/meta/metaService.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/**
 * Created by radigost on 07.09.17.
 */
import axios from 'axios';
//todo импортировать только то что нужно
import * as _ from 'lodash';
import { NODE_TYPE } from '../domain/TYPES';
import { rsearchMeta } from './rsearchMeta';
/**
 * @record
 */
export function NodeType() { }
if (false) {
    /** @type {?} */
    NodeType.prototype.name;
    /** @type {?} */
    NodeType.prototype.sortField;
    /** @type {?} */
    NodeType.prototype.sortAscending;
    /** @type {?} */
    NodeType.prototype.idField;
    /** @type {?} */
    NodeType.prototype.nameField;
    /** @type {?} */
    NodeType.prototype.properties;
}
//todo покрыть тестами
var 
//todo покрыть тестами
Meta = /** @class */ (function () {
    function Meta() {
        this.SEARCHABLE_TYPES = [NODE_TYPE.COMPANY, NODE_TYPE.INDIVIDUAL, NODE_TYPE.ADDRESS, NODE_TYPE.PHONE];
        this.state = {
            inited: false,
            pending: false,
            error: false,
        };
        // @Deprecated
        this.nodeTypesMeta = {};
        this.relationTypesMeta = {};
    }
    Object.defineProperty(Meta.prototype, "inited", {
        get: /**
         * @return {?}
         */
        function () {
            return this.state.inited;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @return {?}
     */
    Meta.prototype.init = /**
     * @return {?}
     */
    function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var _a, _b, err_1;
            var _this = this;
            return tslib_1.__generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        if (!!this.state.inited) return [3 /*break*/, 8];
                        if (!!this.state.pending) return [3 /*break*/, 7];
                        this.state.pending = true;
                        _c.label = 1;
                    case 1:
                        _c.trys.push([1, 4, 5, 6]);
                        _a = this;
                        return [4 /*yield*/, axios.get('/nkbrelation/api/meta/relation/types').then((/**
                             * @param {?} res
                             * @return {?}
                             */
                            function (res) { return res.data; }))];
                    case 2:
                        _a.relationTypes = _c.sent();
                        _b = this;
                        return [4 /*yield*/, axios.get('/nkbrelation/api/meta/node/types').then((/**
                             * @param {?} res
                             * @return {?}
                             */
                            function (res) { return res.data; }))];
                    case 3:
                        _b.nodeTypes = _c.sent();
                        // TODO legacy - refactor this - a lot of additional not needed work
                        _.forEach(this.nodeTypes, (/**
                         * @param {?} nodeType
                         * @return {?}
                         */
                        function (nodeType) {
                            _this.nodeTypesMeta[nodeType.name] = nodeType;
                        }));
                        _.forEach(rsearchMeta.nodeTypes, (/**
                         * @param {?} nodeType
                         * @param {?} nodeTypeName
                         * @return {?}
                         */
                        function (nodeType, nodeTypeName) {
                            _this.nodeTypesMeta[nodeTypeName] = _.extend({}, _this.nodeTypesMeta[nodeTypeName], nodeType);
                        }));
                        // relationTypesMeta
                        _.forEach(this.relationTypes, (/**
                         * @param {?} relationType
                         * @return {?}
                         */
                        function (relationType) {
                            _this.relationTypesMeta[relationType.name] = relationType;
                        }));
                        _.forEach(rsearchMeta.relationTypes, (/**
                         * @param {?} relationType
                         * @param {?} relationTypeName
                         * @return {?}
                         */
                        function (relationType, relationTypeName) {
                            _this.relationTypesMeta[relationTypeName] = _.extend({}, _this.relationTypesMeta[relationTypeName], relationType);
                        }));
                        // end TODO legacy - refactor this - a lot of additional not needed work
                        this.state.inited = true;
                        return [3 /*break*/, 6];
                    case 4:
                        err_1 = _c.sent();
                        console.error(err_1);
                        this.state.error = err_1;
                        return [3 /*break*/, 6];
                    case 5:
                        console.log("meta service inited");
                        this.state.pending = false;
                        return [7 /*endfinally*/];
                    case 6: return [3 /*break*/, 8];
                    case 7: return [2 /*return*/, new Promise((/**
                         * @param {?} resolve
                         * @return {?}
                         */
                        function (resolve) {
                            /** @type {?} */
                            var interval = setInterval((/**
                             * @return {?}
                             */
                            function () {
                                if (_this.state.inited) {
                                    resolve();
                                    clearInterval(interval);
                                }
                            }), 1000);
                        }))];
                    case 8: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * @param {?} node
     * @return {?}
     */
    Meta.prototype.getNodeName = /**
     * @param {?} node
     * @return {?}
     */
    function (node) {
        /** @type {?} */
        var nameField = _.get(_.find(this.nodeTypes, { name: _.get(node, '_type') }), 'nameField');
        return _.get(node, [nameField], _.get(node, '_name', ''));
    };
    /**
     * @param {?} node
     * @return {?}
     */
    Meta.prototype.getNodeId = /**
     * @param {?} node
     * @return {?}
     */
    function (node) {
        if (this.inited) {
            /** @type {?} */
            var idField = _.get(_.find(this.nodeTypes, { name: _.get(node, '_type') }), 'idField');
            return idField === 'id' ? _.get(node, '_id') : _.get(node, idField, _.get(node, '_id'));
        }
    };
    /**
     * @param {?} type
     * @return {?}
     */
    Meta.prototype.getIdFieldForType = /**
     * @param {?} type
     * @return {?}
     */
    function (type) {
        return _.get(_.find(this.nodeTypes, { name: type }), 'idField', '_id');
    };
    /**
     * @param {?} node
     * @return {?}
     */
    Meta.prototype.buildRelationDataByNodeInfo = /**
     * @param {?} node
     * @return {?}
     */
    function (node) {
        var _this = this;
        /** @type {?} */
        var relationCountMap = {};
        /** @type {?} */
        var relationCounts = [];
        /** @type {?} */
        var groups = {};
        _.each(['in', 'out'], (/**
         * @param {?} infoDirection
         * @return {?}
         */
        function (infoDirection) {
            _.each(_.get(node, ['_info', infoDirection]), (/**
             * @param {?} info
             * @param {?} relationType
             * @return {?}
             */
            function (info, relationType) {
                /** @type {?} */
                var historyRelationCounts = _this.getHistoryRelationCountsByInfo(info);
                /** @type {?} */
                var relationCount = historyRelationCounts.all;
                /** @type {?} */
                var direction = _this.getDirection(infoDirection);
                /** @type {?} */
                var relationTypeMeta = _this.getRelationTypeMeta(relationType, direction);
                /** @type {?} */
                var groupKey = relationTypeMeta.group;
                /** @type {?} */
                var relationGroupMeta = _this.getRelationGroupMeta(groupKey);
                /** @type {?} */
                var mergedTypeInfo = _this.getMergedTypeInfoByRelationType(relationType, direction);
                /** @type {?} */
                var mergedTypeKey = mergedTypeInfo.mergedType && _this.buildNodeRelationKey(direction, mergedTypeInfo.mergedType);
                /** @type {?} */
                var mergedTypeCountData = relationCountMap[mergedTypeKey];
                /** @type {?} */
                var relationKey = _this.buildNodeRelationKey(direction, relationType);
                /** @type {?} */
                var relationPluralKey = _this.buildNodeRelationPluralKey(direction, relationType);
                /** @type {?} */
                var mergedTypeRelationCount = 0;
                /** @type {?} */
                var group = groups[groupKey] = groups[groupKey] || {
                    key: groupKey,
                    order: relationGroupMeta.order,
                    relationCounts: {},
                };
                // TODO Объединить код с mergedTypeCountData
                /** @type {?} */
                var countData = {
                    key: relationKey,
                    order: relationTypeMeta.order,
                    top: relationTypeMeta.top,
                    history: relationTypeMeta.history,
                    relationType: relationType,
                    direction: direction,
                    relationCount: relationCount,
                    historyRelationCounts: historyRelationCounts,
                    pluralKey: relationPluralKey,
                };
                /** @type {?} */
                var mergedTypeHistoryRelationCounts = {
                    actual: 0,
                    outdated: 0,
                    all: 0,
                };
                _.forEach(!mergedTypeCountData && mergedTypeInfo.relationTypes, (/**
                 * @param {?} t
                 * @return {?}
                 */
                function (t) {
                    /** @type {?} */
                    var info = _.get(node, ['_info', infoDirection, t]);
                    /** @type {?} */
                    var counts = info ? _this.getHistoryRelationCountsByInfo(info) : null;
                    if (!counts) {
                        return;
                    }
                    mergedTypeHistoryRelationCounts.actual += counts.actual;
                    mergedTypeHistoryRelationCounts.outdated += counts.outdated;
                    mergedTypeHistoryRelationCounts.all += counts.all;
                }));
                mergedTypeRelationCount = mergedTypeHistoryRelationCounts.all;
                if (!mergedTypeCountData && mergedTypeRelationCount > relationCount) {
                    /** @type {?} */
                    var mergedType = mergedTypeInfo.mergedType;
                    /** @type {?} */
                    var mergedTypeMeta = _this.getRelationTypeMeta(mergedType, direction);
                    // TODO Объединить код с countData
                    mergedTypeCountData = {
                        key: mergedTypeKey,
                        order: mergedTypeMeta.order,
                        top: mergedTypeMeta.top,
                        history: mergedTypeMeta.history,
                        relationType: mergedType,
                        direction: direction,
                        relationCount: mergedTypeRelationCount,
                        historyRelationCounts: mergedTypeHistoryRelationCounts,
                        pluralKey: _this.buildNodeRelationPluralKey(direction, mergedType),
                    };
                    group.relationCounts[mergedTypeKey] = mergedTypeCountData;
                    relationCountMap[mergedTypeKey] = mergedTypeCountData;
                }
                if (!relationCountMap[mergedTypeKey]) {
                    group.relationCounts[relationKey] = countData;
                }
                relationCountMap[relationKey] = countData;
            }));
        }));
        groups = _.sortBy(groups, 'order');
        _.forEach(groups, (/**
         * @param {?} group
         * @return {?}
         */
        function (group) {
            group.relationCounts = _.sortBy(group.relationCounts, 'order');
            relationCounts = relationCounts.concat(group.relationCounts);
        }));
        return {
            relationCountMap: relationCountMap,
            relationCounts: relationCounts,
            groups: groups,
        };
    };
    /**
     * @param {?} info
     * @return {?}
     */
    Meta.prototype.getHistoryRelationCountsByInfo = /**
     * @param {?} info
     * @return {?}
     */
    function (info) {
        info = info || {};
        return {
            actual: info.actual || 0,
            all: (info.actual || 0) + (info.outdated || 0),
            outdated: info.outdated || 0,
        };
    };
    /**
     * @param {?} infoDirection
     * @return {?}
     */
    Meta.prototype.getDirection = /**
     * @param {?} infoDirection
     * @return {?}
     */
    function (infoDirection) {
        return infoDirection === 'in' ? 'parents' : 'children';
    };
    /**
     * @param {?} relationType
     * @param {?} direction
     * @return {?}
     */
    Meta.prototype.getRelationTypeMeta = /**
     * @param {?} relationType
     * @param {?} direction
     * @return {?}
     */
    function (relationType, direction) {
        return _.get(rsearchMeta.relationTypes, [relationType, direction]);
    };
    /**
     * @param {?} relationGroup
     * @return {?}
     */
    Meta.prototype.getRelationGroupMeta = /**
     * @param {?} relationGroup
     * @return {?}
     */
    function (relationGroup) {
        return rsearchMeta.relationGroups[relationGroup];
    };
    /**
     * @param {?} relationType
     * @param {?} direction
     * @return {?}
     */
    Meta.prototype.getMergedTypeInfoByRelationType = /**
     * @param {?} relationType
     * @param {?} direction
     * @return {?}
     */
    function (relationType, direction) {
        /** @type {?} */
        var info = {
            mergedType: null,
            relationTypes: null,
        };
        _.forEach(rsearchMeta.mergedRelationTypes, (/**
         * @param {?} byDirections
         * @param {?} mergedType
         * @return {?}
         */
        function (byDirections, mergedType) {
            /** @type {?} */
            var relationTypes = byDirections[direction];
            if (_.includes(relationTypes, relationType)) {
                info.mergedType = mergedType;
                info.relationTypes = relationTypes;
                return false;
            }
        }));
        return info;
    };
    /**
     * @param {?} direction
     * @param {?} relationType
     * @return {?}
     */
    Meta.prototype.buildNodeRelationKey = /**
     * @param {?} direction
     * @param {?} relationType
     * @return {?}
     */
    function (direction, relationType) {
        return direction + '::' + relationType;
    };
    /**
     * @param {?} direction
     * @param {?} relationType
     * @return {?}
     */
    Meta.prototype.buildNodeRelationPluralKey = /**
     * @param {?} direction
     * @param {?} relationType
     * @return {?}
     */
    function (direction, relationType) {
        return 'NG_PLURALIZE::RELATION_' + relationType + '-' + direction;
    };
    /**
     * @param {?} node
     * @return {?}
     */
    Meta.prototype.buildNodeExtraMeta = /**
     * @param {?} node
     * @return {?}
     */
    function (node) {
        if (!node) {
            return;
        }
        // uid
        this.buildNodeUID(node);
        //
        node.__relationData = metaService.buildRelationDataByNodeInfo(node);
        //
        node.__idField = _.get(this.nodeTypesMeta[node._type], 'idField');
        if (node._type === 'COMPANY' || node._type === 'EGRUL') { // компания
            this.buildCompanyState(node);
            node.__isFavoritesSupported = true;
        }
        else if (node._type === 'INDIVIDUAL') { // физик
            node.__isFavoritesSupported = true;
        }
        else if (node._type === 'PURCHASE') { // закупка
            node.currency = node.currency || 'RUB';
            node.__lotMap = _.keyBy(node.lots, 'lot');
            this.resolvePurchaseHref(node);
        }
    };
    /**
     * @param {?} node
     * @return {?}
     */
    Meta.prototype.buildNodeUID = /**
     * @param {?} node
     * @return {?}
     */
    function (node) {
        node.__uid = node.$$hashKey = node.__uid || this.buildNodeUIDByType(node._id, node._type);
        return node.__uid;
    };
    /**
     * @param {?} id
     * @param {?} type
     * @return {?}
     */
    Meta.prototype.buildNodeUIDByType = /**
     * @param {?} id
     * @param {?} type
     * @return {?}
     */
    function (id, type) {
        return ('node-' + type + '-' + id);
    };
    /**
     * @param {?} node
     * @return {?}
     */
    Meta.prototype.buildCompanyState = /**
     * @param {?} node
     * @return {?}
     */
    function (node) {
        // юридическое состояние
        /** @type {?} */
        var egrulState = node.egrul_state;
        /** @type {?} */
        var aliveCode = '5';
        // действующее
        /** @type {?} */
        var intermediateCodes = ['6', '111', '121', '122', '123', '124', '131', '132'];
        // коды промежуточных состояний
        /** @type {?} */
        var _liquidate;
        if (node.egrul_liq) {
            _liquidate = {
                state: {
                    _actual: null,
                    _since: node.dead_dt,
                    type: _.upperFirst(_.lowerCase(node.egrul_liq.type)),
                },
            };
        }
        else if (node.dead_dt) {
            _liquidate = {
                state: {
                    _actual: null,
                    _since: node.dead_dt,
                    type: 'Ликвидировано',
                },
            };
        }
        else if (egrulState) {
            if (egrulState.code !== aliveCode) {
                _liquidate = {
                    state: {
                        _actual: egrulState._actual,
                        _since: egrulState._since,
                        type: egrulState.type,
                        intermediate: _.includes(intermediateCodes, egrulState.code),
                    },
                };
            }
        }
        node._liquidate = _liquidate;
        // способ организации компании
        /** @type {?} */
        var egrulReg = node.egrul_reg;
        /** @type {?} */
        var baseCodes = ['11', '01', '03'];
        if (egrulReg && !_.includes(baseCodes, egrulReg.code)) {
            node._reg = {
                state: {
                    _actual: egrulReg._actual,
                    _since: egrulReg._since,
                    type: egrulReg.type,
                },
            };
        }
    };
    /**
     * @param {?} node
     * @return {?}
     */
    Meta.prototype.resolvePurchaseHref = /**
     * @param {?} node
     * @return {?}
     */
    function (node) {
        if (node.law === 'FZ_44') {
            node.__href = node.href;
        }
        else {
            node.__href = 'http://zakupki.gov.ru/epz/order/quicksearch/search_eis.html?strictEqual=on&pageNumber=1&sortDirection=false&recordsPerPage=_10&showLotsInfoHidden=false&fz44=on&fz223=on&fz94=on&regions=&priceFrom=&priceTo=&currencyId=-1&publishDateFrom=&publishDateTo=&updateDateFrom=&updateDateTo=&sortBy=UPDATE_DATE&searchString=' + node._id;
        }
    };
    return Meta;
}());
//todo покрыть тестами
export { Meta };
if (false) {
    /** @type {?} */
    Meta.prototype.relationTypes;
    /** @type {?} */
    Meta.prototype.nodeTypes;
    /** @type {?} */
    Meta.prototype.SEARCHABLE_TYPES;
    /** @type {?} */
    Meta.prototype.state;
    /** @type {?} */
    Meta.prototype.nodeTypesMeta;
    /** @type {?} */
    Meta.prototype.relationTypesMeta;
}
/** @type {?} */
var metaService = new Meta();
export { metaService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWV0YVNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AbmtiL2FuZ3VsYXItbmtiLWxpYi8iLCJzb3VyY2VzIjpbImxpYi9tZXRhL21ldGFTZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7OztBQUlBLE9BQU8sS0FBSyxNQUFNLE9BQU8sQ0FBQzs7QUFHMUIsT0FBTyxLQUFLLENBQUMsTUFBTSxRQUFRLENBQUM7QUFFNUIsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQzVDLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxlQUFlLENBQUM7Ozs7QUFFNUMsOEJBT0M7OztJQU5HLHdCQUFnQjs7SUFDaEIsNkJBQVU7O0lBQ1YsaUNBQWM7O0lBQ2QsMkJBQVE7O0lBQ1IsNkJBQVU7O0lBQ1YsOEJBQTRDOzs7QUFHaEQ7OztJQUFBO1FBR0kscUJBQWdCLEdBQUcsQ0FBQyxTQUFTLENBQUMsT0FBTyxFQUFFLFNBQVMsQ0FBQyxVQUFVLEVBQUUsU0FBUyxDQUFDLE9BQU8sRUFBRSxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUE7UUFDaEcsVUFBSyxHQUFHO1lBQ0osTUFBTSxFQUFFLEtBQUs7WUFDYixPQUFPLEVBQUUsS0FBSztZQUNkLEtBQUssRUFBRSxLQUFLO1NBQ2YsQ0FBQTs7UUFHRCxrQkFBYSxHQUFHLEVBQUUsQ0FBQTtRQUNsQixzQkFBaUIsR0FBRyxFQUFFLENBQUE7SUE0VTFCLENBQUM7SUF6VUcsc0JBQUksd0JBQU07Ozs7UUFBVjtZQUNJLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUM7UUFDN0IsQ0FBQzs7O09BQUE7Ozs7SUFFSyxtQkFBSTs7O0lBQVY7Ozs7Ozs7NkJBQ1EsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sRUFBbEIsd0JBQWtCOzZCQUNkLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLEVBQW5CLHdCQUFtQjt3QkFDbkIsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDOzs7O3dCQUV0QixLQUFBLElBQUksQ0FBQTt3QkFBaUIscUJBQU0sS0FBSyxDQUFDLEdBQUcsQ0FBQyxzQ0FBc0MsQ0FBQyxDQUFDLElBQUk7Ozs7NEJBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxHQUFHLENBQUMsSUFBSSxFQUFSLENBQVEsRUFBQyxFQUFBOzt3QkFBbEcsR0FBSyxhQUFhLEdBQUcsU0FBNkUsQ0FBQzt3QkFDbkcsS0FBQSxJQUFJLENBQUE7d0JBQWEscUJBQU0sS0FBSyxDQUFDLEdBQUcsQ0FBYSxrQ0FBa0MsQ0FBQyxDQUFDLElBQUk7Ozs7NEJBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxHQUFHLENBQUMsSUFBSSxFQUFSLENBQVEsRUFBQyxFQUFBOzt3QkFBdEcsR0FBSyxTQUFTLEdBQUcsU0FBcUYsQ0FBQzt3QkFFdkcsb0VBQW9FO3dCQUNwRSxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxTQUFTOzs7O3dCQUFFLFVBQUMsUUFBUTs0QkFDL0IsS0FBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEdBQUcsUUFBUSxDQUFDO3dCQUNqRCxDQUFDLEVBQUMsQ0FBQzt3QkFDSCxDQUFDLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxTQUFTOzs7Ozt3QkFBRSxVQUFDLFFBQVEsRUFBRSxZQUFZOzRCQUNwRCxLQUFJLENBQUMsYUFBYSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQ3ZDLEVBQUUsRUFDRixLQUFJLENBQUMsYUFBYSxDQUFDLFlBQVksQ0FBQyxFQUNoQyxRQUFRLENBQ1gsQ0FBQzt3QkFDTixDQUFDLEVBQUMsQ0FBQzt3QkFFSCxvQkFBb0I7d0JBQ3BCLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLGFBQWE7Ozs7d0JBQUUsVUFBQyxZQUFZOzRCQUN2QyxLQUFJLENBQUMsaUJBQWlCLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxHQUFHLFlBQVksQ0FBQzt3QkFDN0QsQ0FBQyxFQUFDLENBQUM7d0JBQ0gsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsYUFBYTs7Ozs7d0JBQUUsVUFBQyxZQUFZLEVBQUUsZ0JBQWdCOzRCQUNoRSxLQUFJLENBQUMsaUJBQWlCLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxDQUFDLENBQUMsTUFBTSxDQUMvQyxFQUFFLEVBQ0YsS0FBSSxDQUFDLGlCQUFpQixDQUFDLGdCQUFnQixDQUFDLEVBQ3hDLFlBQVksQ0FDZixDQUFDO3dCQUNOLENBQUMsRUFBQyxDQUFDO3dCQUNILHdFQUF3RTt3QkFHeEUsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDOzs7O3dCQUd6QixPQUFPLENBQUMsS0FBSyxDQUFDLEtBQUcsQ0FBQyxDQUFDO3dCQUNuQixJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssR0FBRyxLQUFHLENBQUM7Ozt3QkFHdkIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO3dCQUNuQyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7Ozs0QkFJL0Isc0JBQU8sSUFBSSxPQUFPOzs7O3dCQUFDLFVBQUMsT0FBTzs7Z0NBQ2pCLFFBQVEsR0FBRyxXQUFXOzs7NEJBQUM7Z0NBQ3pCLElBQUksS0FBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEVBQUU7b0NBQ25CLE9BQU8sRUFBRSxDQUFDO29DQUNWLGFBQWEsQ0FBQyxRQUFRLENBQUMsQ0FBQztpQ0FDM0I7NEJBQ0wsQ0FBQyxHQUFFLElBQUksQ0FBQzt3QkFDWixDQUFDLEVBQUMsRUFBQzs7Ozs7S0FJZDs7Ozs7SUFFRCwwQkFBVzs7OztJQUFYLFVBQVksSUFBSTs7WUFDTixTQUFTLEdBQUcsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsT0FBTyxDQUFDLEVBQUUsQ0FBQyxFQUFFLFdBQVcsQ0FBQztRQUM1RixPQUFPLENBQUMsQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsT0FBTyxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUM7SUFDOUQsQ0FBQzs7Ozs7SUFFRCx3QkFBUzs7OztJQUFULFVBQVUsSUFBSTtRQUNWLElBQUksSUFBSSxDQUFDLE1BQU0sRUFBRTs7Z0JBQ1AsT0FBTyxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLE9BQU8sQ0FBQyxFQUFFLENBQUMsRUFBRSxTQUFTLENBQUM7WUFDeEYsT0FBTyxPQUFPLEtBQUssSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsT0FBTyxFQUFFLENBQUMsQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDLENBQUM7U0FDM0Y7SUFDTCxDQUFDOzs7OztJQUVELGdDQUFpQjs7OztJQUFqQixVQUFrQixJQUFJO1FBQ2xCLE9BQU8sQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLENBQUMsRUFBRSxTQUFTLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDM0UsQ0FBQzs7Ozs7SUFFRCwwQ0FBMkI7Ozs7SUFBM0IsVUFBNEIsSUFBSTtRQUFoQyxpQkF3R0M7O1lBdkdTLGdCQUFnQixHQUFHLEVBQUU7O1lBQ3ZCLGNBQWMsR0FBRyxFQUFFOztZQUNuQixNQUFNLEdBQVEsRUFBRTtRQUVwQixDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQzs7OztRQUFFLFVBQUMsYUFBYTtZQUNoQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLENBQUMsT0FBTyxFQUFFLGFBQWEsQ0FBQyxDQUFDOzs7OztZQUFFLFVBQUMsSUFBSSxFQUFFLFlBQVk7O29CQUV2RCxxQkFBcUIsR0FBRyxLQUFJLENBQUMsOEJBQThCLENBQUMsSUFBSSxDQUFDOztvQkFDakUsYUFBYSxHQUFHLHFCQUFxQixDQUFDLEdBQUc7O29CQUV6QyxTQUFTLEdBQUcsS0FBSSxDQUFDLFlBQVksQ0FBQyxhQUFhLENBQUM7O29CQUM1QyxnQkFBZ0IsR0FBRyxLQUFJLENBQUMsbUJBQW1CLENBQUMsWUFBWSxFQUFFLFNBQVMsQ0FBQzs7b0JBQ3BFLFFBQVEsR0FBRyxnQkFBZ0IsQ0FBQyxLQUFLOztvQkFDakMsaUJBQWlCLEdBQUcsS0FBSSxDQUFDLG9CQUFvQixDQUFDLFFBQVEsQ0FBQzs7b0JBQ3ZELGNBQWMsR0FBRyxLQUFJLENBQUMsK0JBQStCLENBQUMsWUFBWSxFQUFFLFNBQVMsQ0FBQzs7b0JBQzlFLGFBQWEsR0FBRyxjQUFjLENBQUMsVUFBVSxJQUFJLEtBQUksQ0FBQyxvQkFBb0IsQ0FBQyxTQUFTLEVBQUUsY0FBYyxDQUFDLFVBQVUsQ0FBQzs7b0JBQzlHLG1CQUFtQixHQUFHLGdCQUFnQixDQUFDLGFBQWEsQ0FBQzs7b0JBQ25ELFdBQVcsR0FBRyxLQUFJLENBQUMsb0JBQW9CLENBQUMsU0FBUyxFQUFFLFlBQVksQ0FBQzs7b0JBQ2hFLGlCQUFpQixHQUFHLEtBQUksQ0FBQywwQkFBMEIsQ0FBQyxTQUFTLEVBQUUsWUFBWSxDQUFDOztvQkFDOUUsdUJBQXVCLEdBQUcsQ0FBQzs7b0JBRXpCLEtBQUssR0FBRyxNQUFNLENBQUMsUUFBUSxDQUFDLEdBQUcsTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJO29CQUNqRCxHQUFHLEVBQUUsUUFBUTtvQkFDYixLQUFLLEVBQUUsaUJBQWlCLENBQUMsS0FBSztvQkFDOUIsY0FBYyxFQUFFLEVBQUU7aUJBQ3JCOzs7b0JBR0ssU0FBUyxHQUFHO29CQUNkLEdBQUcsRUFBRSxXQUFXO29CQUNoQixLQUFLLEVBQUUsZ0JBQWdCLENBQUMsS0FBSztvQkFDN0IsR0FBRyxFQUFFLGdCQUFnQixDQUFDLEdBQUc7b0JBQ3pCLE9BQU8sRUFBRSxnQkFBZ0IsQ0FBQyxPQUFPO29CQUNqQyxZQUFZLGNBQUE7b0JBQ1osU0FBUyxXQUFBO29CQUNULGFBQWEsZUFBQTtvQkFDYixxQkFBcUIsdUJBQUE7b0JBQ3JCLFNBQVMsRUFBRSxpQkFBaUI7aUJBQy9COztvQkFFSywrQkFBK0IsR0FBRztvQkFDcEMsTUFBTSxFQUFFLENBQUM7b0JBQ1QsUUFBUSxFQUFFLENBQUM7b0JBQ1gsR0FBRyxFQUFFLENBQUM7aUJBQ1Q7Z0JBRUQsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLG1CQUFtQixJQUFJLGNBQWMsQ0FBQyxhQUFhOzs7O2dCQUFFLFVBQUMsQ0FBQzs7d0JBQ3hELElBQUksR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxDQUFDLE9BQU8sRUFBRSxhQUFhLEVBQUUsQ0FBQyxDQUFDLENBQUM7O3dCQUMvQyxNQUFNLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFJLENBQUMsOEJBQThCLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUk7b0JBRXRFLElBQUksQ0FBQyxNQUFNLEVBQUU7d0JBQ1QsT0FBTztxQkFDVjtvQkFFRCwrQkFBK0IsQ0FBQyxNQUFNLElBQUksTUFBTSxDQUFDLE1BQU0sQ0FBQztvQkFDeEQsK0JBQStCLENBQUMsUUFBUSxJQUFJLE1BQU0sQ0FBQyxRQUFRLENBQUM7b0JBQzVELCtCQUErQixDQUFDLEdBQUcsSUFBSSxNQUFNLENBQUMsR0FBRyxDQUFDO2dCQUV0RCxDQUFDLEVBQUMsQ0FBQztnQkFFSCx1QkFBdUIsR0FBRywrQkFBK0IsQ0FBQyxHQUFHLENBQUM7Z0JBRTlELElBQUksQ0FBQyxtQkFBbUIsSUFBSSx1QkFBdUIsR0FBRyxhQUFhLEVBQUU7O3dCQUMzRCxVQUFVLEdBQUcsY0FBYyxDQUFDLFVBQVU7O3dCQUN0QyxjQUFjLEdBQUcsS0FBSSxDQUFDLG1CQUFtQixDQUFDLFVBQVUsRUFBRSxTQUFTLENBQUM7b0JBRXRFLGtDQUFrQztvQkFDbEMsbUJBQW1CLEdBQUc7d0JBQ2xCLEdBQUcsRUFBRSxhQUFhO3dCQUNsQixLQUFLLEVBQUUsY0FBYyxDQUFDLEtBQUs7d0JBQzNCLEdBQUcsRUFBRSxjQUFjLENBQUMsR0FBRzt3QkFDdkIsT0FBTyxFQUFFLGNBQWMsQ0FBQyxPQUFPO3dCQUMvQixZQUFZLEVBQUUsVUFBVTt3QkFDeEIsU0FBUyxXQUFBO3dCQUNULGFBQWEsRUFBRSx1QkFBdUI7d0JBQ3RDLHFCQUFxQixFQUFFLCtCQUErQjt3QkFDdEQsU0FBUyxFQUFFLEtBQUksQ0FBQywwQkFBMEIsQ0FBQyxTQUFTLEVBQUUsVUFBVSxDQUFDO3FCQUNwRSxDQUFDO29CQUVGLEtBQUssQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUFDLEdBQUcsbUJBQW1CLENBQUM7b0JBQzFELGdCQUFnQixDQUFDLGFBQWEsQ0FBQyxHQUFHLG1CQUFtQixDQUFDO2lCQUN6RDtnQkFFRCxJQUFJLENBQUMsZ0JBQWdCLENBQUMsYUFBYSxDQUFDLEVBQUU7b0JBQ2xDLEtBQUssQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDLEdBQUcsU0FBUyxDQUFDO2lCQUNqRDtnQkFFRCxnQkFBZ0IsQ0FBQyxXQUFXLENBQUMsR0FBRyxTQUFTLENBQUM7WUFDOUMsQ0FBQyxFQUFDLENBQUM7UUFDUCxDQUFDLEVBQUMsQ0FBQztRQUVILE1BQU0sR0FBRyxDQUFDLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRSxPQUFPLENBQUMsQ0FBQztRQUVuQyxDQUFDLENBQUMsT0FBTyxDQUFDLE1BQU07Ozs7UUFBRSxVQUFDLEtBQUs7WUFDcEIsS0FBSyxDQUFDLGNBQWMsR0FBRyxDQUFDLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxjQUFjLEVBQUUsT0FBTyxDQUFDLENBQUM7WUFDL0QsY0FBYyxHQUFHLGNBQWMsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyxDQUFDO1FBQ2pFLENBQUMsRUFBQyxDQUFDO1FBRUgsT0FBTztZQUNILGdCQUFnQixrQkFBQTtZQUNoQixjQUFjLGdCQUFBO1lBQ2QsTUFBTSxRQUFBO1NBQ1QsQ0FBQztJQUNOLENBQUM7Ozs7O0lBRUQsNkNBQThCOzs7O0lBQTlCLFVBQStCLElBQUk7UUFDL0IsSUFBSSxHQUFHLElBQUksSUFBSSxFQUFFLENBQUM7UUFDbEIsT0FBTztZQUNILE1BQU0sRUFBRSxJQUFJLENBQUMsTUFBTSxJQUFJLENBQUM7WUFDeEIsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLE1BQU0sSUFBSSxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxRQUFRLElBQUksQ0FBQyxDQUFDO1lBQzlDLFFBQVEsRUFBRSxJQUFJLENBQUMsUUFBUSxJQUFJLENBQUM7U0FFL0IsQ0FBQztJQUNOLENBQUM7Ozs7O0lBRUQsMkJBQVk7Ozs7SUFBWixVQUFhLGFBQWE7UUFDdEIsT0FBTyxhQUFhLEtBQUssSUFBSSxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQztJQUMzRCxDQUFDOzs7Ozs7SUFFRCxrQ0FBbUI7Ozs7O0lBQW5CLFVBQW9CLFlBQVksRUFBRSxTQUFTO1FBQ3ZDLE9BQU8sQ0FBQyxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUMsYUFBYSxFQUFFLENBQUMsWUFBWSxFQUFFLFNBQVMsQ0FBQyxDQUFDLENBQUM7SUFDdkUsQ0FBQzs7Ozs7SUFFRCxtQ0FBb0I7Ozs7SUFBcEIsVUFBcUIsYUFBYTtRQUM5QixPQUFPLFdBQVcsQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUFDLENBQUM7SUFDckQsQ0FBQzs7Ozs7O0lBRUQsOENBQStCOzs7OztJQUEvQixVQUFnQyxZQUFZLEVBQUUsU0FBUzs7WUFDN0MsSUFBSSxHQUFHO1lBQ1QsVUFBVSxFQUFFLElBQUk7WUFDaEIsYUFBYSxFQUFFLElBQUk7U0FDdEI7UUFFRCxDQUFDLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxtQkFBbUI7Ozs7O1FBQUUsVUFBQyxZQUFZLEVBQUUsVUFBVTs7Z0JBQzFELGFBQWEsR0FBRyxZQUFZLENBQUMsU0FBUyxDQUFDO1lBQzdDLElBQUksQ0FBQyxDQUFDLFFBQVEsQ0FBQyxhQUFhLEVBQUUsWUFBWSxDQUFDLEVBQUU7Z0JBQ3pDLElBQUksQ0FBQyxVQUFVLEdBQUcsVUFBVSxDQUFDO2dCQUM3QixJQUFJLENBQUMsYUFBYSxHQUFHLGFBQWEsQ0FBQztnQkFDbkMsT0FBTyxLQUFLLENBQUM7YUFDaEI7UUFDTCxDQUFDLEVBQUMsQ0FBQztRQUVILE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7Ozs7OztJQUVELG1DQUFvQjs7Ozs7SUFBcEIsVUFBcUIsU0FBUyxFQUFFLFlBQVk7UUFDeEMsT0FBTyxTQUFTLEdBQUcsSUFBSSxHQUFHLFlBQVksQ0FBQztJQUMzQyxDQUFDOzs7Ozs7SUFFRCx5Q0FBMEI7Ozs7O0lBQTFCLFVBQTJCLFNBQVMsRUFBRSxZQUFZO1FBQzlDLE9BQU8seUJBQXlCLEdBQUcsWUFBWSxHQUFHLEdBQUcsR0FBRyxTQUFTLENBQUM7SUFDdEUsQ0FBQzs7Ozs7SUFFRCxpQ0FBa0I7Ozs7SUFBbEIsVUFBbUIsSUFBSTtRQUNuQixJQUFJLENBQUMsSUFBSSxFQUFFO1lBQ1AsT0FBTztTQUNWO1FBRUQsTUFBTTtRQUNOLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7UUFFeEIsRUFBRTtRQUNGLElBQUksQ0FBQyxjQUFjLEdBQUcsV0FBVyxDQUFDLDJCQUEyQixDQUFDLElBQUksQ0FBQyxDQUFDO1FBRXBFLEVBQUU7UUFDRixJQUFJLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsU0FBUyxDQUFDLENBQUM7UUFFbEUsSUFBSSxJQUFJLENBQUMsS0FBSyxLQUFLLFNBQVMsSUFBSSxJQUFJLENBQUMsS0FBSyxLQUFLLE9BQU8sRUFBRSxFQUFFLFdBQVc7WUFDakUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzdCLElBQUksQ0FBQyxzQkFBc0IsR0FBRyxJQUFJLENBQUM7U0FDdEM7YUFBTSxJQUFJLElBQUksQ0FBQyxLQUFLLEtBQUssWUFBWSxFQUFFLEVBQWMsUUFBUTtZQUMxRCxJQUFJLENBQUMsc0JBQXNCLEdBQUcsSUFBSSxDQUFDO1NBQ3RDO2FBQU0sSUFBSSxJQUFJLENBQUMsS0FBSyxLQUFLLFVBQVUsRUFBRSxFQUFFLFVBQVU7WUFDOUMsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsUUFBUSxJQUFJLEtBQUssQ0FBQztZQUN2QyxJQUFJLENBQUMsUUFBUSxHQUFHLENBQUMsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxLQUFLLENBQUMsQ0FBQztZQUMxQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDbEM7SUFFTCxDQUFDOzs7OztJQUNELDJCQUFZOzs7O0lBQVosVUFBYSxJQUFJO1FBQ2IsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxLQUFLLElBQUksSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzFGLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQztJQUN0QixDQUFDOzs7Ozs7SUFFRCxpQ0FBa0I7Ozs7O0lBQWxCLFVBQW1CLEVBQUUsRUFBRSxJQUFJO1FBQ3ZCLE9BQU8sQ0FBQyxPQUFPLEdBQUcsSUFBSSxHQUFHLEdBQUcsR0FBRyxFQUFFLENBQUMsQ0FBQztJQUN2QyxDQUFDOzs7OztJQUVELGdDQUFpQjs7OztJQUFqQixVQUFrQixJQUFJOzs7WUFFZCxVQUFVLEdBQUcsSUFBSSxDQUFDLFdBQVc7O1lBQzdCLFNBQVMsR0FBRyxHQUFHOzs7WUFDZixpQkFBaUIsR0FBRyxDQUFDLEdBQUcsRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLENBQUM7OztZQUMxRSxVQUFVO1FBQ2QsSUFBSSxJQUFJLENBQUMsU0FBUyxFQUFFO1lBQ2hCLFVBQVUsR0FBRztnQkFDVCxLQUFLLEVBQUU7b0JBQ0gsT0FBTyxFQUFFLElBQUk7b0JBQ2IsTUFBTSxFQUFFLElBQUksQ0FBQyxPQUFPO29CQUNwQixJQUFJLEVBQUUsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUM7aUJBQ3ZEO2FBQ0osQ0FBQztTQUNMO2FBQU0sSUFBSSxJQUFJLENBQUMsT0FBTyxFQUFFO1lBQ3JCLFVBQVUsR0FBRztnQkFDVCxLQUFLLEVBQUU7b0JBQ0gsT0FBTyxFQUFFLElBQUk7b0JBQ2IsTUFBTSxFQUFFLElBQUksQ0FBQyxPQUFPO29CQUNwQixJQUFJLEVBQUUsZUFBZTtpQkFDeEI7YUFDSixDQUFDO1NBQ0w7YUFBTSxJQUFJLFVBQVUsRUFBRTtZQUNuQixJQUFJLFVBQVUsQ0FBQyxJQUFJLEtBQUssU0FBUyxFQUFFO2dCQUMvQixVQUFVLEdBQUc7b0JBQ1QsS0FBSyxFQUFFO3dCQUNILE9BQU8sRUFBRSxVQUFVLENBQUMsT0FBTzt3QkFDM0IsTUFBTSxFQUFFLFVBQVUsQ0FBQyxNQUFNO3dCQUN6QixJQUFJLEVBQUUsVUFBVSxDQUFDLElBQUk7d0JBQ3JCLFlBQVksRUFBRSxDQUFDLENBQUMsUUFBUSxDQUFDLGlCQUFpQixFQUFFLFVBQVUsQ0FBQyxJQUFJLENBQUM7cUJBQy9EO2lCQUNKLENBQUM7YUFDTDtTQUNKO1FBRUQsSUFBSSxDQUFDLFVBQVUsR0FBRyxVQUFVLENBQUM7OztZQUd2QixRQUFRLEdBQUcsSUFBSSxDQUFDLFNBQVM7O1lBQ3pCLFNBQVMsR0FBRyxDQUFDLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDO1FBRXBDLElBQUksUUFBUSxJQUFJLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxTQUFTLEVBQUUsUUFBUSxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQ25ELElBQUksQ0FBQyxJQUFJLEdBQUc7Z0JBQ1IsS0FBSyxFQUFFO29CQUNILE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTztvQkFDekIsTUFBTSxFQUFFLFFBQVEsQ0FBQyxNQUFNO29CQUN2QixJQUFJLEVBQUUsUUFBUSxDQUFDLElBQUk7aUJBQ3RCO2FBQ0osQ0FBQztTQUNMO0lBQ0wsQ0FBQzs7Ozs7SUFFRCxrQ0FBbUI7Ozs7SUFBbkIsVUFBb0IsSUFBSTtRQUNwQixJQUFJLElBQUksQ0FBQyxHQUFHLEtBQUssT0FBTyxFQUFFO1lBQ3RCLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQztTQUMzQjthQUFNO1lBQ0gsSUFBSSxDQUFDLE1BQU0sR0FBRyw0VEFBNFQsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDO1NBQ3pWO0lBQ0wsQ0FBQztJQUdMLFdBQUM7QUFBRCxDQUFDLEFBeFZELElBd1ZDOzs7OztJQXZWRyw2QkFBYzs7SUFDZCx5QkFBc0I7O0lBQ3RCLGdDQUFnRzs7SUFDaEcscUJBSUM7O0lBR0QsNkJBQWtCOztJQUNsQixpQ0FBc0I7OztJQStVcEIsV0FBVyxHQUFHLElBQUksSUFBSSxFQUFFO0FBQzlCLE9BQU8sRUFBRSxXQUFXLEVBQUUsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbIi8qKlxyXG4gKiBDcmVhdGVkIGJ5IHJhZGlnb3N0IG9uIDA3LjA5LjE3LlxyXG4gKi9cclxuXHJcbmltcG9ydCBheGlvcyBmcm9tICdheGlvcyc7XHJcblxyXG4vL3RvZG8g0LjQvNC/0L7RgNGC0LjRgNC+0LLQsNGC0Ywg0YLQvtC70YzQutC+INGC0L4g0YfRgtC+INC90YPQttC90L5cclxuaW1wb3J0ICogYXMgXyBmcm9tICdsb2Rhc2gnO1xyXG5cclxuaW1wb3J0IHsgTk9ERV9UWVBFIH0gZnJvbSAnLi4vZG9tYWluL1RZUEVTJztcclxuaW1wb3J0IHsgcnNlYXJjaE1ldGEgfSBmcm9tICcuL3JzZWFyY2hNZXRhJztcclxuXHJcbmV4cG9ydCBpbnRlcmZhY2UgTm9kZVR5cGUge1xyXG4gICAgbmFtZTogTk9ERV9UWVBFO1xyXG4gICAgc29ydEZpZWxkO1xyXG4gICAgc29ydEFzY2VuZGluZztcclxuICAgIGlkRmllbGQ7XHJcbiAgICBuYW1lRmllbGQ7XHJcbiAgICBwcm9wZXJ0aWVzOiBbeyBbcHJvcGVydHk6IHN0cmluZ106IHN0cmluZyB9XVxyXG59XHJcbi8vdG9kbyDQv9C+0LrRgNGL0YLRjCDRgtC10YHRgtCw0LzQuFxyXG5leHBvcnQgY2xhc3MgTWV0YSB7XHJcbiAgICByZWxhdGlvblR5cGVzO1xyXG4gICAgbm9kZVR5cGVzOiBOb2RlVHlwZVtdO1xyXG4gICAgU0VBUkNIQUJMRV9UWVBFUyA9IFtOT0RFX1RZUEUuQ09NUEFOWSwgTk9ERV9UWVBFLklORElWSURVQUwsIE5PREVfVFlQRS5BRERSRVNTLCBOT0RFX1RZUEUuUEhPTkVdXHJcbiAgICBzdGF0ZSA9IHtcclxuICAgICAgICBpbml0ZWQ6IGZhbHNlLFxyXG4gICAgICAgIHBlbmRpbmc6IGZhbHNlLFxyXG4gICAgICAgIGVycm9yOiBmYWxzZSxcclxuICAgIH1cclxuXHJcbiAgICAvLyBARGVwcmVjYXRlZFxyXG4gICAgbm9kZVR5cGVzTWV0YSA9IHt9XHJcbiAgICByZWxhdGlvblR5cGVzTWV0YSA9IHt9XHJcblxyXG5cclxuICAgIGdldCBpbml0ZWQoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuc3RhdGUuaW5pdGVkO1xyXG4gICAgfVxyXG5cclxuICAgIGFzeW5jIGluaXQoKSB7XHJcbiAgICAgICAgaWYgKCF0aGlzLnN0YXRlLmluaXRlZCkge1xyXG4gICAgICAgICAgICBpZiAoIXRoaXMuc3RhdGUucGVuZGluZykge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zdGF0ZS5wZW5kaW5nID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5yZWxhdGlvblR5cGVzID0gYXdhaXQgYXhpb3MuZ2V0KCcvbmticmVsYXRpb24vYXBpL21ldGEvcmVsYXRpb24vdHlwZXMnKS50aGVuKHJlcyA9PiByZXMuZGF0YSk7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5ub2RlVHlwZXMgPSBhd2FpdCBheGlvcy5nZXQ8Tm9kZVR5cGVbXT4oJy9ua2JyZWxhdGlvbi9hcGkvbWV0YS9ub2RlL3R5cGVzJykudGhlbihyZXMgPT4gcmVzLmRhdGEpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAvLyBUT0RPIGxlZ2FjeSAtIHJlZmFjdG9yIHRoaXMgLSBhIGxvdCBvZiBhZGRpdGlvbmFsIG5vdCBuZWVkZWQgd29ya1xyXG4gICAgICAgICAgICAgICAgICAgIF8uZm9yRWFjaCh0aGlzLm5vZGVUeXBlcywgKG5vZGVUeXBlKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubm9kZVR5cGVzTWV0YVtub2RlVHlwZS5uYW1lXSA9IG5vZGVUeXBlO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIF8uZm9yRWFjaChyc2VhcmNoTWV0YS5ub2RlVHlwZXMsIChub2RlVHlwZSwgbm9kZVR5cGVOYW1lKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubm9kZVR5cGVzTWV0YVtub2RlVHlwZU5hbWVdID0gXy5leHRlbmQoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB7fSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubm9kZVR5cGVzTWV0YVtub2RlVHlwZU5hbWVdLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbm9kZVR5cGUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIC8vIHJlbGF0aW9uVHlwZXNNZXRhXHJcbiAgICAgICAgICAgICAgICAgICAgXy5mb3JFYWNoKHRoaXMucmVsYXRpb25UeXBlcywgKHJlbGF0aW9uVHlwZSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnJlbGF0aW9uVHlwZXNNZXRhW3JlbGF0aW9uVHlwZS5uYW1lXSA9IHJlbGF0aW9uVHlwZTtcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICBfLmZvckVhY2gocnNlYXJjaE1ldGEucmVsYXRpb25UeXBlcywgKHJlbGF0aW9uVHlwZSwgcmVsYXRpb25UeXBlTmFtZSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnJlbGF0aW9uVHlwZXNNZXRhW3JlbGF0aW9uVHlwZU5hbWVdID0gXy5leHRlbmQoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB7fSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucmVsYXRpb25UeXBlc01ldGFbcmVsYXRpb25UeXBlTmFtZV0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZWxhdGlvblR5cGUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gZW5kIFRPRE8gbGVnYWN5IC0gcmVmYWN0b3IgdGhpcyAtIGEgbG90IG9mIGFkZGl0aW9uYWwgbm90IG5lZWRlZCB3b3JrXHJcblxyXG5cclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnN0YXRlLmluaXRlZCA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBjYXRjaCAoZXJyKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5lcnJvcihlcnIpO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc3RhdGUuZXJyb3IgPSBlcnI7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBmaW5hbGx5IHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcIm1ldGEgc2VydmljZSBpbml0ZWRcIik7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zdGF0ZS5wZW5kaW5nID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgZWxzZSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBpbnRlcnZhbCA9IHNldEludGVydmFsKCgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMuc3RhdGUuaW5pdGVkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXNvbHZlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbGVhckludGVydmFsKGludGVydmFsKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH0sIDEwMDApO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGdldE5vZGVOYW1lKG5vZGUpIHtcclxuICAgICAgICBjb25zdCBuYW1lRmllbGQgPSBfLmdldChfLmZpbmQodGhpcy5ub2RlVHlwZXMsIHsgbmFtZTogXy5nZXQobm9kZSwgJ190eXBlJykgfSksICduYW1lRmllbGQnKTtcclxuICAgICAgICByZXR1cm4gXy5nZXQobm9kZSwgW25hbWVGaWVsZF0sIF8uZ2V0KG5vZGUsICdfbmFtZScsICcnKSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0Tm9kZUlkKG5vZGUpIHtcclxuICAgICAgICBpZiAodGhpcy5pbml0ZWQpIHtcclxuICAgICAgICAgICAgY29uc3QgaWRGaWVsZCA9IF8uZ2V0KF8uZmluZCh0aGlzLm5vZGVUeXBlcywgeyBuYW1lOiBfLmdldChub2RlLCAnX3R5cGUnKSB9KSwgJ2lkRmllbGQnKTtcclxuICAgICAgICAgICAgcmV0dXJuIGlkRmllbGQgPT09ICdpZCcgPyBfLmdldChub2RlLCAnX2lkJykgOiBfLmdldChub2RlLCBpZEZpZWxkLCBfLmdldChub2RlLCAnX2lkJykpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBnZXRJZEZpZWxkRm9yVHlwZSh0eXBlKSB7XHJcbiAgICAgICAgcmV0dXJuIF8uZ2V0KF8uZmluZCh0aGlzLm5vZGVUeXBlcywgeyBuYW1lOiB0eXBlIH0pLCAnaWRGaWVsZCcsICdfaWQnKTtcclxuICAgIH1cclxuXHJcbiAgICBidWlsZFJlbGF0aW9uRGF0YUJ5Tm9kZUluZm8obm9kZSkge1xyXG4gICAgICAgIGNvbnN0IHJlbGF0aW9uQ291bnRNYXAgPSB7fTtcclxuICAgICAgICBsZXQgcmVsYXRpb25Db3VudHMgPSBbXTtcclxuICAgICAgICBsZXQgZ3JvdXBzOiBhbnkgPSB7fTtcclxuXHJcbiAgICAgICAgXy5lYWNoKFsnaW4nLCAnb3V0J10sIChpbmZvRGlyZWN0aW9uKSA9PiB7XHJcbiAgICAgICAgICAgIF8uZWFjaChfLmdldChub2RlLCBbJ19pbmZvJywgaW5mb0RpcmVjdGlvbl0pLCAoaW5mbywgcmVsYXRpb25UeXBlKSA9PiB7XHJcblxyXG4gICAgICAgICAgICAgICAgY29uc3QgaGlzdG9yeVJlbGF0aW9uQ291bnRzID0gdGhpcy5nZXRIaXN0b3J5UmVsYXRpb25Db3VudHNCeUluZm8oaW5mbyk7XHJcbiAgICAgICAgICAgICAgICBjb25zdCByZWxhdGlvbkNvdW50ID0gaGlzdG9yeVJlbGF0aW9uQ291bnRzLmFsbDtcclxuXHJcbiAgICAgICAgICAgICAgICBjb25zdCBkaXJlY3Rpb24gPSB0aGlzLmdldERpcmVjdGlvbihpbmZvRGlyZWN0aW9uKTtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHJlbGF0aW9uVHlwZU1ldGEgPSB0aGlzLmdldFJlbGF0aW9uVHlwZU1ldGEocmVsYXRpb25UeXBlLCBkaXJlY3Rpb24pO1xyXG4gICAgICAgICAgICAgICAgY29uc3QgZ3JvdXBLZXkgPSByZWxhdGlvblR5cGVNZXRhLmdyb3VwO1xyXG4gICAgICAgICAgICAgICAgY29uc3QgcmVsYXRpb25Hcm91cE1ldGEgPSB0aGlzLmdldFJlbGF0aW9uR3JvdXBNZXRhKGdyb3VwS2V5KTtcclxuICAgICAgICAgICAgICAgIGNvbnN0IG1lcmdlZFR5cGVJbmZvID0gdGhpcy5nZXRNZXJnZWRUeXBlSW5mb0J5UmVsYXRpb25UeXBlKHJlbGF0aW9uVHlwZSwgZGlyZWN0aW9uKTtcclxuICAgICAgICAgICAgICAgIGNvbnN0IG1lcmdlZFR5cGVLZXkgPSBtZXJnZWRUeXBlSW5mby5tZXJnZWRUeXBlICYmIHRoaXMuYnVpbGROb2RlUmVsYXRpb25LZXkoZGlyZWN0aW9uLCBtZXJnZWRUeXBlSW5mby5tZXJnZWRUeXBlKTtcclxuICAgICAgICAgICAgICAgIGxldCBtZXJnZWRUeXBlQ291bnREYXRhID0gcmVsYXRpb25Db3VudE1hcFttZXJnZWRUeXBlS2V5XTtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHJlbGF0aW9uS2V5ID0gdGhpcy5idWlsZE5vZGVSZWxhdGlvbktleShkaXJlY3Rpb24sIHJlbGF0aW9uVHlwZSk7XHJcbiAgICAgICAgICAgICAgICBjb25zdCByZWxhdGlvblBsdXJhbEtleSA9IHRoaXMuYnVpbGROb2RlUmVsYXRpb25QbHVyYWxLZXkoZGlyZWN0aW9uLCByZWxhdGlvblR5cGUpO1xyXG4gICAgICAgICAgICAgICAgbGV0IG1lcmdlZFR5cGVSZWxhdGlvbkNvdW50ID0gMDtcclxuXHJcbiAgICAgICAgICAgICAgICBjb25zdCBncm91cCA9IGdyb3Vwc1tncm91cEtleV0gPSBncm91cHNbZ3JvdXBLZXldIHx8IHtcclxuICAgICAgICAgICAgICAgICAgICBrZXk6IGdyb3VwS2V5LFxyXG4gICAgICAgICAgICAgICAgICAgIG9yZGVyOiByZWxhdGlvbkdyb3VwTWV0YS5vcmRlcixcclxuICAgICAgICAgICAgICAgICAgICByZWxhdGlvbkNvdW50czoge30sXHJcbiAgICAgICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgICAgIC8vIFRPRE8g0J7QsdGK0LXQtNC40L3QuNGC0Ywg0LrQvtC0INGBIG1lcmdlZFR5cGVDb3VudERhdGFcclxuICAgICAgICAgICAgICAgIGNvbnN0IGNvdW50RGF0YSA9IHtcclxuICAgICAgICAgICAgICAgICAgICBrZXk6IHJlbGF0aW9uS2V5LFxyXG4gICAgICAgICAgICAgICAgICAgIG9yZGVyOiByZWxhdGlvblR5cGVNZXRhLm9yZGVyLFxyXG4gICAgICAgICAgICAgICAgICAgIHRvcDogcmVsYXRpb25UeXBlTWV0YS50b3AsXHJcbiAgICAgICAgICAgICAgICAgICAgaGlzdG9yeTogcmVsYXRpb25UeXBlTWV0YS5oaXN0b3J5LFxyXG4gICAgICAgICAgICAgICAgICAgIHJlbGF0aW9uVHlwZSxcclxuICAgICAgICAgICAgICAgICAgICBkaXJlY3Rpb24sXHJcbiAgICAgICAgICAgICAgICAgICAgcmVsYXRpb25Db3VudCxcclxuICAgICAgICAgICAgICAgICAgICBoaXN0b3J5UmVsYXRpb25Db3VudHMsXHJcbiAgICAgICAgICAgICAgICAgICAgcGx1cmFsS2V5OiByZWxhdGlvblBsdXJhbEtleSxcclxuICAgICAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICAgICAgY29uc3QgbWVyZ2VkVHlwZUhpc3RvcnlSZWxhdGlvbkNvdW50cyA9IHtcclxuICAgICAgICAgICAgICAgICAgICBhY3R1YWw6IDAsXHJcbiAgICAgICAgICAgICAgICAgICAgb3V0ZGF0ZWQ6IDAsXHJcbiAgICAgICAgICAgICAgICAgICAgYWxsOiAwLFxyXG4gICAgICAgICAgICAgICAgfTtcclxuXHJcbiAgICAgICAgICAgICAgICBfLmZvckVhY2goIW1lcmdlZFR5cGVDb3VudERhdGEgJiYgbWVyZ2VkVHlwZUluZm8ucmVsYXRpb25UeXBlcywgKHQpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBpbmZvID0gXy5nZXQobm9kZSwgWydfaW5mbycsIGluZm9EaXJlY3Rpb24sIHRdKTtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBjb3VudHMgPSBpbmZvID8gdGhpcy5nZXRIaXN0b3J5UmVsYXRpb25Db3VudHNCeUluZm8oaW5mbykgOiBudWxsO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICBpZiAoIWNvdW50cykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICBtZXJnZWRUeXBlSGlzdG9yeVJlbGF0aW9uQ291bnRzLmFjdHVhbCArPSBjb3VudHMuYWN0dWFsO1xyXG4gICAgICAgICAgICAgICAgICAgIG1lcmdlZFR5cGVIaXN0b3J5UmVsYXRpb25Db3VudHMub3V0ZGF0ZWQgKz0gY291bnRzLm91dGRhdGVkO1xyXG4gICAgICAgICAgICAgICAgICAgIG1lcmdlZFR5cGVIaXN0b3J5UmVsYXRpb25Db3VudHMuYWxsICs9IGNvdW50cy5hbGw7XHJcblxyXG4gICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAgICAgbWVyZ2VkVHlwZVJlbGF0aW9uQ291bnQgPSBtZXJnZWRUeXBlSGlzdG9yeVJlbGF0aW9uQ291bnRzLmFsbDtcclxuXHJcbiAgICAgICAgICAgICAgICBpZiAoIW1lcmdlZFR5cGVDb3VudERhdGEgJiYgbWVyZ2VkVHlwZVJlbGF0aW9uQ291bnQgPiByZWxhdGlvbkNvdW50KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgbWVyZ2VkVHlwZSA9IG1lcmdlZFR5cGVJbmZvLm1lcmdlZFR5cGU7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgbWVyZ2VkVHlwZU1ldGEgPSB0aGlzLmdldFJlbGF0aW9uVHlwZU1ldGEobWVyZ2VkVHlwZSwgZGlyZWN0aW9uKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gVE9ETyDQntCx0YrQtdC00LjQvdC40YLRjCDQutC+0LQg0YEgY291bnREYXRhXHJcbiAgICAgICAgICAgICAgICAgICAgbWVyZ2VkVHlwZUNvdW50RGF0YSA9IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAga2V5OiBtZXJnZWRUeXBlS2V5LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBvcmRlcjogbWVyZ2VkVHlwZU1ldGEub3JkZXIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRvcDogbWVyZ2VkVHlwZU1ldGEudG9wLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBoaXN0b3J5OiBtZXJnZWRUeXBlTWV0YS5oaXN0b3J5LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZWxhdGlvblR5cGU6IG1lcmdlZFR5cGUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRpcmVjdGlvbixcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVsYXRpb25Db3VudDogbWVyZ2VkVHlwZVJlbGF0aW9uQ291bnQsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGhpc3RvcnlSZWxhdGlvbkNvdW50czogbWVyZ2VkVHlwZUhpc3RvcnlSZWxhdGlvbkNvdW50cyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcGx1cmFsS2V5OiB0aGlzLmJ1aWxkTm9kZVJlbGF0aW9uUGx1cmFsS2V5KGRpcmVjdGlvbiwgbWVyZ2VkVHlwZSksXHJcbiAgICAgICAgICAgICAgICAgICAgfTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgZ3JvdXAucmVsYXRpb25Db3VudHNbbWVyZ2VkVHlwZUtleV0gPSBtZXJnZWRUeXBlQ291bnREYXRhO1xyXG4gICAgICAgICAgICAgICAgICAgIHJlbGF0aW9uQ291bnRNYXBbbWVyZ2VkVHlwZUtleV0gPSBtZXJnZWRUeXBlQ291bnREYXRhO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIGlmICghcmVsYXRpb25Db3VudE1hcFttZXJnZWRUeXBlS2V5XSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGdyb3VwLnJlbGF0aW9uQ291bnRzW3JlbGF0aW9uS2V5XSA9IGNvdW50RGF0YTtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICByZWxhdGlvbkNvdW50TWFwW3JlbGF0aW9uS2V5XSA9IGNvdW50RGF0YTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIGdyb3VwcyA9IF8uc29ydEJ5KGdyb3VwcywgJ29yZGVyJyk7XHJcblxyXG4gICAgICAgIF8uZm9yRWFjaChncm91cHMsIChncm91cCkgPT4ge1xyXG4gICAgICAgICAgICBncm91cC5yZWxhdGlvbkNvdW50cyA9IF8uc29ydEJ5KGdyb3VwLnJlbGF0aW9uQ291bnRzLCAnb3JkZXInKTtcclxuICAgICAgICAgICAgcmVsYXRpb25Db3VudHMgPSByZWxhdGlvbkNvdW50cy5jb25jYXQoZ3JvdXAucmVsYXRpb25Db3VudHMpO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICByZWxhdGlvbkNvdW50TWFwLFxyXG4gICAgICAgICAgICByZWxhdGlvbkNvdW50cyxcclxuICAgICAgICAgICAgZ3JvdXBzLFxyXG4gICAgICAgIH07XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0SGlzdG9yeVJlbGF0aW9uQ291bnRzQnlJbmZvKGluZm8pIHtcclxuICAgICAgICBpbmZvID0gaW5mbyB8fCB7fTtcclxuICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICBhY3R1YWw6IGluZm8uYWN0dWFsIHx8IDAsXHJcbiAgICAgICAgICAgIGFsbDogKGluZm8uYWN0dWFsIHx8IDApICsgKGluZm8ub3V0ZGF0ZWQgfHwgMCksXHJcbiAgICAgICAgICAgIG91dGRhdGVkOiBpbmZvLm91dGRhdGVkIHx8IDAsXHJcblxyXG4gICAgICAgIH07XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0RGlyZWN0aW9uKGluZm9EaXJlY3Rpb24pIHtcclxuICAgICAgICByZXR1cm4gaW5mb0RpcmVjdGlvbiA9PT0gJ2luJyA/ICdwYXJlbnRzJyA6ICdjaGlsZHJlbic7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0UmVsYXRpb25UeXBlTWV0YShyZWxhdGlvblR5cGUsIGRpcmVjdGlvbikge1xyXG4gICAgICAgIHJldHVybiBfLmdldChyc2VhcmNoTWV0YS5yZWxhdGlvblR5cGVzLCBbcmVsYXRpb25UeXBlLCBkaXJlY3Rpb25dKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRSZWxhdGlvbkdyb3VwTWV0YShyZWxhdGlvbkdyb3VwKSB7XHJcbiAgICAgICAgcmV0dXJuIHJzZWFyY2hNZXRhLnJlbGF0aW9uR3JvdXBzW3JlbGF0aW9uR3JvdXBdO1xyXG4gICAgfVxyXG5cclxuICAgIGdldE1lcmdlZFR5cGVJbmZvQnlSZWxhdGlvblR5cGUocmVsYXRpb25UeXBlLCBkaXJlY3Rpb24pIHtcclxuICAgICAgICBjb25zdCBpbmZvID0ge1xyXG4gICAgICAgICAgICBtZXJnZWRUeXBlOiBudWxsLFxyXG4gICAgICAgICAgICByZWxhdGlvblR5cGVzOiBudWxsLFxyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIF8uZm9yRWFjaChyc2VhcmNoTWV0YS5tZXJnZWRSZWxhdGlvblR5cGVzLCAoYnlEaXJlY3Rpb25zLCBtZXJnZWRUeXBlKSA9PiB7XHJcbiAgICAgICAgICAgIGNvbnN0IHJlbGF0aW9uVHlwZXMgPSBieURpcmVjdGlvbnNbZGlyZWN0aW9uXTtcclxuICAgICAgICAgICAgaWYgKF8uaW5jbHVkZXMocmVsYXRpb25UeXBlcywgcmVsYXRpb25UeXBlKSkge1xyXG4gICAgICAgICAgICAgICAgaW5mby5tZXJnZWRUeXBlID0gbWVyZ2VkVHlwZTtcclxuICAgICAgICAgICAgICAgIGluZm8ucmVsYXRpb25UeXBlcyA9IHJlbGF0aW9uVHlwZXM7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgcmV0dXJuIGluZm87XHJcbiAgICB9XHJcblxyXG4gICAgYnVpbGROb2RlUmVsYXRpb25LZXkoZGlyZWN0aW9uLCByZWxhdGlvblR5cGUpIHtcclxuICAgICAgICByZXR1cm4gZGlyZWN0aW9uICsgJzo6JyArIHJlbGF0aW9uVHlwZTtcclxuICAgIH1cclxuXHJcbiAgICBidWlsZE5vZGVSZWxhdGlvblBsdXJhbEtleShkaXJlY3Rpb24sIHJlbGF0aW9uVHlwZSkge1xyXG4gICAgICAgIHJldHVybiAnTkdfUExVUkFMSVpFOjpSRUxBVElPTl8nICsgcmVsYXRpb25UeXBlICsgJy0nICsgZGlyZWN0aW9uO1xyXG4gICAgfVxyXG5cclxuICAgIGJ1aWxkTm9kZUV4dHJhTWV0YShub2RlKSB7XHJcbiAgICAgICAgaWYgKCFub2RlKSB7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIHVpZFxyXG4gICAgICAgIHRoaXMuYnVpbGROb2RlVUlEKG5vZGUpO1xyXG5cclxuICAgICAgICAvL1xyXG4gICAgICAgIG5vZGUuX19yZWxhdGlvbkRhdGEgPSBtZXRhU2VydmljZS5idWlsZFJlbGF0aW9uRGF0YUJ5Tm9kZUluZm8obm9kZSk7XHJcblxyXG4gICAgICAgIC8vXHJcbiAgICAgICAgbm9kZS5fX2lkRmllbGQgPSBfLmdldCh0aGlzLm5vZGVUeXBlc01ldGFbbm9kZS5fdHlwZV0sICdpZEZpZWxkJyk7XHJcblxyXG4gICAgICAgIGlmIChub2RlLl90eXBlID09PSAnQ09NUEFOWScgfHwgbm9kZS5fdHlwZSA9PT0gJ0VHUlVMJykgeyAvLyDQutC+0LzQv9Cw0L3QuNGPXHJcbiAgICAgICAgICAgIHRoaXMuYnVpbGRDb21wYW55U3RhdGUobm9kZSk7XHJcbiAgICAgICAgICAgIG5vZGUuX19pc0Zhdm9yaXRlc1N1cHBvcnRlZCA9IHRydWU7XHJcbiAgICAgICAgfSBlbHNlIGlmIChub2RlLl90eXBlID09PSAnSU5ESVZJRFVBTCcpIHsgICAgICAgICAgICAgLy8g0YTQuNC30LjQulxyXG4gICAgICAgICAgICBub2RlLl9faXNGYXZvcml0ZXNTdXBwb3J0ZWQgPSB0cnVlO1xyXG4gICAgICAgIH0gZWxzZSBpZiAobm9kZS5fdHlwZSA9PT0gJ1BVUkNIQVNFJykgeyAvLyDQt9Cw0LrRg9C/0LrQsFxyXG4gICAgICAgICAgICBub2RlLmN1cnJlbmN5ID0gbm9kZS5jdXJyZW5jeSB8fCAnUlVCJztcclxuICAgICAgICAgICAgbm9kZS5fX2xvdE1hcCA9IF8ua2V5Qnkobm9kZS5sb3RzLCAnbG90Jyk7XHJcbiAgICAgICAgICAgIHRoaXMucmVzb2x2ZVB1cmNoYXNlSHJlZihub2RlKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgfVxyXG4gICAgYnVpbGROb2RlVUlEKG5vZGUpIHtcclxuICAgICAgICBub2RlLl9fdWlkID0gbm9kZS4kJGhhc2hLZXkgPSBub2RlLl9fdWlkIHx8IHRoaXMuYnVpbGROb2RlVUlEQnlUeXBlKG5vZGUuX2lkLCBub2RlLl90eXBlKTtcclxuICAgICAgICByZXR1cm4gbm9kZS5fX3VpZDtcclxuICAgIH1cclxuXHJcbiAgICBidWlsZE5vZGVVSURCeVR5cGUoaWQsIHR5cGUpIHtcclxuICAgICAgICByZXR1cm4gKCdub2RlLScgKyB0eXBlICsgJy0nICsgaWQpO1xyXG4gICAgfVxyXG5cclxuICAgIGJ1aWxkQ29tcGFueVN0YXRlKG5vZGUpIHtcclxuICAgICAgICAvLyDRjtGA0LjQtNC40YfQtdGB0LrQvtC1INGB0L7RgdGC0L7Rj9C90LjQtVxyXG4gICAgICAgIGxldCBlZ3J1bFN0YXRlID0gbm9kZS5lZ3J1bF9zdGF0ZTtcclxuICAgICAgICBsZXQgYWxpdmVDb2RlID0gJzUnOyAvLyDQtNC10LnRgdGC0LLRg9GO0YnQtdC1XHJcbiAgICAgICAgbGV0IGludGVybWVkaWF0ZUNvZGVzID0gWyc2JywgJzExMScsICcxMjEnLCAnMTIyJywgJzEyMycsICcxMjQnLCAnMTMxJywgJzEzMiddOyAvLyDQutC+0LTRiyDQv9GA0L7QvNC10LbRg9GC0L7Rh9C90YvRhSDRgdC+0YHRgtC+0Y/QvdC40LlcclxuICAgICAgICBsZXQgX2xpcXVpZGF0ZTtcclxuICAgICAgICBpZiAobm9kZS5lZ3J1bF9saXEpIHtcclxuICAgICAgICAgICAgX2xpcXVpZGF0ZSA9IHtcclxuICAgICAgICAgICAgICAgIHN0YXRlOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgX2FjdHVhbDogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICBfc2luY2U6IG5vZGUuZGVhZF9kdCxcclxuICAgICAgICAgICAgICAgICAgICB0eXBlOiBfLnVwcGVyRmlyc3QoXy5sb3dlckNhc2Uobm9kZS5lZ3J1bF9saXEudHlwZSkpLFxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICB9IGVsc2UgaWYgKG5vZGUuZGVhZF9kdCkge1xyXG4gICAgICAgICAgICBfbGlxdWlkYXRlID0ge1xyXG4gICAgICAgICAgICAgICAgc3RhdGU6IHtcclxuICAgICAgICAgICAgICAgICAgICBfYWN0dWFsOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgIF9zaW5jZTogbm9kZS5kZWFkX2R0LFxyXG4gICAgICAgICAgICAgICAgICAgIHR5cGU6ICfQm9C40LrQstC40LTQuNGA0L7QstCw0L3QvicsXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgIH0gZWxzZSBpZiAoZWdydWxTdGF0ZSkge1xyXG4gICAgICAgICAgICBpZiAoZWdydWxTdGF0ZS5jb2RlICE9PSBhbGl2ZUNvZGUpIHtcclxuICAgICAgICAgICAgICAgIF9saXF1aWRhdGUgPSB7XHJcbiAgICAgICAgICAgICAgICAgICAgc3RhdGU6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgX2FjdHVhbDogZWdydWxTdGF0ZS5fYWN0dWFsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBfc2luY2U6IGVncnVsU3RhdGUuX3NpbmNlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0eXBlOiBlZ3J1bFN0YXRlLnR5cGUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGludGVybWVkaWF0ZTogXy5pbmNsdWRlcyhpbnRlcm1lZGlhdGVDb2RlcywgZWdydWxTdGF0ZS5jb2RlKSxcclxuICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgbm9kZS5fbGlxdWlkYXRlID0gX2xpcXVpZGF0ZTtcclxuXHJcbiAgICAgICAgLy8g0YHQv9C+0YHQvtCxINC+0YDQs9Cw0L3QuNC30LDRhtC40Lgg0LrQvtC80L/QsNC90LjQuFxyXG4gICAgICAgIGNvbnN0IGVncnVsUmVnID0gbm9kZS5lZ3J1bF9yZWc7XHJcbiAgICAgICAgY29uc3QgYmFzZUNvZGVzID0gWycxMScsICcwMScsICcwMyddOyAvLyDQutC+0LTRiyDRgdGC0LDQvdC00LDRgNGC0L3Ri9GFINGA0LXQs9C40YHRgtGA0LDRhtC40LlcclxuXHJcbiAgICAgICAgaWYgKGVncnVsUmVnICYmICFfLmluY2x1ZGVzKGJhc2VDb2RlcywgZWdydWxSZWcuY29kZSkpIHtcclxuICAgICAgICAgICAgbm9kZS5fcmVnID0ge1xyXG4gICAgICAgICAgICAgICAgc3RhdGU6IHtcclxuICAgICAgICAgICAgICAgICAgICBfYWN0dWFsOiBlZ3J1bFJlZy5fYWN0dWFsLFxyXG4gICAgICAgICAgICAgICAgICAgIF9zaW5jZTogZWdydWxSZWcuX3NpbmNlLFxyXG4gICAgICAgICAgICAgICAgICAgIHR5cGU6IGVncnVsUmVnLnR5cGUsXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICByZXNvbHZlUHVyY2hhc2VIcmVmKG5vZGUpIHtcclxuICAgICAgICBpZiAobm9kZS5sYXcgPT09ICdGWl80NCcpIHtcclxuICAgICAgICAgICAgbm9kZS5fX2hyZWYgPSBub2RlLmhyZWY7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgbm9kZS5fX2hyZWYgPSAnaHR0cDovL3pha3Vwa2kuZ292LnJ1L2Vwei9vcmRlci9xdWlja3NlYXJjaC9zZWFyY2hfZWlzLmh0bWw/c3RyaWN0RXF1YWw9b24mcGFnZU51bWJlcj0xJnNvcnREaXJlY3Rpb249ZmFsc2UmcmVjb3Jkc1BlclBhZ2U9XzEwJnNob3dMb3RzSW5mb0hpZGRlbj1mYWxzZSZmejQ0PW9uJmZ6MjIzPW9uJmZ6OTQ9b24mcmVnaW9ucz0mcHJpY2VGcm9tPSZwcmljZVRvPSZjdXJyZW5jeUlkPS0xJnB1Ymxpc2hEYXRlRnJvbT0mcHVibGlzaERhdGVUbz0mdXBkYXRlRGF0ZUZyb209JnVwZGF0ZURhdGVUbz0mc29ydEJ5PVVQREFURV9EQVRFJnNlYXJjaFN0cmluZz0nICsgbm9kZS5faWQ7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuXHJcbn1cclxuXHJcblxyXG5jb25zdCBtZXRhU2VydmljZSA9IG5ldyBNZXRhKCk7XHJcbmV4cG9ydCB7IG1ldGFTZXJ2aWNlIH07XHJcbiJdfQ==