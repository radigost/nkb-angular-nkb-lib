/**
 * @fileoverview added by tsickle
 * Generated from: lib/domain/SearchFilter.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as _ from 'lodash';
//Класс Фильтров для каждого из типов поиска
/**
 * @return {?}
 */
export default function Filter() {
    return {
        regions: [],
        inns: [],
        makeActive: makeActive
    };
    /**
     * @param {?} elementToMake
     * @return {?}
     */
    function makeActive(elementToMake) {
        _.forEach(this.regions, (/**
         * @param {?} region
         * @return {?}
         */
        function (region) {
            region.active = (elementToMake && region.regionCode === elementToMake.regionCode);
        }));
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiU2VhcmNoRmlsdGVyLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQG5rYi9hbmd1bGFyLW5rYi1saWIvIiwic291cmNlcyI6WyJsaWIvZG9tYWluL1NlYXJjaEZpbHRlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sS0FBSyxDQUFDLE1BQU0sUUFBUSxDQUFDOzs7OztBQUU1QixNQUFNLENBQUMsT0FBTyxVQUFVLE1BQU07SUFDMUIsT0FBTztRQUNILE9BQU8sRUFBRSxFQUFFO1FBQ1gsSUFBSSxFQUFFLEVBQUU7UUFDUixVQUFVLFlBQUE7S0FDYixDQUFDOzs7OztJQUVGLFNBQVMsVUFBVSxDQUFDLGFBQWE7UUFDN0IsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsT0FBTzs7OztRQUFFLFVBQVUsTUFBTTtZQUNwQyxNQUFNLENBQUMsTUFBTSxHQUFHLENBQUMsYUFBYSxJQUFJLE1BQU0sQ0FBQyxVQUFVLEtBQUssYUFBYSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQ3RGLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQztBQUNMLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgKiBhcyBfIGZyb20gJ2xvZGFzaCc7XHJcbi8v0JrQu9Cw0YHRgSDQpNC40LvRjNGC0YDQvtCyINC00LvRjyDQutCw0LbQtNC+0LPQviDQuNC3INGC0LjQv9C+0LIg0L/QvtC40YHQutCwXHJcbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIEZpbHRlcigpIHtcclxuICAgIHJldHVybiB7XHJcbiAgICAgICAgcmVnaW9uczogW10sXHJcbiAgICAgICAgaW5uczogW10sXHJcbiAgICAgICAgbWFrZUFjdGl2ZVxyXG4gICAgfTtcclxuXHJcbiAgICBmdW5jdGlvbiBtYWtlQWN0aXZlKGVsZW1lbnRUb01ha2UpIHtcclxuICAgICAgICBfLmZvckVhY2godGhpcy5yZWdpb25zLCBmdW5jdGlvbiAocmVnaW9uKSB7XHJcbiAgICAgICAgICAgIHJlZ2lvbi5hY3RpdmUgPSAoZWxlbWVudFRvTWFrZSAmJiByZWdpb24ucmVnaW9uQ29kZSA9PT0gZWxlbWVudFRvTWFrZS5yZWdpb25Db2RlKTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxufVxyXG4iXX0=