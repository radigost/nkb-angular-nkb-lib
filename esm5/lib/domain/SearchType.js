/**
 * @fileoverview added by tsickle
 * Generated from: lib/domain/SearchType.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as _ from 'lodash';
import Filter from './SearchFilter';
//Класс типов поиска
/**
 * @param {?} nodeType
 * @param {?} data
 * @return {?}
 */
export default function SearchType(nodeType, data) {
    /** @type {?} */
    var searchType = {
        nodeType: nodeType,
        resultPriority: data.searchResultPriority,
        accentedResultPriority: data.accentedResultPriority,
        pageConfig: null,
        filters: null,
        filter: Filter(),
        request: null,
        result: null,
        nodeList: null,
        active: false,
        setResultRegions: setResultRegions,
        setResultInns: setResultInns,
        resetActive: resetActive
    };
    return searchType;
    /**
     * @return {?}
     */
    function resetActive() {
        searchType.active = false;
    }
    /**
     * @return {?}
     */
    function setResultRegions() {
        /** @type {?} */
        var regions = [];
        if (hasRegionCodes.call(this)) {
            /** @type {?} */
            var data_1 = this.result['info']['nodeFacet']['region_code'];
            _.forOwn(data_1, (/**
             * @param {?} number
             * @param {?} regionCode
             * @return {?}
             */
            function (number, regionCode) {
                /** @type {?} */
                var res = {
                    number: number,
                    regionCode: regionCode,
                    active: false
                };
                regions.push(res);
            }));
        }
        this.filter.regions = regions;
        /**
         * @return {?}
         */
        function hasRegionCodes() {
            return _.get(this, 'result.info.nodeFacet.region_code', false);
        }
    }
    /**
     * @param {?} searchType
     * @return {?}
     */
    function setResultInns(searchType) {
        /** @type {?} */
        var inns = [];
        hasResult(searchType) ? _.forEach(searchType.result.list, (/**
         * @param {?} element
         * @return {?}
         */
        function (element) {
            element.inn ? inns.push(element.inn) : '';
        })) : '';
        searchType.filter.inns = inns;
        /**
         * @param {?} searchType
         * @return {?}
         */
        function hasResult(searchType) {
            return _.get(searchType, 'result.list', undefined);
        }
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiU2VhcmNoVHlwZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bua2IvYW5ndWxhci1ua2ItbGliLyIsInNvdXJjZXMiOlsibGliL2RvbWFpbi9TZWFyY2hUeXBlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxLQUFLLENBQUMsTUFBTSxRQUFRLENBQUM7QUFDNUIsT0FBTyxNQUFNLE1BQU0sZ0JBQWdCLENBQUM7Ozs7Ozs7QUFFcEMsTUFBTSxDQUFDLE9BQU8sVUFBVSxVQUFVLENBQUMsUUFBUSxFQUFDLElBQUk7O1FBQ3hDLFVBQVUsR0FBQztRQUNYLFFBQVEsRUFBRSxRQUFRO1FBQ2xCLGNBQWMsRUFBRSxJQUFJLENBQUMsb0JBQW9CO1FBQ3pDLHNCQUFzQixFQUFFLElBQUksQ0FBQyxzQkFBc0I7UUFDbkQsVUFBVSxFQUFFLElBQUk7UUFDaEIsT0FBTyxFQUFFLElBQUk7UUFDYixNQUFNLEVBQUMsTUFBTSxFQUFFO1FBQ2YsT0FBTyxFQUFFLElBQUk7UUFDYixNQUFNLEVBQUUsSUFBSTtRQUNaLFFBQVEsRUFBRSxJQUFJO1FBQ2QsTUFBTSxFQUFDLEtBQUs7UUFDWixnQkFBZ0Isa0JBQUE7UUFDaEIsYUFBYSxlQUFBO1FBQ2IsV0FBVyxhQUFBO0tBQ2Q7SUFDRCxPQUFPLFVBQVUsQ0FBQzs7OztJQUNsQixTQUFTLFdBQVc7UUFDaEIsVUFBVSxDQUFDLE1BQU0sR0FBQyxLQUFLLENBQUM7SUFDNUIsQ0FBQzs7OztJQUNELFNBQVMsZ0JBQWdCOztZQUNmLE9BQU8sR0FBRyxFQUFFO1FBRWxCLElBQUcsY0FBYyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBQzs7Z0JBQ25CLE1BQUksR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLFdBQVcsQ0FBQyxDQUFDLGFBQWEsQ0FBQztZQUM1RCxDQUFDLENBQUMsTUFBTSxDQUFDLE1BQUk7Ozs7O1lBQUMsVUFBUyxNQUFNLEVBQUMsVUFBVTs7b0JBQzlCLEdBQUcsR0FBRztvQkFDUixNQUFNLEVBQUMsTUFBTTtvQkFDYixVQUFVLEVBQUMsVUFBVTtvQkFDckIsTUFBTSxFQUFDLEtBQUs7aUJBQ2Y7Z0JBQ0QsT0FBTyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUN0QixDQUFDLEVBQUMsQ0FBQztTQUNOO1FBQ0QsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDOzs7O1FBQzlCLFNBQVMsY0FBYztZQUNuQixPQUFPLENBQUMsQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFDLG1DQUFtQyxFQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ2pFLENBQUM7SUFDTCxDQUFDOzs7OztJQUNELFNBQVMsYUFBYSxDQUFDLFVBQVU7O1lBQ3ZCLElBQUksR0FBRyxFQUFFO1FBQ2YsU0FBUyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsSUFBSTs7OztRQUFDLFVBQUMsT0FBTztZQUM3RCxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQSxDQUFDLENBQUMsRUFBRSxDQUFDO1FBQzdDLENBQUMsRUFBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDUixVQUFVLENBQUMsTUFBTSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7Ozs7O1FBRTlCLFNBQVMsU0FBUyxDQUFDLFVBQVU7WUFDekIsT0FBTyxDQUFDLENBQUMsR0FBRyxDQUFDLFVBQVUsRUFBQyxhQUFhLEVBQUMsU0FBUyxDQUFDLENBQUM7UUFDckQsQ0FBQztJQUNMLENBQUM7QUFFTCxDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0ICogYXMgXyBmcm9tICdsb2Rhc2gnO1xyXG5pbXBvcnQgRmlsdGVyIGZyb20gJy4vU2VhcmNoRmlsdGVyJztcclxuLy/QmtC70LDRgdGBINGC0LjQv9C+0LIg0L/QvtC40YHQutCwXHJcbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIFNlYXJjaFR5cGUobm9kZVR5cGUsZGF0YSkge1xyXG4gICAgdmFyIHNlYXJjaFR5cGU9e1xyXG4gICAgICAgIG5vZGVUeXBlOiBub2RlVHlwZSxcclxuICAgICAgICByZXN1bHRQcmlvcml0eTogZGF0YS5zZWFyY2hSZXN1bHRQcmlvcml0eSxcclxuICAgICAgICBhY2NlbnRlZFJlc3VsdFByaW9yaXR5OiBkYXRhLmFjY2VudGVkUmVzdWx0UHJpb3JpdHksXHJcbiAgICAgICAgcGFnZUNvbmZpZzogbnVsbCxcclxuICAgICAgICBmaWx0ZXJzOiBudWxsLFxyXG4gICAgICAgIGZpbHRlcjpGaWx0ZXIoKSxcclxuICAgICAgICByZXF1ZXN0OiBudWxsLFxyXG4gICAgICAgIHJlc3VsdDogbnVsbCxcclxuICAgICAgICBub2RlTGlzdDogbnVsbCxcclxuICAgICAgICBhY3RpdmU6ZmFsc2UsXHJcbiAgICAgICAgc2V0UmVzdWx0UmVnaW9ucyxcclxuICAgICAgICBzZXRSZXN1bHRJbm5zLFxyXG4gICAgICAgIHJlc2V0QWN0aXZlXHJcbiAgICB9O1xyXG4gICAgcmV0dXJuIHNlYXJjaFR5cGU7XHJcbiAgICBmdW5jdGlvbiByZXNldEFjdGl2ZSgpIHtcclxuICAgICAgICBzZWFyY2hUeXBlLmFjdGl2ZT1mYWxzZTtcclxuICAgIH1cclxuICAgIGZ1bmN0aW9uIHNldFJlc3VsdFJlZ2lvbnMoKXtcclxuICAgICAgICBjb25zdCByZWdpb25zID0gW107XHJcblxyXG4gICAgICAgIGlmKGhhc1JlZ2lvbkNvZGVzLmNhbGwodGhpcykpe1xyXG4gICAgICAgICAgICBjb25zdCBkYXRhID0gdGhpcy5yZXN1bHRbJ2luZm8nXVsnbm9kZUZhY2V0J11bJ3JlZ2lvbl9jb2RlJ107XHJcbiAgICAgICAgICAgIF8uZm9yT3duKGRhdGEsZnVuY3Rpb24obnVtYmVyLHJlZ2lvbkNvZGUpIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHJlcyA9IHtcclxuICAgICAgICAgICAgICAgICAgICBudW1iZXI6bnVtYmVyLFxyXG4gICAgICAgICAgICAgICAgICAgIHJlZ2lvbkNvZGU6cmVnaW9uQ29kZSxcclxuICAgICAgICAgICAgICAgICAgICBhY3RpdmU6ZmFsc2VcclxuICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICByZWdpb25zLnB1c2gocmVzKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuZmlsdGVyLnJlZ2lvbnMgPSByZWdpb25zO1xyXG4gICAgICAgIGZ1bmN0aW9uIGhhc1JlZ2lvbkNvZGVzKCkge1xyXG4gICAgICAgICAgICByZXR1cm4gXy5nZXQodGhpcywncmVzdWx0LmluZm8ubm9kZUZhY2V0LnJlZ2lvbl9jb2RlJyxmYWxzZSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgZnVuY3Rpb24gc2V0UmVzdWx0SW5ucyhzZWFyY2hUeXBlKSB7XHJcbiAgICAgICAgY29uc3QgaW5ucyA9IFtdO1xyXG4gICAgICAgIGhhc1Jlc3VsdChzZWFyY2hUeXBlKSA/IF8uZm9yRWFjaChzZWFyY2hUeXBlLnJlc3VsdC5saXN0LChlbGVtZW50KT0+IHtcclxuICAgICAgICAgICAgZWxlbWVudC5pbm4gPyBpbm5zLnB1c2goZWxlbWVudC5pbm4pOiAnJztcclxuICAgICAgICB9KSA6ICcnO1xyXG4gICAgICAgIHNlYXJjaFR5cGUuZmlsdGVyLmlubnMgPSBpbm5zO1xyXG5cclxuICAgICAgICBmdW5jdGlvbiBoYXNSZXN1bHQoc2VhcmNoVHlwZSkge1xyXG4gICAgICAgICAgICByZXR1cm4gXy5nZXQoc2VhcmNoVHlwZSwncmVzdWx0Lmxpc3QnLHVuZGVmaW5lZCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxufSJdfQ==