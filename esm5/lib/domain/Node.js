/**
 * @fileoverview added by tsickle
 * Generated from: lib/domain/Node.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * Created by radigost on 27.09.17.
 */
import * as _ from 'lodash';
import { metaService } from '../meta/metaService';
// import { nkbSearchMetaService } from '../meta/npRsearchMeta';
import { NODE_TYPE } from './TYPES';
/**
 * @record
 */
export function IEgrulProfile() { }
/**
 * @record
 */
function IGrn() { }
if (false) {
    /** @type {?} */
    IGrn.prototype.date;
    /** @type {?} */
    IGrn.prototype.grn;
}
/**
 * @record
 */
export function IDisqualifiedChef() { }
if (false) {
    /** @type {?} */
    IDisqualifiedChef.prototype.dateBegin;
    /** @type {?} */
    IDisqualifiedChef.prototype.dateEnd;
    /** @type {?} */
    IDisqualifiedChef.prototype.grn;
}
var Node = /** @class */ (function () {
    function Node(newNode) {
        /** @type {?} */
        var node = Object.assign({}, newNode);
        // TODO add wrapper for this - need somehow add this info to node, when needed
        // if (nkbSearchMetaService) {
        metaService.buildNodeExtraMeta(node);
        // }
        node.id = metaService.getNodeId(node);
        node._name = metaService.getNodeName(node);
        delete node.name;
        Object.assign(this, node, { _metaService: metaService });
    }
    /**
     * @param {?} node
     * @return {?}
     */
    Node.nodeWrapper = /**
     * @param {?} node
     * @return {?}
     */
    function (node) {
        if (node) {
            return node instanceof Node ? node : new Node(node);
        }
        else {
            return new Node(node);
        }
    };
    Object.defineProperty(Node.prototype, "idField", {
        get: /**
         * @return {?}
         */
        function () {
            /** @type {?} */
            var node = this;
            return metaService.getIdFieldForType(node.type);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Node.prototype, "aggregates", {
        get: /**
         * @return {?}
         */
        function () {
            return _.get(this, '_aggregates', {});
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Node.prototype, "name", {
        get: /**
         * @return {?}
         */
        function () {
            /** @type {?} */
            var node = this;
            return node._name;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Node.prototype, "fullName", {
        get: /**
         * @return {?}
         */
        function () {
            return _.get(this, 'namesort', '');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Node.prototype, "chiefName", {
        get: /**
         * @return {?}
         */
        function () {
            return _.get(this, 'chief_name', '');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Node.prototype, "type", {
        get: /**
         * @return {?}
         */
        function () {
            return _.get(this, '_type', '');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Node.prototype, "debts", {
        get: /**
         * @return {?}
         */
        function () {
            return _.get(this, '_debts', null);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Node.prototype, "licencies", {
        get: /**
         * @return {?}
         */
        function () {
            return _.map(_.get(this._egrulProfile, 'infoLicense'), (/**
             * @param {?} licence
             * @return {?}
             */
            function (licence) {
                return {
                    terminationDate: _.get(_.head(_.get(licence, 'profile')), 'terminationDate', false),
                    number: _.get(licence, 'data.number'),
                    type: _.map(_.get(licence, 'data.type'), (/**
                     * @param {?} type
                     * @return {?}
                     */
                    function (type) { return _.upperFirst(_.lowerCase(type)); })),
                    issued: _.upperFirst(_.lowerCase(_.get(licence, 'data.issued'))),
                    dateBegin: _.get(licence, 'data.dateBegin'),
                    date: _.get(licence, 'data.date'),
                };
            }));
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Node.prototype, "relations", {
        get: /**
         * @return {?}
         */
        function () {
            return _.get(this, '_relations', []);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Node.prototype, "disqualification", {
        get: /**
         * @return {?}
         */
        function () {
            /** @type {?} */
            var disqualification = _.head(_.get(_.head(_.get(this._egrulProfile, 'infoPersonOfficial')), 'data.disqualification'));
            return disqualification;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @return {?}
     */
    Node.prototype.getFullName = /**
     * @return {?}
     */
    function () {
        return _.get(this, 'namesort', '');
    };
    /**
     * @return {?}
     */
    Node.prototype.getChiefName = /**
     * @return {?}
     */
    function () {
        return _.get(this, 'chief_name', '');
    };
    /**
     * @return {?}
     */
    Node.prototype.getType = /**
     * @return {?}
     */
    function () {
        return _.get(this, '_type', '');
    };
    /**
     * @return {?}
     */
    Node.prototype.getCity = /**
     * @return {?}
     */
    function () {
        if (this.type === NODE_TYPE.INDIVIDUAL_IDENTITY) {
            return _.capitalize(_.get(_.head(_.get(this, 'selfemployedInfo.infoAddress')), 'data.адресРФ.город.наимГород', ''));
        }
        if (this.type === NODE_TYPE.COMPANY) {
            return _.capitalize(_.get(_.head(_.get(this, '_egrulProfile.infoAddress')), 'data.address.regionName', ''));
        }
    };
    /**
     * @return {?}
     */
    Node.prototype.getOgrnName = /**
     * @return {?}
     */
    function () {
        return this.ogrn ? 'ОГРН' : this.selfemployedInfo ? 'ОГРНИП' : '';
    };
    /**
     * @return {?}
     */
    Node.prototype.getEgrulName = /**
     * @return {?}
     */
    function () {
        return this.ogrn ? 'ЕГРЮЛ' : this.selfemployedInfo ? 'ЕГРИП' : '';
    };
    /**
     * @return {?}
     */
    Node.prototype.getOgrn = /**
     * @return {?}
     */
    function () {
        return this.ogrn ? this.ogrn : this.selfemployedInfo ? this.selfemployedInfo.ogrnip : '';
    };
    /**
     * @param {?} selfemployedInfo
     * @return {?}
     */
    Node.prototype.setSelfemployedInfo = /**
     * @param {?} selfemployedInfo
     * @return {?}
     */
    function (selfemployedInfo) {
        if (selfemployedInfo !== undefined) {
            this.selfemployedInfo = selfemployedInfo;
        }
    };
    /**
     * @param {?} egrulProfile
     * @return {?}
     */
    Node.prototype.setEgrulProfile = /**
     * @param {?} egrulProfile
     * @return {?}
     */
    function (egrulProfile) {
        this._egrulProfile = egrulProfile;
    };
    /**
     * @param {?} balance
     * @return {?}
     */
    Node.prototype.setBalance = /**
     * @param {?} balance
     * @return {?}
     */
    function (balance) {
        this._balance = balance;
    };
    return Node;
}());
export { Node };
if (false) {
    /** @type {?} */
    Node.prototype._id;
    /** @type {?} */
    Node.prototype._type;
    /** @type {?} */
    Node.prototype._aggregates;
    /** @type {?} */
    Node.prototype._name;
    /** @type {?} */
    Node.prototype.ogrn;
    /** @type {?} */
    Node.prototype._balance;
    /** @type {?} */
    Node.prototype._egrulProfile;
    /** @type {?} */
    Node.prototype.ogrnip;
    /** @type {?} */
    Node.prototype.selfemployedInfo;
    /** @type {?} */
    Node.prototype._debts;
    /** @type {?} */
    Node.prototype.inn;
    /** @type {?} */
    Node.prototype.id;
    /** @type {?} */
    Node.prototype.state;
    /** @type {?} */
    Node.prototype.balance;
    /** @type {?} */
    Node.prototype.chief_name;
    /** @type {?} */
    Node.prototype.chief_gender;
    /** @type {?} */
    Node.prototype.shortname;
    /** @type {?} */
    Node.prototype.founded_dt;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiTm9kZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bua2IvYW5ndWxhci1ua2ItbGliLyIsInNvdXJjZXMiOlsibGliL2RvbWFpbi9Ob2RlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBSUEsT0FBTyxLQUFLLENBQUMsTUFBTSxRQUFRLENBQUM7QUFDNUIsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLHFCQUFxQixDQUFDOztBQUVsRCxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sU0FBUyxDQUFDOzs7O0FBQ3BDLG1DQUVDOzs7O0FBR0QsbUJBR0M7OztJQUZHLG9CQUFhOztJQUNiLG1CQUFZOzs7OztBQUdoQix1Q0FJQzs7O0lBSEcsc0NBQWtCOztJQUNsQixvQ0FBZ0I7O0lBQ2hCLGdDQUFVOztBQUlkO0lBZ0ZJLGNBQVksT0FBTzs7WUFDVCxJQUFJLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsT0FBTyxDQUFDO1FBR3ZDLDhFQUE4RTtRQUM5RSw4QkFBOEI7UUFDOUIsV0FBVyxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3JDLElBQUk7UUFFSixJQUFJLENBQUMsRUFBRSxHQUFHLFdBQVcsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDdEMsSUFBSSxDQUFDLEtBQUssR0FBRyxXQUFXLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzNDLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQztRQUNqQixNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxJQUFJLEVBQUUsRUFBRSxZQUFZLEVBQUUsV0FBVyxFQUFFLENBQUMsQ0FBQztJQUM3RCxDQUFDOzs7OztJQTVGTSxnQkFBVzs7OztJQUFsQixVQUFtQixJQUFJO1FBQ25CLElBQUksSUFBSSxFQUFFO1lBQ04sT0FBTyxJQUFJLFlBQVksSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ3ZEO2FBQU07WUFDSCxPQUFPLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ3pCO0lBRUwsQ0FBQztJQW9CRCxzQkFBSSx5QkFBTzs7OztRQUFYOztnQkFDVSxJQUFJLEdBQUcsSUFBSTtZQUNqQixPQUFPLFdBQVcsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDcEQsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSw0QkFBVTs7OztRQUFkO1lBQ0ksT0FBTyxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxhQUFhLEVBQUUsRUFBRSxDQUFDLENBQUM7UUFDMUMsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSxzQkFBSTs7OztRQUFSOztnQkFDVSxJQUFJLEdBQUcsSUFBSTtZQUNqQixPQUFPLElBQUksQ0FBQyxLQUFLLENBQUM7UUFDdEIsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSwwQkFBUTs7OztRQUFaO1lBQ0ksT0FBTyxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxVQUFVLEVBQUUsRUFBRSxDQUFDLENBQUM7UUFDdkMsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSwyQkFBUzs7OztRQUFiO1lBQ0ksT0FBTyxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxZQUFZLEVBQUUsRUFBRSxDQUFDLENBQUM7UUFDekMsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSxzQkFBSTs7OztRQUFSO1lBQ0ksT0FBTyxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxPQUFPLEVBQUUsRUFBRSxDQUFDLENBQUM7UUFDcEMsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSx1QkFBSzs7OztRQUFUO1lBQ0ksT0FBTyxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxRQUFRLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDdkMsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSwyQkFBUzs7OztRQUFiO1lBQ0ksT0FBTyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLGFBQWEsRUFBRSxhQUFhLENBQUM7Ozs7WUFBRSxVQUFDLE9BQU87Z0JBQzNELE9BQU87b0JBQ0gsZUFBZSxFQUFFLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLE9BQU8sRUFBRSxTQUFTLENBQUMsQ0FBQyxFQUFFLGlCQUFpQixFQUFFLEtBQUssQ0FBQztvQkFDbkYsTUFBTSxFQUFFLENBQUMsQ0FBQyxHQUFHLENBQUMsT0FBTyxFQUFFLGFBQWEsQ0FBQztvQkFDckMsSUFBSSxFQUFFLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxPQUFPLEVBQUUsV0FBVyxDQUFDOzs7O29CQUFFLFVBQUMsSUFBSSxJQUFLLE9BQUEsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQS9CLENBQStCLEVBQUM7b0JBQ25GLE1BQU0sRUFBRSxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxPQUFPLEVBQUUsYUFBYSxDQUFDLENBQUMsQ0FBQztvQkFDaEUsU0FBUyxFQUFFLENBQUMsQ0FBQyxHQUFHLENBQUMsT0FBTyxFQUFFLGdCQUFnQixDQUFDO29CQUMzQyxJQUFJLEVBQUUsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxPQUFPLEVBQUUsV0FBVyxDQUFDO2lCQUNwQyxDQUFDO1lBQ04sQ0FBQyxFQUFDLENBQUM7UUFDUCxDQUFDOzs7T0FBQTtJQUVELHNCQUFJLDJCQUFTOzs7O1FBQWI7WUFDSSxPQUFPLENBQUMsQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLFlBQVksRUFBRSxFQUFFLENBQUMsQ0FBQztRQUN6QyxDQUFDOzs7T0FBQTtJQUVELHNCQUFJLGtDQUFnQjs7OztRQUFwQjs7Z0JBQ1UsZ0JBQWdCLEdBQXNCLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLGFBQWEsRUFBRSxvQkFBb0IsQ0FBQyxDQUFDLEVBQUUsdUJBQXVCLENBQUMsQ0FBQztZQUMzSSxPQUFPLGdCQUFnQixDQUFDO1FBQzVCLENBQUM7OztPQUFBOzs7O0lBaUJELDBCQUFXOzs7SUFBWDtRQUNJLE9BQU8sQ0FBQyxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsVUFBVSxFQUFFLEVBQUUsQ0FBQyxDQUFDO0lBQ3ZDLENBQUM7Ozs7SUFFRCwyQkFBWTs7O0lBQVo7UUFDSSxPQUFPLENBQUMsQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLFlBQVksRUFBRSxFQUFFLENBQUMsQ0FBQztJQUN6QyxDQUFDOzs7O0lBRUQsc0JBQU87OztJQUFQO1FBQ0ksT0FBTyxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxPQUFPLEVBQUUsRUFBRSxDQUFDLENBQUM7SUFDcEMsQ0FBQzs7OztJQUVELHNCQUFPOzs7SUFBUDtRQUNJLElBQUksSUFBSSxDQUFDLElBQUksS0FBSyxTQUFTLENBQUMsbUJBQW1CLEVBQUU7WUFDN0MsT0FBTyxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSw4QkFBOEIsQ0FBQyxDQUFDLEVBQUUsOEJBQThCLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQztTQUN2SDtRQUNELElBQUksSUFBSSxDQUFDLElBQUksS0FBSyxTQUFTLENBQUMsT0FBTyxFQUFFO1lBQ2pDLE9BQU8sQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsMkJBQTJCLENBQUMsQ0FBQyxFQUFFLHlCQUF5QixFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUM7U0FDL0c7SUFDTCxDQUFDOzs7O0lBRUQsMEJBQVc7OztJQUFYO1FBQ0ksT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7SUFDdEUsQ0FBQzs7OztJQUVELDJCQUFZOzs7SUFBWjtRQUNJLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO0lBQ3RFLENBQUM7Ozs7SUFFRCxzQkFBTzs7O0lBQVA7UUFDSSxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO0lBQzdGLENBQUM7Ozs7O0lBRUQsa0NBQW1COzs7O0lBQW5CLFVBQW9CLGdCQUFnQjtRQUNoQyxJQUFJLGdCQUFnQixLQUFLLFNBQVMsRUFBRTtZQUNoQyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsZ0JBQWdCLENBQUM7U0FDNUM7SUFDTCxDQUFDOzs7OztJQUVELDhCQUFlOzs7O0lBQWYsVUFBZ0IsWUFBWTtRQUN4QixJQUFJLENBQUMsYUFBYSxHQUFHLFlBQVksQ0FBQztJQUN0QyxDQUFDOzs7OztJQUVELHlCQUFVOzs7O0lBQVYsVUFBVyxPQUFPO1FBQ2QsSUFBSSxDQUFDLFFBQVEsR0FBRyxPQUFPLENBQUM7SUFDNUIsQ0FBQztJQUNMLFdBQUM7QUFBRCxDQUFDLEFBN0lELElBNklDOzs7O0lBbklHLG1CQUFZOztJQUNaLHFCQUFPOztJQUNQLDJCQUFhOztJQUNiLHFCQUFPOztJQUNQLG9CQUFNOztJQUNOLHdCQUFVOztJQUNWLDZCQUFlOztJQUNmLHNCQUFROztJQUNSLGdDQUFrQjs7SUFDbEIsc0JBQVE7O0lBQ1IsbUJBQUs7O0lBQ0wsa0JBQUc7O0lBQ0gscUJBQXdCOztJQUN4Qix1QkFBbUI7O0lBQ25CLDBCQUFvQjs7SUFDcEIsNEJBQXFCOztJQUNyQix5QkFBa0I7O0lBQ2xCLDBCQUFXIiwic291cmNlc0NvbnRlbnQiOlsiLyoqXHJcbiAqIENyZWF0ZWQgYnkgcmFkaWdvc3Qgb24gMjcuMDkuMTcuXHJcbiAqL1xyXG5cclxuaW1wb3J0ICogYXMgXyBmcm9tICdsb2Rhc2gnO1xyXG5pbXBvcnQgeyBtZXRhU2VydmljZSB9IGZyb20gJy4uL21ldGEvbWV0YVNlcnZpY2UnO1xyXG4vLyBpbXBvcnQgeyBua2JTZWFyY2hNZXRhU2VydmljZSB9IGZyb20gJy4uL21ldGEvbnBSc2VhcmNoTWV0YSc7XHJcbmltcG9ydCB7IE5PREVfVFlQRSB9IGZyb20gJy4vVFlQRVMnO1xyXG5leHBvcnQgaW50ZXJmYWNlIElFZ3J1bFByb2ZpbGUge1xyXG5cclxufVxyXG5cclxuXHJcbmludGVyZmFjZSBJR3JuIHtcclxuICAgIGRhdGU6IHN0cmluZztcclxuICAgIGdybjogbnVtYmVyO1xyXG59XHJcblxyXG5leHBvcnQgaW50ZXJmYWNlIElEaXNxdWFsaWZpZWRDaGVmIHtcclxuICAgIGRhdGVCZWdpbjogc3RyaW5nO1xyXG4gICAgZGF0ZUVuZDogc3RyaW5nO1xyXG4gICAgZ3JuOiBJR3JuO1xyXG59XHJcblxyXG5cclxuZXhwb3J0IGNsYXNzIE5vZGUge1xyXG4gICAgc3RhdGljIG5vZGVXcmFwcGVyKG5vZGUpIHtcclxuICAgICAgICBpZiAobm9kZSkge1xyXG4gICAgICAgICAgICByZXR1cm4gbm9kZSBpbnN0YW5jZW9mIE5vZGUgPyBub2RlIDogbmV3IE5vZGUobm9kZSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgcmV0dXJuIG5ldyBOb2RlKG5vZGUpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICB9XHJcblxyXG4gICAgX2lkOiBzdHJpbmc7XHJcbiAgICBfdHlwZT87XHJcbiAgICBfYWdncmVnYXRlcz87XHJcbiAgICBfbmFtZT87XHJcbiAgICBvZ3JuPztcclxuICAgIF9iYWxhbmNlPztcclxuICAgIF9lZ3J1bFByb2ZpbGU/O1xyXG4gICAgb2dybmlwPztcclxuICAgIHNlbGZlbXBsb3llZEluZm8/O1xyXG4gICAgX2RlYnRzPztcclxuICAgIGlubj87XHJcbiAgICBpZDtcclxuICAgIHN0YXRlOiB7IF9zaW5jZSwgdHlwZSB9O1xyXG4gICAgYmFsYW5jZT86IG51bWJlcltdO1xyXG4gICAgY2hpZWZfbmFtZT86IHN0cmluZzsgLy8gb25seSBmb3IgY29tcGFuaWVzXHJcbiAgICBjaGllZl9nZW5kZXI6IHN0cmluZzsgLy8gb25seSBmb3IgY29tcGFuaWVzXHJcbiAgICBzaG9ydG5hbWU6IHN0cmluZztcclxuICAgIGZvdW5kZWRfZHQ7XHJcbiAgICBnZXQgaWRGaWVsZCgpIHtcclxuICAgICAgICBjb25zdCBub2RlID0gdGhpcztcclxuICAgICAgICByZXR1cm4gbWV0YVNlcnZpY2UuZ2V0SWRGaWVsZEZvclR5cGUobm9kZS50eXBlKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXQgYWdncmVnYXRlcygpIHtcclxuICAgICAgICByZXR1cm4gXy5nZXQodGhpcywgJ19hZ2dyZWdhdGVzJywge30pO1xyXG4gICAgfVxyXG5cclxuICAgIGdldCBuYW1lKCkge1xyXG4gICAgICAgIGNvbnN0IG5vZGUgPSB0aGlzO1xyXG4gICAgICAgIHJldHVybiBub2RlLl9uYW1lO1xyXG4gICAgfVxyXG5cclxuICAgIGdldCBmdWxsTmFtZSgpIHtcclxuICAgICAgICByZXR1cm4gXy5nZXQodGhpcywgJ25hbWVzb3J0JywgJycpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldCBjaGllZk5hbWUoKSB7XHJcbiAgICAgICAgcmV0dXJuIF8uZ2V0KHRoaXMsICdjaGllZl9uYW1lJywgJycpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldCB0eXBlKCkge1xyXG4gICAgICAgIHJldHVybiBfLmdldCh0aGlzLCAnX3R5cGUnLCAnJyk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IGRlYnRzKCkge1xyXG4gICAgICAgIHJldHVybiBfLmdldCh0aGlzLCAnX2RlYnRzJywgbnVsbCk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IGxpY2VuY2llcygpIHtcclxuICAgICAgICByZXR1cm4gXy5tYXAoXy5nZXQodGhpcy5fZWdydWxQcm9maWxlLCAnaW5mb0xpY2Vuc2UnKSwgKGxpY2VuY2UpID0+IHtcclxuICAgICAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgICAgIHRlcm1pbmF0aW9uRGF0ZTogXy5nZXQoXy5oZWFkKF8uZ2V0KGxpY2VuY2UsICdwcm9maWxlJykpLCAndGVybWluYXRpb25EYXRlJywgZmFsc2UpLFxyXG4gICAgICAgICAgICAgICAgbnVtYmVyOiBfLmdldChsaWNlbmNlLCAnZGF0YS5udW1iZXInKSxcclxuICAgICAgICAgICAgICAgIHR5cGU6IF8ubWFwKF8uZ2V0KGxpY2VuY2UsICdkYXRhLnR5cGUnKSwgKHR5cGUpID0+IF8udXBwZXJGaXJzdChfLmxvd2VyQ2FzZSh0eXBlKSkpLFxyXG4gICAgICAgICAgICAgICAgaXNzdWVkOiBfLnVwcGVyRmlyc3QoXy5sb3dlckNhc2UoXy5nZXQobGljZW5jZSwgJ2RhdGEuaXNzdWVkJykpKSxcclxuICAgICAgICAgICAgICAgIGRhdGVCZWdpbjogXy5nZXQobGljZW5jZSwgJ2RhdGEuZGF0ZUJlZ2luJyksXHJcbiAgICAgICAgICAgICAgICBkYXRlOiBfLmdldChsaWNlbmNlLCAnZGF0YS5kYXRlJyksXHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IHJlbGF0aW9ucygpIHtcclxuICAgICAgICByZXR1cm4gXy5nZXQodGhpcywgJ19yZWxhdGlvbnMnLCBbXSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IGRpc3F1YWxpZmljYXRpb24oKTogSURpc3F1YWxpZmllZENoZWYgfCB1bmRlZmluZWQge1xyXG4gICAgICAgIGNvbnN0IGRpc3F1YWxpZmljYXRpb246IElEaXNxdWFsaWZpZWRDaGVmID0gXy5oZWFkKF8uZ2V0KF8uaGVhZChfLmdldCh0aGlzLl9lZ3J1bFByb2ZpbGUsICdpbmZvUGVyc29uT2ZmaWNpYWwnKSksICdkYXRhLmRpc3F1YWxpZmljYXRpb24nKSk7XHJcbiAgICAgICAgcmV0dXJuIGRpc3F1YWxpZmljYXRpb247XHJcbiAgICB9XHJcblxyXG4gICAgY29uc3RydWN0b3IobmV3Tm9kZSkge1xyXG4gICAgICAgIGNvbnN0IG5vZGUgPSBPYmplY3QuYXNzaWduKHt9LCBuZXdOb2RlKTtcclxuXHJcblxyXG4gICAgICAgIC8vIFRPRE8gYWRkIHdyYXBwZXIgZm9yIHRoaXMgLSBuZWVkIHNvbWVob3cgYWRkIHRoaXMgaW5mbyB0byBub2RlLCB3aGVuIG5lZWRlZFxyXG4gICAgICAgIC8vIGlmIChua2JTZWFyY2hNZXRhU2VydmljZSkge1xyXG4gICAgICAgIG1ldGFTZXJ2aWNlLmJ1aWxkTm9kZUV4dHJhTWV0YShub2RlKTtcclxuICAgICAgICAvLyB9XHJcblxyXG4gICAgICAgIG5vZGUuaWQgPSBtZXRhU2VydmljZS5nZXROb2RlSWQobm9kZSk7XHJcbiAgICAgICAgbm9kZS5fbmFtZSA9IG1ldGFTZXJ2aWNlLmdldE5vZGVOYW1lKG5vZGUpO1xyXG4gICAgICAgIGRlbGV0ZSBub2RlLm5hbWU7XHJcbiAgICAgICAgT2JqZWN0LmFzc2lnbih0aGlzLCBub2RlLCB7IF9tZXRhU2VydmljZTogbWV0YVNlcnZpY2UgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0RnVsbE5hbWUoKSB7XHJcbiAgICAgICAgcmV0dXJuIF8uZ2V0KHRoaXMsICduYW1lc29ydCcsICcnKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRDaGllZk5hbWUoKSB7XHJcbiAgICAgICAgcmV0dXJuIF8uZ2V0KHRoaXMsICdjaGllZl9uYW1lJywgJycpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFR5cGUoKSB7XHJcbiAgICAgICAgcmV0dXJuIF8uZ2V0KHRoaXMsICdfdHlwZScsICcnKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRDaXR5KCkge1xyXG4gICAgICAgIGlmICh0aGlzLnR5cGUgPT09IE5PREVfVFlQRS5JTkRJVklEVUFMX0lERU5USVRZKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBfLmNhcGl0YWxpemUoXy5nZXQoXy5oZWFkKF8uZ2V0KHRoaXMsICdzZWxmZW1wbG95ZWRJbmZvLmluZm9BZGRyZXNzJykpLCAnZGF0YS7QsNC00YDQtdGB0KDQpC7Qs9C+0YDQvtC0LtC90LDQuNC80JPQvtGA0L7QtCcsICcnKSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICh0aGlzLnR5cGUgPT09IE5PREVfVFlQRS5DT01QQU5ZKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBfLmNhcGl0YWxpemUoXy5nZXQoXy5oZWFkKF8uZ2V0KHRoaXMsICdfZWdydWxQcm9maWxlLmluZm9BZGRyZXNzJykpLCAnZGF0YS5hZGRyZXNzLnJlZ2lvbk5hbWUnLCAnJykpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBnZXRPZ3JuTmFtZSgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5vZ3JuID8gJ9Ce0JPQoNCdJyA6IHRoaXMuc2VsZmVtcGxveWVkSW5mbyA/ICfQntCT0KDQndCY0J8nIDogJyc7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0RWdydWxOYW1lKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLm9ncm4gPyAn0JXQk9Cg0K7QmycgOiB0aGlzLnNlbGZlbXBsb3llZEluZm8gPyAn0JXQk9Cg0JjQnycgOiAnJztcclxuICAgIH1cclxuXHJcbiAgICBnZXRPZ3JuKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLm9ncm4gPyB0aGlzLm9ncm4gOiB0aGlzLnNlbGZlbXBsb3llZEluZm8gPyB0aGlzLnNlbGZlbXBsb3llZEluZm8ub2dybmlwIDogJyc7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0U2VsZmVtcGxveWVkSW5mbyhzZWxmZW1wbG95ZWRJbmZvKSB7XHJcbiAgICAgICAgaWYgKHNlbGZlbXBsb3llZEluZm8gIT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgICAgICB0aGlzLnNlbGZlbXBsb3llZEluZm8gPSBzZWxmZW1wbG95ZWRJbmZvO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBzZXRFZ3J1bFByb2ZpbGUoZWdydWxQcm9maWxlKSB7XHJcbiAgICAgICAgdGhpcy5fZWdydWxQcm9maWxlID0gZWdydWxQcm9maWxlO1xyXG4gICAgfVxyXG5cclxuICAgIHNldEJhbGFuY2UoYmFsYW5jZSkge1xyXG4gICAgICAgIHRoaXMuX2JhbGFuY2UgPSBiYWxhbmNlO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==