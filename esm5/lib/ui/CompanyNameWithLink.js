/**
 * @fileoverview added by tsickle
 * Generated from: lib/ui/CompanyNameWithLink.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/**
 * Created by radigost on 19.04.18.
 */
import * as _ from 'lodash';
import { searchService } from '../search/searchService';
import { NODE_TYPE } from '../domain/TYPES';
import { Node } from '../domain/Node';
import { Component, Input, Output, EventEmitter } from '@angular/core';
var CompanyNameWithLinkComponent = /** @class */ (function () {
    function CompanyNameWithLinkComponent() {
        this.addBreadcrumb = new EventEmitter();
        this.onCompanyClick = new EventEmitter();
        this.loading = false;
    }
    Object.defineProperty(CompanyNameWithLinkComponent.prototype, "title", {
        get: /**
         * @return {?}
         */
        function () {
            return this.node.state ? this.node.state._since + " : " + this.node.state.type : '';
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @return {?}
     */
    CompanyNameWithLinkComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        if (this.showParams === undefined) {
            this.showParams = {
                ogrn: true,
                inn: false,
                created: false,
            };
        }
    };
    /**
     * @param {?} company
     * @return {?}
     */
    CompanyNameWithLinkComponent.prototype.goToDetails = /**
     * @param {?} company
     * @return {?}
     */
    function (company) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var nodeType, node, getNodeTypeByOgrn, getNodeTypeByInn, getNode, params;
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        getNodeTypeByOgrn = (/**
                         * @param {?} ogrn
                         * @return {?}
                         */
                        function (ogrn) {
                            return _this.isCompanyOgrn(ogrn) ? NODE_TYPE.COMPANY : _this.isIndividualOgrn(ogrn) ? NODE_TYPE.INDIVIDUAL_IDENTITY : false;
                        });
                        getNodeTypeByInn = (/**
                         * @param {?} inn
                         * @return {?}
                         */
                        function (inn) {
                            return _this.isCompanyInn(inn) ? NODE_TYPE.COMPANY : _this.isIndividualInn(inn) ? NODE_TYPE.INDIVIDUAL_IDENTITY : false;
                        });
                        getNode = (/**
                         * @param {?} nodeType
                         * @param {?} id
                         * @return {?}
                         */
                        function (nodeType, id) { return tslib_1.__awaiter(_this, void 0, void 0, function () {
                            var res;
                            return tslib_1.__generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0: return [4 /*yield*/, searchService.searchByTypeAndText({ nodeType: nodeType }, id)];
                                    case 1:
                                        res = _a.sent();
                                        return [2 /*return*/, _.head(_.get(res, 'result.list', _.get(res, 'list')))];
                                }
                            });
                        }); });
                        company = this.node;
                        this.loading = true;
                        if (!(company.id && company.type)) return [3 /*break*/, 1];
                        this.addBreadcrumb.emit({ id: company.id });
                        params = {
                            searchtype: company.type,
                            text: company.name,
                            id: company.id,
                        };
                        this.onCompanyClick.emit({ node: company });
                        return [3 /*break*/, 6];
                    case 1:
                        if (!company.ogrn) return [3 /*break*/, 3];
                        nodeType = getNodeTypeByOgrn(company.ogrn);
                        return [4 /*yield*/, getNode(nodeType, company.ogrn)];
                    case 2:
                        node = _a.sent();
                        return [3 /*break*/, 5];
                    case 3:
                        if (!company.inn) return [3 /*break*/, 5];
                        nodeType = getNodeTypeByInn(company.inn);
                        return [4 /*yield*/, getNode(nodeType, company.inn)];
                    case 4:
                        node = _a.sent();
                        _a.label = 5;
                    case 5:
                        node = Node.nodeWrapper(node);
                        this.addBreadcrumb.emit({ id: node.id });
                        this.onCompanyClick.emit({ node: node });
                        _a.label = 6;
                    case 6: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * @param {?} node
     * @return {?}
     */
    CompanyNameWithLinkComponent.prototype.isNotActive = /**
     * @param {?} node
     * @return {?}
     */
    function (node) {
        return _.toNumber(_.get(node, 'state.code')) === 0;
    };
    /**
     * @param {?} textOgrn
     * @param {?} queryOgrn
     * @return {?}
     */
    CompanyNameWithLinkComponent.prototype.isHighlightSearch = /**
     * @param {?} textOgrn
     * @param {?} queryOgrn
     * @return {?}
     */
    function (textOgrn, queryOgrn) {
        return _.toString(textOgrn) === queryOgrn;
    };
    /**
     * @param {?} inn
     * @return {?}
     */
    CompanyNameWithLinkComponent.prototype.isIndividualInn = /**
     * @param {?} inn
     * @return {?}
     */
    function (inn) {
        return String(inn).length === 12;
    };
    /**
     * @param {?} ogrn
     * @return {?}
     */
    CompanyNameWithLinkComponent.prototype.isCompanyOgrn = /**
     * @param {?} ogrn
     * @return {?}
     */
    function (ogrn) {
        return String(ogrn).length === 13;
    };
    /**
     * @param {?} ogrn
     * @return {?}
     */
    CompanyNameWithLinkComponent.prototype.isIndividualOgrn = /**
     * @param {?} ogrn
     * @return {?}
     */
    function (ogrn) {
        return String(ogrn).length === 15;
    };
    /**
     * @param {?} inn
     * @return {?}
     */
    CompanyNameWithLinkComponent.prototype.isCompanyInn = /**
     * @param {?} inn
     * @return {?}
     */
    function (inn) {
        return String(inn).length === 10;
    };
    CompanyNameWithLinkComponent.decorators = [
        { type: Component, args: [{
                    selector: 'nkb-company-name-with-link',
                    template: "\n        <div *ngIf=\"node.shortname\">{{node.shortname}}</div>\n        <a\n                *ngIf=\"node.ogrn || node.inn\"\n                (click)=\"goToDetails(node)\"\n                [ngClass]=\"{danger:isNotActive(node),'autokad__company-name--highlight':isHighlightSearch(node.ogrn,ogrn)}\"\n                class=\"autokad__company-name\"\n                [title]=\"title\"\n        >\n            {{node.name}}\n        </a>\n        <span\n                *ngIf=\"!node.ogrn && !node.inn\"\n                [ngClass]=\"{danger:isNotActive(node),'autokad__company-name--highlight':isHighlightSearch(node.ogrn,ogrn)}\"\n                [title]=\"title\"\n        >\n        {{node.name}}\n    </span>\n\n        <span *ngIf=\"node.ogrn!==undefined && showParams.ogrn\">\n        <span *ngIf=\"isCompanyOgrn(node.ogrn)\">\u041E\u0413\u0420\u041D:</span>\n        <span *ngIf=\"isIndividualOgrn(node.ogrn)\">\u041E\u0413\u0420\u041D\u0418\u041F:</span>\n            {{node.ogrn}}\n    </span>\n        <span *ngIf=\"node.ogrnip!==undefined\">\n        <span>\u041E\u0413\u0420\u041D\u0418\u041F:</span>\n            {{node.ogrnip}}\n    </span>\n        <span *ngIf=\"node.inn && showParams.inn\">\n        <span>\u0418\u041D\u041D:</span>\n            {{node.inn}}\n    </span>\n\n        <span *ngIf=\"node && node.founded_dt && showParams.created\">\n          c      {{ node.founded_dt | date: 'dd.MM.yyyy'}}\n          <!-- c      {{ node.founded_dt | date}} -->\n            </span>\n        <div class=\"loader\" *ngIf=\"loading\"></div>\n\n    "
                }] }
    ];
    /** @nocollapse */
    CompanyNameWithLinkComponent.ctorParameters = function () { return []; };
    CompanyNameWithLinkComponent.propDecorators = {
        node: [{ type: Input }],
        type: [{ type: Input }],
        showParams: [{ type: Input }],
        ogrn: [{ type: Input }],
        addBreadcrumb: [{ type: Output }],
        onCompanyClick: [{ type: Output }]
    };
    return CompanyNameWithLinkComponent;
}());
export { CompanyNameWithLinkComponent };
if (false) {
    /** @type {?} */
    CompanyNameWithLinkComponent.prototype.node;
    /** @type {?} */
    CompanyNameWithLinkComponent.prototype.type;
    /** @type {?} */
    CompanyNameWithLinkComponent.prototype.showParams;
    /** @type {?} */
    CompanyNameWithLinkComponent.prototype.ogrn;
    /** @type {?} */
    CompanyNameWithLinkComponent.prototype.addBreadcrumb;
    /** @type {?} */
    CompanyNameWithLinkComponent.prototype.onCompanyClick;
    /** @type {?} */
    CompanyNameWithLinkComponent.prototype.loading;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQ29tcGFueU5hbWVXaXRoTGluay5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bua2IvYW5ndWxhci1ua2ItbGliLyIsInNvdXJjZXMiOlsibGliL3VpL0NvbXBhbnlOYW1lV2l0aExpbmsudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7O0FBR0EsT0FBTyxLQUFLLENBQUMsTUFBTSxRQUFRLENBQUM7QUFDNUIsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLHlCQUF5QixDQUFDO0FBQ3hELE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUM1QyxPQUFPLEVBQUUsSUFBSSxFQUFFLE1BQU0sZ0JBQWdCLENBQUE7QUFDckMsT0FBTyxFQUFFLFNBQVMsRUFBVSxLQUFLLEVBQUUsTUFBTSxFQUFFLFlBQVksRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUkvRTtJQXlESTtRQUpVLGtCQUFhLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUNuQyxtQkFBYyxHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFDOUMsWUFBTyxHQUFHLEtBQUssQ0FBQztJQUloQixDQUFDO0lBZEQsc0JBQUksK0NBQUs7Ozs7UUFBVDtZQUNJLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sV0FBTSxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFNLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztRQUN4RixDQUFDOzs7T0FBQTs7OztJQWNELCtDQUFROzs7SUFBUjtRQUNJLElBQUksSUFBSSxDQUFDLFVBQVUsS0FBSyxTQUFTLEVBQUU7WUFDL0IsSUFBSSxDQUFDLFVBQVUsR0FBRztnQkFDZCxJQUFJLEVBQUUsSUFBSTtnQkFDVixHQUFHLEVBQUUsS0FBSztnQkFDVixPQUFPLEVBQUUsS0FBSzthQUNqQixDQUFDO1NBQ0w7SUFDTCxDQUFDOzs7OztJQUVLLGtEQUFXOzs7O0lBQWpCLFVBQWtCLE9BQU87Ozs7Ozs7d0JBRWYsaUJBQWlCOzs7O3dCQUFHLFVBQUMsSUFBSTs0QkFDM0IsT0FBTyxLQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxLQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO3dCQUM5SCxDQUFDLENBQUE7d0JBQ0ssZ0JBQWdCOzs7O3dCQUFHLFVBQUMsR0FBRzs0QkFDekIsT0FBTyxLQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxLQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQzt3QkFDMUgsQ0FBQyxDQUFBO3dCQUNLLE9BQU87Ozs7O3dCQUFHLFVBQU8sUUFBUSxFQUFFLEVBQUU7Ozs7NENBQ25CLHFCQUFNLGFBQWEsQ0FBQyxtQkFBbUIsQ0FBQyxFQUFFLFFBQVEsVUFBQSxFQUFFLEVBQUUsRUFBRSxDQUFDLEVBQUE7O3dDQUEvRCxHQUFHLEdBQUcsU0FBeUQ7d0NBQ3JFLHNCQUFPLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxHQUFHLEVBQUUsYUFBYSxFQUFFLENBQUMsQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBQzs7OzZCQUNoRSxDQUFBO3dCQUVELE9BQU8sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO3dCQUNwQixJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQzs2QkFFaEIsQ0FBQSxPQUFPLENBQUMsRUFBRSxJQUFJLE9BQU8sQ0FBQyxJQUFJLENBQUEsRUFBMUIsd0JBQTBCO3dCQUMxQixJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxFQUFFLEVBQUUsRUFBRSxPQUFPLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQzt3QkFDdEMsTUFBTSxHQUFHOzRCQUNYLFVBQVUsRUFBRSxPQUFPLENBQUMsSUFBSTs0QkFDeEIsSUFBSSxFQUFFLE9BQU8sQ0FBQyxJQUFJOzRCQUNsQixFQUFFLEVBQUUsT0FBTyxDQUFDLEVBQUU7eUJBQ2pCO3dCQUNELElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLEVBQUUsSUFBSSxFQUFFLE9BQU8sRUFBRSxDQUFDLENBQUM7Ozs2QkFHeEMsT0FBTyxDQUFDLElBQUksRUFBWix3QkFBWTt3QkFDWixRQUFRLEdBQUcsaUJBQWlCLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO3dCQUNwQyxxQkFBTSxPQUFPLENBQUMsUUFBUSxFQUFFLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBQTs7d0JBQTVDLElBQUksR0FBRyxTQUFxQyxDQUFDOzs7NkJBQ3RDLE9BQU8sQ0FBQyxHQUFHLEVBQVgsd0JBQVc7d0JBQ2xCLFFBQVEsR0FBRyxnQkFBZ0IsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUM7d0JBQ2xDLHFCQUFNLE9BQU8sQ0FBQyxRQUFRLEVBQUUsT0FBTyxDQUFDLEdBQUcsQ0FBQyxFQUFBOzt3QkFBM0MsSUFBSSxHQUFHLFNBQW9DLENBQUM7Ozt3QkFFaEQsSUFBSSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLENBQUM7d0JBRTlCLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLEVBQUUsRUFBRSxFQUFFLElBQUksQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDO3dCQUN6QyxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxFQUFFLElBQUksTUFBQSxFQUFFLENBQUMsQ0FBQzs7Ozs7O0tBUzFDOzs7OztJQUVELGtEQUFXOzs7O0lBQVgsVUFBWSxJQUFJO1FBQ1osT0FBTyxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLFlBQVksQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ3ZELENBQUM7Ozs7OztJQUVELHdEQUFpQjs7Ozs7SUFBakIsVUFBa0IsUUFBUSxFQUFFLFNBQVM7UUFDakMsT0FBTyxDQUFDLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxLQUFLLFNBQVMsQ0FBQztJQUM5QyxDQUFDOzs7OztJQUdELHNEQUFlOzs7O0lBQWYsVUFBZ0IsR0FBRztRQUNmLE9BQU8sTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLE1BQU0sS0FBSyxFQUFFLENBQUM7SUFDckMsQ0FBQzs7Ozs7SUFFRCxvREFBYTs7OztJQUFiLFVBQWMsSUFBSTtRQUNkLE9BQU8sTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDLE1BQU0sS0FBSyxFQUFFLENBQUM7SUFDdEMsQ0FBQzs7Ozs7SUFFRCx1REFBZ0I7Ozs7SUFBaEIsVUFBaUIsSUFBSTtRQUNqQixPQUFPLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxNQUFNLEtBQUssRUFBRSxDQUFDO0lBQ3RDLENBQUM7Ozs7O0lBRUQsbURBQVk7Ozs7SUFBWixVQUFhLEdBQUc7UUFDWixPQUFPLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxNQUFNLEtBQUssRUFBRSxDQUFDO0lBQ3JDLENBQUM7O2dCQTdJSixTQUFTLFNBQUM7b0JBQ1AsUUFBUSxFQUFFLDRCQUE0QjtvQkFDdEMsUUFBUSxFQUFFLDBoREF1Q1Q7aUJBQ0o7Ozs7O3VCQU9JLEtBQUs7dUJBQ0wsS0FBSzs2QkFDTCxLQUFLO3VCQUNMLEtBQUs7Z0NBQ0wsTUFBTTtpQ0FDTixNQUFNOztJQXdGWCxtQ0FBQztDQUFBLEFBOUlELElBOElDO1NBbEdZLDRCQUE0Qjs7O0lBS3JDLDRDQUFvQjs7SUFDcEIsNENBQWM7O0lBQ2Qsa0RBQTBFOztJQUMxRSw0Q0FBc0I7O0lBQ3RCLHFEQUE2Qzs7SUFDN0Msc0RBQThDOztJQUM5QywrQ0FBZ0IiLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIENyZWF0ZWQgYnkgcmFkaWdvc3Qgb24gMTkuMDQuMTguXG4gKi9cbmltcG9ydCAqIGFzIF8gZnJvbSAnbG9kYXNoJztcbmltcG9ydCB7IHNlYXJjaFNlcnZpY2UgfSBmcm9tICcuLi9zZWFyY2gvc2VhcmNoU2VydmljZSc7XG5pbXBvcnQgeyBOT0RFX1RZUEUgfSBmcm9tICcuLi9kb21haW4vVFlQRVMnO1xuaW1wb3J0IHsgTm9kZSB9IGZyb20gJy4uL2RvbWFpbi9Ob2RlJ1xuaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIElucHV0LCBPdXRwdXQsIEV2ZW50RW1pdHRlciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5cblxuQENvbXBvbmVudCh7XG4gICAgc2VsZWN0b3I6ICdua2ItY29tcGFueS1uYW1lLXdpdGgtbGluaycsXG4gICAgdGVtcGxhdGU6IGBcbiAgICAgICAgPGRpdiAqbmdJZj1cIm5vZGUuc2hvcnRuYW1lXCI+e3tub2RlLnNob3J0bmFtZX19PC9kaXY+XG4gICAgICAgIDxhXG4gICAgICAgICAgICAgICAgKm5nSWY9XCJub2RlLm9ncm4gfHwgbm9kZS5pbm5cIlxuICAgICAgICAgICAgICAgIChjbGljayk9XCJnb1RvRGV0YWlscyhub2RlKVwiXG4gICAgICAgICAgICAgICAgW25nQ2xhc3NdPVwie2Rhbmdlcjppc05vdEFjdGl2ZShub2RlKSwnYXV0b2thZF9fY29tcGFueS1uYW1lLS1oaWdobGlnaHQnOmlzSGlnaGxpZ2h0U2VhcmNoKG5vZGUub2dybixvZ3JuKX1cIlxuICAgICAgICAgICAgICAgIGNsYXNzPVwiYXV0b2thZF9fY29tcGFueS1uYW1lXCJcbiAgICAgICAgICAgICAgICBbdGl0bGVdPVwidGl0bGVcIlxuICAgICAgICA+XG4gICAgICAgICAgICB7e25vZGUubmFtZX19XG4gICAgICAgIDwvYT5cbiAgICAgICAgPHNwYW5cbiAgICAgICAgICAgICAgICAqbmdJZj1cIiFub2RlLm9ncm4gJiYgIW5vZGUuaW5uXCJcbiAgICAgICAgICAgICAgICBbbmdDbGFzc109XCJ7ZGFuZ2VyOmlzTm90QWN0aXZlKG5vZGUpLCdhdXRva2FkX19jb21wYW55LW5hbWUtLWhpZ2hsaWdodCc6aXNIaWdobGlnaHRTZWFyY2gobm9kZS5vZ3JuLG9ncm4pfVwiXG4gICAgICAgICAgICAgICAgW3RpdGxlXT1cInRpdGxlXCJcbiAgICAgICAgPlxuICAgICAgICB7e25vZGUubmFtZX19XG4gICAgPC9zcGFuPlxuXG4gICAgICAgIDxzcGFuICpuZ0lmPVwibm9kZS5vZ3JuIT09dW5kZWZpbmVkICYmIHNob3dQYXJhbXMub2dyblwiPlxuICAgICAgICA8c3BhbiAqbmdJZj1cImlzQ29tcGFueU9ncm4obm9kZS5vZ3JuKVwiPtCe0JPQoNCdOjwvc3Bhbj5cbiAgICAgICAgPHNwYW4gKm5nSWY9XCJpc0luZGl2aWR1YWxPZ3JuKG5vZGUub2dybilcIj7QntCT0KDQndCY0J86PC9zcGFuPlxuICAgICAgICAgICAge3tub2RlLm9ncm59fVxuICAgIDwvc3Bhbj5cbiAgICAgICAgPHNwYW4gKm5nSWY9XCJub2RlLm9ncm5pcCE9PXVuZGVmaW5lZFwiPlxuICAgICAgICA8c3Bhbj7QntCT0KDQndCY0J86PC9zcGFuPlxuICAgICAgICAgICAge3tub2RlLm9ncm5pcH19XG4gICAgPC9zcGFuPlxuICAgICAgICA8c3BhbiAqbmdJZj1cIm5vZGUuaW5uICYmIHNob3dQYXJhbXMuaW5uXCI+XG4gICAgICAgIDxzcGFuPtCY0J3QnTo8L3NwYW4+XG4gICAgICAgICAgICB7e25vZGUuaW5ufX1cbiAgICA8L3NwYW4+XG5cbiAgICAgICAgPHNwYW4gKm5nSWY9XCJub2RlICYmIG5vZGUuZm91bmRlZF9kdCAmJiBzaG93UGFyYW1zLmNyZWF0ZWRcIj5cbiAgICAgICAgICBjICAgICAge3sgbm9kZS5mb3VuZGVkX2R0IHwgZGF0ZTogJ2RkLk1NLnl5eXknfX1cbiAgICAgICAgICA8IS0tIGMgICAgICB7eyBub2RlLmZvdW5kZWRfZHQgfCBkYXRlfX0gLS0+XG4gICAgICAgICAgICA8L3NwYW4+XG4gICAgICAgIDxkaXYgY2xhc3M9XCJsb2FkZXJcIiAqbmdJZj1cImxvYWRpbmdcIj48L2Rpdj5cblxuICAgIGAsXG59KVxuXG5leHBvcnQgY2xhc3MgQ29tcGFueU5hbWVXaXRoTGlua0NvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG4gICAgZ2V0IHRpdGxlKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5ub2RlLnN0YXRlID8gYCR7dGhpcy5ub2RlLnN0YXRlLl9zaW5jZX0gOiAke3RoaXMubm9kZS5zdGF0ZS50eXBlfWAgOiAnJztcbiAgICB9XG5cbiAgICBASW5wdXQoKSBub2RlOiBOb2RlO1xuICAgIEBJbnB1dCgpIHR5cGU7XG4gICAgQElucHV0KCkgc2hvd1BhcmFtczogeyBpbm4/OiBib29sZWFuLCBvZ3JuPzogYm9vbGVhbiwgY3JlYXRlZD86IGJvb2xlYW4gfTtcbiAgICBASW5wdXQoKSBvZ3JuOiBzdHJpbmc7XG4gICAgQE91dHB1dCgpIGFkZEJyZWFkY3J1bWIgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG4gICAgQE91dHB1dCgpIG9uQ29tcGFueUNsaWNrID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICAgIGxvYWRpbmcgPSBmYWxzZTtcblxuICAgIGNvbnN0cnVjdG9yKCkge1xuXG4gICAgfVxuXG4gICAgbmdPbkluaXQoKSB7XG4gICAgICAgIGlmICh0aGlzLnNob3dQYXJhbXMgPT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgdGhpcy5zaG93UGFyYW1zID0ge1xuICAgICAgICAgICAgICAgIG9ncm46IHRydWUsXG4gICAgICAgICAgICAgICAgaW5uOiBmYWxzZSxcbiAgICAgICAgICAgICAgICBjcmVhdGVkOiBmYWxzZSxcbiAgICAgICAgICAgIH07XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBhc3luYyBnb1RvRGV0YWlscyhjb21wYW55KSB7XG4gICAgICAgIGxldCBub2RlVHlwZSwgbm9kZTtcbiAgICAgICAgY29uc3QgZ2V0Tm9kZVR5cGVCeU9ncm4gPSAob2dybikgPT4ge1xuICAgICAgICAgICAgcmV0dXJuIHRoaXMuaXNDb21wYW55T2dybihvZ3JuKSA/IE5PREVfVFlQRS5DT01QQU5ZIDogdGhpcy5pc0luZGl2aWR1YWxPZ3JuKG9ncm4pID8gTk9ERV9UWVBFLklORElWSURVQUxfSURFTlRJVFkgOiBmYWxzZTtcbiAgICAgICAgfTtcbiAgICAgICAgY29uc3QgZ2V0Tm9kZVR5cGVCeUlubiA9IChpbm4pID0+IHtcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmlzQ29tcGFueUlubihpbm4pID8gTk9ERV9UWVBFLkNPTVBBTlkgOiB0aGlzLmlzSW5kaXZpZHVhbElubihpbm4pID8gTk9ERV9UWVBFLklORElWSURVQUxfSURFTlRJVFkgOiBmYWxzZTtcbiAgICAgICAgfTtcbiAgICAgICAgY29uc3QgZ2V0Tm9kZSA9IGFzeW5jIChub2RlVHlwZSwgaWQpID0+IHtcbiAgICAgICAgICAgIGNvbnN0IHJlcyA9IGF3YWl0IHNlYXJjaFNlcnZpY2Uuc2VhcmNoQnlUeXBlQW5kVGV4dCh7IG5vZGVUeXBlIH0sIGlkKTtcbiAgICAgICAgICAgIHJldHVybiBfLmhlYWQoXy5nZXQocmVzLCAncmVzdWx0Lmxpc3QnLCBfLmdldChyZXMsICdsaXN0JykpKTtcbiAgICAgICAgfTtcblxuICAgICAgICBjb21wYW55ID0gdGhpcy5ub2RlO1xuICAgICAgICB0aGlzLmxvYWRpbmcgPSB0cnVlO1xuXG4gICAgICAgIGlmIChjb21wYW55LmlkICYmIGNvbXBhbnkudHlwZSkge1xuICAgICAgICAgICAgdGhpcy5hZGRCcmVhZGNydW1iLmVtaXQoeyBpZDogY29tcGFueS5pZCB9KTtcbiAgICAgICAgICAgIGNvbnN0IHBhcmFtcyA9IHtcbiAgICAgICAgICAgICAgICBzZWFyY2h0eXBlOiBjb21wYW55LnR5cGUsXG4gICAgICAgICAgICAgICAgdGV4dDogY29tcGFueS5uYW1lLFxuICAgICAgICAgICAgICAgIGlkOiBjb21wYW55LmlkLFxuICAgICAgICAgICAgfTtcbiAgICAgICAgICAgIHRoaXMub25Db21wYW55Q2xpY2suZW1pdCh7IG5vZGU6IGNvbXBhbnkgfSk7XG4gICAgICAgICAgICAvLyB0aGlzLiRzdGF0ZS5nbygnbWFpbi5zZWFyY2guZGV0YWlscycsIHBhcmFtcyk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBpZiAoY29tcGFueS5vZ3JuKSB7XG4gICAgICAgICAgICAgICAgbm9kZVR5cGUgPSBnZXROb2RlVHlwZUJ5T2dybihjb21wYW55Lm9ncm4pO1xuICAgICAgICAgICAgICAgIG5vZGUgPSBhd2FpdCBnZXROb2RlKG5vZGVUeXBlLCBjb21wYW55Lm9ncm4pO1xuICAgICAgICAgICAgfSBlbHNlIGlmIChjb21wYW55Lmlubikge1xuICAgICAgICAgICAgICAgIG5vZGVUeXBlID0gZ2V0Tm9kZVR5cGVCeUlubihjb21wYW55Lmlubik7XG4gICAgICAgICAgICAgICAgbm9kZSA9IGF3YWl0IGdldE5vZGUobm9kZVR5cGUsIGNvbXBhbnkuaW5uKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIG5vZGUgPSBOb2RlLm5vZGVXcmFwcGVyKG5vZGUpO1xuXG4gICAgICAgICAgICB0aGlzLmFkZEJyZWFkY3J1bWIuZW1pdCh7IGlkOiBub2RlLmlkIH0pO1xuICAgICAgICAgICAgdGhpcy5vbkNvbXBhbnlDbGljay5lbWl0KHsgbm9kZSB9KTtcbiAgICAgICAgICAgIC8vIGNvbnN0IHBhcmFtcyA9IHtcbiAgICAgICAgICAgIC8vICAgICBzZWFyY2h0eXBlOiBub2RlLnR5cGUsXG4gICAgICAgICAgICAvLyAgICAgdGV4dDogbm9kZS5uYW1lLFxuICAgICAgICAgICAgLy8gICAgIGlkOiBub2RlLmlkLFxuICAgICAgICAgICAgLy8gfTtcbiAgICAgICAgICAgIC8vIHRoaXMuJHN0YXRlLmdvKCdtYWluLnNlYXJjaC5kZXRhaWxzJywgcGFyYW1zKTtcbiAgICAgICAgfVxuXG4gICAgfVxuXG4gICAgaXNOb3RBY3RpdmUobm9kZSkge1xuICAgICAgICByZXR1cm4gXy50b051bWJlcihfLmdldChub2RlLCAnc3RhdGUuY29kZScpKSA9PT0gMDtcbiAgICB9XG5cbiAgICBpc0hpZ2hsaWdodFNlYXJjaCh0ZXh0T2dybiwgcXVlcnlPZ3JuKSB7XG4gICAgICAgIHJldHVybiBfLnRvU3RyaW5nKHRleHRPZ3JuKSA9PT0gcXVlcnlPZ3JuO1xuICAgIH1cblxuXG4gICAgaXNJbmRpdmlkdWFsSW5uKGlubikge1xuICAgICAgICByZXR1cm4gU3RyaW5nKGlubikubGVuZ3RoID09PSAxMjtcbiAgICB9XG5cbiAgICBpc0NvbXBhbnlPZ3JuKG9ncm4pIHtcbiAgICAgICAgcmV0dXJuIFN0cmluZyhvZ3JuKS5sZW5ndGggPT09IDEzO1xuICAgIH1cblxuICAgIGlzSW5kaXZpZHVhbE9ncm4ob2dybikge1xuICAgICAgICByZXR1cm4gU3RyaW5nKG9ncm4pLmxlbmd0aCA9PT0gMTU7XG4gICAgfVxuXG4gICAgaXNDb21wYW55SW5uKGlubikge1xuICAgICAgICByZXR1cm4gU3RyaW5nKGlubikubGVuZ3RoID09PSAxMDtcbiAgICB9XG59XG4iXX0=